/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentBindingBase;
using org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner;

namespace org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect
{
	public interface BaseIBindingDirect<C,S> : BaseIClientBase<C>, BaseIServerBase<S>, ISynchronizerKind 
		where C:IEnvironmentPortTypeSinglePartner
		where S:IEnvironmentPortTypeSinglePartner
	{
	}
}