using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner;
using org.hpcshelf.binding.environment.EnvironmentBindingBase;

namespace org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect
{
	public interface IBindingDirect<C,S> : BaseIBindingDirect<C,S>, IClientBase<C>, IServerBase<S>
		where C:IEnvironmentPortTypeSinglePartner
		where S:IEnvironmentPortTypeSinglePartner
	{
	}
}