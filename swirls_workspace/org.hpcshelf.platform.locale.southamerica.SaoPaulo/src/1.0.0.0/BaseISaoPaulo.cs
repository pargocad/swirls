/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.southamerica.Brazil;

namespace org.hpcshelf.platform.locale.southamerica.SaoPaulo
{
	public interface BaseISaoPaulo : BaseIBrazil, IQualifierKind 
	{
	}
}