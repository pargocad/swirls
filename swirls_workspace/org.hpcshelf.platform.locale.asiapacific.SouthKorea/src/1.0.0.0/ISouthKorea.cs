using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.SouthKorea
{
	public interface ISouthKorea : BaseISouthKorea, IAsiaPacific
	{
	}
}