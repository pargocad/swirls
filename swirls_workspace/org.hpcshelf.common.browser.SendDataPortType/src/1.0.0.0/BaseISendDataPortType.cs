/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.common.BrowserPortType;

namespace org.hpcshelf.common.browser.SendDataPortType
{
	public interface BaseISendDataPortType : BaseIBrowserPortType, IQualifierKind 
	{
	}
}