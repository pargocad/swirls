using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;
using org.hpcshelf.backend.ec2.family.Storage;

namespace org.hpcshelf.backend.ec2.type.storage.I3
{
	public interface IEC2TypeI3 : BaseIEC2TypeI3, IEC2Type<IEC2FamilyStorage>
	{
	}
}