using org.hpcshelf.platform.locale.northamerica.US_West;

namespace org.hpcshelf.platform.locale.northamerica.Nevada
{
	public interface INevada : BaseINevada, IUS_West
	{
	}
}