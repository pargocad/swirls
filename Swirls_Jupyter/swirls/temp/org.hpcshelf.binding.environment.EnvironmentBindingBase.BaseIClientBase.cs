/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentPortType;

namespace org.hpcshelf.binding.environment.EnvironmentBindingBase
{
	public interface BaseIClientBase<C> : ISynchronizerKind 
		where C:IEnvironmentPortType
	{
	}
}