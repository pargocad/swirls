using org.hpcshelf.blas.configure.DIAG;

namespace org.hpcshelf.blas.configure.diag.NA
{
	public interface NA : BaseNA, IDIAG
	{
	}
}