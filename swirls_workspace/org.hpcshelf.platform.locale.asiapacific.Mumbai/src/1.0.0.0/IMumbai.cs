using org.hpcshelf.platform.locale.asiapacific.India;

namespace org.hpcshelf.platform.locale.asiapacific.Mumbai
{
	public interface IMumbai : BaseIMumbai, IIndia
	{
	}
}