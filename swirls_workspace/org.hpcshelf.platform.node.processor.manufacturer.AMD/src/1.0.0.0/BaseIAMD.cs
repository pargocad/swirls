/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Manufacturer;

namespace org.hpcshelf.platform.node.processor.manufacturer.AMD
{
	public interface BaseIAMD : BaseIProcessorManufacturer, IQualifierKind 
	{
	}
}