/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.mpi.binding.IntercommunicatorBinding;
using org.hpcshelf.mpi.IntercommunicatorPortType;
using org.hpcshelf.mpi.MPIConnector;

namespace org.hpcshelf.mpi.MPIConnectorForC
{
	public interface BaseIMPIConnectorForC : BaseIMPIConnector<org.hpcshelf.mpi.language.C.ILanguage_C, org.hpcshelf.mpi.intercommunicator.BasicSendReceive.IBasicSendReceive>, ISynchronizerKind 
	{
		IIntercommunicatorBinding<org.hpcshelf.mpi.intercommunicator.BasicSendReceive.IBasicSendReceive> Port {get;}
	}
}