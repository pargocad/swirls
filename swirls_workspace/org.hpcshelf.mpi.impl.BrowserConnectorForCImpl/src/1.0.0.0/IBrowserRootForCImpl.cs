using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.mpi.BrowserConnectorForC;
using org.hpcshelf.common.browser.RecvDataPortType;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

namespace org.hpcshelf.mpi.impl.BrowserConnectorForCImpl
{
    public class IBrowserRootForCImpl : BaseIBrowserRootForCImpl, IBrowserRootForC
    {
        private IRecvDataPortType browse_port = null;

        private IDictionary<int, IDictionary<int, Thread>> thread_receive_message_dict = new ConcurrentDictionary<int, IDictionary<int, Thread>>();

        private ConcurrentQueue<string> cq = new ConcurrentQueue<string>();
        private Semaphore m = new Semaphore(0, int.MaxValue);

        bool termination_flag_1 = false;

        IDictionary<int, IDictionary<int, bool>> termination_flag_2 = new ConcurrentDictionary<int, IDictionary<int, bool>>();
        bool released = false;
        IDictionary<int, IDictionary<int, AutoResetEvent>> next_browse_iteration = new ConcurrentDictionary<int, IDictionary<int, AutoResetEvent>>();

        public override void after_initialize()
        {
            Console.WriteLine("IBrowseRootImpl AFTER INITIALIZE BEGIN");
            Console.WriteLine("IBrowseRootImpl -- {0}", this.FacetIndexes[1].Length);

            foreach (int f in this.FacetIndexes[1])
            {
                termination_flag_2[f] = new ConcurrentDictionary<int, bool>();
                thread_receive_message_dict[f] = new ConcurrentDictionary<int, Thread>();
                next_browse_iteration[f] = new ConcurrentDictionary<int, AutoResetEvent>();
                for (int r = 0; r < UnitSizeInFacet[f]["browser_peer"]; r++)
                {
                    Console.WriteLine("IBrowserRootImpl -- CREATE THREAD facet={0} rank={1}", f, r);
                    next_browse_iteration[f][r] = new AutoResetEvent(false);

                    Thread thread_receive_message = new Thread(new ParameterizedThreadStart((object o) =>
                    {
                        Tuple<int, int> t = (Tuple<int, int>)o;
                        int facet = t.Item1;
                        int rank = t.Item2;
                        while (!termination_flag_1)
                        {
                            Console.WriteLine("IBrowserRootImpl 1 -- NEW iteration (termination_flag_1) facet={0} rank={1}", facet, rank);
                            next_browse_iteration[facet][rank].WaitOne();
                            Console.WriteLine("IBrowserRootImpl 2 -- NEW iteration (termination_flag_1) facet={0} rank={1} {2} {3}", facet, rank, termination_flag_2.ContainsKey(facet), termination_flag_2[facet].ContainsKey(rank));
                            termination_flag_2[facet][rank] = false;
                            Console.WriteLine("IBrowserRootImpl 3 -- NEW iteration (termination_flag_1) facet={0} rank={1} {2} {3}", facet, rank, termination_flag_2.ContainsKey(facet), termination_flag_2[facet].ContainsKey(rank));
                            while (!termination_flag_2[facet][rank] && !released)
                            {
                                Console.WriteLine("IBrowserRootImpl -- BEGIN RECEIVE NATIVE facet={0} rank={1}", facet, rank);
                                byte[] a = Channel.ReceiveNative(new Tuple<int, int>(facet, rank), 0);
                                var str = System.Text.Encoding.Default.GetString(a);
                                termination_flag_2[facet][rank] = str.Equals("!EOS");
                                Console.WriteLine("IBrowserRootImpl -- AFTER RECEIVE NATIVE facet={0} rank={1} str={2} {3} termination_flag_2[{0}][{1}]={4}", facet, rank, str, str.Equals("!EOS"), termination_flag_2[facet][rank]);
                                cq.Enqueue(str);
                                m.Release();
                            }
                        }
                    }));

                    thread_receive_message_dict[f][r] = thread_receive_message;
                }
            }

            foreach (int f in thread_receive_message_dict.Keys)
                foreach (int r in thread_receive_message_dict[f].Keys)
                    thread_receive_message_dict[f][r].Start(new Tuple<int, int>(f, r));

            Console.WriteLine("IBrowseRootImpl BEFORE browse_port assignment"); 

            browse_port = new RecvDataPortTypeImpl(cq, m, termination_flag_2, next_browse_iteration, thread_receive_message_dict);
        }

        public override void main()
        {
            Browse_port.Server = browse_port;
        }

        public override void release1()
        {
            released = true;
            termination_flag_1 = true;
            foreach (int f in thread_receive_message_dict.Keys)
                foreach (int r in thread_receive_message_dict[f].Keys)
                    next_browse_iteration[f][r].Set();
            thread_receive_message_dict.Clear();

            Browse_port.Server = null;

            Services.releasePort("channel");
            Services.releasePort("browse_port");

            base.release1();
        }
    }

    class RecvDataPortTypeImpl : IRecvDataPortType
    {
        private ConcurrentQueue<string> cq = null;
        private Semaphore m;
        private IDictionary<int, IDictionary<int, bool>> termination_flag_2;
        bool any_active = false;
        IDictionary<int, IDictionary<int, AutoResetEvent>> next_browse_iteration;
        IDictionary<int, IDictionary<int, Thread>> thread_receive_message_dict;

        public RecvDataPortTypeImpl(ConcurrentQueue<string> cq, Semaphore m, IDictionary<int, IDictionary<int, bool>> termination_flag_2, IDictionary<int, IDictionary<int,AutoResetEvent>> next_browse_iteration, IDictionary<int, IDictionary<int, Thread>> thread_receive_message_dict)
        {
            this.cq = cq;
            this.m = m;
            this.termination_flag_2 = termination_flag_2;
            this.next_browse_iteration = next_browse_iteration;
            this.thread_receive_message_dict = thread_receive_message_dict;
        }

        public IList<string> receive_output_message()
        {
            Console.WriteLine("IBrowserRootImpl -- BEGIN receive_output_message 1 -- {0}", any_active);

            if (!any_active)
            {
                Console.WriteLine("IBrowserRootImpl -- ACTIVATE -- receive_output_message 1 {0}", thread_receive_message_dict.Count);

                foreach (int f in thread_receive_message_dict.Keys)
                    foreach (int r in thread_receive_message_dict[f].Keys)
                    {
                        Console.WriteLine("next_browse_iteration[{0}][{1}].Set();", f, r);
                        next_browse_iteration[f][r].Set();
                    }
            }

            IList<string> result = new List<string>();
            string s;

            Console.WriteLine("IBrowserRootImpl -- BEGIN receive_output_message 2 -- {0}", any_active);

            m.WaitOne();

            // peek the first message;
            cq.TryDequeue(out s);
            if (!s.Equals("!EOS")) result.Add(s);

            Console.WriteLine("IBrowserRootImpl -- FIRST output message -- {0}", s);

            // peek the next existing messages.
            while (cq.TryDequeue(out s))
            {
                Console.WriteLine("IBrowserRootImpl -- NEXT output message -- {0}", s);
                if (!s.Equals("!EOS")) result.Add(s);
                m.WaitOne();
                Console.WriteLine("IBrowserRootImpl -- receive_output_message -- new iteration");
            }

            Console.WriteLine("IBrowserRootImpl -- FINISH output message -- {0}", s);

            any_active = false;
            foreach (int facet in termination_flag_2.Keys)
                foreach (int rank in termination_flag_2[facet].Keys)
                {
                    Console.WriteLine("termination_flag_2[{0}][{1}]={2}", facet, rank, termination_flag_2[facet][rank]);
                    any_active = any_active || !termination_flag_2[facet][rank];
                }

            if (!any_active)
            {
                while (cq.TryDequeue(out s))
                {
                    Console.WriteLine("IBrowserRootImpl -- REMAINING output message -- {0}", s);
                    if (!s.Equals("!EOS")) result.Add(s);
                }
                Console.WriteLine("IBrowserRootImpl -- LAST output message");
                result.Add("!EOS");
            }

            return result;
        }

    }
}
