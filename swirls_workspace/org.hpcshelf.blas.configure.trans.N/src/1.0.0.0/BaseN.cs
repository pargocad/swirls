/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.configure.TRANS;

namespace org.hpcshelf.blas.configure.trans.N
{
	public interface BaseN : BaseITRANS, IQualifierKind 
	{
	}
}