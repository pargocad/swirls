/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.europe.WestEurope;

namespace org.hpcshelf.platform.locale.europe.Belgium
{
	public interface BaseIBelgium : BaseIWestEurope, IQualifierKind 
	{
	}
}