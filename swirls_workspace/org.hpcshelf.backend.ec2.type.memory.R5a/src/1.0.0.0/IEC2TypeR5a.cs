using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.memory.R5a
{
	public interface IEC2TypeR5a : BaseIEC2TypeR5a, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}