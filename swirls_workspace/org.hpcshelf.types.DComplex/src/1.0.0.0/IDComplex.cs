using org.hpcshelf.kinds;
using org.hpcshelf.types.Data;
using System;

namespace org.hpcshelf.types.DComplex { 

	public interface IDComplex : BaseIDComplex, IData
	{
		IDComplexInstance newInstance(double d);
	} // end main interface 

	public interface IDComplexInstance : IDataInstance, ICloneable
	{
		double Value { set; get; }
	}

} // end namespace 
