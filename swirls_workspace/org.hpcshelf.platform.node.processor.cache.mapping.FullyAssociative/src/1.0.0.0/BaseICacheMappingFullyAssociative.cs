/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace org.hpcshelf.platform.node.processor.cache.mapping.FullyAssociative
{
	public interface BaseICacheMappingFullyAssociative : BaseICacheMappingSetAssociative<org.hpcshelf.quantifier.IntUp.IntUp>, IQualifierKind 
	{
	}
}