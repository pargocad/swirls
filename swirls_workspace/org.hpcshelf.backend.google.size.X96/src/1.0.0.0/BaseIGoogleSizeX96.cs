/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X96
{
 public interface BaseIGoogleSizeX96 : BaseIGoogleSize, IQualifierKind 
 {
 }
}