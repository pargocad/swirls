/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.storage.Type;

namespace org.hpcshelf.backend.ec2.storage.type.EBS
{
	public interface BaseIEC2StorageType_EBS : BaseIEC2StorageType, IQualifierKind 
	{
	}
}