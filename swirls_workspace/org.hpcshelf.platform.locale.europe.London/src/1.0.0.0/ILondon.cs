using org.hpcshelf.platform.locale.europe.UnitedKingdon;

namespace org.hpcshelf.platform.locale.europe.London
{
	public interface ILondon : BaseILondon, IUnitedKingdon
	{
	}
}