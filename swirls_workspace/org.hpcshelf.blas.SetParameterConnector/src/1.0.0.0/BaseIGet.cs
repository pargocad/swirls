/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect;
using org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner;
using org.hpcshelf.blas.GetPortType;
using org.hpcshelf.types.Data;

namespace org.hpcshelf.blas.SetParameterConnector
{
	public interface BaseIGet<T_GET> : ISynchronizerKind 
		where T_GET:IData
	{
		IBindingDirect<IEnvironmentPortTypeSinglePartner, IGetPortType<T_GET>> Get {get;}
	}
}