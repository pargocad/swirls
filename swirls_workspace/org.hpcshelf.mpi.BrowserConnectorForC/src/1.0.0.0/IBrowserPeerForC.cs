using org.hpcshelf.common.BrowserConnector;

namespace org.hpcshelf.mpi.BrowserConnectorForC
{
	public interface IBrowserPeerForC : BaseIBrowserPeerForC, IBrowserPeer<org.hpcshelf.mpi.language.C.ILanguage_C, org.hpcshelf.common.browser.SendDataPortType.ISendDataPortType>
	{
	}
}