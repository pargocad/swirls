using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large32x
{
	public interface IEC2SizeLarge32x : BaseIEC2SizeLarge32x, IEC2Size
	{
	}
}