/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect;
using org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner;
using org.hpcshelf.blas.MatrixPortType;
using org.hpcshelf.types.Data;
using org.hpcshelf.blas.data.DataDistribution;

namespace org.hpcshelf.blas.MatrixReader
{
	public interface BaseIMatrixReader<DIST, T> : IComputationKind 
		where DIST:IDataDistribution
		where T:IData
	{
		IBindingDirect<IEnvironmentPortTypeSinglePartner, IMatrixPortType<T, DIST>> Matrix {get;}
	}
}