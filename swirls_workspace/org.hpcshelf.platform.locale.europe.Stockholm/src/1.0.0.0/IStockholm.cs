using org.hpcshelf.platform.locale.europe.Sweden;

namespace org.hpcshelf.platform.locale.europe.Stockholm
{
	public interface IStockholm : BaseIStockholm, ISweden
	{
	}
}