using org.hpcshelf.kinds;
using org.hpcshelf.mpi.Language;

namespace org.hpcshelf.mpi.language.C
{
	public interface ILanguage_C : BaseILanguage_C, ILanguage
	{
	}
}