/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X30
{
 public interface BaseIGoogleSizeX30 : BaseIGoogleSize, IQualifierKind 
 {
 }
}