using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.memory.M2_Ultramem
{
 public interface IGoogleTypeM2_Ultramem : BaseIGoogleTypeM2_Ultramem, IGoogleType<family.Memory.IGoogleFamilyMemory>
 {
 }
}