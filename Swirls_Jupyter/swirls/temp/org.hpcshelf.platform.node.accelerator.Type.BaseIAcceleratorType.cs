/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.accelerator.Manufacturer;

namespace org.hpcshelf.platform.node.accelerator.Type
{
	public interface BaseIAcceleratorType<MAN> : IQualifierKind 
		where MAN:IAcceleratorManufacturer
	{
	}
}