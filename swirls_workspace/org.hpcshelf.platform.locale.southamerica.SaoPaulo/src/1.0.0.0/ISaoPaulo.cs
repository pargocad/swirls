using org.hpcshelf.platform.locale.southamerica.Brazil;

namespace org.hpcshelf.platform.locale.southamerica.SaoPaulo
{
	public interface ISaoPaulo : BaseISaoPaulo, IBrazil
	{
	}
}