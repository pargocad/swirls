/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_20Way
{
	public interface BaseISetAssociative_20Way : BaseICacheMappingSetAssociative<org.hpcshelf.quantifier.IntUp.IntUp>, IQualifierKind 
	{
	}
}