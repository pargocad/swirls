using org.hpcshelf.platform.locale.europe.NorthEurope;

namespace org.hpcshelf.platform.locale.europe.Finland
{
	public interface IFinland : BaseIFinland, INorthEurope
	{
	}
}