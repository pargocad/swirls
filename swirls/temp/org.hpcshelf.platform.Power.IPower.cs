using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.quantifier.IntUp;

namespace org.hpcshelf.platform.Power
{
	public interface IPower<P0, P1, P2, P3> : BaseIPower<P0, P1, P2, P3>
		where P0:IntDown
		where P1:IntUp
		where P2:IntDown
		where P3:IntUp
	{
	}
}