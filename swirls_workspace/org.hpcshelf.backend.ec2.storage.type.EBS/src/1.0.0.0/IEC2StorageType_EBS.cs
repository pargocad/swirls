using org.hpcshelf.backend.ec2.storage.Type;

namespace org.hpcshelf.backend.ec2.storage.type.EBS
{
	public interface IEC2StorageType_EBS : BaseIEC2StorageType_EBS, IEC2StorageType
	{
	}
}