using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_8Way
{
	public interface ISetAssociative_8Way : BaseISetAssociative_8Way, ICacheMappingSetAssociative<org.hpcshelf.quantifier.IntUp.IntUp>
	{
	}
}