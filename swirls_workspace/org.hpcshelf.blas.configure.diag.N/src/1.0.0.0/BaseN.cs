/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.configure.DIAG;

namespace org.hpcshelf.blas.configure.diag.N
{
	public interface BaseN : BaseIDIAG, IQualifierKind 
	{
	}
}