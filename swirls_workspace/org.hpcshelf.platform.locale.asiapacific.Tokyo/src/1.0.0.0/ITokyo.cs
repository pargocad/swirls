using org.hpcshelf.platform.locale.asiapacific.Japan;

namespace org.hpcshelf.platform.locale.asiapacific.Tokyo
{
	public interface ITokyo : BaseITokyo, IJapan
	{
	}
}