using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Nano
{
	public interface IEC2SizeNano : BaseIEC2SizeNano, IEC2Size
	{
	}
}