/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large9x
{
	public interface BaseIEC2SizeLarge9x : BaseIEC2Size, IQualifierKind 
	{
	}
}