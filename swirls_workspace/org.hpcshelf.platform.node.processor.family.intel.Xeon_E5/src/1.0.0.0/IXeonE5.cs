using org.hpcshelf.platform.node.processor.Family;

namespace org.hpcshelf.platform.node.processor.family.intel.Xeon_E5
{
	public interface IXeonE5 : BaseIXeonE5, IProcessorFamily<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>
	{
	}
}