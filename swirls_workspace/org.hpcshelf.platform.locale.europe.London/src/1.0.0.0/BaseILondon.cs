/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.europe.UnitedKingdon;

namespace org.hpcshelf.platform.locale.europe.London
{
	public interface BaseILondon : BaseIUnitedKingdon, IQualifierKind 
	{
	}
}