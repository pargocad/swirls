/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large
{
	public interface BaseIEC2SizeLarge : BaseIEC2Size, IQualifierKind 
	{
	}
}