#!/bin/bash
echo "STARTING - GCP Backend at $2"
#ssh-keygen -f "~/.ssh/known_hosts" -R $2
ssh -i $1 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no ubuntu@$2 uptime
while [ "$?" -ne "0" ]
do
	ssh -i $1 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no ubuntu@$2 uptime
done

scp -i $1 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${GOOGLE_APPLICATION_CREDENTIALS} ubuntu@$2:gcp_credentials.json
while [ "$?" -ne "0" ]
do
	scp -i $1 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${GOOGLE_APPLICATION_CREDENTIALS} ubuntu@$2:gcp_credentials.json
done

nohup ssh -i $1 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@$2 "HPCShelf_HOME=/home/ubuntu/HPCShelf /home/ubuntu/swirls/run_gcp-backend_services_local.sh" &
while [ "$?" -ne "0" ]
do
	nohup ssh -i $1 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@$2 "HPCShelf_HOME=/home/ubuntu/HPCShelf /home/ubuntu/swirls/run_gcp-backend_services_local.sh" &
done

echo "STARTED - GCP Backend at $2"
