using org.hpcshelf.kinds;
using org.hpcshelf.mpi.Language;

namespace org.hpcshelf.language.Python
{
	public interface ILanguage_Python : BaseILanguage_Python, ILanguage
	{
	}
}