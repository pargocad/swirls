/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;

namespace org.hpcshelf.backend.ec2.family.Acceleration
{
	public interface BaseIEC2FamilyAcceleration : BaseIEC2Family, IQualifierKind 
	{
	}
}