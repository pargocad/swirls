using org.hpcshelf.platform.locale.asiapacific.SouthKorea;

namespace org.hpcshelf.platform.locale.asiapacific.Seoul
{
	public interface ISeoul : BaseISeoul, ISouthKorea
	{
	}
}