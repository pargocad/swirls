/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.India
{
	public interface BaseIIndia : BaseIAsiaPacific, IQualifierKind 
	{
	}
}