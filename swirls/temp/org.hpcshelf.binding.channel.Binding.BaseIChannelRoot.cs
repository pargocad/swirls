/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.kind.Binding;

namespace org.hpcshelf.binding.channel.Binding
{
	public interface BaseIChannelRoot : BaseIRoot, IBindingKind 
	{
	}
}