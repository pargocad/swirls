using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;

namespace org.hpcshelf.backend.ec2.type.Type
{
	public interface IEC2Type<F> : BaseIEC2Type<F>
		where F:IEC2Family
	{
	}
}