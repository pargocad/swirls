using org.hpcshelf.kinds;
using org.hpcshelf.types.Data;
using System;

namespace org.hpcshelf.types.Real { 

	public interface IReal : BaseIReal, IData
	{
		IRealInstance newInstance(double d);
	} // end main interface 

	public interface IRealInstance : IDataInstance, ICloneable
	{
		double Value { set; get; }
	}

} // end namespace 
