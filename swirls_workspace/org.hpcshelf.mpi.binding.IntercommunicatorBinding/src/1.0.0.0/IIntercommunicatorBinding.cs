using org.hpcshelf.mpi.IntercommunicatorPortType;
using org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect;

namespace org.hpcshelf.mpi.binding.IntercommunicatorBinding
{
	public interface IIntercommunicatorBinding<S> : BaseIIntercommunicatorBinding<S>, IBindingDirect<org.hpcshelf.mpi.IntercommunicatorPortType.IIntercommunicatorPortType, S>
		where S:IIntercommunicatorPortType
	{
	}
}