using org.hpcshelf.platform.node.accelerator.Manufacturer;
using org.hpcshelf.platform.node.accelerator.Type;
using org.hpcshelf.platform.node.accelerator.Architecture;

namespace org.hpcshelf.platform.node.accelerator.Model
{
	public interface IAcceleratorModel<MAN, TYPE, ARC> : BaseIAcceleratorModel<MAN, TYPE, ARC>
		where MAN:IAcceleratorManufacturer
		where TYPE:IAcceleratorType<MAN>
		where ARC:IAcceleratorArchitecture
	{
	}
}