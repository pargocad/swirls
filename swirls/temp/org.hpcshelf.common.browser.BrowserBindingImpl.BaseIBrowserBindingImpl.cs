/* Automatically Generated Code */

using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentPortType;
using org.hpcshelf.common.BrowserPortType;
using org.hpcshelf.common.BrowserBinding;

namespace org.hpcshelf.common.browser.BrowserBindingImpl 
{
	public abstract class BaseIBrowserBindingImpl<S>: Synchronizer, BaseIBrowserBinding<S>
		where S:IBrowserPortType
	{
		private S server_port_type = default(S);

		protected S Server_port_type
		{
			get
			{
				if (this.server_port_type == null)
					this.server_port_type = (S) Services.getPort("server_port_type");
				return this.server_port_type;
			}
		}
		private IEnvironmentPortType client_port_type = null;

		protected IEnvironmentPortType Client_port_type
		{
			get
			{
				if (this.client_port_type == null)
					this.client_port_type = (IEnvironmentPortType) Services.getPort("client_port_type");
				return this.client_port_type;
			}
		}
	}
}