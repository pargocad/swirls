/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.storage.Type;

namespace org.hpcshelf.backend.ec2.storage.type.HDD
{
	public interface BaseIEC2StorageType_HDD : BaseIEC2StorageType, IQualifierKind 
	{
	}
}