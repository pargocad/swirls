/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner;
using org.hpcshelf.binding.environment.EnvironmentBindingBase;

namespace org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect
{
    public interface BaseIBindingDirect<C,S> : BaseIClientBase<C>, BaseIServerBase<S>, ISynchronizerKind 
		where C:IEnvironmentPortTypeSinglePartner
		where S:IEnvironmentPortTypeSinglePartner
	{
	}
}