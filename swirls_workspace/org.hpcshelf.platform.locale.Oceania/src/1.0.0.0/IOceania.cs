using org.hpcshelf.platform.locale.AnyWhere;

namespace org.hpcshelf.platform.locale.Oceania
{
	public interface IOceania : BaseIOceania, IAnyWhere
	{
	}
}