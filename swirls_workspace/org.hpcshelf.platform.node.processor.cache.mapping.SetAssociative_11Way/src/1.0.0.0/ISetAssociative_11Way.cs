using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_11Way
{
	public interface ISetAssociative_11Way : BaseISetAssociative_11Way, ICacheMappingSetAssociative<org.hpcshelf.quantifier.IntUp.IntUp>
	{
	}
}