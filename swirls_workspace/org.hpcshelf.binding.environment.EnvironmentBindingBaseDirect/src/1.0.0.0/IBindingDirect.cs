using org.hpcshelf.binding.environment.EnvironmentBindingBase;
using org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner;

namespace org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect
{
    public interface IBindingDirect<C,S> : BaseIBindingDirect<C,S>, IClientBase<C>, IServerBase<S>
		where C:IEnvironmentPortTypeSinglePartner
		where S:IEnvironmentPortTypeSinglePartner
	{
	}
}