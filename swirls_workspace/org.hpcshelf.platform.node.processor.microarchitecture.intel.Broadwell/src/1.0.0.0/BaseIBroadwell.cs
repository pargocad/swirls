/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Microarchitecture;

namespace org.hpcshelf.platform.node.processor.microarchitecture.intel.Broadwell
{
	public interface BaseIBroadwell : BaseIProcessorMicroarchitecture<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>, IQualifierKind 
	{
	}
}