/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.node.processor.cache.mapping.Direct;

namespace org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative
{
	public interface BaseICacheMappingSetAssociative<K> : BaseICacheMappingDirect, IQualifierKind 
		where K:IntUp
	{
	}
}