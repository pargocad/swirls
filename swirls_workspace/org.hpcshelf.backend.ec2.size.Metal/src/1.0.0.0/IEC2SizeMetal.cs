using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Metal
{
	public interface IEC2SizeMetal : BaseIEC2SizeMetal, IEC2Size
	{
	}
}