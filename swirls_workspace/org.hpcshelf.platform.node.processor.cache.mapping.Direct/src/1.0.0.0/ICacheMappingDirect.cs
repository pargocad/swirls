using org.hpcshelf.platform.node.processor.cache.Mapping;

namespace org.hpcshelf.platform.node.processor.cache.mapping.Direct
{
	public interface ICacheMappingDirect : BaseICacheMappingDirect, ICacheMapping
	{
	}
}