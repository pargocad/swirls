/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.memory.R5d
{
	public interface BaseIEC2TypeR5d : BaseIEC2Type<family.Memory.IEC2FamilyMemory>, IQualifierKind 
	{
	}
}