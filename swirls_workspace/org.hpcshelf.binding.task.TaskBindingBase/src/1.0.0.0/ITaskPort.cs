using org.hpcshelf.binding.task.TaskPortType;

namespace org.hpcshelf.binding.task.TaskBindingBase
{
    public interface ITaskPort<T> : BaseITaskPort<T>
		where T : ITaskPortType
	{
	}

}