/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.Medium
{
 public interface BaseIGoogleSizeMedium : BaseIGoogleSize, IQualifierKind 
 {
 }
}