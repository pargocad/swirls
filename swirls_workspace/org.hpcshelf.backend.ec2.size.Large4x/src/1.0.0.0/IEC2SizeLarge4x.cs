using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large4x
{
	public interface IEC2SizeLarge4x : BaseIEC2SizeLarge4x, IEC2Size
	{
	}
}