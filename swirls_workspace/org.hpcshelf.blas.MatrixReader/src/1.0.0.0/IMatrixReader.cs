using org.hpcshelf.blas.data.DataDistribution;
using org.hpcshelf.types.Data;

namespace org.hpcshelf.blas.MatrixReader
{
	public interface IMatrixReader<DIST, T> : BaseIMatrixReader<DIST, T>
		where DIST:IDataDistribution
		where T:IData
	{
	}
}