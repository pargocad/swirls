/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.matrixtype.General;

namespace org.hpcshelf.blas.matrixtype.Hermitian
{
	public interface BaseIHermitianMatrix : BaseIGeneralMatrix, IQualifierKind 
	{
	}
}