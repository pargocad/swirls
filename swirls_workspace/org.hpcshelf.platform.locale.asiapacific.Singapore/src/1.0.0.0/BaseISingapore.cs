/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.Singapore
{
	public interface BaseISingapore : BaseIAsiaPacific, IQualifierKind 
	{
	}
}