using org.hpcshelf.platform.node.accelerator.Architecture;

namespace org.hpcshelf.platform.node.accelerator.architecture.Turing
{
	public interface ITuring : BaseITuring, IAcceleratorArchitecture
	{
	}
}