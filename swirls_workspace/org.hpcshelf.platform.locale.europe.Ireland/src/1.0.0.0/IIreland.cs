using org.hpcshelf.platform.locale.europe.WestEurope;

namespace org.hpcshelf.platform.locale.europe.Ireland
{
	public interface IIreland : BaseIIreland, IWestEurope
	{
	}
}