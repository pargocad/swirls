/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.accelerator.Model;

namespace org.hpcshelf.platform.node.accelerator.model.Tesla_K80
{
	public interface BaseITesla_K80 : BaseIAcceleratorModel<org.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA, org.hpcshelf.platform.node.accelerator.type.Tesla.ITesla, org.hpcshelf.platform.node.accelerator.architecture.Kepler.IKepler>, IQualifierKind 
	{
	}
}