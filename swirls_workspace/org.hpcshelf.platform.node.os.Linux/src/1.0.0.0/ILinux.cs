using org.hpcshelf.platform.node.OS;

namespace org.hpcshelf.platform.node.os.Linux
{
	public interface ILinux : BaseILinux, IOperatingSystem
	{
	}
}