using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Micro
{
	public interface IEC2SizeMicro : BaseIEC2SizeMicro, IEC2Size
	{
	}
}