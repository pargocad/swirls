/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X40
{
 public interface BaseIGoogleSizeX40 : BaseIGoogleSize, IQualifierKind 
 {
 }
}