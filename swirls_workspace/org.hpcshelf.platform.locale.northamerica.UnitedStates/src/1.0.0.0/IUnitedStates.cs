using org.hpcshelf.platform.locale.NorthAmerica;

namespace org.hpcshelf.platform.locale.northamerica.UnitedStates
{
	public interface IUnitedStates : BaseIUnitedStates, INorthAmerica
	{
	}
}