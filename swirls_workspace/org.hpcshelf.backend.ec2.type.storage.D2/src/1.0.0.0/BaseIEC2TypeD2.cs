/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.storage.D2
{
	public interface BaseIEC2TypeD2 : BaseIEC2Type<family.Storage.IEC2FamilyStorage>, IQualifierKind 
	{
	}
}