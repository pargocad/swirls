using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.common.BrowserPortType;
using org.hpcshelf.common.BrowserBinding;
using System.Threading;

namespace org.hpcshelf.common.browser.BrowserBindingImpl
{
	public class IBrowserBindingImpl<S> : BaseIBrowserBindingImpl<S>, IBrowserBinding<S>
		where S:IBrowserPortType
	{
		public override void main()
		{
		}
		
 		private ManualResetEvent sync = new ManualResetEvent(false);				

        public IBrowserPortType Client
        {
            get
            {
		   	   sync.WaitOne ();
			   return server;
            }
        }

        private S server = default(S);

        public S Server
        {
            set
            {
   		    	this.server = value;
                if (this.server == null)
                {
                    Console.WriteLine("IBrowseBindingImpl - SERVER RESET");
                    sync.Reset();
                }
                else
                {
                    Console.WriteLine("IBrowseBindingImpl - SERVER SET");
                    sync.Set ();
                }
            }
        }
	}
}
