/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect;
using org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner;
using org.hpcshelf.blas.MatrixPortType;
using org.hpcshelf.types.Data;
using org.hpcshelf.blas.data.DataDistribution;

namespace org.hpcshelf.blas.MatrixConnector
{
	public interface BaseIOutput<T_GET, DIST_GET> : ISynchronizerKind 
		where T_GET:IData
		where DIST_GET:IDataDistribution
	{
		IBindingDirect<IEnvironmentPortTypeSinglePartner, IMatrixPortType<T_GET, DIST_GET>> Get {get;}
	}
}