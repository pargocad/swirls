/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.interconnection.Topology;

namespace org.hpcshelf.platform.Interconnection
{
	public interface BaseIInterconnection<STT, NLT, BAN, TOP> : IQualifierKind 
		where STT:IntDown
		where NLT:IntDown
		where BAN:IntUp
		where TOP:ITopology
	{
	}
}