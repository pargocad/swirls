/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.N2_Standard
{
 public interface BaseIGoogleTypeN2_Standard : BaseIGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>, IQualifierKind 
 {
 }
}