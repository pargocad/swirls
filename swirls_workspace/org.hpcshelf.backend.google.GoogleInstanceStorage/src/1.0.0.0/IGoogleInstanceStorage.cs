using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.backend.google.storage.Type;
using org.hpcshelf.platform.node.Storage;

namespace org.hpcshelf.backend.google.GoogleInstanceStorage
{
	public interface IGoogleInstanceStorage<SIZ, LAT, BAND, NETBAND, TYPE> : BaseIGoogleInstanceStorage<SIZ, LAT, BAND, NETBAND, TYPE>, IStorage<SIZ, LAT, BAND, NETBAND>
		where SIZ:IntUp
		where LAT:IntDown
		where BAND:IntUp
		where NETBAND:IntUp
		where TYPE:IGoogleStorageType
	{
	}
}