/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.Asia;

namespace org.hpcshelf.platform.locale.AsiaPacific
{
	public interface BaseIAsiaPacific : BaseIAsia, IQualifierKind 
	{
	}
}