using org.hpcshelf.common.BrowserPortType;
using org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect;

namespace org.hpcshelf.common.BrowserBinding
{
	public interface IBrowserBinding<S> : BaseIBrowserBinding<S>, IBindingDirect<org.hpcshelf.common.BrowserPortType.IBrowserPortType, S>
		where S:IBrowserPortType
	{
	}
}