/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.Asia;

namespace org.hpcshelf.platform.locale.MiddleEast
{
	public interface BaseIMiddleEast : BaseIAsia, IQualifierKind 
	{
	}
}