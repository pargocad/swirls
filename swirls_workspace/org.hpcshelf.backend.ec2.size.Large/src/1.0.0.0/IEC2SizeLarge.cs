using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large
{
	public interface IEC2SizeLarge : BaseIEC2SizeLarge, IEC2Size
	{
	}
}