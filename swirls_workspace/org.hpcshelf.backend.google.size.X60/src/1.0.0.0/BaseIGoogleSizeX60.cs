/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X60
{
 public interface BaseIGoogleSizeX60 : BaseIGoogleSize, IQualifierKind 
 {
 }
}