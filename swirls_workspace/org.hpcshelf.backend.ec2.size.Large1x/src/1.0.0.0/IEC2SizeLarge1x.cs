using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large1x
{
	public interface IEC2SizeLarge1x : BaseIEC2SizeLarge1x, IEC2Size
	{
	}
}