/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Family;

namespace org.hpcshelf.platform.node.processor.family.amd.EPYC
{
	public interface BaseIEPYC : BaseIProcessorFamily<org.hpcshelf.platform.node.processor.manufacturer.AMD.IAMD>, IQualifierKind 
	{
	}
}