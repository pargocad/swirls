using org.hpcshelf.platform.node.processor.Microarchitecture;

namespace org.hpcshelf.platform.node.processor.microarchitecture.intel.Haswell
{
	public interface IHaswell : BaseIHaswell, IProcessorMicroarchitecture<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>
	{
	}
}