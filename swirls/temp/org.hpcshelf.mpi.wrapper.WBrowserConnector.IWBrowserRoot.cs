using org.hpcshelf.common.BrowserConnector;

namespace org.hpcshelf.mpi.wrapper.WBrowserConnector
{
	public interface IWBrowserRoot : BaseIWBrowserRoot, IBrowserRoot<org.hpcshelf.common.browser.RecvDataPortType.IRecvDataPortType>
	{
	}
}