/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.mpi.IntercommunicatorPortType;
using org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect;

namespace org.hpcshelf.mpi.binding.IntercommunicatorBinding
{
	public interface BaseIIntercommunicatorBinding<C> : BaseIBindingDirect<C, org.hpcshelf.mpi.IntercommunicatorPortType.IIntercommunicatorPortType>, ISynchronizerKind 
		where C:IIntercommunicatorPortType
	{
	}
}