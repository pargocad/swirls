using org.hpcshelf.blas.configure.SIDE;

namespace org.hpcshelf.blas.configure.side.NA
{
	public interface NA : BaseNA, ISIDE
	{
	}
}