using org.hpcshelf.types.YesOrNo;

namespace org.hpcshelf.types.yesorno.No
{
	public interface INo : BaseINo, IYesOrNo
	{
	}
}