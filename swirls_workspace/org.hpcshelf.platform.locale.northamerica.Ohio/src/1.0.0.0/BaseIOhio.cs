/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.northamerica.US_East;

namespace org.hpcshelf.platform.locale.northamerica.Ohio
{
	public interface BaseIOhio : BaseIUS_East, IQualifierKind 
	{
	}
}