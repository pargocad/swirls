/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.OS;

namespace org.hpcshelf.platform.node.os.Linux
{
	public interface BaseILinux : BaseIOperatingSystem, IQualifierKind 
	{
	}
}