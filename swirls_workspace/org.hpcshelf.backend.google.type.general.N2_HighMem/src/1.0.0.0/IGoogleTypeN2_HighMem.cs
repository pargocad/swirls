using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.N2_HighMem
{
 public interface IGoogleTypeN2_HighMem : BaseIGoogleTypeN2_HighMem, IGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>
 {
 }
}