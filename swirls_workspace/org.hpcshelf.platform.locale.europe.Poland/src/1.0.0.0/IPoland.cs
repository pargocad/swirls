using org.hpcshelf.platform.locale.europe.CentralEurope;

namespace org.hpcshelf.platform.locale.europe.Poland
{
	public interface IPoland : BaseIPoland, ICentralEurope
	{
	}
}