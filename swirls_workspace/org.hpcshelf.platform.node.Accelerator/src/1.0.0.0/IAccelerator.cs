using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.node.accelerator.Manufacturer;
using org.hpcshelf.platform.node.accelerator.Type;
using org.hpcshelf.platform.node.accelerator.Architecture;
using org.hpcshelf.platform.node.accelerator.Model;

namespace org.hpcshelf.platform.node.Accelerator
{
	public interface IAccelerator<ACT, MAN, TYP, ARC, MOD, MEM> : BaseIAccelerator<ACT, MAN, TYP, ARC, MOD, MEM>
		where ACT:IntUp
		where MAN:IAcceleratorManufacturer
		where TYP:IAcceleratorType<MAN>
		where ARC:IAcceleratorArchitecture
		where MOD:IAcceleratorModel<MAN, TYP, ARC>
		where MEM:IntUp
	{
	}
}