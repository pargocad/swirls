using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;

namespace org.hpcshelf.backend.ec2.family.Memory
{
	public interface IEC2FamilyMemory : BaseIEC2FamilyMemory, IEC2Family
	{
	}
}