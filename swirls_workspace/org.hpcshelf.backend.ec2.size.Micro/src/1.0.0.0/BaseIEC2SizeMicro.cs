/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Micro
{
	public interface BaseIEC2SizeMicro : BaseIEC2Size, IQualifierKind 
	{
	}
}