/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.types.Data;
using org.hpcshelf.blas.data.DataDistribution;
using org.hpcshelf.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace org.hpcshelf.blas.MatrixPortType
{
	public interface BaseIMatrixPortType<T, DIST> : BaseIEnvironmentPortTypeMultiplePartner, IQualifierKind 
		where T:IData
		where DIST:IDataDistribution
	{
	}
}