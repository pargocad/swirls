using org.hpcshelf.platform.locale.northamerica.US_West;

namespace org.hpcshelf.platform.locale.northamerica.Oregon
{
	public interface IOregon : BaseIOregon, IUS_West
	{
	}
}