using org.hpcshelf.platform.node.processor.Microarchitecture;

namespace org.hpcshelf.platform.node.processor.microarchitecture.intel.Broadwell
{
	public interface IBroadwell : BaseIBroadwell, IProcessorMicroarchitecture<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>
	{
	}
}