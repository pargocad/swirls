using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X4g
{
 public interface IGoogleSizeX4g : BaseIGoogleSizeX4g, IGoogleSize
 {
 }
}