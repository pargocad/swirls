using org.hpcshelf.platform.locale.Asia;

namespace org.hpcshelf.platform.locale.MiddleEast
{
	public interface IMiddleEast : BaseIMiddleEast, IAsia
	{
	}
}