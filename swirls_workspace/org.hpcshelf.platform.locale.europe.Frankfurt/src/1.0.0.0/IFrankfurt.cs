using org.hpcshelf.platform.locale.europe.Germany;

namespace org.hpcshelf.platform.locale.europe.Frankfurt
{
	public interface IFrankfurt : BaseIFrankfurt, IGermany
	{
	}
}