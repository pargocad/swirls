using org.hpcshelf.platform.locale.Oceania;

namespace org.hpcshelf.platform.locale.oceania.Australia
{
	public interface IAustralia : BaseIAustralia, IOceania
	{
	}
}