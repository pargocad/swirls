/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.NorthAmerica;

namespace org.hpcshelf.platform.locale.northamerica.UnitedStates
{
	public interface BaseIUnitedStates : BaseINorthAmerica, IQualifierKind 
	{
	}
}