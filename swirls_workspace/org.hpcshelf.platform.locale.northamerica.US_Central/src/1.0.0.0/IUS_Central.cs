using org.hpcshelf.platform.locale.northamerica.UnitedStates;

namespace org.hpcshelf.platform.locale.northamerica.US_Central
{
	public interface IUS_Central : BaseIUS_Central, IUnitedStates
	{
	}
}