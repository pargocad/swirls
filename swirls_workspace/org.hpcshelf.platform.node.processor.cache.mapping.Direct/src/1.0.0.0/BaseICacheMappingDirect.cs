/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.cache.Mapping;

namespace org.hpcshelf.platform.node.processor.cache.mapping.Direct
{
	public interface BaseICacheMappingDirect : BaseICacheMapping, IQualifierKind 
	{
	}
}