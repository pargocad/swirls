using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X96
{
 public interface IGoogleSizeX96 : BaseIGoogleSizeX96, IGoogleSize
 {
 }
}