using System;
//using MPI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Runtime.InteropServices;

namespace org.hpcshelf.binding.channel.Binding
{
	/*
    public enum AliencommunicatorOperation {
        SEND, 
        RECEIVE, 
        SEND_ARRAY,
        RECEIVE_ARRAY,
        PROBE,
        ALL_GATHER,
        ALL_GATHER_FLATTENED,
        ALL_REDUCE,
        ALL_REDUCE_ARRAY,
        ALL_TO_ALL,
        ALL_TO_ALL_FLATTENED,
        REDUCE_SCATTER,
        BROADCAST,
        BROADCAST_ARRAY,
        SCATTER,
        SCATTER_FROM_FLATTENED,
        GATHER,
        GATHER_FLATTENED,
        REDUCE,
        REDUCE_ARRAY
    };
    */

	public class AliencommunicatorOperation
	{
		public const int SEND = 0;
		public const int RECEIVE = 1;
		public const int SEND_ARRAY = 2;
		public const int RECEIVE_ARRAY = 3;
		public const int PROBE = 4;
		public const int ALL_GATHER = 5;
		public const int ALL_GATHER_FLATTENED = 6;
		public const int ALL_REDUCE = 7;
		public const int ALL_REDUCE_ARRAY = 8;
		public const int ALL_TO_ALL = 9;
		public const int ALL_TO_ALL_FLATTENED = 10;
		public const int REDUCE_SCATTER = 11;
		public const int BROADCAST = 12;
		public const int BROADCAST_ARRAY = 13;
		public const int SCATTER = 14;
		public const int SCATTER_FROM_FLATTENED = 15;
		public const int GATHER = 16;
		public const int GATHER_FLATTENED = 17;
		public const int REDUCE = 18;
		public const int REDUCE_ARRAY = 19;
		public const int SYNC_SEND = 20;
	};

	public interface Aliencommunicator
	{
		#region point-to-point operations




		// Value versions ...

		void Send<T>(T value, Tuple<int, int> dest, int tag);

		//      void SendReceive<T> (T inValue, int dest, int tag, out T outValue); /* ok */
		//      void SendReceive<T> (T inValue, int dest, int sendTag, int source, int recvTag, out T outValue); /* ok */
		//      void SendReceive<T> (T inValue, int dest, int sendTag, int source, int recvTag, out T outValue, out CompletedStatus status);

		T Receive<T>(Tuple<int, int> source, int tag); /* ok */
		void Receive<T>(Tuple<int, int> source, int tag, out T value); /* ok */
		void Receive<T>(Tuple<int, int> source, int tag, out T value, out CompletedStatus status);

		byte[] ReceiveNative(Tuple<int, int> source, int tag); /* ok */
		void ReceiveNative(Tuple<int, int> source, int tag, out byte[] value); /* ok */
		void ReceiveNative(Tuple<int, int> source, int tag, out byte[] value, out CompletedStatus status);

		IRequest ImmediateSend<T>(T value, Tuple<int, int> dest, int tag);
		IReceiveRequest ImmediateReceive<T>(Tuple<int, int> source, int tag);

		// Array versions ... 
		void Send<T>(T[] values, Tuple<int, int> dest, int tag);

		void Receive<T>(Tuple<int, int> source, int tag, ref T[] values); /* ok */
		void Receive<T>(Tuple<int, int> source, int tag, ref T[] values, out CompletedStatus status);

		IRequest ImmediateSend<T>(T[] values, Tuple<int, int> dest, int tag);
		IReceiveRequest ImmediateReceive<T>(Tuple<int, int> source, int tag, T[] values);

		// Probe.

		//TODO      Status Probe (Tuple<int,int> source, int tag);
		//TODO      Status ImmediateProbe (Tuple<int,int> source, int tag);

		//      void SendReceive<T>(T[] inValues, int dest, int tag, ref T[] outValues); /* ok */
		//      void SendReceive<T>(T[] inValues, int dest, int sendTag, int source, int recvTag, ref T[] outValues); /* ok */
		//      void SendReceive<T> (T[] inValues, int dest, int sendTag, int source, int recvTag, ref T[] outValues, out CompletedStatus status);

		#endregion point-to-point operations

		#region collective operations

		#region AllToAll

		//TODO      T[] Allgather<T> (int facet, T value);
		//TODO      void Allgather<T> (int facet, T inValue, ref T[] outValues);

		//TODO      void AllgatherFlattened<T> (int facet, T[] inValues, int count, ref T[] outValues);
		//TODO      void AllgatherFlattened<T> (int facet, T[] inValues, int[] counts, ref T[] outValues);

		//TODO      T Allreduce<T>(int facet, T value, ReductionOperation<T> op);
		//TODO      T[] Allreduce<T> (int facet, T[] values, ReductionOperation<T> op);
		//TODO      void Allreduce<T> (int facet, T[] inValues, ReductionOperation<T> op, ref T[] outValues);

		//TODO      T[] Alltoall<T> (int facet, T[] values);
		//TODO      void Alltoall<T> (int facet, T[] inValues, ref T[] outValues);

		//TODO void AlltoallFlattened<T> (int facet, T[] inValues, int[] sendCounts, int[] recvCounts, ref T[] outValues);

		//TODO      T[] ReduceScatter<T> (int facet, T[] values, ReductionOperation<T> op, int[] counts);
		//TODO      void ReduceScatter<T> (int facet, T[] inValues, ReductionOperation<T> op, int[] counts, ref T[] outValues);

		#endregion AllToAll


		#region OneToAll

		//TODO      void Broadcast<T> (int facet, ref T value, int root);
		//TODO      void Broadcast<T> (int facet, ref T[] values, int root);

		//TODO      void Scatter<T> (int facet, T[] values);
		//TODO      T Scatter<T>(int facet, int root);
		//TODO      void Scatter<T>(int facet);

		//TODO      void ScatterFromFlattened<T> (int facet, T[] inValues, int count);
		//TODO      void ScatterFromFlattened<T> (int facet, T[] inValues, int[] counts);
		//TODO      void ScatterFromFlattened<T> (int facet, int count, int root, ref T[] outValues);
		//TODO      void ScatterFromFlattened<T> (int facet, int[] counts, int root, ref T[] outValues);
		//TODO      void ScatterFromFlattened<T> (int facet);
		//TODO      void ScatterFromFlattened<T> (int facet, T[] inValues, int count, int root, ref T[] outValues);
		//TODO      void ScatterFromFlattened<T> (int facet, T[] inValues, int[] counts, int root, ref T[] outValues);

		#endregion OneToAll


		#region AllToOne

		//TODO      T[] Gather<T> (int facet, T value, int root);
		//TODO      T[] Gather<T> (int facet, int root);
		//TODO      void Gather<T>(int facet);
		//TODO      void Gather<T>(int facet, T inValue, int root, ref T[] outValues);

		//TODO      void GatherFlattened<T>(int facet, int count, ref T[] outValues);
		//TODO      T[] GatherFlattened<T>(int facet, int count);
		//TODO      void GatherFlattened<T> (int facet, T[] inValues, int root);
		//TODO      void GatherFlattened<T>(int facet);
		//TODO      void GatherFlattened<T> (int facet, int[] counts, ref T[] outValues);
		//TODO      T[] GatherFlattened<T>(int facet, int[] counts);

		//TODO      T Reduce<T> (int facet, T value, ReductionOperation<T> op, int root);
		//TODO      T[] Reduce<T>(int facet, T[] values, ReductionOperation<T> op, int root);
		//TODO      void Reduce<T>(int facet, T[] inValues, ReductionOperation<T> op, int root, ref T[] outValues);

		#endregion AllToOne


		#endregion collective operations

	}


	public interface Status
	{
		Tuple<int, int> Source { get; }
		int Tag { get; }
		int? Count(Type type);
		bool Cancelled { get; }
	}

	public interface CompletedStatus : Status
	{
		int? Count { get; }
	}

    public interface IRequest
    {
        void registerWaitingSet(AutoResetEvent waiting_set);
        void unregisterWaitingSet(AutoResetEvent waiting_set);
        CompletedStatus Wait();
        CompletedStatus Test();
        void Cancel();
    }

    public interface IReceiveRequest : IRequest
    {
        object GetValue();
    }













	/// <summary>
	/// A non-blocking receive request. 
	/// </summary>
	/// <remarks>
	/// This class allows one to test a receive
	/// request for completion, wait for completion of a request, cancel a request,
	/// or extract the value received by this communication request.
	/// </remarks>
/*	public class ArrayReceiveRequest<T> : ReceiveRequest
	{
		private T[] values = null;
		private byte[] v = null;
		Tuple<int, int> source = null;
		int? tag = null;

		internal ArrayReceiveRequest(ManualResetEvent e, Tuple<int, int> source, int tag, T[] values) : base(e, source, tag)
		{
			this.values = values;
			this.source = source;
			this.tag = null;
		}

		/// <summary>
		/// Retrieve the value received via this communication. The value
		/// will only be available when the communication has completed.
		/// </summary>
		/// <returns>The value received by this communication.</returns>
		public override object GetValue()
		{
            Wait();

			if (v == null)
				return null;

			T[] values_ = (T[])ByteArrayToObject(v);

			// Copy the received values to the destination array (forcing original MPI semantics)
			int size = values.Length <= values_.Length ? values.Length : values_.Length;
			for (int i = 0; i < size; i++)
				values[i] = values_[i];

			return values;
		}

		public void SetValue(byte[] v)
		{
			values = (T[])ByteArrayToObject(v);
		}


		public static ArrayReceiveRequest<T> createRequest(ManualResetEvent e, Tuple<int, int> source, int tag, T[] values)
		{
			return new ArrayReceiveRequest<T>(e, source, tag, values);
		}

		public new CompletedStatus Wait()
		{
			SAFeCompletedStatus status = (SAFeCompletedStatus)base.Wait();
			status.Count = values.Length;
			return status;
		}

		public new CompletedStatus Test()
		{
			SAFeCompletedStatus status = (SAFeCompletedStatus)base.Test();

			if (status != null)
				status.Count = values.Length;

			return status;
		}

	}
    */


        /*
	/// <summary>
	/// A request list contains a list of outstanding MPI requests. 
	/// </summary>
	/// 
	/// <remarks>
	/// The requests in a <c>RequestList</c>
	/// are typically non-blocking send or receive operations (e.g.,
	/// <see cref="Communicator.ImmediateSend&lt;T&gt;(T, int, int)"/>,
	/// <see cref="Communicator.ImmediateReceive&lt;T&gt;(int, int)"/>). The
	/// request list provides the ability to operate on the set of MPI requests
	/// as a whole, for example by waiting until all requests complete before
	/// returning or testing whether any of the requests have completed.
	/// </remarks>
	public class RequestList
	{
		/// <summary>
		/// Create a new, empty request list.
		/// </summary>
		public RequestList()
		{
			this.requests = new List<IRequest>();
		}



		/// <summary>
		/// Add a new request to the request list.
		/// </summary>
		/// <param name="request">The request to add.</param>
		public void Add(IRequest request)
		{
			requests.Add(request);

			if (sync_request != null)
				request.registerWaitingSet(sync_request);
		}

		/// <summary>
		/// Remove a request from the request list.
		/// </summary>
		/// <param name="request">Request to remove.</param>
		public void Remove(IRequest request)
		{
			requests.Remove(request);
		}

		/// <summary>
		/// Retrieves the number of elements in this list of requests.
		/// </summary>
		public int Count
		{
			get
			{
				return this.requests.Count;
			}
		}

		AutoResetEvent sync_request = null;

		/// <summary>
		/// Waits until any request has completed. That request will then be removed 
		/// from the request list and returned.
		/// </summary>
		/// <returns>The completed request, which has been removed from the request list.</returns>
		public IRequest WaitAny()
		{
			if (requests.Count == 0)
				throw new ArgumentException("Cannot call MPI.RequestList.WaitAny with an empty request list");

			sync_request = new AutoResetEvent(false);

			foreach (IRequest req_item in requests)
				req_item.registerWaitingSet(sync_request);

			sync_request.WaitOne();
			sync_request = null;

			IRequest req = this.TestAny();

			foreach (IRequest req_item in requests)
				req_item.unregisterWaitingSet(sync_request);

			return req;


			

		}

		/// <summary>
		/// Determines whether any request has completed. If so, that request will be removed
		/// from the request list and returned. 
		/// </summary>
		/// <returns>
		///   The first request that has completed, if any. Otherwise, returns <c>null</c> to
		///   indicate that no request has completed.
		/// </returns>
		public IRequest TestAny()
		{
			int n = requests.Count;
			for (int i = 0; i < n; ++i)
			{
				IRequest req = requests[i];
				if (req.Test() != null)
				{
					requests.RemoveAt(i);
					return req;
				}
			}

			return null;
		}

		/// <summary>
		/// Wait until all of the requests has completed before returning.
		/// </summary>
		/// <returns>A list containing all of the completed requests.</returns>
		public List<IRequest> WaitAll()
		{
			List<IRequest> result = new List<IRequest>();
			foreach (IRequest req in requests)
			{
				req.Wait();
				result.Add(req);
			}

		

			return result;
		}

		/// <summary>
		/// Test whether all of the requests have completed. If all of the
		/// requests have completed, the result is the list of requests. 
		/// Otherwise, the result is <c>null</c>.
		/// </summary>
		/// <returns>Either the list of all completed requests, or null.</returns>
		public List<IRequest> TestAll()
		{
			int n = requests.Count;
			for (int i = 0; i < n; ++i)
			{
				if (requests[i].Test() == null)
					return null;
			}

			List<IRequest> result = requests;
			requests = new List<IRequest>();
			return result;
		}

		/// <summary>
		/// Wait for at least one request to complete, then return a list of
		/// all of the requests that have completed at this point.
		/// </summary>
		/// <returns>
		///   A list of all of the requests that have completed, which
		///   will contain at least one element.
		/// </returns>
		public List<IRequest> WaitSome()
		{
			if (requests.Count == 0)
				throw new ArgumentException("Cannot call MPI.RequestList.WaitAny with an empty request list");

			List<IRequest> result = new List<IRequest>();
			while (result.Count == 0)
			{
				int n = requests.Count;
				for (int i = 0; i < n; ++i)
				{
					IRequest req = requests[i];
					if (req.Test() != null)
					{
						requests.RemoveAt(i);
						--i;
						--n;
						result.Add(req);
					}
				}
			}
			return result;
		}

		/// <summary>
		/// Return a list of all requests that have completed.
		/// </summary>
		/// <returns>
		///   A list of all of the requests that have completed. If
		///   no requests have completed, returns <c>null</c>.
		/// </returns>
		public List<IRequest> TestSome()
		{
			List<IRequest> result = null;
			int n = requests.Count;
			for (int i = 0; i < n; ++i)
			{
				IRequest req = requests[i];
				if (req.Test() != null)
				{
					requests.RemoveAt(i);
					--i;
					--n;

					if (result == null)
						result = new List<IRequest>();
					result.Add(req);
				}
			}
			return result;
		}

		/// <summary>
		/// The actual list of requests.
		/// </summary>
		protected List<IRequest> requests;

        public List<IRequest> Requests
        {
            get { return requests; }
        }
	}

    */

}

