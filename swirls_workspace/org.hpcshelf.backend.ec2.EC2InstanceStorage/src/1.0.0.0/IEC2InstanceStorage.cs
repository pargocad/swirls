using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.backend.ec2.storage.Type;
using org.hpcshelf.platform.node.Storage;

namespace org.hpcshelf.backend.ec2.EC2InstanceStorage
{
	public interface IEC2InstanceStorage<SIZ, LAT, BAND, NETBAND, TYPE> : BaseIEC2InstanceStorage<SIZ, LAT, BAND, NETBAND, TYPE>, IStorage<SIZ, LAT, BAND, NETBAND>
		where SIZ:IntUp
		where LAT:IntDown
		where BAND:IntUp
		where NETBAND:IntUp
		where TYPE:IEC2StorageType
	{
	}
}