using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.memory.R5
{
	public interface IEC2TypeR5 : BaseIEC2TypeR5, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}