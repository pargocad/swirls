using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.Japan
{
	public interface IJapan : BaseIJapan, IAsiaPacific
	{
	}
}