using org.hpcshelf.mpi.MPIConnector;

namespace org.hpcshelf.mpi.MPIConnectorForC
{
	public interface IMPIConnectorForC : BaseIMPIConnectorForC, IMPIConnector<org.hpcshelf.mpi.language.C.ILanguage_C, org.hpcshelf.mpi.intercommunicator.BasicSendReceive.IBasicSendReceive>
	{
	}
}