/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.common.BrowserBinding;
using org.hpcshelf.common.BrowserPortType;
using org.hpcshelf.mpi.Language;

namespace org.hpcshelf.common.BrowserConnector
{
	public interface BaseIBrowserPeer<L, S1> : ISynchronizerKind 
		where L:ILanguage
		where S1:IBrowserPortType
	{
		IBrowserBinding<S1> Send_data_port {get;}
	}
}