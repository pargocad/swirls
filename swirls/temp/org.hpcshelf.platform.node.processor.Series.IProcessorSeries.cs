using org.hpcshelf.platform.node.processor.Manufacturer;
using org.hpcshelf.platform.node.processor.Family;

namespace org.hpcshelf.platform.node.processor.Series
{
	public interface IProcessorSeries<MAN, FAM> : BaseIProcessorSeries<MAN, FAM>
		where MAN:IProcessorManufacturer
		where FAM:IProcessorFamily<MAN>
	{
	}
}