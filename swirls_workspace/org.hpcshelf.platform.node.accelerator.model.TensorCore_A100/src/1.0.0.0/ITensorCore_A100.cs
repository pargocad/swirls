using org.hpcshelf.platform.node.accelerator.Model;

namespace org.hpcshelf.platform.node.accelerator.model.TensorCore_A100
{
	public interface ITensorCore_A100 : BaseITensorCore_A100, IAcceleratorModel<org.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA, org.hpcshelf.platform.node.accelerator.type.TensorCore.ITensorCore, org.hpcshelf.platform.node.accelerator.architecture.Volta.IVolta>
	{
	}
}