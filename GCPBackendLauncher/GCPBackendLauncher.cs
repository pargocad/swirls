﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using BackendLauncher;
using org.hpcshelf.command;
using org.hpcshelf.core;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Compute.v1;
using Google.Apis.Compute.v1.Data;
using Google.Apis.Services;
using Newtonsoft.Json;

public class GCPBackendLauncher : IBackendLauncher
{
    private string name = null;
    private CoreServices core_services = null;
    private string zone;

    public string launch(string name, string zone, CoreServices core_services)
    {
        this.name = name;
        this.core_services = core_services;
        this.zone = zone;

        if (!zone.Equals("local"))
        {
            // launch backend service.
            launchGoogleInstance(name, zone);

            // configure environment
            launchGoogleBackendService();

            // create abstract qualifier component.
            ComponentType c_abstract = createAbstractQualifier(name);

            // create concrete qualifier component.
            ComponentType c_concrete = createConcreteQualifier(name, backend_address);

            deployComponent(c_abstract, core_services);
            deployComponent(c_concrete, core_services);
        }
        else
            launchGCPBackendServiceLocally();

        return backend_address;
    }

    private string launchGCPBackendServiceLocally()
    {
        string script_command = Path.Combine(SWIRLS_HOME, "run_gcp-backend_services_local.sh");

        Utils.commandExecBash(string.Format("{0}", script_command));

        platform_address = "127.0.0.1";

        return (backend_address = string.Format("http://{0}:8078/BackendServices.asmx", platform_address));
    }

    private string SWIRLS_HOME
    {
        get
        {
            string path = Environment.GetEnvironmentVariable("Swirls_HOME");
            if (path == null)
            {
                Console.WriteLine("Swirls_HOME not set. Using the current directory.");
                path = Utils.CURRENT_FOLDER;
            }
            return path;
        }
    }


    private void launchGoogleBackendService()
    {
        // DEFAULT_AMI_ID and run_google-backend_service_local.sh are placed at the platform.
        // 1. copy the user credentials ("scp -i $1 ~/.aws/config  ubuntu@$2:.aws/config", "scp -i $1 ~/.aws/credentials  ubuntu@$2:.aws/credentials").
        // 2. launch the service ("nohup rsh -i ~/hpc-shelf-credential.pem ubuntu@$1 "bash ~/run_google-backend_service.sh" &")

        string script_command = Path.Combine(SWIRLS_HOME, "run_gcp-backend_services_remote.sh");

        string command = string.Format("{0} {1} {2}", script_command, credential_file_name, platform_address);
        Console.WriteLine("COMMAND: {0}", command);

        Utils.commandExecBash(command);
    }

    public void stop()
    {
        if (zone.Equals("local"))
            throw new Exception("The operation 'stop' does not apply to a local EC2 backend");

        // launch backend service.
        stopBackendService();
    }

    public string start()
    {
        if (zone.Equals("local"))
            throw new Exception("The operation 'start' does not apply to a local EC2 backend");

        // launch backend service.
        startBackendService();

        // create abstract qualifier component.
        ComponentType c_abstract = createAbstractQualifier(name);

        // create concrete qualifier component.
        ComponentType c_concrete = createConcreteQualifier(name, backend_address);

        deployComponent(c_abstract, core_services);
        deployComponent(c_concrete, core_services);

        return backend_address;
    }

    private string startBackendService()
    {
       //string platform_address = ""; // TODO

        return (backend_address = string.Format("http://{0}:8078/BackendServices.asmx", platform_address));
    }

    private void stopBackendService()
    {
        backend_address = "<stopped>";
    }

    public void terminate()
    {
        //InstancesManager.TerminateInstance(instanceId, client);
    }


    private string backend_address = "<pending>";
    private string platform_address = null;
    private string credential_file_name = null;

    public string BackendAddress { get { return backend_address; } }
    
    public static GoogleCredential GetCredential()
    {
        GoogleCredential credential = Task.Run(() => GoogleCredential.GetApplicationDefaultAsync()).Result;
        if (credential.IsCreateScopedRequired)
        {
            credential = credential.CreateScoped("https://www.googleapis.com/auth/cloud-platform");
        }
        return credential;
    }

    private string GCP_BACKEND_IMAGE_ID { get { return File.ReadAllLines(Path.Combine(SWIRLS_HOME, "GCP_BACKEND_IMAGE_ID"))[0]; } }

    private class ServiceAccountJson
    {
        public string type { get; set; }
        public string project_id { get; set; }
        public string private_key_id { get; set; }
        public string private_key { get; set; }
        public string client_email { get; set; }
        public string client_id { get; set; }
        public string auth_uri { get; set; }
        public string token_uri { get; set; }
        public string auth_provider_x509_cert_url { get; set; }
        public string client_x509_cert_url { get; set; }
    }

    private string project_id
    {
        get
        {
            string credentialsString = File.ReadAllText(Environment.GetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS"));
            ServiceAccountJson credential_data = JsonConvert.DeserializeObject<ServiceAccountJson>(credentialsString);
            return credential_data.project_id;
        }
    }

    private string launchGoogleInstance(string qname, string zone)
    {
        string keyPairName = "hpc-shelf-credential";
        credential_file_name = Path.Combine(SWIRLS_HOME, string.Format("{0}.pem", keyPairName));

        GoogleCredential credential = GetCredential();
        
        ComputeService computeService = new ComputeService(new BaseClientService.Initializer
        {
            HttpClientInitializer = credential,
            ApplicationName = "HPC Shelf - Google Backend",
        });

        // FETCH THE INSTANCE CONFIGURATION
        string project = project_id;
        string image_id = GCP_BACKEND_IMAGE_ID;
        string machine_type = string.Format("zones/{0}/machineTypes/n1-standard-1", zone);
        string network_id = "default";

        // FETCH THE IMAGE
        ImagesResource.GetRequest request_image = computeService.Images.Get(project, image_id);
        Image image = request_image.Execute();

        // FETCH THE NETWORK
        NetworksResource.GetRequest get_network = computeService.Networks.Get(project, network_id);
        Network network = get_network.Execute();

        // CONFIGURE THE NetworkInterface OBJECT
        NetworkInterface network_interface = new NetworkInterface();
        network_interface.Network = network.SelfLink;

        // CREATE AN AccessConfig OBJECT FOR HAVING A PUBLIC IP.
        AccessConfig access_config = new AccessConfig();
        network_interface.AccessConfigs = new List<AccessConfig>();
        network_interface.AccessConfigs.Add(access_config);

        string timestamp = DateTime.Now.Millisecond.ToString();
        string suffix = string.Format("{0}.{1}", qname, timestamp).Replace('.','-').Replace("_","").ToLower();

        // CREATE DISK FROM IMAGE FOR THE INSTANCE
        Disk disk_description = new Disk();
        disk_description.SourceImage = image.SelfLink;
        disk_description.Name = "hpcshelf-disk-" + suffix;
        DisksResource.InsertRequest request_disk = computeService.Disks.Insert(disk_description, project, zone);
        request_disk.Execute();

        // Thread.Sleep(2000);

        // FETCH THE CREATED DISK
        DisksResource.GetRequest get_disk = computeService.Disks.Get(project, zone, "hpcshelf-disk-" + suffix);
        Disk new_disk; // = get_disk.Execute();

        do
        {
            new_disk = get_disk.Execute();
            Console.WriteLine("DISK {0} is {1}", new_disk.Name, new_disk.Status);
            Thread.Sleep(500);
        }
        while (!new_disk.Status.Equals("READY"));

        // CONFIGURE THE DISK TO BE ATTACHED IN THE INSTANCE
        AttachedDisk disk = new AttachedDisk();
        disk.Source = new_disk.SelfLink;
        disk.Boot = true;

        // CONFIGURE REQUEST INSTANCE OBJECT
        Instance request_instance = new Instance();
        request_instance.Name = string.Format("hpcshelf-backend-google-{0}", suffix);
        request_instance.Zone = zone;
        request_instance.MachineType = machine_type;
        request_instance.Disks = new List<AttachedDisk>(new AttachedDisk[1] { disk });
        request_instance.NetworkInterfaces = new List<NetworkInterface>(new NetworkInterface[1] { network_interface });

        // REQUEST INSTANCE CREATION
        InstancesResource.InsertRequest request = computeService.Instances.Insert(request_instance, project, zone);
        request.Execute();
        // Thread.Sleep(5000);

        // FETCH THE INSTANCE
        InstancesResource.GetRequest get_instance = computeService.Instances.Get(project, zone, request_instance.Name);
        Instance instance; // = get_instance.Execute();

        do
        {
            instance = get_instance.Execute();
            Console.WriteLine("INSTANCE {0} is {1}", instance.Name, instance.Status);
            Thread.Sleep(1000);
        }
        while (!instance.Status.Equals("RUNNING"));


        platform_address = instance.NetworkInterfaces[0].AccessConfigs[0].NatIP;

        Console.WriteLine("ADDRESS; {0}", platform_address);

        return (backend_address = string.Format("http://{0}:8078/BackendServices.asmx", platform_address));
    }


    private void deployComponent(ComponentType c, CoreServices core_services)
    {
        string module_data_out_abstract = LoaderApp.serialize<ComponentType>(c);

        File.WriteAllText(c.header.name + ".hpe", module_data_out_abstract);

        string snk_fn = c.header.name + ".snk";
        string pub_fn = c.header.name + ".pub";

        byte[] snk_contents = File.ReadAllBytes(snk_fn);

        core_services.register(module_data_out_abstract, snk_contents);

        File.Delete(snk_fn);
        File.Delete(pub_fn);
    }

    private ComponentType createAbstractQualifier(string qname)
    {
        string packagePath = qname.Substring(0, qname.LastIndexOf('.'));
        string name = qname.Substring(qname.LastIndexOf('.') + 1);

        string snk_fn_abstract = name + ".snk";
        string pub_fn_abstract = name + ".pub";

        string uid_abstract = FileUtil.create_snk_file(snk_fn_abstract, pub_fn_abstract);

        ComponentType c = new ComponentType
        {
            header = new ComponentHeaderType
            {
                hash_component_UID = uid_abstract,
                isAbstract = true,
                isAbstractSpecified = true,
                kind = SupportedKinds.Environment,
                kindSpecified = true,
                name = name,
                packagePath = packagePath,
                baseType = new BaseTypeType()
                {
                    extensionType = new ExtensionTypeType
                    {
                        ItemElementName = ItemChoiceType.extends,
                        Item = true
                    },
                    component = new ComponentInUseType
                    {
                        localRef = "base",
                        location = "org.hpcshelf.platform.maintainer.Google/Google.hpe",
                        name = "Google",
                        package = "org.hpcshelf.platform.maintainer",
                        version = "1.0.0.0",
                        unitBounds = new UnitBoundsType[]
                        {
                            new UnitBoundsType
                            {
                                facet = 0,
                                facet_instanceSpecified = true,
                                facet_instance = 0,
                                facet_instance_enclosing = 0,
                                parallelSpecified = true,
                                parallel = true,
                                uRef = "google"
                            }
                        }
                    }
                },
                versions = new VersionType[]
                {
                    new VersionType
                    {
                        field1Specified = true, field1 = 1,
                        field2Specified = true, field2 = 0,
                        field3Specified = true, field3 = 0,
                        field4Specified = true, field4 = 0
                    }
                },
                facet_configuration = new FacetConfigurationType[]
                {
                    new FacetConfigurationType
                    {
                        facet = 0, facetSpecified = true,
                        multiple = false, multipleSpecified = true
                    }
                }
            },
            componentInfo = new object[]
            {
                new InterfaceType
                {
                    iRef = string.Format("I{0}",name),
                    nArgs =0, nArgsSpecified = true
                    //sources = new SourceType[] {}
                },

                new UnitType
                {
                    facet = 0,
                    iRef = string.Format("I{0}",name),
                    multiple = true, multipleSpecified = true,
                    @private = false, privateSpecified = false,
                    replica = 0, replicaSpecified = true,
                    uRef = "google",
                    visibleInterface = true,
                    super = new UnitRefType[1]
                    {
                        new UnitRefType
                        {
                            cRef = "base",
                            slice_replica = 0,
                            uRef = "google"
                        }

                    }
                }
            }
        };

        return c;
    }

    private ComponentType createConcreteQualifier(string qname, string backend_address)
    {
        Console.WriteLine("BACKEND ADDRESS of {1}: {0}", backend_address, qname);

        string packagePath_abstract = qname.Substring(0, qname.LastIndexOf('.'));
        string packagePath_concrete = string.Format("{0}.impl", packagePath_abstract);

        string name_abstract = qname.Substring(qname.LastIndexOf('.') + 1);
        string name_concrete = string.Format("{0}_impl", name_abstract);

        string snk_fn_concrete = string.Format("{0}.snk", name_concrete);
        string pub_fn_concrete = string.Format("{0}.pub", name_concrete);

        string uid_concrete = FileUtil.create_snk_file(snk_fn_concrete, pub_fn_concrete);

        ComponentType c = new ComponentType
        {
            header = new ComponentHeaderType
            {
                hash_component_UID = uid_concrete,
                isAbstract = false,
                isAbstractSpecified = true,
                kind = SupportedKinds.Environment,
                kindSpecified = true,
                name = name_concrete,
                packagePath = packagePath_concrete,
                baseType = new BaseTypeType()
                {
                    extensionType = new ExtensionTypeType
                    {
                        ItemElementName = ItemChoiceType.implements,
                        Item = true
                    },
                    component = new ComponentInUseType
                    {
                        localRef = "base",
                        location = string.Format("{0}.{1}/{1}.hpe", packagePath_abstract, name_abstract),
                        name = name_abstract,
                        package = packagePath_abstract,
                        version = "1.0.0.0",
                        unitBounds = new UnitBoundsType[]
                        {
                            new UnitBoundsType
                            {
                                facet = 0,
                                facet_instanceSpecified = true,
                                facet_instance = 0,
                                facet_instance_enclosing = 0,
                                parallelSpecified = true,
                                parallel = true,
                                uRef = name_abstract.ToLower()
                            }
                        }
                    }
                },
                versions = new VersionType[]
                {
                    new VersionType
                    {
                        field1Specified = true, field1 = 1,
                        field2Specified = true, field2 = 0,
                        field3Specified = true, field3 = 0,
                        field4Specified = true, field4 = 0
                    }
                },
                facet_configuration = new FacetConfigurationType[]
                {
                    new FacetConfigurationType
                    {
                        facet = 0, facetSpecified = true,
                        multiple = false, multipleSpecified = true
                    }
                }
            },
            componentInfo = new object[]
            {
                new InterfaceType
                {
                    iRef = string.Format("I{0}", name_concrete),
                    nArgs =0, nArgsSpecified = true,
                    sources = new SourceType[]
                    {
                        new SourceType
                        {
                            deployType = "platform.settings",
                            moduleName = "platform.settings",
                            sourceType = "platform.settings",
                            file = new SourceFileType[]
                            {
                                new SourceFileType
                                {
                                    contents = backend_address,
                                    fileType = "platform.settings",
                                    name = "platform.settings",
                                    srcType = "platform.settings"
                                }
                            }
                        }
                    }
                },

                new UnitType
                {
                    facet = 0,
                    iRef = string.Format("I{0}", name_concrete),
                    multiple = true, multipleSpecified = true,
                    @private = false, privateSpecified = false,
                    replica = 0, replicaSpecified = true,
                    uRef = "google",
                    visibleInterface = true,
                    super = new UnitRefType[1]
                    {
                        new UnitRefType
                        {
                            cRef = "base",
                            slice_replica = 0,
                            uRef = "google"
                        }

                    }
                }
            }

        };

        return c;
    }

}
