/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.europe.Sweden;

namespace org.hpcshelf.platform.locale.europe.Stockholm
{
	public interface BaseIStockholm : BaseISweden, IQualifierKind 
	{
	}
}