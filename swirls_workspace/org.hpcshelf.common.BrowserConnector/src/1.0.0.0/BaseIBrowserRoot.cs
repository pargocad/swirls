/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.common.BrowserBinding;
using org.hpcshelf.common.BrowserPortType;

namespace org.hpcshelf.common.BrowserConnector
{
	public interface BaseIBrowserRoot<S0> : ISynchronizerKind 
		where S0:IBrowserPortType
	{
		IBrowserBinding<S0> Browse_port {get;}
	}
}