/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.common.BrowserBinding;
using org.hpcshelf.common.BrowserConnector;
using org.hpcshelf.common.browser.RecvDataPortType;

namespace org.hpcshelf.mpi.wrapper.WBrowserConnector
{
    public interface BaseIWBrowserRoot : BaseIBrowserRoot<org.hpcshelf.common.browser.RecvDataPortType.IRecvDataPortType>, ISynchronizerKind 
	{
		new IBrowserBinding<IRecvDataPortType> Browse_port {get;}
	}
}