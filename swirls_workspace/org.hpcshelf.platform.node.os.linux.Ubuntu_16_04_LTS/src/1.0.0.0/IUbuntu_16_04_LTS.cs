using org.hpcshelf.platform.node.os.linux.Ubuntu;

namespace org.hpcshelf.platform.node.os.linux.Ubuntu_16_04_LTS
{
	public interface IUbuntu_16_04_LTS : BaseIUbuntu_16_04_LTS, IUbuntu<org.hpcshelf.platform.node.os.linux.ubuntu.codename.XenialXerus.IXenialXerius, org.hpcshelf.quantifier.IntUp.IntUp, org.hpcshelf.quantifier.IntUp.IntUp>
	{
	}
}