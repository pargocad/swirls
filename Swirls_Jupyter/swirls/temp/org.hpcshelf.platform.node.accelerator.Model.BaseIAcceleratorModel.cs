/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.accelerator.Manufacturer;
using org.hpcshelf.platform.node.accelerator.Type;
using org.hpcshelf.platform.node.accelerator.Architecture;

namespace org.hpcshelf.platform.node.accelerator.Model
{
	public interface BaseIAcceleratorModel<MAN, TYPE, ARC> : IQualifierKind 
		where MAN:IAcceleratorManufacturer
		where TYPE:IAcceleratorType<MAN>
		where ARC:IAcceleratorArchitecture
	{
	}
}