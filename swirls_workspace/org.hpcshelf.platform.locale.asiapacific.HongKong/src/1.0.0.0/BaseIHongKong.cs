/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.HongKong
{
	public interface BaseIHongKong : BaseIAsiaPacific, IQualifierKind 
	{
	}
}