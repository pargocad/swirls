
**Swirls** is a general-purpose **HPC Shelf** application for interactive building and offloading deployment/execution of parallel computing systems, using either [Jupyter](http://www.jupyter.org) notebooks or a direct command-line interface (CLI).


The current prototype of **Swirls** supports a component framework that makes it possible to run _C_, _C#_, _C++_, and _Python_ [MPI](https://www.mpi-forum.org) programs across virtual platforms deployed (offloading) at [Amazon's AWS EC2](https://aws.amazon.com/pt/ec2) and [Google Cloud Platform](https://cloud.google.com/), as well as, by default, the local computer of the user (useful for prototyping). 

Currently, users have two alternatives to use **Swirls**: 

* By installing Swirls in their own computers;

* By using the last version of the Swirls image (AMI) at AWS EC2.

You can find instructions on how to install and use **Swirls** by using these approaches in [Swirls Install & Usage](https://docs.google.com/document/d/1Vib037goMkItPo1elAvtIdNHolxTZm1gtmABL3x-ncM/edit?usp=sharing).