using org.hpcshelf.blas.configure.UPLO;

namespace org.hpcshelf.blas.configure.uplo.L
{
	public interface L : BaseL, IUPLO
	{
	}
}