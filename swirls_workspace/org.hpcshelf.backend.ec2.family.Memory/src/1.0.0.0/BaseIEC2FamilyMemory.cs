/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;

namespace org.hpcshelf.backend.ec2.family.Memory
{
	public interface BaseIEC2FamilyMemory : BaseIEC2Family, IQualifierKind 
	{
	}
}