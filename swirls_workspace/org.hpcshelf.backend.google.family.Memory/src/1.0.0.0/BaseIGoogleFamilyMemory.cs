/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.family.Family;

namespace org.hpcshelf.backend.google.family.Memory
{
 public interface BaseIGoogleFamilyMemory : BaseIGoogleFamily, IQualifierKind 
 {
 }
}