using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_16Way
{
	public interface ISetAssociative_16Way : BaseISetAssociative_16Way, ICacheMappingSetAssociative<org.hpcshelf.quantifier.IntUp.IntUp>
	{
	}
}