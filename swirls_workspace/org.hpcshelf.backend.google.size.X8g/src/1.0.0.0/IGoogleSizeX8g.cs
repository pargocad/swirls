using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X8g
{
 public interface IGoogleSizeX8g : BaseIGoogleSizeX8g, IGoogleSize
 {
 }
}