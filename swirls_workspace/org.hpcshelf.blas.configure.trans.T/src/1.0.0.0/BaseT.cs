/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.configure.TRANS;

namespace org.hpcshelf.blas.configure.trans.T
{
	public interface BaseT : BaseITRANS, IQualifierKind 
	{
	}
}