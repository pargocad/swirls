using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.interconnection.Topology;
using org.hpcshelf.platform.Interconnection;

namespace org.hpcshelf.backend.ec2.EC2Interconnection
{
	public interface IEC2Interconnection<STT, BAN, TOP, NLT> : BaseIEC2Interconnection<STT, BAN, TOP, NLT>, IInterconnection<STT, NLT, BAN, TOP>
		where STT:IntDown
		where BAN:IntUp
		where TOP:ITopology
		where NLT:IntDown
	{
	}
}