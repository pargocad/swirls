using org.hpcshelf.types.Data;
using org.hpcshelf.blas.data.DataDistribution;

namespace org.hpcshelf.blas.MatrixConnector
{
	public interface IOutput<T_GET, DIST_GET> : BaseIOutput<T_GET, DIST_GET>
		where T_GET:IData
		where DIST_GET:IDataDistribution
	{
	}
}