using org.hpcshelf.platform.node.processor.Series;

namespace org.hpcshelf.platform.node.processor.series.intel.Xeon_E7_8000
{
	public interface IXeonE78000 : BaseIXeonE78000, IProcessorSeries<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, org.hpcshelf.platform.node.processor.family.intel.Xeon_E7.IXeonE7>
	{
	}
}