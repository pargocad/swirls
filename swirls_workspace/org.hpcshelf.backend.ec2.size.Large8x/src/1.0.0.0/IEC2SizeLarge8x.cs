using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large8x
{
	public interface IEC2SizeLarge8x : BaseIEC2SizeLarge8x, IEC2Size
	{
	}
}