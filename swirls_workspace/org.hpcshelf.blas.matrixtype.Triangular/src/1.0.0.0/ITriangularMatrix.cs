using org.hpcshelf.blas.matrixtype.General;

namespace org.hpcshelf.blas.matrixtype.Triangular
{
	public interface ITriangularMatrix : BaseITriangularMatrix, IGeneralMatrix
	{
	}
}