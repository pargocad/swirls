﻿using System;
using System.IO;
using org.hpcshelf.command;

public class LocalCoreLauncher : ICoreLauncher
{
    private string core_address = "<pending>";
    public string CoreAddress { get { return core_address; } }

    private string SWIRLS_HOME
    {
        get
        {
            string path = Environment.GetEnvironmentVariable("Swirls_HOME");
            if (path == null)
            {
                Console.WriteLine("Swirls_HOME not set. Using the current directory.");
                path = Utils.CURRENT_FOLDER;
            }
            return path;
        }
    }

    public LocalCoreLauncher()
    {

    }

    public string launch(string region_id)
    {
        string script_command = Path.Combine(SWIRLS_HOME, "run_core_services_local.sh");

        Utils.commandExecBash(string.Format("{0}", script_command));

        core_address = "127.0.0.1";
        return core_address;
    }

    public void stop()
    {
        throw new Exception("The operatons 'start' and 'stop' do not apply to local core services");
    }

    public string start()
    {
        throw new Exception("The operatons 'start' and 'stop' do not apply to local core services");
    }

    public void terminate()
    {

    }
}

