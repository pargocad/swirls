using org.hpcshelf.platform.locale.europe.NorthEurope;

namespace org.hpcshelf.platform.locale.europe.Sweden
{
	public interface ISweden : BaseISweden, INorthEurope
	{
	}
}