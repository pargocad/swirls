using System;
using org.hpcshelf.binding.channel.Binding;
using MPI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;
using DGAC.Memory;
using System.Threading;
using System.Collections.Generic;

namespace org.hpcshelf.binding.channel.impl.BindingImpl
{
    public class IChannelImpl : BaseIChannelImpl, IChannel
	{
		[DllImport("native_channel.so", EntryPoint = "HPCShelfSend")]
		public static extern int HPCShelfSend_(int channel_key, IntPtr buf, int count, int datatype, int facet, int target, int tag);

		[DllImport("native_channel.so", EntryPoint = "HPCShelfRecv")]
		public static extern int HPCShelfRecv_(int channel_key, IntPtr buf, int count, int datatype, int facet, int source, int tag, IntPtr status);

		[DllImport("native_channel.so", EntryPoint = "registerChannel")]
		public static extern int registerChannel(int comm);

 	    public TypeOfChannelSend HPCShelfSend { get { return HPCShelfSend_; } }
	    public TypeOfChannelRecv HPCShelfRecv { get { return HPCShelfRecv_; } }	    
	   
		public const int TAG_SEND_OPERATION = 999;
		public const int TAG_REPLY = 998;

        private int channel_key = -1;
        
        public int ChannelKey { get { return channel_key; } }

		public override void after_initialize()
		{
			this.TraceFlag = true;		
			channel_key = registerChannel(this.RootCommunicator.comm);	
		}

		private static readonly object sync = new Object();

        private static int next_conversation_tag = 100;

		private static int takeNextConversationTag()
		{
			lock (sync)
			{
				next_conversation_tag++;
				switch (next_conversation_tag)
				{
					case TAG_SEND_OPERATION:
						next_conversation_tag++;
						break;
					case int.MaxValue:
						next_conversation_tag = 100;
						break;
				}
			}
			return /*100*this.PeerRank +*/ next_conversation_tag;
		}


		// AlienCommunicator

		public const int Root = default(int);
		public const int Null = default(int);

		public int RemoteSize
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		#region Point-to-Point implementation

		// Send

		public void Send<T>(T value, Tuple<int, int> dest, int tag)
		{
			int conversation_tag = takeNextConversationTag();
			Trace.WriteLineIf(this.TraceFlag == true, "CHECKING " + this.RootCommunicator.Rank + "," + dest.Item2 + ", tag=" + tag + ", conversation_tag=" + conversation_tag);

			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 1a - BEGIN SEND TO <" + dest.Item1 + "," + dest.Item2 + "> : " + TAG_SEND_OPERATION);
			/* lock (lock_mpi) */
			// Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.SEND, tag), 0, TAG_SEND_OPERATION);
            Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.SEND, conversation_tag), TAG_SEND_OPERATION);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 2a - BEGIN SEND TO <" + dest.Item1 + "," + dest.Item2 + "> : " + tag);

			byte[] value_packet = ObjectToByteArray(value);

			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int, int, byte[]>(dest.Item1, dest.Item2, tag, value_packet), conversation_tag);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 3a - END SEND TO <" + dest.Item1 + "," + dest.Item2 + ">");
		}

        public Binding.IRequest ImmediateSend<T>(T value, Tuple<int, int> dest, int tag)
		{
			int conversation_tag = takeNextConversationTag();
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": CHECKING " + this.RootCommunicator.Rank + "," + dest.Item2 + ", tag=" + tag);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 1b - BEGIN SEND TO <" + dest.Item1 + "," + dest.Item2 + "> : " + TAG_SEND_OPERATION);
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.SEND, conversation_tag), TAG_SEND_OPERATION);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 2b - BEGIN SEND TO <" + dest.Item1 + "," + dest.Item2 + "> : " + tag);
			byte[] value_packet = ObjectToByteArray(value);
			int root_request = ImmediateSend(this.RootCommunicator, new Tuple<int, int, int, byte[]>(dest.Item1, dest.Item2, tag, value_packet), conversation_tag);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 3b - END SEND TO <" + dest.Item1 + "," + dest.Item2 + ">");
            return MPIRequest.createRequest(root_request, dest);
		}

        public Binding.IRequest ImmediateSyncSend<T>(T value, Tuple<int, int> dest, int tag)
		{
			int conversation_tag = takeNextConversationTag();
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": CHECKING " + this.RootCommunicator.Rank + "," + dest.Item2 + ", tag=" + tag);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 1 - BEGIN SEND TO <" + dest.Item1 + "," + dest.Item2 + "> : " + TAG_SEND_OPERATION);
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.SYNC_SEND, conversation_tag), TAG_SEND_OPERATION);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 2 - BEGIN SEND TO <" + dest.Item1 + "," + dest.Item2 + "> : " + tag);
			byte[] value_packet = ObjectToByteArray(value);
			Send(this.RootCommunicator, new Tuple<int, int, int, byte[]>(dest.Item1, dest.Item2, tag, value_packet), conversation_tag);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 3 - END SEND TO <" + dest.Item1 + "," + dest.Item2 + ">");

			int root_request = ImmediateReceive(this.RootCommunicator, 0, TAG_REPLY);

            return MPIRequest.createRequest(root_request, dest);
		}

		// Send Array

		public void Send<T>(T[] values, Tuple<int, int> dest, int tag)
		{
			int conversation_tag = takeNextConversationTag();
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 1 - BEGIN CSEND TO <" + dest.Item1 + "," + dest.Item2 + "> : " + TAG_SEND_OPERATION);
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.SEND_ARRAY, conversation_tag), TAG_SEND_OPERATION);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 2 - BEGIN CSEND TO <" + dest.Item1 + "," + dest.Item2 + "> : " + tag);
			byte[] value_packet = ObjectToByteArray(values);
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int, int, byte[]>(dest.Item1, dest.Item2, tag, value_packet), conversation_tag);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 3 - END CSEND TO <" + dest.Item1 + "," + dest.Item2 + ">");
		}

        public Binding.IRequest ImmediateSend<T>(T[] values, Tuple<int, int> dest, int tag)
		{
			//int conversation_tag = takeNextConversationTag(); 
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 1 - BEGIN ISEND TO <" + dest.Item1 + "," + dest.Item2 + "> : " + TAG_SEND_OPERATION);
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.SEND_ARRAY, tag), TAG_SEND_OPERATION);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 2 - BEGIN ISEND TO <" + dest.Item1 + "," + dest.Item2 + "> : " + tag);
			byte[] value_packet = ObjectToByteArray(values);
			int root_request = ImmediateSend(this.RootCommunicator, new Tuple<int, int, int, byte[]>(dest.Item1, dest.Item2, tag, value_packet), tag);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 3 - END ISEND TO <" + dest.Item1 + "," + dest.Item2 + ">");
            return MPIRequest.createRequest(root_request, dest);
		}

		// Receive

		public T Receive<T>(Tuple<int, int> source, int tag)
		{
			T result;
			Receive<T>(source, tag, out result);
			return result;
		}

		public void Receive<T>(Tuple<int, int> source, int tag, out T value)
		{
            Binding.CompletedStatus status;
			Receive<T>(source, tag, out value, out status);
		}

		//private object lock_mpi = new object();

		public void Receive<T>(Tuple<int, int> source, int tag, out T value, out Binding.CompletedStatus status)
		{
		    byte[] v;
		    ReceiveNative(source, tag, out v, out status);
			value = (T)ByteArrayToObject(v);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 5 - END CRECV FROM <" + source.Item1 + "," + source.Item2 + ">: " + tag);
		}

        public Binding.IReceiveRequest ImmediateReceive<T>(Tuple<int, int> source, int tag)
		{
			int conversation_tag = takeNextConversationTag();
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 1 - BEGIN IRECV FROM <" + source.Item1 + "," + source.Item2 + "> : " + TAG_SEND_OPERATION);
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.RECEIVE, conversation_tag), TAG_SEND_OPERATION);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 2 - BEGIN IRECV FROM <" + source.Item1 + "," + source.Item2 + "> : " + conversation_tag);
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int, int>(source.Item1, source.Item2, tag), conversation_tag);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 3 - BEGIN IRECV FROM <" + source.Item1 + "," + source.Item2 + "> : " + tag);
            // byte[] v = new byte[0];
            // MPI.ReceiveRequest root_request = ImmediateReceive(this.RootCommunicator, 0, tag < 0 ? tag : conversation_tag,  v);
            IntPtr buf;
            //Tuple<int,int> t = ImmediateReceive(this.RootCommunicator, 0, tag < 0 ? tag : conversation_tag, out buf);
            //int root_request = t.Item1;
            //int bufsize = t.Item2;
            int root_request = ImmediateReceive(this.RootCommunicator, 0, tag < 0 ? tag : conversation_tag, out buf);
            Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 4 - END IRECV FROM <" + source.Item1 + "," + source.Item2 + ">");
            return MPIValueReceiveRequest<T>.createRequest(root_request, source, buf, bufsize, this.RootCommunicator.comm);
		}

        // Receive Native
        
       	public byte[] ReceiveNative(Tuple<int, int> source, int tag)
		{
			byte[] result;
			ReceiveNative(source, tag, out result);
			return result;
		}
        
		public void ReceiveNative(Tuple<int, int> source, int tag, out byte[] value)
		{
            Binding.CompletedStatus status;
			ReceiveNative(source, tag, out value, out status);
		}

		public void ReceiveNative(Tuple<int, int> source, int tag, out byte[] value, out Binding.CompletedStatus status)
		{
			int conversation_tag = takeNextConversationTag();
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 1 - BEGIN CRECV FROM <" + source.Item1 + "," + source.Item2 + "> : " + TAG_SEND_OPERATION);
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.RECEIVE, conversation_tag), TAG_SEND_OPERATION);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 2 - BEGIN CRECV FROM <" + source.Item1 + "," + source.Item2 + "> : " + conversation_tag);
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int, int>(source.Item1, source.Item2, tag), conversation_tag);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 3 - BEGIN CRECV FROM <" + source.Item1 + "," + source.Item2 + "> : " + tag);
            Unsafe.MPI_Status status_root;

			Receive(this.RootCommunicator, 0, tag < 0 ? tag : conversation_tag, out value, out status_root);
            Console.WriteLine("{4}/{5}: BYTE ARRAY LENGTH is {0} --- <{1},{2},{3}>", value.Length, source.Item1, source.Item2, tag, this.FacetInstance, this.Rank);
            Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 4 - END CRECV FROM: " + value.GetType() + " <" + source.Item1 + "," + source.Item2 + "> : " + tag);
            
			tag = tag < 0 ? status_root.MPI_TAG : tag;

			status = MPICompletedStatus.createStatus(status_root, source, tag);
		}

		// Receive Array

		public void Receive<T>(Tuple<int, int> source, int tag, ref T[] values)
		{
			org.hpcshelf.binding.channel.Binding.CompletedStatus status;
			Receive(source, tag, ref values, out status);
		}

		public void Receive<T>(Tuple<int, int> source, int tag, ref T[] values, out org.hpcshelf.binding.channel.Binding.CompletedStatus status)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.RECEIVE_ARRAY, conversation_tag), TAG_SEND_OPERATION);
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int, int>(source.Item1, source.Item2, tag), conversation_tag);
			Unsafe.MPI_Status status_root;

            // int size_buffer = this.RootCommunicator.Receive<int>(0, tag < 0 ? tag : conversation_tag);
            byte[] v; // = new byte[size_buffer];
			/* lock (lock_mpi) */
			Receive(this.RootCommunicator, 0, tag < 0 ? tag : conversation_tag, out v, out status_root);
			T[] values_ = (T[])ByteArrayToObject(v);

			// Copy the received values to the destination array (forcing original MPI semantics)
			int size = values.Length <= values_.Length ? values.Length : values_.Length;
			for (int i = 0; i < size; i++)
				values[i] = values_[i];

            tag = tag < 0 ? status_root.MPI_TAG : tag;

            status = MPICompletedStatus.createStatus(status_root, source, tag);

		}

        public Binding.IReceiveRequest ImmediateReceive<T>(Tuple<int, int> source, int tag, T[] values)
		{
			int conversation_tag = takeNextConversationTag();
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 1 - BEGIN IARECV FROM <" + source.Item1 + "," + source.Item2 + "> : " + TAG_SEND_OPERATION);
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.RECEIVE_ARRAY, conversation_tag), TAG_SEND_OPERATION);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 2 - BEGIN IARECV FROM <" + source.Item1 + "," + source.Item2 + "> : " + conversation_tag);
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int, int>(source.Item1, source.Item2, tag), conversation_tag);
			Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 3 - BEGIN IARECV FROM <" + source.Item1 + "," + source.Item2 + "> : " + tag);

            IntPtr buf;
            //Tuple<int,int> t = ImmediateReceive(this.RootCommunicator, 0, tag < 0 ? tag : conversation_tag, out buf);
            //int root_request = t.Item1;
            //int bufsize = t.Item2;
            int root_request = ImmediateReceive(this.RootCommunicator, 0, tag < 0 ? tag : conversation_tag, out buf);

           //MPI.ReceiveRequest root_request = new ValueArrayReceiveRequest<byte>(root_comm.comm, source, tag, result);

            Trace.WriteLineIf(this.TraceFlag == true, this.PeerRank + ": 4 - END IARECV FROM <" + source.Item1 + "," + source.Item2 + ">");
            return MPIArrayReceiveRequest<T>.createRequest(root_request, source, values, buf, bufsize, this.RootCommunicator.comm);
		}

        // Probe

        public Binding.Status Probe(Tuple<int, int> source, int tag)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.PROBE, conversation_tag), TAG_SEND_OPERATION);
			throw new NotImplementedException();
		}

        public Binding.Status ImmediateProbe(Tuple<int, int> source, int tag)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.PROBE, conversation_tag), TAG_SEND_OPERATION);
			throw new NotImplementedException();
		}

		#endregion

		#region Collective implementation


		// All Gather

		public T[] Allgather<T>(int facet, T value)
		{
			T[] result = null;
			Allgather(facet, value, ref result);
			return result;
		}

		public void Allgather<T>(int facet, T inValue, ref T[] outValues)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.ALL_GATHER, conversation_tag), TAG_SEND_OPERATION);
			throw new NotImplementedException();
		}

		// All Gather Flattened

		public void AllgatherFlattened<T>(int facet, T[] inValues, int count, ref T[] outValues)
		{
			int[] counts = new int[Size];
			for (int i = 0; i < Size; i++)
				counts[i] = count;
			AllgatherFlattened(facet, inValues, counts, ref outValues);
		}

		public void AllgatherFlattened<T>(int facet, T[] inValues, int[] counts, ref T[] outValues)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.ALL_GATHER_FLATTENED, conversation_tag), TAG_SEND_OPERATION);
			throw new NotImplementedException();
		}

		// All Reduce

		public T Allreduce<T>(int facet, T value, ReductionOperation<T> op)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.ALL_REDUCE, conversation_tag), TAG_SEND_OPERATION + 0);
			throw new NotImplementedException();
		}

		// All Reduce Array

		public T[] Allreduce<T>(int facet, T[] values, ReductionOperation<T> op)
		{
			T[] result = null;
			Allreduce(facet, values, op, ref result);
			return result;
		}

		public void Allreduce<T>(int facet, T[] inValues, ReductionOperation<T> op, ref T[] outValues)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.ALL_REDUCE_ARRAY, conversation_tag), TAG_SEND_OPERATION + 0);
			throw new NotImplementedException();
		}

		// AllToAll

		public T[] Alltoall<T>(int facet, T[] values)
		{
			T[] result = null;
			Alltoall(facet, values, ref result);
			return result;
		}

		public void Alltoall<T>(int facet, T[] inValues, ref T[] outValues)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.ALL_TO_ALL, conversation_tag), TAG_SEND_OPERATION);
			throw new NotImplementedException();
		}

		// All-to-All Flattened

		public void AlltoallFlattened<T>(int facet, T[] inValues, int[] sendCounts, int[] recvCounts, ref T[] outValues)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.ALL_TO_ALL_FLATTENED, conversation_tag), TAG_SEND_OPERATION + 0);
			throw new NotImplementedException();
		}

		// Reduce-Scatter

		public T[] ReduceScatter<T>(int facet, T[] values, ReductionOperation<T> op, int[] counts)
		{
			T[] result = null;
			ReduceScatter(facet, values, op, counts, ref result);
			return result;
		}

		public void ReduceScatter<T>(int facet, T[] inValues, ReductionOperation<T> op, int[] counts, ref T[] outValues)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.REDUCE_SCATTER, conversation_tag), TAG_SEND_OPERATION + 0);
			throw new NotImplementedException();
		}

		// BROADCAST

		public void Broadcast<T>(int facet, ref T value, int root)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.BROADCAST, conversation_tag), TAG_SEND_OPERATION + 0);
			throw new NotImplementedException();
		}

		public void Broadcast<T>(int facet, ref T[] values, int root)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.BROADCAST_ARRAY, conversation_tag), TAG_SEND_OPERATION);
			throw new NotImplementedException();
		}

		// Scatter

		public void Scatter<T>(int facet, T[] values)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.SCATTER, conversation_tag), TAG_SEND_OPERATION);
			throw new NotImplementedException();
		}

		public T Scatter<T>(int facet, int root)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.SCATTER, conversation_tag), TAG_SEND_OPERATION);
			throw new NotImplementedException();
		}

		public void Scatter<T>(int facet)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.SCATTER, conversation_tag), TAG_SEND_OPERATION);
			throw new NotImplementedException();
		}

		// Scatter from Flattened

		public void ScatterFromFlattened<T>(int facet, T[] inValues, int count)
		{
			T[] temp = new T[0];
			ScatterFromFlattened<T>(facet, inValues, count, Root, ref temp);
		}

		public void ScatterFromFlattened<T>(int facet, T[] inValues, int[] counts)
		{
			T[] temp = new T[0];
			ScatterFromFlattened<T>(facet, inValues, counts, Root, ref temp);
		}

		public void ScatterFromFlattened<T>(int facet, int count, int root, ref T[] outValues)
		{
			ScatterFromFlattened<T>(facet, null, count, root, ref outValues);
		}

		public void ScatterFromFlattened<T>(int facet, int[] counts, int root, ref T[] outValues)
		{
			ScatterFromFlattened<T>(facet, null, counts, root, ref outValues);
		}

		public void ScatterFromFlattened<T>(int facet)
		{
			T[] temp = new T[0];
			ScatterFromFlattened<T>(facet, null, new int[0], Null, ref temp);
		}

		public void ScatterFromFlattened<T>(int facet, T[] inValues, int count, int root, ref T[] outValues)
		{
			int[] counts = new int[Size];
			for (int i = 0; i < Size; i++)
				counts[i] = count;
			ScatterFromFlattened<T>(facet, inValues, counts, root, ref outValues);
		}

		public void ScatterFromFlattened<T>(int facet, T[] inValues, int[] counts, int root, ref T[] outValues)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.SCATTER_FROM_FLATTENED, conversation_tag), TAG_SEND_OPERATION + 0);
			throw new NotImplementedException();
		}

		// Gather

		public T[] Gather<T>(int facet, T value, int root)
		{
			T[] result = null;
			Gather(facet, value, root, ref result);
			return result;
		}

		public T[] Gather<T>(int facet, int root)
		{
			T value = default(T);
			T[] result = null;
			Gather(facet, value, Root, ref result);
			return result;
		}

		public void Gather<T>(int facet)
		{
			T value = default(T);
			T[] result = null;
			Gather(facet, value, Null, ref result);
		}

		public void Gather<T>(int facet, T inValue, int root, ref T[] outValues)
		{
			Gather_impl<T>(facet, (root == Root), RemoteSize, inValue, root, ref outValues);
		}

		internal void Gather_impl<T>(int facet, bool isRoot, int size, T inValue, int root, ref T[] outValues)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.GATHER, conversation_tag), TAG_SEND_OPERATION + 0);
			throw new NotImplementedException();
		}

		// Gather Flattened

		public void GatherFlattened<T>(int facet, int count, ref T[] outValues)
		{
			if (outValues == null || outValues.Length != count * RemoteSize)
				outValues = new T[count * RemoteSize];


			int[] counts = new int[Size];
			for (int i = 0; i < Size; i++)
				counts[i] = count;

			GatherFlattened_impl<T>(facet, true, RemoteSize, new T[0], counts, Root, ref outValues);
		}

		public T[] GatherFlattened<T>(int facet, int count)
		{
			T[] outValues = new T[RemoteSize * count];

			int[] counts = new int[Size];
			for (int i = 0; i < Size; i++)
				counts[i] = count;

			GatherFlattened_impl<T>(facet, true, RemoteSize, new T[0], counts, Root, ref outValues);

			return outValues;
		}

		public void GatherFlattened<T>(int facet, T[] inValues, int root)
		{
			T[] temp = new T[0];
			GatherFlattened_impl<T>(facet, false, Size, inValues, new int[0], root, ref temp);
		}

		public void GatherFlattened<T>(int facet)
		{
			T[] temp = new T[0];
			GatherFlattened_impl<T>(facet, false, RemoteSize, new T[0], new int[0], Null, ref temp);
		}

		public void GatherFlattened<T>(int facet, int[] counts, ref T[] outValues)
		{
			int totalCounts = 0;
			for (int i = 0; i < counts.Length; i++)
				totalCounts += counts[i];

			if (outValues == null || outValues.Length != totalCounts * RemoteSize)
				outValues = new T[totalCounts * RemoteSize];

			GatherFlattened_impl<T>(facet, true, RemoteSize, new T[0], counts, Root, ref outValues);
		}

		public T[] GatherFlattened<T>(int facet, int[] counts)
		{
			int totalCounts = 0;
			for (int i = 0; i < counts.Length; i++)
				totalCounts += counts[i];
			T[] outValues = new T[totalCounts];

			GatherFlattened_impl<T>(facet, true, RemoteSize, new T[0], counts, Root, ref outValues);

			return outValues;

		}

		internal void GatherFlattened_impl<T>(int facet, bool isRoot, int size, T[] inValues, int[] counts, int root, ref T[] outValues)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.GATHER_FLATTENED, conversation_tag), TAG_SEND_OPERATION + 0);
			throw new NotImplementedException();
		}

		// Reduce

		public T Reduce<T>(int facet, T value, ReductionOperation<T> op, int root)
		{
			return Reduce_impl<T>(facet, (root == Root), RemoteSize, value, op, root);
		}

		internal T Reduce_impl<T>(int facet, bool isRoot, int size, T value, ReductionOperation<T> op, int root)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.REDUCE, conversation_tag), TAG_SEND_OPERATION + 0);
			throw new NotImplementedException();
		}

		// Reduce Array

		public T[] Reduce<T>(int facet, T[] values, ReductionOperation<T> op, int root)
		{
			T[] result = null;
			Reduce(facet, values, op, root, ref result);
			return result;
		}

		public void Reduce<T>(int facet, T[] inValues, ReductionOperation<T> op, int root, ref T[] outValues)
		{
			int conversation_tag = takeNextConversationTag();
			/* lock (lock_mpi) */
			Send(this.RootCommunicator, new Tuple<int, int>(AliencommunicatorOperation.REDUCE_ARRAY, conversation_tag), TAG_SEND_OPERATION + 0);
			throw new NotImplementedException();
		}

		#endregion

		/*
        public void SendReceive<T> (T inValue, int dest, int tag, out T outValue)
        {
            CompletedStatus status;
            SendReceive(inValue, dest, tag, dest, tag, out outValue, out status);
        }

        public void SendReceive<T> (T inValue, int dest, int sendTag, int source, int recvTag, out T outValue)
        {
            CompletedStatus status;
            SendReceive(inValue, dest, sendTag, source, recvTag, out outValue, out status);
        }

        public void SendReceive<T> (T inValue, int dest, int sendTag, int source, int recvTag, out T outValue, out CompletedStatus status)
        {
            throw new NotImplementedException ();
        }

        public void SendReceive<T> (T[] inValues, int dest, int tag, ref T[] outValues)
        {
            CompletedStatus status;
            SendReceive(inValues, dest, tag, dest, tag, ref outValues, out status);
        }

        public void SendReceive<T> (T[] inValues, int dest, int sendTag, int source, int recvTag, ref T[] outValues)
        {
            CompletedStatus status;
            SendReceive(inValues, dest, sendTag, source, recvTag, ref outValues, out status);
        }

        public void SendReceive<T> (T[] inValues, int dest, int sendTag, int source, int recvTag, ref T[] outValues, out CompletedStatus status)
        {
            throw new NotImplementedException ();
        }
       */


        protected void Send(Intercommunicator root_comm, Tuple<int, int> v, int tag)
        {
            root_comm.Send<int>(new int[] { v.Item1, v.Item2 }, 0, tag);

            //root_comm.Send<Tuple<int, int>>(new Tuple<int, int>(v.Item1, v.Item2), 0, tag);
        }

        protected void Send(Intercommunicator root_comm, Tuple<int,int,int> v, int tag)
        {
            root_comm.Send<int>(new int[] { v.Item1, v.Item2, v.Item3 }, 0, tag);

            //   root_comm.Send<Tuple<int, int, int>>(new Tuple<int, int, int>(v.Item1, v.Item2, v.Item3), 0, tag);

        }

        private int errorCode = default(int);

        protected unsafe void Send(Intercommunicator root_comm, Tuple<int, int, int, byte[]> v, int tag)
        {
           try
           {
	            int v1 = v.Item1;
	            int v2 = v.Item2;
	            int v3 = v.Item3;
	            byte[] v4 = v.Item4;
	            int v4size = v4.Length;
	
	            Console.WriteLine("{2}: SEND 1 {0} {1} conversation_tag={3}", v, root_comm.comm, root_comm.Rank, tag);
	
	            int outbuf_size = 4 * sizeof(int) + sizeof(byte) * v4.Length;
	            IntPtr outbuf = Marshal.AllocCoTaskMem(outbuf_size); 
	
	            Console.WriteLine("{3}: SEND 2 {0} {1} {2}", v, v4.Length, root_comm.comm, root_comm.Rank);
	
	            int position = 0;
	            errorCode = Unsafe.MPI_Pack((IntPtr) (&v1) /*new IntPtr(&v1)*/, 1, Unsafe.MPI_INT, outbuf, outbuf_size, ref position, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
	            errorCode = Unsafe.MPI_Pack((IntPtr) (&v2) /*new IntPtr(&v2)*/, 1, Unsafe.MPI_INT, outbuf, outbuf_size, ref position, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
	            errorCode = Unsafe.MPI_Pack((IntPtr) (&v3) /*new IntPtr(&v3)*/, 1, Unsafe.MPI_INT, outbuf, outbuf_size, ref position, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
	            errorCode = Unsafe.MPI_Pack((IntPtr) (&v4size) /*new IntPtr(&v4size)*/, 1, Unsafe.MPI_INT, outbuf, outbuf_size, ref position, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
	            fixed (byte* b = &v4[0])
	            {
	                IntPtr pointer = (IntPtr) b; // new IntPtr(b);
	                Unsafe.MPI_Pack(pointer, /*sizeof(int) +*/ v4.Length, Unsafe.MPI_BYTE, outbuf, outbuf_size, ref position, root_comm.comm);
	                Console.WriteLine("{2}: SEND 3 {0} --- {1}", v, Marshal.ReadInt32(pointer), root_comm.Rank);
	            }
	
	          //  string file_name = string.Format("data.{0}.{1}.{2}.{3}.CS", v1, v2, v3, v4size);
	          //  if (!File.Exists(file_name))
	          //  {
	          //      Console.WriteLine("writing {0} ! ", file_name);
	          //      File.WriteAllBytes(file_name, v4);
	          //  }
	          //  else
	          //      Console.WriteLine("{0} exists !", file_name);
	
	            errorCode = Unsafe.MPI_Send((IntPtr) (&position) /*new IntPtr(&position)*/, 1, Unsafe.MPI_INT, 0, tag, root_comm.comm);
	            if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
	            
	            Console.WriteLine("{2}: SEND 4 {0} position={1}", v, position, root_comm.Rank);
	            
	            errorCode = Unsafe.MPI_Send(outbuf, position, Unsafe.MPI_PACKED, 0, tag, root_comm.comm);
	            if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
	
	            Console.WriteLine("{1}: SEND 5 {0}", v, root_comm.Rank);
	            
	            Marshal.FreeCoTaskMem(outbuf);
           }
	       catch (Exception e)
           {
                Console.WriteLine("{1}: EXCEPTION (IChannelImpl/Send): message={0}", e.Message, root_comm.Rank);
                Console.WriteLine("{1}: EXCEPTION (IChannelImpl/Send): stack={0}", e.StackTrace, root_comm.Rank);
                if (e.InnerException != null)
                {
	                Console.WriteLine("{1}: INNER EXCEPTION (IChannelImpl/Send): message={0}", e.InnerException.Message, root_comm.Rank);
	                Console.WriteLine("{1}: INNER EXCEPTION (IChannelImpl/Send): stack={0}", e.InnerException.StackTrace, root_comm.Rank);
                }
                throw e;
           }	            
        }

       private Exception MPIException(int errorCode, int facet_instance)
        {
           Exception e = MPI.Environment.TranslateErrorIntoException(errorCode);
           Console.WriteLine("{0}: MPI Exception - MESSAGE = {1}", facet_instance, e.Message);
           Console.WriteLine("{0}: MPI Exception - STACK = {1}", facet_instance, e.StackTrace);
           if (e.InnerException != null)
           {
	           Console.WriteLine("{0}: MPI Inner Exception - MESSAGE = {1}", facet_instance, e.InnerException.Message);
	           Console.WriteLine("{0}: MPI Inner Exception - STACK = {1}", facet_instance, e.InnerException.StackTrace);
           }
           return e;
        }

        protected unsafe int ImmediateSend(Intercommunicator root_comm, Tuple<int, int, int, byte[]> v, int tag)
        {
            int v1 = v.Item1;
            int v2 = v.Item2;
            int v3 = v.Item3;
            int v4size = v.Item4.Length;
            byte[] v4 = v.Item4;

            Console.WriteLine("IMMEDIATE SEND 1 {0}", v);

            int outbuf_size = 4 * sizeof(int) + sizeof(byte) * v4.Length;
            IntPtr outbuf = Marshal.AllocHGlobal(outbuf_size);

            Console.WriteLine("IMMEDIATE SEND 2 {0} {1}", v, v4.Length);

            int position = 0;
            Unsafe.MPI_Pack((IntPtr) (&v1) /*new IntPtr(&v1)*/, 1, Unsafe.MPI_INT, outbuf, outbuf_size, ref position, root_comm.comm);
            Unsafe.MPI_Pack((IntPtr) (&v2) /*new IntPtr(&v2)*/, 1, Unsafe.MPI_INT, outbuf, outbuf_size, ref position, root_comm.comm);
            Unsafe.MPI_Pack((IntPtr) (&v3) /*new IntPtr(&v3)*/, 1, Unsafe.MPI_INT, outbuf, outbuf_size, ref position, root_comm.comm);
            Unsafe.MPI_Pack((IntPtr) (&v4size) /*new IntPtr(&v4size)*/, 1, Unsafe.MPI_INT, outbuf, outbuf_size, ref position, root_comm.comm);
            GCHandle pinnedArray = GCHandle.Alloc(v4, GCHandleType.Pinned);
            fixed (byte* b = &v4[0])
            {
                IntPtr pointer = (IntPtr) b; // new IntPtr(b);
                Unsafe.MPI_Pack(pointer, v4.Length, Unsafe.MPI_BYTE, outbuf, outbuf_size, ref position, root_comm.comm);
                Console.WriteLine("IMMEDIATE SEND 3 {0} --- {1}", v, Marshal.ReadInt32(pointer));
            }

         //   string file_name = string.Format("data.{0}.{1}.{2}.{3}.IS", v1, v2, v3, v4size);
         //   if (!File.Exists(file_name))
         //   {
         //       Console.WriteLine("writing {0} ! ", file_name);
         //       File.WriteAllBytes(file_name, v4);
         //   }
         //   else
         //       Console.WriteLine("{0} exists !", file_name);

            
            int request;
            Unsafe.MPI_Send((IntPtr) (&position), 1, Unsafe.MPI_INT, 0, tag, root_comm.comm);           
            errorCode = Unsafe.MPI_Isend(outbuf, position, Unsafe.MPI_PACKED, 0, tag, root_comm.comm, out request);

            if (errorCode != Unsafe.MPI_SUCCESS)
            {
                pinnedArray.Free();
                 throw MPIException(errorCode, this.FacetInstance);
            }

            Console.WriteLine("IMMEDIATE SEND 4 {0}", v);

            return request; // new ValueTypeSendRequest(request, 1, pinnedArray);
        }


        private int bufsize = 10000000;

        protected unsafe void Receive(Intercommunicator root_comm, int source, int tag, out byte[] result, out Unsafe.MPI_Status status) 
        {
               int bufsize; IntPtr ptr_bufsize = (IntPtr) (&bufsize);
               errorCode = Unsafe.MPI_Recv(ptr_bufsize, 1, Unsafe.MPI_INT, source, tag, root_comm.comm, out status);
                if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);

               result = new byte[bufsize];
               fixed (byte* b = &result[0])
                {
                    IntPtr buf = (IntPtr) b; // new IntPtr(b);                
                    errorCode = Unsafe.MPI_Recv(buf, bufsize, Unsafe.MPI_BYTE, source, tag, root_comm.comm, out status);
                     if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
                    
                }
               
               

//               root_comm.Receive<byte[]>(source, tag, out result, out status);
               

         /*   IntPtr buf = default(IntPtr);
            try
            {
               int bufsize; IntPtr ptr_bufsize = (IntPtr) (&bufsize);
               Unsafe.MPI_Recv(ptr_bufsize, 1, Unsafe.MPI_INT, source, tag, root_comm.comm, out status);
               
               Console.WriteLine("RECEIVE -------- #1 {0} {1} {2}", root_comm.comm, source, tag);
               buf = Marshal.AllocHGlobal(bufsize);
               Console.WriteLine("RECEIVE -------- #2 {0} {1} {2}", root_comm.comm, source, tag);
               Unsafe.MPI_Recv(buf, bufsize, Unsafe.MPI_PACKED, source, tag, root_comm.comm, out status);
               Console.WriteLine("RECEIVE -------- #3 {0} {1} {2}", root_comm.comm, source, tag);

                int size = 0;

                int position = 0;
                Unsafe.MPI_Unpack(buf, bufsize, ref position, new IntPtr(&size), 1, Unsafe.MPI_INT, root_comm.comm);
                Console.WriteLine("RECEIVE -------- #4 {0} {1} {2}", root_comm.comm, source, tag);
                result = new byte[size];
                fixed (byte* b = &result[0])
                {
                    IntPtr pointer = new IntPtr(b);
                    Unsafe.MPI_Unpack(buf, bufsize, ref position, pointer, size, Unsafe.MPI_BYTE, root_comm.comm);
                    Console.WriteLine("RECEIVE -------- #5 {0} {1} {2}", root_comm.comm, source, tag);
                }
            }
            finally
            {
                if (buf != null)
                   Marshal.FreeHGlobal(buf);
                Console.WriteLine("RECEIVE -------- #6 {0} {1} {2}", root_comm.comm, source, tag);
            }*/
        }

        protected unsafe int ImmediateReceive(Intercommunicator root_comm, int source, int tag) 
        {
            byte v;
            int request;
            Unsafe.MPI_Irecv((IntPtr) (&v) /* new IntPtr(&v)*/, 1, Unsafe.MPI_BYTE, source, tag, root_comm.comm, out request);
            return request;

            // return root_comm.ImmediateReceive<bool>(source, tag);
        }


        protected unsafe int ImmediateReceive(Intercommunicator root_comm, int source, int tag, out IntPtr buf) 
        {
            //Unsafe.MPI_Status status;
            //int bufsize; IntPtr ptr_bufsize = (IntPtr) (&bufsize);
            //Unsafe.MPI_Recv(ptr_bufsize, 1, Unsafe.MPI_INT, source, tag, root_comm.comm, out status);
            
            buf = Marshal.AllocHGlobal(bufsize);
            int request;
            Unsafe.MPI_Irecv(buf, bufsize, Unsafe.MPI_PACKED, source, tag, root_comm.comm, out request);

            //return new Tuple<int,int> (request,bufsize);
            return request;
        }

        // Convert an object to a byte array
        private static byte[] ObjectToByteArray(Object obj)
		{
			if (obj == null)
				return null;
			BinaryFormatter bf = new BinaryFormatter();
			MemoryStream ms = new MemoryStream();
			bf.Serialize(ms, obj);
			return ms.ToArray();
		}

		private static Object ByteArrayToObject(byte[] arrBytes)
		{
			MemoryStream memStream = new MemoryStream();
			BinaryFormatter binForm = new BinaryFormatter();
			memStream.Write(arrBytes, 0, arrBytes.Length);
			memStream.Seek(0, SeekOrigin.Begin);
			Object obj = (Object)binForm.Deserialize(memStream);
			return obj;
		}
		
		
	}

    /// <summary>
    /// A non-blocking receive request. 
    /// </summary>
    /// <remarks>
    /// This class allows one to test a receive
    /// request for completion, wait for completion of a request, cancel a request,
    /// or extract the value received by this communication request.
    /// </remarks>
    public abstract class MPIReceiveRequest : MPIRequest, IReceiveRequest
    {

        internal MPIReceiveRequest(int internal_request, Tuple<int, int> source) : base(internal_request, source)
        {
        }
        /// <summary>
        /// Retrieve the value received via this communication. The value
        /// will only be available when the communication has completed.
        /// </summary>
        /// <returns>The value received by this communication.</returns>
        public abstract object GetValue();
    }



    /// <summary>
    /// A non-blocking receive request. 
    /// </summary>
    /// <remarks>
    /// This class allows one to test a receive
    /// request for completion, wait for completion of a request, cancel a request,
    /// or extract the value received by this communication request.
    /// </remarks>
    public class MPIValueReceiveRequest<T> : MPIReceiveRequest
    {
        private int internal_request;
        private IntPtr buf;
        private int bufsize;
        private int comm;
        private object value = null;

        internal MPIValueReceiveRequest(int internal_request, Tuple<int, int> source, IntPtr buf, int bufsize, int comm) : base(internal_request, source)
        {
            this.internal_request = internal_request;
            this.buf = buf;
            this.bufsize = bufsize;
            this.comm = comm;
        }

        /// <summary>
        /// Retrieve the value received via this communication. The value
        /// will only be available when the communication has completed.
        /// </summary>
        /// <returns>The value received by this communication.</returns>
        public unsafe override object GetValue()
        {
            if (value != null)
            {
                Console.WriteLine("GET VALUE #0 - {0} {1}", this.GetHashCode(), value);
                return value;
            }

            Console.WriteLine("GET VALUE #1 - {0}", this.GetHashCode());
            Wait();
            Console.WriteLine("GET VALUE #2 - {0} {1}", this.GetHashCode(), bufsize);

            int size = 0;

            int position = 0;
            Unsafe.MPI_Unpack(buf, bufsize, ref position, (IntPtr) (&size) /*new IntPtr(&size)*/, 1, Unsafe.MPI_INT, comm);
            Console.WriteLine("GET VALUE #2 - {0} {1} {2}", this.GetHashCode(), size, position);
            byte[] result = new byte[size];
            fixed (byte* b = &result[0])
            {
                IntPtr pointer = (IntPtr) b; // new IntPtr(b);
                Unsafe.MPI_Unpack(buf, bufsize, ref position, pointer, size, Unsafe.MPI_BYTE, comm);
                Console.WriteLine("GET VALUE #3 - {0} {1} {2}", this.GetHashCode(), size, position);
            }

            Marshal.FreeHGlobal(buf);
            Console.WriteLine("GET VALUE #4 - {0} {1} {2}", this.GetHashCode(), size, position);

            value = (T)ByteArrayToObject(result); ;

            Console.WriteLine("GET VALUE #5 - {0} {1}", this.GetHashCode(), value);

            return value;
        }

        public static MPIValueReceiveRequest<T> createRequest(int internal_status, Tuple<int, int> source, IntPtr buf, int bufsize, int comm)
        {
            return new MPIValueReceiveRequest<T>(internal_status, source, buf, bufsize, comm);
        }

    }

    /// <summary>
    /// A non-blocking receive request. 
    /// </summary>
    /// <remarks>
    /// This class allows one to test a receive
    /// request for completion, wait for completion of a request, cancel a request,
    /// or extract the value received by this communication request.
    /// </remarks>
    public class MPIArrayReceiveRequest<T> : MPIReceiveRequest
    {
        private int internal_request;
        private T[] values = null;
        private IntPtr buf;
        private int bufsize;
        private int comm;

        internal MPIArrayReceiveRequest(int internal_request, Tuple<int, int> source, T[] values, IntPtr buf, int bufsize, int comm) : base(internal_request, source)
        {
            this.internal_request = internal_request;
            this.values = values;
            this.buf = buf;
            this.bufsize = bufsize;
            this.comm = comm;
        }

        /// <summary>
        /// Retrieve the value received via this communication. The value
        /// will only be available when the communication has completed.
        /// </summary>
        /// <returns>The value received by this communication.</returns>
        public override unsafe object GetValue()
        {
            if (values != null)
            {
                Console.WriteLine("GET ARRAY VALUE #0 - {0} {1}", this.GetHashCode(), values);
                return values;
            }

            Console.WriteLine("GET ARRAY VALUE #1 - {0}", this.GetHashCode());
            Wait();
            Console.WriteLine("GET ARRAY VALUE #2 - {0} {1}", this.GetHashCode(), bufsize);

            int asize = 0;

            int position = 0;
            Unsafe.MPI_Unpack(buf, bufsize, ref position, (IntPtr) (&asize) /*new IntPtr(&asize)*/, 1, Unsafe.MPI_INT, comm);
            Console.WriteLine("GET ARRAY VALUE #2 - {0} {1} {2}", this.GetHashCode(), asize, position);
            byte[] result = new byte[asize];
            fixed (byte* b = &result[0])
            {
                IntPtr pointer = (IntPtr) b; // new IntPtr(b);
                Unsafe.MPI_Unpack(buf, bufsize, ref position, pointer, asize, Unsafe.MPI_BYTE, comm);
                Console.WriteLine("GET ARRAY VALUE #3 - {0} {1} {2}", this.GetHashCode(), asize, position);
            }

            Marshal.FreeHGlobal(buf);
            Console.WriteLine("GET ARRAY VALUE #4 - {0} {1} {2}", this.GetHashCode(), asize, position);


            //byte[] v = (byte[])this.internal_request.GetValue();
            T[] values_ = (T[])ByteArrayToObject(result);

            Console.WriteLine("GET ARRAY VALUE #5 - {0} {1}", this.GetHashCode(), values_);

            // Copy the received values to the destination array (forcing original MPI semantics)
            int size = values.Length <= values_.Length ? values.Length : values_.Length;
            for (int i = 0; i < size; i++)
                values[i] = values_[i];

            Console.WriteLine("GET ARRAY VALUE #6 - {0} {1}", this.GetHashCode(), values_);

            return values;
        }

        public static MPIArrayReceiveRequest<T> createRequest(int internal_status, Tuple<int, int> source, T[] values, IntPtr buf, int bufsize, int comm)
        {
            return new MPIArrayReceiveRequest<T>(internal_status, source, values, buf, bufsize, comm);
        }


    }

    public class MPIStatus : Binding.Status
    {
        private Unsafe.MPI_Status internal_status;
        private int? tag = null;

        /// <summary>
        ///   Constructs a <code>Status</code> object from a low-level <see cref="Unsafe.MPI_Status"/> structure.
        /// </summary>
        internal MPIStatus(Unsafe.MPI_Status internal_status, Tuple<int, int> source)
        {
            this.source = source;
            this.internal_status = internal_status;
        }

        internal MPIStatus(Unsafe.MPI_Status internal_status, Tuple<int, int> source, int tag)
        {
            this.source = source;
            this.internal_status = internal_status;
            this.tag = tag;
        }

        Tuple<int, int> source;

        /// <summary>
        /// The rank of the process that sent the message.
        /// </summary>
        public Tuple<int, int> Source
        {
            get
            {
                return source;
            }
        }

        /// <summary>
        /// The tag used to send the message.
        /// </summary>
        public int Tag
        {
            get
            {
                return tag ?? internal_status.MPI_TAG;
            }
        }

        /// <summary>
        /// Determine the number of elements transmitted by the communication
        /// operation associated with this object.
        /// </summary>
        /// <param name="type">
        ///   The type of data that will be stored in the message.
        /// </param>
        /// <returns>
        ///   If the type of the data is a value type, returns the number
        ///   of elements in the message. Otherwise, returns <c>null</c>,
        ///   because the number of elements stored in the message won't
        ///   be known until the message is received.
        /// </returns>
        public int? Count(Type type)
        {
            Int32 datatype = DatatypeCache.GetDatatype(type);
            if (datatype != Unsafe.MPI_DATATYPE_NULL)
            {
                int count;
                unsafe
                {
                    int errorCode = Unsafe.MPI_Get_count(ref internal_status, datatype, out count);
                     if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode);
                }
                return count;
            }
            return null;
        }

       private Exception MPIException(int errorCode)
        {
           Exception e = MPI.Environment.TranslateErrorIntoException(errorCode);
           Console.WriteLine("MPI Exception - MESSAGE = {0}", e.Message);
           Console.WriteLine("MPI Exception - STACK = {0}", e.StackTrace);
           if (e.InnerException != null)
           {
	           Console.WriteLine("MPI Inner Exception - MESSAGE = {0}", e.InnerException.Message);
	           Console.WriteLine("MPI Inner Exception - STACK = {0}", e.InnerException.StackTrace);
           }
           return e;
        }

        /// <summary>
        /// Whether the communication was cancelled before it completed.
        /// </summary>
        public bool Cancelled
        {
            get
            {
                int flag;
                unsafe
                {
                    int errorCode = Unsafe.MPI_Test_cancelled(ref internal_status, out flag);
                     if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode);
                }
                return flag != 0;
            }
        }

    }

    /// <summary>
    /// Information about a specific message that has already been
    /// transferred via MPI.
    /// </summary>
    public class MPICompletedStatus : MPIStatus, Binding.CompletedStatus
    {
        private Unsafe.MPI_Status internal_status;

        /// <summary>
        ///   Constructs a <code>Status</code> object from a low-level <see cref="Unsafe.MPI_Status"/> structure
        ///   and a count of the number of elements received.
        /// </summary>
        internal MPICompletedStatus(Unsafe.MPI_Status internal_status, Tuple<int, int> source) : base(internal_status, source)
        {
            this.internal_status = internal_status;
        }

        internal MPICompletedStatus(Unsafe.MPI_Status internal_status, Tuple<int, int> source, int tag) : base(internal_status, source, tag)
        {
            this.internal_status = internal_status;
        }

        public static Binding.CompletedStatus createStatus(Unsafe.MPI_Status internal_status, Tuple<int, int> source)
        {
            return new MPICompletedStatus(internal_status, source);
        }

        public static Binding.CompletedStatus createStatus(Unsafe.MPI_Status internal_status, Tuple<int, int> source, int tag)
        {
            return new MPICompletedStatus(internal_status, source, tag);
        }

        /// <summary>
        /// Determines the number of elements in the transmitted message.
        /// </summary>
        public int? Count
        {
            get { return null;  /*internal_status.Count();*/ }
        }

    }

    /// <summary>
    /// A non-blocking communication request.
    /// </summary>
    /// <remarks>
    /// Each request object refers to a single
    /// communication operation, such as non-blocking send 
    /// (see <see cref="Communicator.ImmediateSend&lt;T&gt;(T, int, int)"/>)
    /// or receive. Non-blocking operations may progress in the background, and can complete
    /// without any user intervention. However, it is crucial that outstanding communication
    /// requests be completed with a successful call to <see cref="Wait"/> or <see cref="Test"/>
    /// before the request object is lost.
    /// </remarks>
    public class MPIRequest : IRequest
    {
        private int internal_request;
        private Tuple<int, int> source;

        private Thread waiting_request = null;
        protected ManualResetEvent initial_signal = new ManualResetEvent(false);
        private bool completed = false;

        internal MPIRequest(int internal_request, Tuple<int, int> source)
        {
            this.internal_request = internal_request;
            this.source = source;
        }


        private object wait_lock = new object();

        private IList<AutoResetEvent> waiting_sets = new List<AutoResetEvent>();

        public void registerWaitingSet(AutoResetEvent waiting_set)
        {
            waiting_request = new Thread(new ThreadStart(delegate
            {
                while (true)
                {
                    initial_signal.WaitOne();
                    Unsafe.MPI_Status status;
                    //internal_request.Wait();
                    Unsafe.MPI_Wait(ref internal_request, out status);
                    lock (wait_lock)
                    {
                        completed = true;
                        foreach (AutoResetEvent ws in waiting_sets)
                            ws.Set();
                    }
                }
            }));

            waiting_request.Start();

            lock (wait_lock)
            {
                if (completed)
                    waiting_set.Set();
                else
                {
                    waiting_sets.Add(waiting_set);
                    initial_signal.Set();
                }
            }
        }

        public void unregisterWaitingSet(AutoResetEvent waiting_set)
        {
            waiting_sets.Remove(waiting_set);
            initial_signal.Reset();
            waiting_request.Abort();
            waiting_request = null;
        }

        /// <summary>
        /// Wait until this non-blocking operation has completed.
        /// </summary>
        /// <returns>
        ///   Information about the completed communication operation.
        /// </returns>
        public Binding.CompletedStatus Wait()
        {
            Unsafe.MPI_Status internal_status;
            //MPI.CompletedStatus internal_status = internal_request.Wait();
            Unsafe.MPI_Wait(ref internal_request, out internal_status);
            return MPICompletedStatus.createStatus(internal_status, source);
        }

        /// <summary>
        /// Determine whether this non-blocking operation has completed.
        /// </summary>
        /// <returns>
        /// If the non-blocking operation has completed, returns information
        /// about the completed communication operation. Otherwise, returns
        /// <c>null</c> to indicate that the operation has not completed.
        /// </returns>
        public Binding.CompletedStatus Test()
        {
            int flag;
            Unsafe.MPI_Status internal_status;
            //MPI.CompletedStatus internal_status = internal_request.Test(); 
            Unsafe.MPI_Test(ref internal_request, out flag, out internal_status);
            return MPICompletedStatus.createStatus(internal_status, source);
        }

        /// <summary>
        /// Cancel this communication request.
        /// </summary>
        public void Cancel()
        {
            //internal_request.Cancel();
            Unsafe.MPI_Cancel(ref internal_request);
        }

        public static IRequest createRequest(int internal_status, Tuple<int, int> source)
        {
            return new MPIRequest(internal_status, source);
        }

        // Convert an object to a byte array
        protected static byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        protected static Object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);
            return obj;
        }

    }


}
