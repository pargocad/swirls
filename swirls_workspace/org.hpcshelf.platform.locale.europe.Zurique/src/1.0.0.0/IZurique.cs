using org.hpcshelf.platform.locale.europe.Swiss;

namespace org.hpcshelf.platform.locale.europe.Zurique
{
	public interface IZurique : BaseIZurique, ISwiss
	{
	}
}