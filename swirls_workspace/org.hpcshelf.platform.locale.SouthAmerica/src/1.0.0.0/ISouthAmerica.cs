using org.hpcshelf.platform.locale.AnyWhere;

namespace org.hpcshelf.platform.locale.SouthAmerica
{
	public interface ISouthAmerica : BaseISouthAmerica, IAnyWhere
	{
	}
}