using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;
using org.hpcshelf.backend.ec2.family.Acceleration;

namespace org.hpcshelf.backend.ec2.type.accelerated.F1
{
	public interface IEC2TypeF1 : BaseIEC2TypeF1, IEC2Type<IEC2FamilyAcceleration>
	{
	}
}