/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Series;

namespace org.hpcshelf.platform.node.processor.series.intel.Xeon_Platinum_8000
{
	public interface BaseIXeonPlatinum8000 : BaseIProcessorSeries<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, org.hpcshelf.platform.node.processor.family.intel.Xeon_Platinum.IXeonPlatinum>, IQualifierKind 
	{
	}
}