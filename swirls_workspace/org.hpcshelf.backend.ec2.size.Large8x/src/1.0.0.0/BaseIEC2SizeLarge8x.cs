/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large8x
{
	public interface BaseIEC2SizeLarge8x : BaseIEC2Size, IQualifierKind 
	{
	}
}