/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.accelerated.P2
{
	public interface BaseIEC2TypeP2 : BaseIEC2Type<family.Acceleration.IEC2FamilyAcceleration>, IQualifierKind 
	{
	}
}