#include <mpi.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int get_hosts_info_for_spawn()
{
	const char* s1 = getenv("HOME");
	const char* s2 = "/workpeers";
	int l1 = strlen(s1);
	int l2 = strlen(s2);
	char* s3 = malloc(l1 + l2 + 1);
	strcpy(s3,s1);
	strcat(s3,s2);

	printf("PATH of workpeers is %s", s3);

	MPI_Info info;
	MPI_Info_create(&info);
	MPI_Info_set(info, "hostfile", s3);
	return info;
}
