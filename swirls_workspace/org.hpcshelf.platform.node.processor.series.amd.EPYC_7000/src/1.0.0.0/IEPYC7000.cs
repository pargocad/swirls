using org.hpcshelf.platform.node.processor.Series;

namespace org.hpcshelf.platform.node.processor.series.amd.EPYC_7000
{
	public interface IEPYC7000 : BaseIEPYC7000, IProcessorSeries<org.hpcshelf.platform.node.processor.manufacturer.AMD.IAMD, org.hpcshelf.platform.node.processor.family.amd.EPYC.IEPYC>
	{
	}
}