using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.backend.ec2.platform.general.EC2_T3_Small;

namespace org.hpcshelf.backend.ec2.impl.platform.general.EC2_T3_Small_Bahrain
{
 public class IEC2_T3_Small_Bahrain<N> : BaseIEC2_T3_Small_Bahrain<N>, IEC2_T3_Small<N>
  where N:IntUp
 {
 }
}
