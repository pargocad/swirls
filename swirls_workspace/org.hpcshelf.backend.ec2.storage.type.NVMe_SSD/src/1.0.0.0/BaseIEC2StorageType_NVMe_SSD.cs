/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.storage.Type;

namespace org.hpcshelf.backend.ec2.storage.type.NVMe_SSD
{
	public interface BaseIEC2StorageType_NVMe_SSD : BaseIEC2StorageType, IQualifierKind 
	{
	}
}