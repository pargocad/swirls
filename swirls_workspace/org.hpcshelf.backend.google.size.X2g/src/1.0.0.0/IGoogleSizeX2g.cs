using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X2g
{
 public interface IGoogleSizeX2g : BaseIGoogleSizeX2g, IGoogleSize
 {
 }
}