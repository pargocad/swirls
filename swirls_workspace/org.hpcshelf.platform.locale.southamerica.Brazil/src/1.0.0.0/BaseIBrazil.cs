/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.SouthAmerica;

namespace org.hpcshelf.platform.locale.southamerica.Brazil
{
	public interface BaseIBrazil : BaseISouthAmerica, IQualifierKind 
	{
	}
}