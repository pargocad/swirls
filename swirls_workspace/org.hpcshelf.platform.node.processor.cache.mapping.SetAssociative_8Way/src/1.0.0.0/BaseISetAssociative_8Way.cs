/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_8Way
{
	public interface BaseISetAssociative_8Way : BaseICacheMappingSetAssociative<org.hpcshelf.quantifier.IntUp.IntUp>, IQualifierKind 
	{
	}
}