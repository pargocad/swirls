using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.mpi.wrapper.WBrowserConnector;
using org.hpcshelf.common.browser.wrapper.WSendDataPortType;
using org.hpcshelf.binding.channel.Binding;
using System.Runtime.InteropServices;

namespace org.hpcshelf.mpi.wrapper.impl.WBrowserConnectorImpl
{
    public class IWBrowserPeerImpl : BaseIWBrowserPeerImpl, IWBrowserPeer
    {
        IWSendDataPortType send_data_port = null;

        private int connector_key = -1;

        public int ConnectorKey { get { return connector_key; } }

        public override void after_initialize()
        {
            send_data_port = new WSendDataPortTypeImpl(Channel);
        }

        public override void main()
        {
            Send_data_port.Server = send_data_port;
        }

        public override void release1()
        {
            Services.releasePort("channel");
            Services.releasePort("send_data_port");

            Send_data_port.Server = null;

            base.release1();         

        }
    }
        
    class WSendDataPortTypeImpl : IWSendDataPortType
    {
       IChannel channel = null;
       
        public void Browse(string message)
        {
            channel.Send<string>(message, new Tuple<int,int>(0,0), 0);
        }
        
        public WSendDataPortTypeImpl(IChannel channel)
        {
           this.channel = channel;
        }
    }
}
