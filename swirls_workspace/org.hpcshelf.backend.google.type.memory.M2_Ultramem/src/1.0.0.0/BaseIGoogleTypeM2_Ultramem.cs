/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.memory.M2_Ultramem
{
 public interface BaseIGoogleTypeM2_Ultramem : BaseIGoogleType<family.Memory.IGoogleFamilyMemory>, IQualifierKind 
 {
 }
}