using org.hpcshelf.platform.locale.oceania.Australia;

namespace org.hpcshelf.platform.locale.oceania.Sydney
{
	public interface ISydney : BaseISydney, IAustralia
	{
	}
}