/* Automatically Generated Code */

using org.hpcshelf.backend.ec2.platform.general.EC2_T3_Small;
using org.hpcshelf.backend.ec2.family.GeneralPurpose;
using org.hpcshelf.backend.ec2.type.general.T3;
using org.hpcshelf.backend.ec2.size.Small;
using org.hpcshelf.platform.maintainer.EC2;
using org.hpcshelf.quantifier.IntUp;

namespace org.hpcshelf.backend.ec2.impl.platform.general.EC2_T3_Small_Bahrain 
{
 public abstract class BaseIEC2_T3_Small_Bahrain<N>: org.hpcshelf.kinds.Environment, BaseIEC2_T3_Small<N>
  where N:IntUp
 {
  private IEC2TypeT3 type = null;

  protected IEC2TypeT3 Type
  {
   get
   {
    if (this.type == null)
     this.type = (IEC2TypeT3) Services.getPort("type");
    return this.type;
   }
  }
  private IEC2SizeSmall size = null;

  protected IEC2SizeSmall Size
  {
   get
   {
    if (this.size == null)
     this.size = (IEC2SizeSmall) Services.getPort("size");
    return this.size;
   }
  }
  private IEC2FamilyGeneralPurpose family = null;

  protected IEC2FamilyGeneralPurpose Family
  {
   get
   {
    if (this.family == null)
     this.family = (IEC2FamilyGeneralPurpose) Services.getPort("family");
    return this.family;
   }
  }
  private IEC2 maintainer = null;

  protected IEC2 Maintainer
  {
   get
   {
    if (this.maintainer == null)
     this.maintainer = (IEC2) Services.getPort("maintainer");
    return this.maintainer;
   }
  }
 }
}