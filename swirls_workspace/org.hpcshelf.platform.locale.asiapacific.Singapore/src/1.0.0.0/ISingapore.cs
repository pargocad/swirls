using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.Singapore
{
	public interface ISingapore : BaseISingapore, IAsiaPacific
	{
	}
}