using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X60
{
 public interface IGoogleSizeX60 : BaseIGoogleSizeX60, IGoogleSize
 {
 }
}