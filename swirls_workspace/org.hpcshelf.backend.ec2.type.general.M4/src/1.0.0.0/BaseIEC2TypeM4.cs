/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;
using org.hpcshelf.backend.ec2.family.GeneralPurpose;

namespace org.hpcshelf.backend.ec2.type.general.M4
{
	public interface BaseIEC2TypeM4 : BaseIEC2Type<IEC2FamilyGeneralPurpose>, IQualifierKind 
	{
	}
}