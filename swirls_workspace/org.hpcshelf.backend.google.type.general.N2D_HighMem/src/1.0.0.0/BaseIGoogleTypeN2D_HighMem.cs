/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.N2D_HighMem
{
 public interface BaseIGoogleTypeN2D_HighMem : BaseIGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>, IQualifierKind 
 {
 }
}