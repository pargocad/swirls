using org.hpcshelf.platform.locale.europe.WestEurope;

namespace org.hpcshelf.platform.locale.europe.Belgium
{
	public interface IBelgium : BaseIBelgium, IWestEurope
	{
	}
}