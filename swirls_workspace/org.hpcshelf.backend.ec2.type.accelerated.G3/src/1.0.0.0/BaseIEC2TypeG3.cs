/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;
using org.hpcshelf.backend.ec2.family.Acceleration;

namespace org.hpcshelf.backend.ec2.type.accelerated.G3
{
	public interface BaseIEC2TypeG3 : BaseIEC2Type<IEC2FamilyAcceleration>, IQualifierKind 
	{
	}
}