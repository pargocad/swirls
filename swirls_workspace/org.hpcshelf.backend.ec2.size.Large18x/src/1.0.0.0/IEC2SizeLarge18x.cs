using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large18x
{
	public interface IEC2SizeLarge18x : BaseIEC2SizeLarge18x, IEC2Size
	{
	}
}