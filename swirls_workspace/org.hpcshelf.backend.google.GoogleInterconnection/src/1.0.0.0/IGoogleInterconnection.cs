using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.interconnection.Topology;
using org.hpcshelf.platform.Interconnection;

namespace org.hpcshelf.backend.google.GoogleInterconnection
{
	public interface IGoogleInterconnection<STT, BAN, TOP, NLT> : BaseIGoogleInterconnection<STT, BAN, TOP, NLT>, IInterconnection<STT, NLT, BAN, TOP>
		where STT:IntDown
		where BAN:IntUp
		where TOP:ITopology
		where NLT:IntDown
	{
	}
}