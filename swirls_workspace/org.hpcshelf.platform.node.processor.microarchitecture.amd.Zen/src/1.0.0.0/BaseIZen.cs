/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Microarchitecture;

namespace org.hpcshelf.platform.node.processor.microarchitecture.amd.Zen
{
	public interface BaseIZen : BaseIProcessorMicroarchitecture<org.hpcshelf.platform.node.processor.manufacturer.AMD.IAMD>, IQualifierKind 
	{
	}
}