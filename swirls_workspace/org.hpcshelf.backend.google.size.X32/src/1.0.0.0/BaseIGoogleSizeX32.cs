/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X32
{
 public interface BaseIGoogleSizeX32 : BaseIGoogleSize, IQualifierKind 
 {
 }
}