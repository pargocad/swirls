using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.N1_HighCPU
{
 public interface IGoogleTypeN1_HighCPU : BaseIGoogleTypeN1_HighCPU, IGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>
 {
 }
}