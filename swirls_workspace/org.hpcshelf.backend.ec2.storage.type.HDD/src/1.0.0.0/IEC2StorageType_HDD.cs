using org.hpcshelf.backend.ec2.storage.Type;

namespace org.hpcshelf.backend.ec2.storage.type.HDD
{
	public interface IEC2StorageType_HDD : BaseIEC2StorageType_HDD, IEC2StorageType
	{
	}
}