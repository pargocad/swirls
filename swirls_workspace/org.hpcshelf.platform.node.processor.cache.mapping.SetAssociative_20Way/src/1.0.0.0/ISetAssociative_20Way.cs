using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_20Way
{
	public interface ISetAssociative_20Way : BaseISetAssociative_20Way, ICacheMappingSetAssociative<org.hpcshelf.quantifier.IntUp.IntUp>
	{
	}
}