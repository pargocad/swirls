/* Automatically Generated Code */

using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.binding.channel.Binding;
using org.hpcshelf.common.BrowserBinding;
using org.hpcshelf.common.BrowserPortType;
using org.hpcshelf.mpi.BrowserConnectorForC;
using org.hpcshelf.common.browser.RecvDataPortType;

namespace org.hpcshelf.mpi.impl.BrowserConnectorForCImpl 
{
	public abstract class BaseIBrowserRootForCImpl: Synchronizer, BaseIBrowserRootForC
	{
		private IChannel channel = null;

		protected IChannel Channel
		{
			get
			{
				if (this.channel == null)
					this.channel = (IChannel) Services.getPort("channel");
				return this.channel;
			}
		}
		private IBrowserBinding<IRecvDataPortType> browse_port = null;

		public IBrowserBinding<IRecvDataPortType> Browse_port
		{
			get
			{
				if (this.browse_port == null)
					this.browse_port = (IBrowserBinding<IRecvDataPortType>) Services.getPort("browse_port");
				return this.browse_port;
			}
		}
		private IRecvDataPortType recv_data_port_type = null;

		protected IRecvDataPortType Recv_data_port_type
		{
			get
			{
				if (this.recv_data_port_type == null)
					this.recv_data_port_type = (IRecvDataPortType) Services.getPort("recv_data_port_type");
				return this.recv_data_port_type;
			}
		}
	}
}