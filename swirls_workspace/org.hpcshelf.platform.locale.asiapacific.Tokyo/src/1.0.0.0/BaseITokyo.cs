/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.asiapacific.Japan;

namespace org.hpcshelf.platform.locale.asiapacific.Tokyo
{
	public interface BaseITokyo : BaseIJapan, IQualifierKind 
	{
	}
}