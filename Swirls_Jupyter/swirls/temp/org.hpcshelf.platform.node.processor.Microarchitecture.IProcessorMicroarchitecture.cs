using org.hpcshelf.platform.node.processor.Manufacturer;

namespace org.hpcshelf.platform.node.processor.Microarchitecture
{
	public interface IProcessorMicroarchitecture<MAN> : BaseIProcessorMicroarchitecture<MAN>
		where MAN:IProcessorManufacturer
	{
	}
}