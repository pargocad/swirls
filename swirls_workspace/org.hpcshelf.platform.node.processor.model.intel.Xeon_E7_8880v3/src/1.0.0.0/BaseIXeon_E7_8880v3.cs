/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Model;

namespace org.hpcshelf.platform.node.processor.model.intel.Xeon_E7_8880v3
{
	public interface BaseIXeon_E7_8880v3 : BaseIProcessorModel<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, org.hpcshelf.platform.node.processor.family.intel.Xeon_E7.IXeonE7, org.hpcshelf.platform.node.processor.series.intel.Xeon_E7_8000.IXeonE78000, org.hpcshelf.platform.node.processor.microarchitecture.intel.Haswell.IHaswell>, IQualifierKind 
	{
	}
}