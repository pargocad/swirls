using org.hpcshelf.platform.node.accelerator.Manufacturer;

namespace org.hpcshelf.platform.node.accelerator.manufacturer.Xilinx
{
	public interface IXilinx : BaseIXilinx, IAcceleratorManufacturer
	{
	}
}