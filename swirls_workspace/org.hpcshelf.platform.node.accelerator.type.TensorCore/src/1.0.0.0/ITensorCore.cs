using org.hpcshelf.platform.node.accelerator.Type;

namespace org.hpcshelf.platform.node.accelerator.type.TensorCore
{
	public interface ITensorCore : BaseITensorCore, IAcceleratorType<org.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA>
	{
	}
}