using org.hpcshelf.platform.locale.europe.CentralEurope;

namespace org.hpcshelf.platform.locale.europe.Germany
{
	public interface IGermany : BaseIGermany, ICentralEurope
	{
	}
}