/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.binding.task.TaskPortType;

namespace org.hpcshelf.binding.task.TaskBindingBase
{
	public interface BaseITaskPort<T> : ITaskBindingKind
		where T : ITaskPortType
	{
	}
}