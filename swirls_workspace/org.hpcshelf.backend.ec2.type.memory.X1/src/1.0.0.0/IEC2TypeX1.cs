using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.memory.X1
{
	public interface IEC2TypeX1 : BaseIEC2TypeX1, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}