/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.mpi.Language;

namespace org.hpcshelf.language.Python
{
	public interface BaseILanguage_Python : BaseILanguage, IQualifierKind 
	{
	}
}