using org.hpcshelf.platform.locale.SouthAmerica;

namespace org.hpcshelf.platform.locale.southamerica.Brazil
{
	public interface IBrazil : BaseIBrazil, ISouthAmerica
	{
	}
}