using org.hpcshelf.platform.node.processor.Family;

namespace org.hpcshelf.platform.node.processor.family.intel.Xeon_Platinum
{
	public interface IXeonPlatinum : BaseIXeonPlatinum, IProcessorFamily<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>
	{
	}
}