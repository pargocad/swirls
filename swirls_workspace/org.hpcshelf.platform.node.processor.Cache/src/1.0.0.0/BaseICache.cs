/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.cache.Mapping;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.quantifier.IntDown;

namespace org.hpcshelf.platform.node.processor.Cache
{
	public interface BaseICache<MAP, SIZ, LAT, LINSIZ> : IQualifierKind 
		where MAP:ICacheMapping
		where SIZ:IntUp
		where LAT:IntDown
		where LINSIZ:IntUp
	{
	}
}