/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.asiapacific.SouthKorea;

namespace org.hpcshelf.platform.locale.asiapacific.Seoul
{
	public interface BaseISeoul : BaseISouthKorea, IQualifierKind 
	{
	}
}