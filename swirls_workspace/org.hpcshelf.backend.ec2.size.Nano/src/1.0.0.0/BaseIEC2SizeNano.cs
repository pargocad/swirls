/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Nano
{
	public interface BaseIEC2SizeNano : BaseIEC2Size, IQualifierKind 
	{
	}
}