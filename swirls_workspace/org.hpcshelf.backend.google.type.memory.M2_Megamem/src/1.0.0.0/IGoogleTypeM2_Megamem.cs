using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.memory.M2_Megamem
{
 public interface IGoogleTypeM2_Megamem : BaseIGoogleTypeM2_Megamem, IGoogleType<family.Memory.IGoogleFamilyMemory>
 {
 }
}