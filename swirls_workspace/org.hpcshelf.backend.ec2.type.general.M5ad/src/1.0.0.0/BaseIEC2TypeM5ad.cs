/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;
using org.hpcshelf.backend.ec2.family.GeneralPurpose;

namespace org.hpcshelf.backend.ec2.type.general.M5ad
{
	public interface BaseIEC2TypeM5ad : BaseIEC2Type<IEC2FamilyGeneralPurpose>, IQualifierKind 
	{
	}
}