using org.hpcshelf.common.BrowserConnector;

namespace org.hpcshelf.mpi.wrapper.WBrowserConnector
{
	public interface IWBrowserPeer : BaseIWBrowserPeer, IBrowserPeer<org.hpcshelf.mpi.Language.ILanguage, org.hpcshelf.common.browser.wrapper.WSendDataPortType.IWSendDataPortType>
	{
	}
}