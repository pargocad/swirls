/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X416
{
 public interface BaseIGoogleSizeX416 : BaseIGoogleSize, IQualifierKind 
 {
 }
}