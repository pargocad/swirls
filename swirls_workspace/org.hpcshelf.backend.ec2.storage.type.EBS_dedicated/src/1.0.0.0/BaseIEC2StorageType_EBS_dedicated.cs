/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.storage.type.EBS;

namespace org.hpcshelf.backend.ec2.storage.type.EBS_dedicated
{
	public interface BaseIEC2StorageType_EBS_dedicated : BaseIEC2StorageType_EBS, IQualifierKind 
	{
	}
}