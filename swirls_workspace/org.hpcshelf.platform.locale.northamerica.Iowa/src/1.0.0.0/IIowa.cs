using org.hpcshelf.platform.locale.northamerica.US_East;

namespace org.hpcshelf.platform.locale.northamerica.Iowa
{
	public interface IIowa : BaseIIowa, IUS_East
	{
	}
}