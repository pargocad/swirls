/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.europe.France;

namespace org.hpcshelf.platform.locale.europe.Paris
{
	public interface BaseIParis : BaseIFrance, IQualifierKind 
	{
	}
}