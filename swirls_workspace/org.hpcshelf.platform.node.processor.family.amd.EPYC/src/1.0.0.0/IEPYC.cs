using org.hpcshelf.platform.node.processor.Family;

namespace org.hpcshelf.platform.node.processor.family.amd.EPYC
{
	public interface IEPYC : BaseIEPYC, IProcessorFamily<org.hpcshelf.platform.node.processor.manufacturer.AMD.IAMD>
	{
	}
}