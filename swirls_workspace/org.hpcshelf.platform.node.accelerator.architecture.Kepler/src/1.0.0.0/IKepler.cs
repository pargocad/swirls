using org.hpcshelf.platform.node.accelerator.Architecture;

namespace org.hpcshelf.platform.node.accelerator.architecture.Kepler
{
	public interface IKepler : BaseIKepler, IAcceleratorArchitecture
	{
	}
}