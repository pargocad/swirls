/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.types.Data;
using org.hpcshelf.blas.data.DataDistribution;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace org.hpcshelf.blas.GetPortType
{
	public interface BaseIGetPortType<T, DIST, DIM> : BaseIEnvironmentPortTypeMultiplePartner, IQualifierKind 
		where T:IData
		where DIST:IDataDistribution
		where DIM:IntUp
	{
	}
}