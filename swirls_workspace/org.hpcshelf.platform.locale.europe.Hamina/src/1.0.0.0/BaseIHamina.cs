/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.europe.Finland;

namespace org.hpcshelf.platform.locale.europe.Hamina
{
	public interface BaseIHamina : BaseIFinland, IQualifierKind 
	{
	}
}