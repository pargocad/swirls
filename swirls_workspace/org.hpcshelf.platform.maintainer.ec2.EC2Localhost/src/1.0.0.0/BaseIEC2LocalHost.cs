/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.maintainer.EC2;

namespace org.hpcshelf.platform.maintainer.ec2.EC2Localhost
{
	public interface BaseIEC2LocalHost : BaseIEC2, IEnvironmentKind 
	{
	}
}