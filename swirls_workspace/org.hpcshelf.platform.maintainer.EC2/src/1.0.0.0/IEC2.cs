using org.hpcshelf.platform.Maintainer;

namespace org.hpcshelf.platform.maintainer.EC2
{
	public interface IEC2 : BaseIEC2, IMaintainer
	{
	}
}