using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.E2_Standard
{
 public interface IGoogleTypeE2_Standard : BaseIGoogleTypeE2_Standard, IGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>
 {
 }
}