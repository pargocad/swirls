using org.hpcshelf.mpi.Language;
using org.hpcshelf.common.BrowserPortType;

namespace org.hpcshelf.common.BrowserConnector
{
	public interface IBrowserPeer<L, S1> : BaseIBrowserPeer<L, S1>
		where L:ILanguage
		where S1:IBrowserPortType
	{
	   int ConnectorKey { get; }
	}
}