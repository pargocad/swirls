using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large6x
{
	public interface IEC2SizeLarge6x : BaseIEC2SizeLarge6x, IEC2Size
	{
	}
}