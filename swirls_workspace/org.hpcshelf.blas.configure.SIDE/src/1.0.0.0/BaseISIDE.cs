/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.CONFIG;

namespace org.hpcshelf.blas.configure.SIDE
{
	public interface BaseISIDE : BaseICONFIG, IQualifierKind 
	{
	}
}