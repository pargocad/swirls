/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.AnyWhere;

namespace org.hpcshelf.platform.locale.NorthAmerica
{
	public interface BaseINorthAmerica : BaseIAnyWhere, IQualifierKind 
	{
	}
}