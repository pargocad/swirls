/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.compute.C5n
{
	public interface BaseIEC2TypeC5n : BaseIEC2Type<family.Computation.IEC2FamilyComputation>, IQualifierKind 
	{
	}
}