/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.types.YesOrNo;

namespace org.hpcshelf.types.yesorno.No
{
	public interface BaseINo : BaseIYesOrNo, IQualifierKind 
	{
	}
}