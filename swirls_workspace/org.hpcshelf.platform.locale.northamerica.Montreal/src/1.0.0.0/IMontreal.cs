using org.hpcshelf.platform.locale.northamerica.Canada;

namespace org.hpcshelf.platform.locale.northamerica.Montreal
{
	public interface IMontreal : BaseIMontreal, ICanada
	{
	}
}