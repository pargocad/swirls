/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.Micro
{
 public interface BaseIGoogleSizeMicro : BaseIGoogleSize, IQualifierKind 
 {
 }
}