using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentPortType;

namespace org.hpcshelf.binding.environment.EnvironmentBindingBase
{
	public interface IServerBase<S> : BaseIServerBase<S>
		where S:IEnvironmentPortType
	{
		S Server { set; }
	}
}