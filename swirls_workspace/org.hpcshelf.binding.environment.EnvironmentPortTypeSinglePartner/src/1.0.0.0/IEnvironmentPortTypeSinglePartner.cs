using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentPortType;

namespace org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner
{
	public interface IEnvironmentPortTypeSinglePartner : BaseIEnvironmentPortTypeSinglePartner, IEnvironmentPortType
	{
	}
}