/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.accelerator.Model;

namespace org.hpcshelf.platform.node.accelerator.model.Tesla_M60
{
	public interface BaseITesla_M60 : BaseIAcceleratorModel<org.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA, org.hpcshelf.platform.node.accelerator.type.Tesla.ITesla, org.hpcshelf.platform.node.accelerator.architecture.Maxwell.IMaxwell>, IQualifierKind 
	{
	}
}