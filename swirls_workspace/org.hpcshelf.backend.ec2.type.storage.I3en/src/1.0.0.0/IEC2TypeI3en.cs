using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.storage.I3en
{
	public interface IEC2TypeI3en : BaseIEC2TypeI3en, IEC2Type<family.Storage.IEC2FamilyStorage>
	{
	}
}