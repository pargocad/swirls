using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;

namespace org.hpcshelf.backend.ec2.family.Computation
{
	public interface IEC2FamilyComputation : BaseIEC2FamilyComputation, IEC2Family
	{
	}
}