/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.configure.TRANS;

namespace org.hpcshelf.blas.configure.trans.NA
{
	public interface BaseNA : BaseITRANS, IQualifierKind 
	{
	}
}