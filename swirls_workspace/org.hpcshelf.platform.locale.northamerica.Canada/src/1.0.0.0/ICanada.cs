using org.hpcshelf.platform.locale.NorthAmerica;

namespace org.hpcshelf.platform.locale.northamerica.Canada
{
	public interface ICanada : BaseICanada, INorthAmerica
	{
	}
}