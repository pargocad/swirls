/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace org.hpcshelf.mpi.IntercommunicatorPortType
{
	public interface BaseIIntercommunicatorPortType : BaseIEnvironmentPortTypeMultiplePartner, IQualifierKind 
	{
	}
}