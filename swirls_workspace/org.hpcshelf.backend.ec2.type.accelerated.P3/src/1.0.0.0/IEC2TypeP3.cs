using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.accelerated.P3
{
	public interface IEC2TypeP3 : BaseIEC2TypeP3, IEC2Type<family.Acceleration.IEC2FamilyAcceleration>
	{
	}
}