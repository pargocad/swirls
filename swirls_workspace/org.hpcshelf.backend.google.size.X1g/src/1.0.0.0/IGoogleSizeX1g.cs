using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X1g
{
 public interface IGoogleSizeX1g : BaseIGoogleSizeX1g, IGoogleSize
 {
 }
}