/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;
using org.hpcshelf.backend.ec2.family.Acceleration;

namespace org.hpcshelf.backend.ec2.type.accelerated.F1
{
	public interface BaseIEC2TypeF1 : BaseIEC2Type<IEC2FamilyAcceleration>, IQualifierKind 
	{
	}
}