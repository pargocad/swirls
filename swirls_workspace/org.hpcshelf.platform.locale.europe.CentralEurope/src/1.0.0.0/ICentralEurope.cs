using org.hpcshelf.platform.locale.Europe;

namespace org.hpcshelf.platform.locale.europe.CentralEurope
{
	public interface ICentralEurope : BaseICentralEurope, IEurope
	{
	}
}