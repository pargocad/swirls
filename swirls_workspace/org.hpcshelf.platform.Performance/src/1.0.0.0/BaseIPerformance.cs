/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntUp;

namespace org.hpcshelf.platform.Performance
{
	public interface BaseIPerformance<P0, P1, P2> : IQualifierKind 
		where P0:IntUp
		where P1:IntUp
		where P2:IntUp
	{
	}
}