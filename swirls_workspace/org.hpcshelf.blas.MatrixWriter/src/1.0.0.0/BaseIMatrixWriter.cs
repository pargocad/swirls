/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect;
using org.hpcshelf.blas.MatrixPortType;
using org.hpcshelf.types.Data;
using org.hpcshelf.blas.data.DataDistribution;
using org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner;

namespace org.hpcshelf.blas.MatrixWriter
{
	public interface BaseIMatrixWriter<DIST, T> : IComputationKind 
		where DIST:IDataDistribution
		where T:IData
	{
		IBindingDirect<IMatrixPortType<T, DIST>, IEnvironmentPortTypeSinglePartner> Matrix {get;}
	}
}