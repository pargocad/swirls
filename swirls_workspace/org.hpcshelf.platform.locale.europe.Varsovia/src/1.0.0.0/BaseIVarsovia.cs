/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.europe.Poland;

namespace org.hpcshelf.platform.locale.europe.Varsovia
{
	public interface BaseIVarsovia : BaseIPoland, IQualifierKind 
	{
	}
}