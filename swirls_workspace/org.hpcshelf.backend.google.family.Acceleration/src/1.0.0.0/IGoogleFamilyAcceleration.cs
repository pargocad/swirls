using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.family.Family;

namespace org.hpcshelf.backend.google.family.Acceleration
{
 public interface IGoogleFamilyAcceleration : BaseIGoogleFamilyAcceleration, IGoogleFamily
 {
 }
}