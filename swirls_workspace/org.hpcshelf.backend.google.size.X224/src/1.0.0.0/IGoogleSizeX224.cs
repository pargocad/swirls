using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X224
{
 public interface IGoogleSizeX224 : BaseIGoogleSizeX224, IGoogleSize
 {
 }
}