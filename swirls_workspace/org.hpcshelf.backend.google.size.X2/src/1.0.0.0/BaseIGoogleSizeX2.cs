/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X2
{
 public interface BaseIGoogleSizeX2 : BaseIGoogleSize, IQualifierKind 
 {
 }
}