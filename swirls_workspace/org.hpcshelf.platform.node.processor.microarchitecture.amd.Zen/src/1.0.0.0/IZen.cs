using org.hpcshelf.platform.node.processor.Microarchitecture;

namespace org.hpcshelf.platform.node.processor.microarchitecture.amd.Zen
{
	public interface IZen : BaseIZen, IProcessorMicroarchitecture<org.hpcshelf.platform.node.processor.manufacturer.AMD.IAMD>
	{
	}
}