/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.northamerica.UnitedStates;

namespace org.hpcshelf.platform.locale.northamerica.US_Central
{
	public interface BaseIUS_Central : BaseIUnitedStates, IQualifierKind 
	{
	}
}