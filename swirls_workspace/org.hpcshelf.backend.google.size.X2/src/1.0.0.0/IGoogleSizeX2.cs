using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X2
{
 public interface IGoogleSizeX2 : BaseIGoogleSizeX2, IGoogleSize
 {
 }
}