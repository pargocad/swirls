using org.hpcshelf.kinds;
using org.hpcshelf.types.Data;
using System;

namespace org.hpcshelf.types.SComplex { 

	public interface ISComplex : BaseISComplex, IData
	{
		ISComplexInstance newInstance(double d);
	} // end main interface 

	public interface ISComplexInstance : IDataInstance, ICloneable
	{
		double Value { set; get; }
	}

} // end namespace 
