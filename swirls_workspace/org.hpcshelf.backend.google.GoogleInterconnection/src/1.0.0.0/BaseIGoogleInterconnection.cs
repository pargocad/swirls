/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.interconnection.Topology;
using org.hpcshelf.platform.Interconnection;

namespace org.hpcshelf.backend.google.GoogleInterconnection
{
	public interface BaseIGoogleInterconnection<STT, BAN, TOP, NLT> : BaseIInterconnection<STT, NLT, BAN, TOP>, IQualifierKind 
		where STT:IntDown
		where BAN:IntUp
		where TOP:ITopology
		where NLT:IntDown
	{
	}
}