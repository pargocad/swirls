using org.hpcshelf.kinds;
using org.hpcshelf.types.Data;
using System;

namespace org.hpcshelf.types.Double { 

	public interface IDouble : BaseIDouble, IData
	{
		IDoubleInstance newInstance(double d);
	} // end main interface 

	public interface IDoubleInstance : IDataInstance, ICloneable
	{
		double Value { set; get; }
	}

} // end namespace 
