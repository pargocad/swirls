/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Manufacturer;
using org.hpcshelf.platform.node.processor.Family;

namespace org.hpcshelf.platform.node.processor.Series
{
	public interface BaseIProcessorSeries<MAN, FAM> : IQualifierKind 
		where MAN:IProcessorManufacturer
		where FAM:IProcessorFamily<MAN>
	{
	}
}