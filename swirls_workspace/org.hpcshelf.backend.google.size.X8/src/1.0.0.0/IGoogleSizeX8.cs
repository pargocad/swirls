using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X8
{
 public interface IGoogleSizeX8 : BaseIGoogleSizeX8, IGoogleSize
 {
 }
}