using org.hpcshelf.platform.node.accelerator.Architecture;

namespace org.hpcshelf.platform.node.accelerator.architecture.Maxwell
{
	public interface IMaxwell : BaseIMaxwell, IAcceleratorArchitecture
	{
	}
}