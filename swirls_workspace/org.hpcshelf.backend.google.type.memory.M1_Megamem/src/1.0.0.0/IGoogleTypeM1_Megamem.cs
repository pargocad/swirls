using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.memory.M1_Megamem
{
 public interface IGoogleTypeM1_Megamem : BaseIGoogleTypeM1_Megamem, IGoogleType<family.Memory.IGoogleFamilyMemory>
 {
 }
}