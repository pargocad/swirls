using org.hpcshelf.platform.locale.northamerica.UnitedStates;

namespace org.hpcshelf.platform.locale.northamerica.US_East
{
	public interface IUS_East : BaseIUS_East, IUnitedStates
	{
	}
}