using org.hpcshelf.platform.locale.europe.CentralEurope;

namespace org.hpcshelf.platform.locale.europe.France
{
	public interface IFrance : BaseIFrance, ICentralEurope
	{
	}
}