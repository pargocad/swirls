using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.Small
{
 public interface IGoogleSizeSmall : BaseIGoogleSizeSmall, IGoogleSize
 {
 }
}