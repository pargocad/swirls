using org.hpcshelf.kinds;
using org.hpcshelf.types.Data;
using System;

namespace org.hpcshelf.types.DFloat { 

	public interface IDFloat : BaseIDFloat, IData
	{
		IDFloatInstance newInstance(double d);
	} // end main interface 

	public interface IDFloatInstance : IDataInstance, ICloneable
	{
		double Value { set; get; }
	}

} // end namespace 
