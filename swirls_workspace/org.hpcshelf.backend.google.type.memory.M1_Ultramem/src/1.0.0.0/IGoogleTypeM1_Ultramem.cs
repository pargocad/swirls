using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.memory.M1_Ultramem
{
 public interface IGoogleTypeM1_Ultramem : BaseIGoogleTypeM1_Ultramem, IGoogleType<family.Memory.IGoogleFamilyMemory>
 {
 }
}