/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.asiapacific.India;

namespace org.hpcshelf.platform.locale.asiapacific.Mumbai
{
	public interface BaseIMumbai : BaseIIndia, IQualifierKind 
	{
	}
}