/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.common.BrowserPortType;
using org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect;

namespace org.hpcshelf.common.BrowserBinding
{
	public interface BaseIBrowserBinding<S> : BaseIBindingDirect<org.hpcshelf.common.BrowserPortType.IBrowserPortType, S>, ISynchronizerKind 
		where S:IBrowserPortType
	{
	}
}