/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.node.processor.cache.Mapping;
using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.platform.node.processor.Cache;

namespace org.hpcshelf.platform.node.processor.Core
{
	public interface BaseICore<NCT, CLK, TPC, MAP1i, SIZ1i, LAT1i, LINSIZ1i, CL1i, MAP1d, SIZ1d, LAT1d, LINSIZ1d, CL1d, MAP2, SIZ2, LAT2, LINSIZ2, CL2> : IQualifierKind 
		where NCT:IntUp
		where CLK:IntUp
		where TPC:IntUp
		where MAP1i:ICacheMapping
		where SIZ1i:IntUp
		where LAT1i:IntDown
		where LINSIZ1i:IntUp
		where CL1i:ICache<MAP1i, SIZ1i, LAT1i, LINSIZ1i>
		where MAP1d:ICacheMapping
		where SIZ1d:IntUp
		where LAT1d:IntDown
		where LINSIZ1d:IntUp
		where CL1d:ICache<MAP1d, SIZ1d, LAT1d, LINSIZ1d>
		where MAP2:ICacheMapping
		where SIZ2:IntUp
		where LAT2:IntDown
		where LINSIZ2:IntUp
		where CL2:ICache<MAP2, SIZ2, LAT2, LINSIZ2>
	{
	}
}