using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.memory.R5dn
{
	public interface IEC2TypeR5dn : BaseIEC2TypeR5dn, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}