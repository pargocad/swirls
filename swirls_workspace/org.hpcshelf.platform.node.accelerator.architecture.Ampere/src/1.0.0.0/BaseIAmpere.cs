/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.accelerator.Architecture;

namespace org.hpcshelf.platform.node.accelerator.architecture.Ampere
{
	public interface BaseIAmpere : BaseIAcceleratorArchitecture, IQualifierKind 
	{
	}
}