using org.hpcshelf.platform.node.processor.cache.Mapping;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.quantifier.IntDown;

namespace org.hpcshelf.platform.node.processor.Cache
{
	public interface ICache<MAP, SIZ, LAT, LINSIZ> : BaseICache<MAP, SIZ, LAT, LINSIZ>
		where MAP:ICacheMapping
		where SIZ:IntUp
		where LAT:IntDown
		where LINSIZ:IntUp
	{
	}
}