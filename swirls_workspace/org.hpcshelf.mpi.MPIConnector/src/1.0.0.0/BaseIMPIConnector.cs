/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.mpi.Language;
using org.hpcshelf.mpi.IntercommunicatorPortType;

namespace org.hpcshelf.mpi.MPIConnector
{
	public interface BaseIMPIConnector<L,S> : ISynchronizerKind 
		where L:ILanguage
		where S:IIntercommunicatorPortType
	{
	}
}