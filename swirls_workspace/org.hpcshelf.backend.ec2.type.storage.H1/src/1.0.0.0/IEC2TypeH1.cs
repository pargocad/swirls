using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;
using org.hpcshelf.backend.ec2.family.Storage;

namespace org.hpcshelf.backend.ec2.type.storage.H1
{
	public interface IEC2TypeH1 : BaseIEC2TypeH1, IEC2Type<IEC2FamilyStorage>
	{
	}
}