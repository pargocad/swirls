using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X160
{
 public interface IGoogleSizeX160 : BaseIGoogleSizeX160, IGoogleSize
 {
 }
}