using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.general.T3
{
	public interface IEC2TypeT3 : BaseIEC2TypeT3, IEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>
	{
	}
}