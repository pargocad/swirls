using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.node.processor.cache.mapping.Direct;

namespace org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative
{
	public interface ICacheMappingSetAssociative<K> : BaseICacheMappingSetAssociative<K>, ICacheMappingDirect
		where K:IntUp
	{
	}
}