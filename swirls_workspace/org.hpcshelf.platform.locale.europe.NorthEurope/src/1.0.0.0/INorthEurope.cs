using org.hpcshelf.platform.locale.Europe;

namespace org.hpcshelf.platform.locale.europe.NorthEurope
{
	public interface INorthEurope : BaseINorthEurope, IEurope
	{
	}
}