using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.quantifier.DecimalDown;
using org.hpcshelf.platform.locale.northamerica.Ohio;
using org.hpcshelf.backend.ec2.platform.general.EC2_T3_Nano;

namespace org.hpcshelf.backend.ec2.impl.platform.general.EC2_T3_Nano_Ohio
{
	public class IEC2_T3_Nano_Ohio<NCT, NOD_STO_SIZ, CPH, LOC> : BaseIEC2_T3_Nano_Ohio<NCT, NOD_STO_SIZ, CPH, LOC>, IProcessingNode<NCT, NOD_STO_SIZ, CPH, LOC>
		where NCT:IntUp
		where NOD_STO_SIZ:IntUp
		where CPH:DecimalDown
		where LOC:IOhio
	{
	}
}
