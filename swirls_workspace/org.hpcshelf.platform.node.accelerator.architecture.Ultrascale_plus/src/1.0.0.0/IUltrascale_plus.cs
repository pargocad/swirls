using org.hpcshelf.platform.node.accelerator.Architecture;

namespace org.hpcshelf.platform.node.accelerator.architecture.Ultrascale_plus
{
	public interface IUltrascale_plus : BaseIUltrascale_plus, IAcceleratorArchitecture
	{
	}
}