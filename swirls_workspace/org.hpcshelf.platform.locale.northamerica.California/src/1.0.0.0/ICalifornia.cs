using org.hpcshelf.platform.locale.northamerica.US_West;

namespace org.hpcshelf.platform.locale.northamerica.California
{
	public interface ICalifornia : BaseICalifornia, IUS_West
	{
	}
}