using org.hpcshelf.common.BrowserPortType;

namespace org.hpcshelf.common.BrowserConnector
{
	public interface IBrowserRoot<S0> : BaseIBrowserRoot<S0>
		where S0:IBrowserPortType
	{
	}
}