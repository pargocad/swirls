using org.hpcshelf.blas.matrixtype.General;

namespace org.hpcshelf.blas.matrixtype.Hermitian
{
	public interface IHermitianMatrix : BaseIHermitianMatrix, IGeneralMatrix
	{
	}
}