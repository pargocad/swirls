using org.hpcshelf.platform.locale.europe.WestEurope;

namespace org.hpcshelf.platform.locale.europe.Swiss
{
	public interface ISwiss : BaseISwiss, IWestEurope
	{
	}
}