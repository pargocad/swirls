/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.Language;

namespace br.ufc.mdcc.hpcshelf.mpi.language.C
{
	public interface BaseILanguage_C : BaseILanguage, IQualifierKind 
	{
	}
}