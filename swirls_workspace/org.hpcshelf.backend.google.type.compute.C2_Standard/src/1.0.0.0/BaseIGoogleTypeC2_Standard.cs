/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.compute.C2_Standard
{
 public interface BaseIGoogleTypeC2_Standard : BaseIGoogleType<family.Computation.IGoogleFamilyComputation>, IQualifierKind 
 {
 }
}