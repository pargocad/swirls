using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large24x
{
	public interface IEC2SizeLarge24x : BaseIEC2SizeLarge24x, IEC2Size
	{
	}
}