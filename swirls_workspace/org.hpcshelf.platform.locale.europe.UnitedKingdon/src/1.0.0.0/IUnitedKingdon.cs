using org.hpcshelf.platform.locale.europe.WestEurope;

namespace org.hpcshelf.platform.locale.europe.UnitedKingdon
{
	public interface IUnitedKingdon : BaseIUnitedKingdon, IWestEurope
	{
	}
}