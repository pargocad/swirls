using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.quantifier.IntDown;

namespace org.hpcshelf.platform.node.Storage
{
	public interface IStorage<SIZ, LAT, BAND, NETBAND> : BaseIStorage<SIZ, LAT, BAND, NETBAND>
		where SIZ:IntUp
		where LAT:IntDown
		where BAND:IntUp
		where NETBAND:IntUp
	{
	}
}