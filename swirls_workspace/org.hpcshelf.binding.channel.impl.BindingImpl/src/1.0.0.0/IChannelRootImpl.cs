using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using org.hpcshelf.binding.channel.Binding;
using org.hpcshelf.kinds;
using MPI;
using EnvelopType = System.Tuple<int, int, int, int, int, int>;

namespace org.hpcshelf.binding.channel.impl.BindingImpl
{
    public class IChannelRootImpl : BaseIChannelRootImpl, IChannelRoot
    {
        public const int TAG_SEND_OPERATION = 999;
        public const int TAG_REPLY = 998;

        private IDictionary<int, Thread> thread_receive_requests = null;

        public override void release1()
        {
            base.release1();
        }

        public override void release2()
        {
            base.release2();
            Console.WriteLine("CHANNEL BINDING - RELEASE2 1");
            foreach (Thread t in thread_receive_requests.Values)
                t.Abort();
            Console.WriteLine("CHANNEL BINDING - RELEASE2 2");
            foreach (Socket s in server_socket_facet.Values)
                s.Close();
            Console.WriteLine("CHANNEL BINDING - RELEASE2 3");
            foreach (Socket s in client_socket_facet.Values)
            {
                s.Disconnect(false);
                s.Close();
            }
            Console.WriteLine("CHANNEL BINDING - RELEASE2 4");
        }

        #region implemented abstract members of BindingRoot
        public override void server()
        {
            this.TraceFlag = true;

            Trace.WriteLineIf(this.TraceFlag == true, this.GlobalRank + ": BEFORE CREATE SOCKETS !!! " + this.Facet + " / " + this.FacetInstance + " : " + this.CID.getInstanceName());

            createSockets();

            Trace.WriteLineIf(this.TraceFlag == true, this.GlobalRank + ": AFTER CREATED SOCKETS !!! " + this.Facet + " / " + this.FacetInstance + " : " + this.CID.getInstanceName());

            Synchronizer_monitor = new SynchronizerMonitor(this, client_socket_facet, server_socket_facet_ack, this.FacetInstance, this.GlobalRank, this.CID.getInstanceName());

            sockets_initialized_flag.Set();

            // Create the threads that will listen the sockets for each other facet.
            thread_receive_requests = new Dictionary<int, Thread>();

            foreach (int facet in this.Facet.Keys)
            {
                // * if (facet != this.FacetInstance)
                {
                    Trace.WriteLineIf(this.TraceFlag == true, "loop create thread_receive_requests: " + facet + " / " + this.FacetInstance);
                    Socket server_socket = server_socket_facet[facet];
                    Socket client_socket_ack = client_socket_facet_ack[facet];
                    thread_receive_requests[facet] = new Thread(new ThreadStart(() => Synchronizer_monitor.serverReceiveRequests(facet, client_socket_ack, server_socket)));
                    thread_receive_requests[facet].Start();
                }
            }

            while (true)
            {
                Thread.Sleep(100);
                Synchronizer_monitor.serverReadRequest();
            }

        }

        private AutoResetEvent sockets_initialized_flag = new AutoResetEvent(false);

        public override void client()
        {
            this.TraceFlag = true;

            //while (!sockets_initialized_flag) Thread.Sleep (100);
            sockets_initialized_flag.WaitOne();

            Trace.WriteLineIf(this.TraceFlag == true, "GO LISTEN WORKERS !!!");
            ManualResetEvent seq = new ManualResetEvent(true);
            while (true)
            {                
                seq = listen_worker(seq);
            }
        }
        #endregion

        private SynchronizerMonitor synchronizer_monitor;

        private IDictionary<int, Socket> client_socket_facet = new Dictionary<int, Socket>();
        private IDictionary<int, Socket> server_socket_facet = new Dictionary<int, Socket>();

        private IDictionary<int, Socket> client_socket_facet_ack = new Dictionary<int, Socket>();
		private IDictionary<int, Socket> server_socket_facet_ack = new Dictionary<int, Socket>();

        private void connectSockets()
        {
            foreach (KeyValuePair<int, FacetAccess> facet_access in this.Facet)
            {
                int facet = facet_access.Key;
                Socket socket = client_socket_facet[facet];
                IPEndPoint endPoint = end_point_client[facet];

                Socket socket_ack = client_socket_facet_ack[facet];
                IPEndPoint endPoint_ack = end_point_client_ack[facet];

                bool isConnected = false;
                while (!isConnected)
                {
                    try
                    {
                        Trace.WriteLineIf(this.TraceFlag == true, "CONNECTING " + "facet=" + facet + " / " + endPoint + " / " + this.CID.getInstanceName());
                        socket.Connect(endPoint);
                        Trace.WriteLineIf(this.TraceFlag == true, "CONNECTED " + "facet=" + facet + " / " + endPoint + " / " + this.CID.getInstanceName());

                        isConnected = true;
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLineIf(this.TraceFlag == true, "CONNECTION FAILED N --- " + e.Message + " --- " + endPoint);
                        Thread.Sleep(1000);
                    }
                }

				if (!isConnected)
				{
                    Trace.WriteLineIf(this.TraceFlag == true, "createSockets --- It was not possible to talk to the server --- endPoint=" + endPoint);
					throw new Exception("createSockets --- It was not possible to talk to the server");
				}
				
                bool isConnected_ack = false;
                while (!isConnected_ack)
                {
                    try
                    {
                        Trace.WriteLineIf(this.TraceFlag == true, "CONNECTING ACK" + "facet=" + facet + " / " + endPoint_ack + " / " + this.CID.getInstanceName());
                        socket_ack.Connect(endPoint_ack);
                        Trace.WriteLineIf(this.TraceFlag == true, "CONNECTED ACK" + "facet=" + facet + " / " + endPoint_ack + " / " + this.CID.getInstanceName());

                        isConnected_ack = true;
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLineIf(this.TraceFlag == true, "CONNECTION ACK FAILED N --- " + e.Message + " --- " + endPoint_ack);
                        Thread.Sleep(1000);
                    }
                }
                if (!isConnected_ack)
                {
                    Trace.WriteLineIf(this.TraceFlag == true, "createSockets ACK --- It was not possible to talk to the server --- endPoint_ack=" + endPoint_ack);
                    throw new Exception("createSockets ACK --- It was not possible to talk to the server");
                }

            }

        }

        private void acceptSockets()
        {
            foreach (KeyValuePair<int, FacetAccess> facet_access in this.Facet)
            {
                int facet = facet_access.Key;
                // * if (facet != this.FacetInstance)
                {
                    Socket socket = server_socket_facet[facet];
                    IPEndPoint endPoint = end_point_server[facet];

					Socket socket_ack = server_socket_facet_ack[facet];
					IPEndPoint endPoint_ack = end_point_server_ack[facet];
					
                    Trace.WriteLineIf(this.TraceFlag == true, "BINDING " + endPoint + " -- " + facet + " / " + this.CID.getInstanceName());
                    socket.Bind(endPoint);
                    socket.Listen(10);
                    server_socket_facet[facet] = socket.Accept();
                    Trace.WriteLineIf(this.TraceFlag == true, "BINDED " + endPoint + " -- " + facet + " / " + this.CID.getInstanceName());

					Trace.WriteLineIf(this.TraceFlag == true, "BINDING ACK" + endPoint_ack + " -- " + facet + " / " + this.CID.getInstanceName());
					socket_ack.Bind(endPoint_ack);
					socket_ack.Listen(10);
					server_socket_facet_ack[facet] = socket_ack.Accept();
					Trace.WriteLineIf(this.TraceFlag == true, "BINDED ACK " + endPoint_ack + " -- " + facet + " / " + this.CID.getInstanceName());
				}
            }
        }

        private IDictionary<int, IPEndPoint> end_point_client = new Dictionary<int, IPEndPoint>();
        private IDictionary<int, IPEndPoint> end_point_server = new Dictionary<int, IPEndPoint>();
		
        private IDictionary<int, IPEndPoint> end_point_client_ack = new Dictionary<int, IPEndPoint>();
		private IDictionary<int, IPEndPoint> end_point_server_ack = new Dictionary<int, IPEndPoint>();

		private void createSockets()
        {
            SortedList l = new SortedList();
            foreach (int facet_instance in this.Facet.Keys)
                l.Add(facet_instance, facet_instance);

            int[] u = new int[l.Count];
            l.GetKeyList().CopyTo(u, 0);

            IDictionary<int, int> u_inv = new Dictionary<int, int>();
            for (int i = 0; i < u.Length; i++)
                u_inv[u[i]] = i*2;

            FacetAccess facet_access_server = this.Facet[FacetInstance];
			string ip_address_server = facet_access_server.ip_address;

			foreach (KeyValuePair<int, FacetAccess> facet_access_client in this.Facet)
            {
                int facet_instance = facet_access_client.Key;
                // * if (facet_instance != this.FacetInstance)
                {
                    string ip_address_client = facet_access_client.Value.ip_address;

                    Console.WriteLine("ip_address_client={0} ---- ip_address_server={1} ", ip_address_client, ip_address_server);
                    if (ip_address_client.Equals(ip_address_server))
                        ip_address_client = "127.0.0.1";

					int port_client = facet_access_client.Value.port + u_inv[this.FacetInstance];
                    IPAddress ipAddress_client = Dns.GetHostAddresses(ip_address_client)[0]; // ipHostInfo_client.AddressList[0];
                    IPEndPoint endPoint_client = new IPEndPoint(ipAddress_client, port_client);
                    end_point_client[facet_instance] = endPoint_client;

					int port_client_ack = facet_access_client.Value.port + u_inv[this.FacetInstance] + 1;
					IPAddress ipAddress_client_ack = Dns.GetHostAddresses(ip_address_client)[0]; // ipHostInfo_client.AddressList[0];
					IPEndPoint endPoint_client_ack = new IPEndPoint(ipAddress_client_ack, port_client_ack);
					end_point_client_ack[facet_instance] = endPoint_client_ack;
					
                    Trace.WriteLineIf(this.TraceFlag == true, "CREATE SOCKETS - end_point_client[" + facet_instance + "]=" + endPoint_client);
					Trace.WriteLineIf(this.TraceFlag == true, "CREATE SOCKETS - end_point_client_ack[" + facet_instance + "]=" + endPoint_client_ack);

					int port_server = facet_access_server.port + u_inv[facet_instance];
                    IPEndPoint endPoint_server = new IPEndPoint(IPAddress.Any, port_server);
                    end_point_server[facet_instance] = endPoint_server;

                    int port_server_ack = facet_access_server.port + u_inv[facet_instance] + 1;
					IPEndPoint endPoint_server_ack = new IPEndPoint(IPAddress.Any, port_server_ack);
					end_point_server_ack[facet_instance] = endPoint_server_ack;
					
                    Trace.WriteLineIf(this.TraceFlag == true, "CREATE SOCKETS - end_point_server[" + facet_instance + "]=" + endPoint_server);
					Trace.WriteLineIf(this.TraceFlag == true, "CREATE SOCKETS - end_point_server_ack[" + facet_instance + "]=" + endPoint_server_ack);

					// Create a TCP/IP client socket.
                    Socket client_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    Socket client_socket_ack = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

					// Create a TCP/IP server socket.
					Socket server_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
					Socket server_socket_ack = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

					server_socket.SendTimeout = server_socket.ReceiveTimeout = client_socket.SendTimeout = client_socket.ReceiveTimeout = -1;
                    client_socket_facet[facet_instance] = client_socket;
                    server_socket_facet[facet_instance] = server_socket;

					server_socket_ack.SendTimeout = server_socket_ack.ReceiveTimeout = client_socket_ack.SendTimeout = client_socket_ack.ReceiveTimeout = -1;
					client_socket_facet_ack[facet_instance] = client_socket_ack;
					server_socket_facet_ack[facet_instance] = server_socket_ack;
				}
            }

            Thread thread_connect_sockets = new Thread(new ThreadStart(connectSockets));
            Thread thread_accept_sockets = new Thread(new ThreadStart(acceptSockets));

            thread_connect_sockets.Start();
            thread_accept_sockets.Start();

            Trace.WriteLineIf(this.TraceFlag == true, "CREATE_SOCKETS - connectSockets and acceptSockets launched !!!");

            thread_connect_sockets.Join();
            thread_accept_sockets.Join();

            Trace.WriteLineIf(this.TraceFlag == true, "CREATE_SOCKETS - connectSockets and acceptSockets finished !!!");
        }



        private ManualResetEvent listen_worker(ManualResetEvent previous_seq)
        {
            Tuple<int, int> operation;
            MPI.CompletedStatus status = null;

            Trace.WriteLineIf(this.TraceFlag == true, "listen_workers - WAITING ... " + MPI.Environment.Threading);

            //lock (lock_recv)
            Receive(this.RootCommunicator, MPI.Communicator.anySource, TAG_SEND_OPERATION, out operation, out status);
            
            ManualResetEvent seq = new ManualResetEvent(false);

            Trace.WriteLineIf(this.TraceFlag == true, "listen_workers - RECEIVED FROM WORKER source=" + status.Source + ", tag=" + status.Tag + " / operation = " + operation);

            switch (operation.Item1)
            {
                case AliencommunicatorOperation.SEND:
                case AliencommunicatorOperation.SEND_ARRAY:
                    (new Thread((object t) => handle_SEND(operation, status, ((Tuple<ManualResetEvent,ManualResetEvent>)t).Item1, ((Tuple<ManualResetEvent,ManualResetEvent>)t).Item2))).Start(new Tuple<ManualResetEvent,ManualResetEvent>(previous_seq, seq));
                    break;
                case AliencommunicatorOperation.SYNC_SEND:
                    (new Thread((object t) => handle_SYNCSEND(operation, status, ((Tuple<ManualResetEvent,ManualResetEvent>)t).Item1, ((Tuple<ManualResetEvent,ManualResetEvent>)t).Item2))).Start(new Tuple<ManualResetEvent,ManualResetEvent>(previous_seq, seq));
                    break;
                case AliencommunicatorOperation.RECEIVE:
                case AliencommunicatorOperation.RECEIVE_ARRAY:
                    (new Thread((object t) => handle_RECEIVE(operation, status, ((Tuple<ManualResetEvent,ManualResetEvent>)t).Item1, ((Tuple<ManualResetEvent,ManualResetEvent>)t).Item2))).Start(new Tuple<ManualResetEvent,ManualResetEvent>(previous_seq, seq));
                    break;
                case AliencommunicatorOperation.PROBE:
                    (new Thread(() => handle_PROBE(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.ALL_GATHER:
                    (new Thread(() => handle_ALL_GATHER(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.ALL_GATHER_FLATTENED:
                    (new Thread(() => handle_ALL_GATHER_FLATTENED(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.ALL_REDUCE:
                    handle_ALL_REDUCE(operation, status);
                    break;
                case AliencommunicatorOperation.ALL_REDUCE_ARRAY:
                    (new Thread(() => handle_ALL_REDUCE(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.ALL_TO_ALL:
                    (new Thread(() => handle_ALL_TO_ALL(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.ALL_TO_ALL_FLATTENED:
                    (new Thread(() => handle_ALL_TO_ALL_FLATTENED(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.REDUCE_SCATTER:
                    (new Thread(() => handle_REDUCE_SCATTER(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.BROADCAST:
                    (new Thread(() => handle_BROADCAST(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.BROADCAST_ARRAY:
                    (new Thread(() => handle_BROADCAST(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.SCATTER:
                    handle_SCATTER(operation, status);
                    break;
                case AliencommunicatorOperation.SCATTER_FROM_FLATTENED:
                    (new Thread(() => handle_SCATTER_FROM_FLATTENED(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.GATHER:
                    (new Thread(() => handle_GATHER(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.GATHER_FLATTENED:
                    (new Thread(() => handle_GATHER_FLATTENED(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.REDUCE:
                    (new Thread(() => handle_REDUCE(operation, status))).Start();
                    break;
                case AliencommunicatorOperation.REDUCE_ARRAY:
                    (new Thread(() => handle_REDUCE(operation, status))).Start();
                    break;
                default:
                    Trace.WriteLineIf(this.TraceFlag == true, "UNRECOGNIZED OPERATION");
                    throw new ArgumentOutOfRangeException();
            }
            
            return seq;
        }

        protected unsafe void Send(Intercommunicator root_comm, byte[] v1, int source, int tag)
        {
            int size = v1.Length - 24;

        //    int outbuf_size = 1 * sizeof(int) + size * sizeof(byte);
        //    IntPtr outbuf = Marshal.AllocHGlobal(outbuf_size);

//            int position = 0;
//            Unsafe.MPI_Pack(new IntPtr(&size), 1, Unsafe.MPI_INT, outbuf, outbuf_size, ref position, root_comm.comm);

//            fixed (byte* b = &v1[0])
//            {
//                IntPtr pointer = new IntPtr(b);
//                Unsafe.MPI_Pack(pointer, size, Unsafe.MPI_BYTE, outbuf, outbuf_size, ref position, root_comm.comm);
//            }

            // v1 at offset 24
            fixed (byte* b = &v1[24])
            {
                IntPtr pointer = (IntPtr) b; // new IntPtr(b);
                //int errorCode = Unsafe.MPI_Send(outbuf, position, Unsafe.MPI_PACKED, source, tag, root_comm.comm);
                
                Console.WriteLine("{4}: Send --- Unsafe.MPI_Send({0}, 1, Unsafe.MPI_INT, {1}, {2}, {3}) - BEFORE", size, source, tag, root_comm.comm,  this.FacetInstance);
                errorCode = Unsafe.MPI_Send((IntPtr) (&size), 1, Unsafe.MPI_INT, source, tag, root_comm.comm);
                if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
                Console.WriteLine("{4}: Send --- Unsafe.MPI_Send({0}, 1, Unsafe.MPI_INT, {1}, {2}, {3}) - AFTER", size, source, tag, root_comm.comm, this.FacetInstance);
                
                Console.WriteLine("{4}: Send --- Unsafe.MPI_Send(pointer, {0}, Unsafe.MPI_BYTE, {1}, {2}, {3}); - BEFORE", size, source, tag, root_comm.comm, this.FacetInstance);
                errorCode = Unsafe.MPI_Send(pointer, size, Unsafe.MPI_BYTE, source, tag, root_comm.comm);
                if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);                
                Console.WriteLine("{4}: Send --- Unsafe.MPI_Send(pointer, {0}, Unsafe.MPI_BYTE, {1}, {2}, {3}); - AFTER", size, source, tag, root_comm.comm, this.FacetInstance);
            }



        //    Marshal.FreeHGlobal(outbuf);

       //     Console.WriteLine("SEND -------- #5 {0} {1} {2}", root_comm.comm, source, tag);

        }

        private Exception MPIException(int errorCode, int facet_instance)
        {
           Exception e = MPI.Environment.TranslateErrorIntoException(errorCode);
           Console.WriteLine("{0}: MPI Exception - MESSAGE = {1}", facet_instance, e.Message);
           Console.WriteLine("{0}: MPI Exception - STACK = {1}", facet_instance, e.StackTrace);
           if (e.InnerException != null)
           {
	           Console.WriteLine("{0}: MPI Inner Exception - MESSAGE = {1}", facet_instance, e.InnerException.Message);
	           Console.WriteLine("{0}: MPI Inner Exception - STACK = {1}", facet_instance, e.InnerException.StackTrace);
           }
           return e;
        }


        protected unsafe void Send(Intercommunicator root_comm, int source, int tag)
        {
            byte b = default(byte);
            errorCode = Unsafe.MPI_Send((IntPtr) (&b) /*new IntPtr(&b)*/, 1, Unsafe.MPI_BYTE, source, tag, root_comm.comm);
             if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
            

           // root_comm.Send<bool>(v1, source, tag);
        }

        // private int bufsize = 10000000;

        private int errorCode = default(int);
        
        protected unsafe void Receive(Intercommunicator root_comm, int source, int tag, out IntPtr buf, out int bufsize0)
        {
            
            //result = null;
            //IntPtr buf = default(IntPtr);
            try
            {
                Unsafe.MPI_Status status;

                Console.WriteLine("{1}: RECEIVE 0.1 {0} conversation_tag={2}", root_comm.comm, this.FacetInstance, tag);
                
                int bufsize; IntPtr ptr_bufsize = (IntPtr) (&bufsize);
                errorCode = Unsafe.MPI_Recv(ptr_bufsize, 1, Unsafe.MPI_INT, source, tag, root_comm.comm, out status);
                if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
                
                bufsize0 = bufsize;
                
                Console.WriteLine("{2}: RECEIVE 0.2 bufsize={1} {0}", root_comm.comm, bufsize, this.FacetInstance);
                buf = Marshal.AllocHGlobal(bufsize);
                Console.WriteLine("{2}: RECEIVE 1 bufsize={1} {0}", root_comm.comm, bufsize, this.FacetInstance);
                
                errorCode = Unsafe.MPI_Recv(buf, bufsize, Unsafe.MPI_PACKED, source, tag, root_comm.comm, out status);
                if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);

              //  int v1 = 0, v2 = 0, v3 = 0, size = 0;
              //
              //  Console.WriteLine("{1}: RECEIVE 2 {0}", root_comm.comm, this.FacetInstance);

              //  int position = 0;
              //  errorCode = Unsafe.MPI_Unpack(buf, bufsize, ref position, (IntPtr) (&v1) , 1, Unsafe.MPI_INT, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
              //  Console.WriteLine("{2}: RECEIVE 21 {0} - {1}", v1, root_comm.comm, this.FacetInstance);
              //   errorCode = Unsafe.MPI_Unpack(buf, bufsize, ref position, (IntPtr) (&v2) , 1, Unsafe.MPI_INT, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
              //  Console.WriteLine("{2}: RECEIVE 22 {0} - {1}", v2, root_comm.comm, this.FacetInstance);
             //   errorCode = Unsafe.MPI_Unpack(buf, bufsize, ref position, (IntPtr) (&v3) , 1, Unsafe.MPI_INT, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
             //   Console.WriteLine("{2}: RECEIVE 23 {0} - {1}", v3, root_comm.comm, this.FacetInstance);
             //   errorCode = Unsafe.MPI_Unpack(buf, bufsize, ref position, (IntPtr) (&size) , 1, Unsafe.MPI_INT, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
             //   Console.WriteLine("{2}: RECEIVE 24 {0} - {1}", size, root_comm.comm, this.FacetInstance);
               // byte[] data = new byte[size];
               // fixed (byte* b = &data[0])
               // {
               //     IntPtr pointer = (IntPtr) b; 
               //     errorCode = Unsafe.MPI_Unpack(buf, bufsize, ref position, pointer, size, Unsafe.MPI_BYTE, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
               //     Console.WriteLine("{3}: RECEIVE 25 -- {1} -- position={2} --  {0}", root_comm.comm, Marshal.ReadInt32(pointer), position, this.FacetInstance); 
               // }

               // result = new Tuple<int, int, int, byte[]>(v1, v2, v3, buf, position, size);
            }
            catch (Exception e)
            {
                Console.WriteLine("{1}: EXCEPTION (IChannelRootImpl/Receive): message={0}", e.Message, this.FacetInstance);
                Console.WriteLine("{1}: EXCEPTION (IChannelRootImpl/Receive): stack={0}", e.StackTrace, this.FacetInstance);
                if (e.InnerException != null)
                {
	                Console.WriteLine("{1}: INNER EXCEPTION (IChannelRootImpl/Receive): message={0}", e.InnerException.Message, this.FacetInstance);
	                Console.WriteLine("{1}: INNER EXCEPTION (IChannelRootImpl/Receive): stack={0}", e.InnerException.StackTrace, this.FacetInstance);
                }
                throw e;
            }
            finally
            {
               //if (buf != null)
               //   Marshal.FreeHGlobal(buf);
            }
        }

        protected void Receive(Intercommunicator root_comm, int source, int tag, out Tuple<int, int, int> result) 
        {
            int[] v = new int[3];
            root_comm.Receive<int>(source, tag, ref v);
            result = new Tuple<int, int, int>(v[0], v[1], v[2]);

            // root_comm.Receive<Tuple<int, int, int>>(source, tag, out result);
        }

        protected void Receive(Intercommunicator root_comm, int source, int tag, out Tuple<int, int> result, out MPI.CompletedStatus status1) 
        {
            int[] v = new int[2];
            root_comm.Receive<int>(source, tag, ref v, out status1);
            result = new Tuple<int,int>(v[0], v[1]);

            //root_comm.Receive<Tuple<int, int>> (MPI.Communicator.anySource, TAG_SEND_OPERATION, out result, out status1);
        }

        unsafe void handle_SEND(Tuple<int, int> operation, MPI.CompletedStatus status, ManualResetEvent seq0, ManualResetEvent seq1)
        {
            Trace.WriteLineIf(this.TraceFlag == true, this.FacetInstance + "/" + status.Source + ": handle_SEND 1 " + operation);

            //Tuple<int, int, int, IntPtr, int> operation_info;
            int conversation_tag = operation.Item2;
            //lock (lock_recv) 
            Intercommunicator root_comm = this.RootCommunicator;
            IntPtr buf;
            int bufsize;
            Receive(root_comm, status.Source, conversation_tag, out buf, out bufsize);

            Trace.WriteLineIf(this.TraceFlag == true, this.FacetInstance + "/" + status.Source + ": handle_SEND 2 --- operation = " + operation);

            int operation_type = operation.Item1;
            int facet_src = this.FacetInstance;
            int src = status.Source;

            int v1 = 0, v2 = 0, v3 = 0, size = 0;
              
            Console.WriteLine("{1}: RECEIVE 2 {0}", root_comm.comm, this.FacetInstance);

            int position = 0;
            errorCode = Unsafe.MPI_Unpack(buf, bufsize, ref position, (IntPtr) (&v1), 1, Unsafe.MPI_INT, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
            Console.WriteLine("{2}: RECEIVE 21 {0} - {1}", v1, root_comm.comm, this.FacetInstance);
            errorCode = Unsafe.MPI_Unpack(buf, bufsize, ref position, (IntPtr) (&v2), 1, Unsafe.MPI_INT, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
            Console.WriteLine("{2}: RECEIVE 22 {0} - {1}", v2, root_comm.comm, this.FacetInstance);
            errorCode = Unsafe.MPI_Unpack(buf, bufsize, ref position, (IntPtr) (&v3), 1, Unsafe.MPI_INT, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
            Console.WriteLine("{2}: RECEIVE 23 {0} - {1}", v3, root_comm.comm, this.FacetInstance);
            errorCode = Unsafe.MPI_Unpack(buf, bufsize, ref position, (IntPtr) (&size), 1, Unsafe.MPI_INT, root_comm.comm); if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this.FacetInstance);
            Console.WriteLine("{2}: RECEIVE 24 {0} - {1}", size, root_comm.comm, this.FacetInstance);

            int facet_dst = v1;
            int dst = v2;
            int tag = v3;

            EnvelopType envelop = new EnvelopType(operation_type, facet_src, facet_dst, src, dst, tag);

            IntPtr message1 = buf;
            int message1_length = size;

            Trace.WriteLineIf(this.TraceFlag == true, this.FacetInstance + "/" + status.Source + ": handle_SEND 3 --- " + facet_src + "," + facet_dst + "," + src + "," + dst + "," + tag + ", operation = " + operation);

            if (facet_src == facet_dst)
                handle_LocalMessage_SEND(envelop, message1, position, message1_length);
            else
            {
                Trace.WriteLineIf(this.TraceFlag == true, this.FacetInstance + "/" + status.Source + ": handle_SEND 4 --- operation = " + operation + " --- " + envelop.ToString());

                if (tag >= 0 /* tag */)
                    Synchronizer_monitor.clientSendRequest(envelop, message1, position, message1_length, seq0, seq1);
                else
                    Synchronizer_monitor.clientSendRequestAnyTag(envelop, message1, position, message1_length, seq0, seq1, ref tag);
            }

            Trace.WriteLineIf(this.TraceFlag == true, this.FacetInstance + "/" + status.Source + ": handle_SEND 5 --- operation = " + operation + " --- " + envelop.ToString());
            // Marshal.FreeHGlobal(buf);
        }

        void handle_SYNCSEND(Tuple<int, int> operation, MPI.CompletedStatus status, ManualResetEvent seq0, ManualResetEvent seq1)
        {
            handle_SEND(operation, status, seq0, seq1);
            Send(this.RootCommunicator, status.Source, TAG_REPLY);

        }

        //private object lock_recv = new object();

        void handle_RECEIVE(Tuple<int, int> operation, MPI.CompletedStatus status, ManualResetEvent seq0, ManualResetEvent seq1)
        {
            int conversation_tag = operation.Item2;
            Trace.WriteLineIf(this.TraceFlag == true, this.FacetInstance + "/" + status.Source + ": handle_RECEIVE 1 - source=" + status.Source + ", tag=" + conversation_tag);
            Tuple<int, int, int> operation_info;

            //lock (lock_recv)
            Receive(this.RootCommunicator, status.Source, conversation_tag, out operation_info);

            Trace.WriteLineIf(this.TraceFlag == true, this.FacetInstance + "/" + status.Source + ": handle_RECEIVE 2");

            int operation_type = operation.Item1;
            int facet_src = this.FacetInstance;
            int facet_dst = operation_info.Item1;
            int src = status.Source;
            int dst = operation_info.Item2;
            int tag = operation_info.Item3;

            EnvelopType envelop = new EnvelopType(operation_type, facet_src, facet_dst, src, dst, tag);

            Trace.WriteLineIf(this.TraceFlag == true, this.FacetInstance + "/" + status.Source + ": handle_RECEIVE 3 --- " + envelop.ToString());

            byte[] message2;

            int any_tag = tag;
            if (facet_src == facet_dst)
                message2 = handle_LocalMessage_RECV(envelop);
            else
            {
                message2 = tag < 0 ? Synchronizer_monitor.clientSendRequestAnyTag(envelop, IntPtr.Zero, 0, 0, seq0, seq1, ref any_tag) :
                                     Synchronizer_monitor.clientSendRequest(envelop, IntPtr.Zero, 0, 0, seq0, seq1);
            }

            Trace.WriteLineIf(this.TraceFlag == true, this.FacetInstance + "/" + status.Source + ": handle_RECEIVE 4 " + envelop.ToString() + " --- " + (message2 == null) + " *** tag=" + tag);

            //lock (lock_recv)
            // this.RootCommunicator.Send<int>(message2.Length, src, tag < 0 ? any_tag : conversation_tag);
            // this.RootCommunicator.Send<byte>(message2, src, tag < 0 ? any_tag : conversation_tag);
            Send(this.RootCommunicator, message2, src, tag < 0 ? any_tag : conversation_tag);

            Trace.WriteLineIf(this.TraceFlag == true, this.FacetInstance + "/" + status.Source + ": handle_RECEIVE 5" + envelop.ToString());

        }

        private object sync_local_msg = new object();

        private IDictionary<EnvelopKey, IDictionary<int, IList<Tuple<AutoResetEvent, byte[]>>>> local_message_request_send = new Dictionary<EnvelopKey, IDictionary<int, IList<Tuple<AutoResetEvent, byte[]>>>>();
        private IDictionary<EnvelopKey, IDictionary<int, IList<AutoResetEvent>>> local_message_request_recv = new Dictionary<EnvelopKey, IDictionary<int, IList<AutoResetEvent>>>();

        private byte[] handle_LocalMessage_RECV(EnvelopType envelop)
        {
            byte[] result = null;
            EnvelopKey envelop_key = new EnvelopKey(envelop);
            int tag = envelop.Item6;

            Console.WriteLine("handle_LocalMessage_RECV BEGIN --- {0}-{1}", envelop_key, tag);

            Monitor.Enter(sync_local_msg);
            try
            {
                AutoResetEvent sync = new AutoResetEvent(false);
                if (!local_message_request_recv.ContainsKey(envelop_key))
                    local_message_request_recv[envelop_key] = new Dictionary<int, IList<AutoResetEvent>>();

                if (!local_message_request_recv[envelop_key].ContainsKey(tag))
                    local_message_request_recv[envelop_key][tag] = new List<AutoResetEvent>();

                local_message_request_recv[envelop_key][tag].Add(sync);

                if (local_message_request_send.ContainsKey(envelop_key) && local_message_request_send[envelop_key].ContainsKey(tag))
                    local_message_request_send[envelop_key][tag][0].Item1.Set();
                else
                {
                    Monitor.Exit(sync_local_msg);
                    sync.WaitOne();
                    Monitor.Enter(sync_local_msg);
                }

                result = local_message_request_send[envelop_key][tag][0].Item2;

                local_message_request_send[envelop_key][tag].RemoveAt(0);
                if (local_message_request_send[envelop_key][tag].Count == 0)
                    local_message_request_send[envelop_key].Remove(tag);
                if (local_message_request_send[envelop_key].Count == 0)
                    local_message_request_send.Remove(envelop_key);

            }
            catch (Exception e)
            {
                Console.WriteLine("EXCEPTION in handle_LocalMessage_RECV");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);
                    Console.WriteLine(e.InnerException.StackTrace);
                }
            }
            finally
            {
                Monitor.Exit(sync_local_msg);
            }

            Console.WriteLine("handle_LocalMessage_RECV END --- {0}-{1}", envelop_key, tag);

            return result;
        }

        private void handle_LocalMessage_SEND(EnvelopType envelop, IntPtr buf, int position, int message_length)
        {
            IntPtr message_ptr = IntPtr.Add(buf, position);
            
            EnvelopKey envelop_key = new EnvelopKey(envelop);
            int tag = envelop.Item6;

            Console.WriteLine("handle_LocalMessage_SEND BEGIN --- {0}-{1}", envelop_key, tag);

            byte[] message = SynchronizerMonitor.MessageToByteArray(envelop, message_ptr, message_length);
			Marshal.FreeHGlobal(buf);

            Monitor.Enter(sync_local_msg);
            try
            {
                AutoResetEvent sync = new AutoResetEvent(false);
                if (!local_message_request_send.ContainsKey(envelop_key))
                    local_message_request_send[envelop_key] = new Dictionary<int, IList<Tuple<AutoResetEvent, byte[]>>>();

                if (!local_message_request_send[envelop_key].ContainsKey(tag))
                    local_message_request_send[envelop_key][tag] = new List<Tuple<AutoResetEvent, byte[]>>();

                local_message_request_send[envelop_key][tag].Add(new Tuple<AutoResetEvent, byte[]>(sync, message));

                if (local_message_request_recv.ContainsKey(envelop_key) && local_message_request_recv[envelop_key].ContainsKey(tag))
                    local_message_request_recv[envelop_key][tag][0].Set();
                else
                {
                    Monitor.Exit(sync_local_msg);
                    sync.WaitOne();
                    Monitor.Enter(sync_local_msg);
                }

                local_message_request_recv[envelop_key][tag].RemoveAt(0);
                if (local_message_request_recv[envelop_key][tag].Count == 0)
                    local_message_request_recv[envelop_key].Remove(tag);
                if (local_message_request_recv[envelop_key].Count == 0)
                    local_message_request_recv.Remove(envelop_key);
            }
            catch (Exception e)
            {
                Console.WriteLine("EXCEPTION in handle_LocalMessage_SEND");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);
                    Console.WriteLine(e.InnerException.StackTrace);
                }
            }
            finally
            {
                Monitor.Exit(sync_local_msg);
            }

            Console.WriteLine("handle_LocalMessage_SEND END --- {0}-{1}", envelop_key, tag);
        }

        void handle_PROBE(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        void handle_ALL_GATHER(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        void handle_ALL_GATHER_FLATTENED(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        void handle_ALL_REDUCE(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        void handle_ALL_TO_ALL(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        void handle_ALL_TO_ALL_FLATTENED(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        void handle_REDUCE_SCATTER(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        void handle_BROADCAST(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        void handle_SCATTER(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        void handle_SCATTER_FROM_FLATTENED(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        void handle_GATHER(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        void handle_GATHER_FLATTENED(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        void handle_REDUCE(Tuple<int, int> operation, MPI.CompletedStatus status)
        {
            throw new NotImplementedException();
        }

        #region IDisposable implementation
        private bool disposed = false;

		internal SynchronizerMonitor Synchronizer_monitor
        {
            get
            {
                if (synchronizer_monitor == null)
                {
                    Console.WriteLine("SYNCHRONIZER MONITOR --- WAIT BEFORE --- {0}", this.CID);
                    synchronizer_monitor_initialized.WaitOne();
                    Console.WriteLine("SYNCHRONIZER MONITOR --- WAIT AFTER  --- {0}", this.CID);
                }
                return synchronizer_monitor;
            }
            set
            {
                synchronizer_monitor = value;
                synchronizer_monitor_initialized.Set();
                Console.WriteLine("SYNCHRONIZER MONITOR --- SET --- {0}", this.CID);
            }
        }

		private ManualResetEvent synchronizer_monitor_initialized = new ManualResetEvent(false);

		protected override void Dispose(bool disposing)
        {

            if (!disposed)
            {
                if (disposing)
                {
                    Trace.WriteLineIf(this.TraceFlag == true, "DISPOSING BINDING ROOT ...");
                    foreach (int i in thread_receive_requests.Keys)
                        //for (int i=0; i<thread_receive_requests.Count; i++)
                        // * if (i != this.FacetInstance)
                        thread_receive_requests[i].Abort();
                }
                base.Dispose(disposing);
            }
            //dispose unmanaged resources
            disposed = true;
        }

        #endregion

    }

    class SynchronizerMonitor
    {
        private int server_facet = default(int);
        private int rank = default(int);
        private string instance_name = null;
        private IDictionary<int, Socket> client_socket_facet = new Dictionary<int, Socket>();
		private IDictionary<int, Socket> server_socket_facet_ack = new Dictionary<int, Socket>();
		private IDictionary<EnvelopKey, IDictionary<int, Queue<byte[]>>> reply_pending_list = new Dictionary<EnvelopKey, IDictionary<int, Queue<byte[]>>>();
		private IDictionary<EnvelopKey, IDictionary<int, int>> reply_pending_list_flag = new Dictionary<EnvelopKey, IDictionary<int, int>>();
        private IDictionary<EnvelopKey, IDictionary<int, Queue<AutoResetEvent>>> request_pending_list = new Dictionary<EnvelopKey, IDictionary<int, Queue<AutoResetEvent>>>();
        private IChannelRootImpl unit;

        private int reply_pending_list_size = 0;

        private object sync = new object();

        private byte[] buffer_ack = new byte[1];

        public SynchronizerMonitor(IChannelRootImpl unit, IDictionary<int, Socket> client_socket_facet, IDictionary<int, Socket> server_socket_facet_ack, int server_facet, int rank, string instance_name)
        {
            this.unit = unit;
            this.client_socket_facet = client_socket_facet;
			this.server_socket_facet_ack = server_socket_facet_ack;
			this.server_facet = server_facet;
            this.rank = rank;
            this.instance_name = instance_name;
        }

        public byte[] clientSendRequest(EnvelopType envelop, IntPtr buf, int position, int message_length, ManualResetEvent seq0, ManualResetEvent seq1)
        {
            IntPtr messageSide1 = buf == IntPtr.Zero ? IntPtr.Zero : IntPtr.Add(buf, position);
           
            EnvelopKey envelop_key = new EnvelopKey(envelop);
            int envelop_tag = envelop.Item6;
            Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest 1 / " + envelop_key + " -- " + instance_name);

            byte[] messageSide2 = null;
            
            // envia a requisição para o root parceiro
            int facet = envelop.Item3;
            Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest send to facet " + facet + " - nofsockets=" + client_socket_facet.Count + " / " + envelop_key + " -- " + instance_name);
            foreach (int f in client_socket_facet.Keys)
                Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest --- FACET KEY=" + f);
            Socket client_socket = client_socket_facet[facet];
			Socket server_socket_ack = server_socket_facet_ack[facet];
			byte[] messageSide1_enveloped_raw = MessageToByteArray(envelop, messageSide1, message_length);
			Marshal.FreeHGlobal(buf);
			
            seq0.WaitOne();
    		client_socket.Send(messageSide1_enveloped_raw);
            seq1.Set();                

            Monitor.Enter(sync);            
            try
            {                
				Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest 2 / " + envelop_key + " nbytes=" + messageSide1_enveloped_raw.Length);
				
                // Verifica se já há resposta para a requisição no "conjunto de respostas pendentes de requisição"
                if (!(reply_pending_list_flag.ContainsKey(envelop_key) && reply_pending_list_flag[envelop_key].ContainsKey(envelop_tag)) ||
                    (reply_pending_list_flag.ContainsKey(envelop_key) && reply_pending_list_flag[envelop_key].ContainsKey(envelop_tag) && reply_pending_list_flag[envelop_key][envelop_tag]==0))
                {
                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest 3 - BEFORE WAIT " + envelop_key);
                    // Se não houver, coloca um item no "conjunto de requisições pendentes de resposta" e espera.

                    if (!request_pending_list.ContainsKey(envelop_key))
                        request_pending_list[envelop_key] = new Dictionary<int, Queue<AutoResetEvent>>();

                    if (!request_pending_list[envelop_key].ContainsKey(envelop_tag))
                    {
                        request_pending_list[envelop_key][envelop_tag] = new Queue<AutoResetEvent>();
                        request_pending_list[envelop_key][envelop_tag].Enqueue(new AutoResetEvent(false));
                    }

                    AutoResetEvent sync_send = request_pending_list[envelop_key][envelop_tag].Peek();

                    Monitor.Exit(sync);
                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest - WAIT / " + unit.CID.getInstanceName() + "/" + sync_send.GetHashCode() + " BEFORE !!! ");
                    sync_send.WaitOne();
                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest - WAIT / " + unit.CID.getInstanceName() + "/" + sync_send.GetHashCode() + " AFTER !!! ");
                    Monitor.Enter(sync);
                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest 3 - AFTER WAIT " + envelop_key);
                }
                else
                {
                    if (!reply_pending_list_flag.ContainsKey(envelop_key))
	                    reply_pending_list_flag[envelop_key] = new Dictionary<int, int>();
	
                    reply_pending_list_flag[envelop_key][envelop_tag]--;                    
                }
                
                Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest 4" + " / envelop_key=" + envelop_key + " / envelop_tag=" + envelop_tag);

                Queue<byte[]> pending_replies = reply_pending_list[envelop_key][envelop_tag];
                Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest 5 / envelop_key=" + envelop_key + " / envelop_tag=" + envelop_tag + " -- pending_replies.Count = " + pending_replies.Count);
                if (pending_replies.Count > 0) 
                {
                    messageSide2 = pending_replies.Dequeue();
                    reply_pending_list_size -= messageSide2.Length;
                }

                if (pending_replies.Count == 0)
                    reply_pending_list[envelop_key].Remove(envelop_tag);

                if (reply_pending_list[envelop_key].Count == 0)
                    reply_pending_list.Remove(envelop_key);

                Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest 6 -- messageSide2.Length = " + messageSide2.Length+ " / envelop_key=" + envelop_key + " / envelop_tag=" + envelop_tag);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}/{1}: EXCEPTION (clientSendRequest)", server_facet, this.rank);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);
                    Console.WriteLine(e.InnerException.StackTrace);
                }
                throw e;
            }
            finally
            {
                Monitor.Exit(sync);
            }

            Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest 5");
            // retorna a mensagem ...
            return messageSide2;
        }

        public byte[] clientSendRequestAnyTag(EnvelopType envelop, IntPtr buf, int position, int message_length, ManualResetEvent seq0, ManualResetEvent seq1, ref int envelop_tag)
        {
            IntPtr messageSide1 = buf == IntPtr.Zero ? buf : IntPtr.Add(buf, position);
           
            EnvelopKey envelop_key = new EnvelopKey(envelop);
            Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequestAnyTag 1" + " / " + envelop_key + " -- " + instance_name);

            byte[] messageSide2 = null;
            
            // envia a requisição para o root parceiro
            int facet = envelop.Item3;
            Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequestAnyTag send to facet " + facet + " - nofsockets=" + client_socket_facet.Count + " / " + envelop_key + " -- " + instance_name);
            foreach (int f in client_socket_facet.Keys)
                Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequestAnyTag --- FACET KEY=" + f);
            Socket client_socket = client_socket_facet[facet];
			Socket server_socket_ack = server_socket_facet_ack[facet];
			byte[] messageSide1_enveloped_raw = MessageToByteArray(envelop, messageSide1, message_length);
			Marshal.FreeHGlobal(buf);

            seq0.WaitOne();
            client_socket.Send(messageSide1_enveloped_raw);
            seq1.Set();

            Monitor.Enter(sync);
            
            try
            {

				// ACK
				//server_socket_ack.Receive(buffer_ack);

				Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequestAnyTag 2 nbytes=" + messageSide1_enveloped_raw.Length + " / " + envelop_key);

                // Verifica se já há resposta para a requisição no "conjunto de respostas pendentes de requisição"
                if (!reply_pending_list.ContainsKey(envelop_key))
                {
                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequestAnyTag 3 - BEFORE WAIT " + envelop_key);
                    // Se não houver, coloca um item no "conjunto de requisições pendentes de resposta" e espera.

                    if (!request_pending_list.ContainsKey(envelop_key))
                        request_pending_list[envelop_key] = new Dictionary<int, Queue<AutoResetEvent>>();

                    if (!request_pending_list[envelop_key].ContainsKey(-1))
                    {
                        request_pending_list[envelop_key][-1] = new Queue<AutoResetEvent>();
                        request_pending_list[envelop_key][-1].Enqueue(new AutoResetEvent(false));
                    }

                    AutoResetEvent sync_send = request_pending_list[envelop_key][-1].Peek();

                    //request_pending_list [envelop_key][envelop_tag] = sync_send;
                    Monitor.Exit(sync);
                    Trace.WriteLineIf(unit.TraceFlag == true, "clientSendRequestAny - WAIT / " + unit.CID.getInstanceName() + "/" + sync_send.GetHashCode() + " BEFORE !!! ");
                    sync_send.WaitOne();
                    Trace.WriteLineIf(unit.TraceFlag == true, "clientSendRequestAny - WAIT / " + unit.CID.getInstanceName() + "/" + sync_send.GetHashCode() + " AFTER !!! ");
                    Monitor.Enter(sync);
                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequestAnyTag 3 - AFTER WAIT " + envelop_key);
                }
                Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequestAnyTag 4" + " / " + envelop_key);

                int[] keys_vector = new int[reply_pending_list[envelop_key].Keys.Count];
                reply_pending_list[envelop_key].Keys.CopyTo(keys_vector, 0);

                envelop_tag = keys_vector[0];
                Queue<byte[]> pending_replies = reply_pending_list[envelop_key][envelop_tag];
                Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequestAnyTag 5 -- pending_replies.Count = " + pending_replies.Count);
                if (pending_replies.Count > 0)
                {
                    messageSide2 = reply_pending_list[envelop_key][envelop_tag].Dequeue();
                    reply_pending_list_size -= messageSide2.Length;
                }

                if (pending_replies.Count == 0)
                    reply_pending_list[envelop_key].Remove(envelop_tag);

                if (reply_pending_list[envelop_key].Count == 0)
                    reply_pending_list.Remove(envelop_key);

                //reply_pending_list.Remove(envelop_key);
            }
			catch (Exception e)
			{
                Console.WriteLine("{0}/{1}: EXCEPTION (clientSendRequestAnyTag)", server_facet, this.rank);
				Console.WriteLine(e.Message);
				Console.WriteLine(e.StackTrace);
				if (e.InnerException != null)
				{
					Console.WriteLine(e.InnerException.Message);
					Console.WriteLine(e.InnerException.StackTrace);
				}
				throw e;
			}
			finally
            {
                Monitor.Exit(sync);
            }

            Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": clientSendRequest 5");
            // retorna a menagem ...
            return messageSide2;
        }

        private static int BUFFER_SIZE = 8192 * 8192;
        
        private ProducerConsumerQueue<byte[]> requestQueue = new ProducerConsumerQueue<byte[]>();
        private int requestQueue_size = 0;

        public void serverReceiveRequests(int facet, Socket client_socket_ack, Socket server_socket)
        {
            try
            {
                byte[] buffer = new byte[BUFFER_SIZE];
                byte[] buffer2 = new byte[BUFFER_SIZE];

                int nbytes = default(int);

                Trace.WriteLineIf(unit.TraceFlag == true, "serverReceiveRequest RECEIVE " + unit.CID.getInstanceName() + " / facet=" + facet + " BEFORE 1");
                nbytes = server_socket.Receive(buffer);
                Trace.WriteLineIf(unit.TraceFlag == true, "serverReceiveRequest RECEIVE " + unit.CID.getInstanceName() + " / facet=" + facet + " AFTER 1");

                Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReceiveRequests 1 - RECEIVED " + nbytes + " bytes -- " + instance_name);

                if (nbytes == 0)
                {
                    string error_message = server_facet + "/" + rank + ": serverReceiveRequests  -- the partner " + this.server_facet + " is died " + instance_name;
                    Trace.WriteLineIf(unit.TraceFlag == true, error_message);
                    throw new Exception(error_message);
                }

                while (true)
                {
                    while (nbytes < 4)
                    {
                        int nbytes2 = server_socket.Receive(buffer2);
                        Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReceiveRequests 1.5 nbytes=" + nbytes + " nbytes2=" + nbytes2 + " -- " + instance_name);
                        Array.Copy(buffer2, 0, buffer, nbytes, nbytes2);
                        nbytes += nbytes2;
                    }

                    int length = BitConverter.ToInt32(buffer, 0);
                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReceiveRequests LENGTH=" + length);
                    nbytes = nbytes - 4;
                    byte[] message = new byte[length];

                    if (length > buffer.Length)
                    {
                        Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReceiveRequests ADJUST buffer - from " + buffer.Length + " to " + (length * 2));

                        // adjust buffer size.
                        byte[] buffer_copy = buffer;
                        buffer = new byte[length * 2];
                        Array.Copy(buffer_copy, buffer, buffer_copy.Length);
                    }

                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReceiveRequests 2 - length is " + length + " bytes" + " / nbytes = " + nbytes);

                    while (nbytes < length)
                    {
                        Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": server_socket.Receive -- facet = " + facet); 
                        int nbytes2 = server_socket.Receive(buffer2, length - nbytes, SocketFlags.None);
                        Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReceiveRequests 2 - LOOP - length is " + length + " bytes" + " / nbytes2 = " + nbytes2 + " / nbytes = " + nbytes + " facet=" + facet + ", " + server_socket.LocalEndPoint);

                        Array.Copy(buffer2, 0, buffer, nbytes + 4, nbytes2);
                        nbytes += nbytes2;
                    }

                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReceiveRequests 2 - AFTER LOOP - length is " + length + " bytes" + " / nbytes = " + nbytes + " facet=" + facet + ", " + server_socket.LocalEndPoint);

                    Array.Copy(buffer, 4, message, 0, length);
                    requestQueue.Add(message);
                    requestQueue_size += length;

                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReceiveRequests 2 - requestQueue.Count=" + requestQueue.Count + " requestQueue_size=" + requestQueue_size);
                    
                    if (nbytes == length)
                    {
                        Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ":serverReceiveRequest RECEIVE " + unit.CID.getInstanceName() + " / facet=" + facet + " BEFORE 2");
                        nbytes = server_socket.Receive(buffer);
                        Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ":serverReceiveRequest RECEIVE " + unit.CID.getInstanceName() + " / facet=" + facet + " AFTER 2");
                        Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReceiveRequests 3 - RECEIVED " + nbytes + " bytes --- facet=" + facet + ", " + server_socket.LocalEndPoint + " " + instance_name);

                        if (nbytes == 0)
                        {
                            string error_message = server_facet + "/" + rank + ": serverReceiveRequests  -- the partner " + this.server_facet + " is died " + instance_name;
                            Trace.WriteLineIf(unit.TraceFlag == true, error_message);
                            throw new Exception(error_message);
                        }
                    }
                    else if (nbytes > length)
                    {
                        // assume that nbytes - length > 4
                        byte[] aux = buffer;
                        nbytes = nbytes - length;

                        Array.Copy(buffer, length + 4, buffer2, 0, nbytes);
                        buffer = buffer2;
                        buffer2 = aux;
                        Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReceiveRequests 4 - nbytes=" + nbytes);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}/{1}: EXCEPTION in serverReceiveRequests - facet={2}", server_facet, rank, facet);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);
                    Console.WriteLine(e.InnerException.StackTrace);
                }
                throw e;
            }
        }

        public void serverReadRequest()
        {
            EnvelopKey envelop_key = null;
           
            Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReadRequest 1 ");

            byte[] buffer = requestQueue.Take();
            int nbytes = buffer.Length;
            requestQueue_size -= nbytes;

            Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReadRequest 2 " + nbytes + " bytes received.");

            Monitor.Enter(sync);
            try
            {
                // Aguarda uma resposta proveniente do outro root parceiro.
                byte[] messageSide1_enveloped_raw = buffer /*new byte[nbytes]*/;
                Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReadRequest 2-1 nbytes=" + nbytes);

                // TODO: otimizar isso ...
                //for (int i=0; i<nbytes; i++)
                //  messageSide1_enveloped_raw[i] = buffer[i];

                //Array.Copy(buffer, 0, messageSide1_enveloped_raw, 0, nbytes);                

                Tuple<EnvelopType, byte[]> messageSide1_enveloped = ByteArrayToMessage(messageSide1_enveloped_raw);

                EnvelopType envelop = messageSide1_enveloped.Item1;
                envelop_key = new EnvelopKey(envelop);
                int envelop_tag = envelop.Item6;

                // Coloca a resposta no "conjunto de respostas pendentes de requisição"
                if (!reply_pending_list.ContainsKey(envelop_key))
                    reply_pending_list[envelop_key] = new Dictionary<int, Queue<byte[]>>();

                if (!reply_pending_list[envelop_key].ContainsKey(envelop_tag))
                    reply_pending_list[envelop_key][envelop_tag] = new Queue<byte[]>();

                reply_pending_list[envelop_key][envelop_tag].Enqueue(messageSide1_enveloped.Item2);
                reply_pending_list_size += messageSide1_enveloped.Item2.Length;
                Console.WriteLine("===> reply_pending_list[{0}][{1}].Count={2} Size={3}", envelop_key, envelop_tag, reply_pending_list[envelop_key][envelop_tag].Count, reply_pending_list_size);

                Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReadRequest 3 " + envelop.Item1 + "," + envelop_key);
                foreach (EnvelopKey ek in request_pending_list.Keys)
                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + ": key: " + ek);

                // Busca, no "conjunto de requisições pendentes de resposta", a requisição correspondente a resposta.
                if (request_pending_list.ContainsKey(envelop_key) && request_pending_list[envelop_key].ContainsKey(envelop_tag))
                {
                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReadRequest 3-1" + " / " + envelop_key);
                    AutoResetEvent sync_send = request_pending_list[envelop_key][envelop_tag].Dequeue();

                    sync_send.Set();

                    if (request_pending_list[envelop_key][envelop_tag].Count == 0)
                        request_pending_list[envelop_key].Remove(envelop_tag);

                    if (request_pending_list[envelop_key].Count == 0)
                        request_pending_list.Remove(envelop_key);

                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReadRequest 3-2" + " / " + envelop_key);
                }
                else if (request_pending_list.ContainsKey(envelop_key) && request_pending_list[envelop_key].ContainsKey(-1))
                {
                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReadRequest 3-1x" + " / " + envelop_key);                 //Monitor.Pulse (sync_send);
                    AutoResetEvent sync_send = request_pending_list[envelop_key][-1].Dequeue();

                    sync_send.Set();

                    if (request_pending_list[envelop_key][-1].Count == 0)
                        request_pending_list[envelop_key].Remove(-1);

                    if (request_pending_list[envelop_key].Count == 0)
                        request_pending_list.Remove(envelop_key);

                    Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReadRequest 3-2x" + " / " + envelop_key);
                }
                else
                {
	                if (!reply_pending_list_flag.ContainsKey(envelop_key))
	                    reply_pending_list_flag[envelop_key] = new Dictionary<int, int>();
	
 	                if (!reply_pending_list_flag[envelop_key].ContainsKey(envelop_tag))
                       reply_pending_list_flag[envelop_key][envelop_tag] = 0;
                       
                    reply_pending_list_flag[envelop_key][envelop_tag]++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}/{1}: EXCEPTION in serverReadRequest", server_facet, rank);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);
                    Console.WriteLine(e.InnerException.StackTrace);
                }
                throw e;
            }
            finally
            {
                Monitor.Exit(sync);
            }
            Trace.WriteLineIf(unit.TraceFlag == true, server_facet + "/" + rank + ": serverReadRequest 4 / " + envelop_key);
        }

        // Convert an object to a byte array
        private static byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        // Convert a byte array to an Object
        public static Object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);
            return obj;
        }

        public static byte[] MessageToByteArray(EnvelopType envelop, IntPtr message, int message_length)
        {
            byte[] i1 = BitConverter.GetBytes(envelop.Item1);
            byte[] i2 = BitConverter.GetBytes(envelop.Item2);
            byte[] i3 = BitConverter.GetBytes(envelop.Item3);
            byte[] i4 = BitConverter.GetBytes(envelop.Item4);
            byte[] i5 = BitConverter.GetBytes(envelop.Item5);
            byte[] i6 = BitConverter.GetBytes(envelop.Item6);

            int l = 4; 
    
            byte[] result = new byte[l + i1.Length + i2.Length + i3.Length + i4.Length + i5.Length + i6.Length + message_length];

            i1.CopyTo(result, l);
            i2.CopyTo(result, l + i1.Length);
            i3.CopyTo(result, l + i1.Length + i2.Length);
            i4.CopyTo(result, l + i1.Length + i2.Length + i3.Length);
            i5.CopyTo(result, l + i1.Length + i2.Length + i3.Length + i4.Length);
            i6.CopyTo(result, l + i1.Length + i2.Length + i3.Length + i4.Length + i5.Length);
            if (message != IntPtr.Zero)
               Marshal.Copy(message, result, 4 + i1.Length + i2.Length + i3.Length + i4.Length + i5.Length + i6.Length, message_length);
           
            Int32 length = result.Length - l;
            BitConverter.GetBytes(length).CopyTo(result, 0);

            return result;
        }



        private static Tuple<EnvelopType, byte[]> ByteArrayToMessage(byte[] data)
        {
            byte[] message = new byte[data.Length - 24];

            int operation_type = BitConverter.ToInt32(data, 0);
            int facet_src = BitConverter.ToInt32(data, 4);
            int facet_dst = BitConverter.ToInt32(data, 8);
            int src = BitConverter.ToInt32(data, 12);
            int dst = BitConverter.ToInt32(data, 16);
            int tag = BitConverter.ToInt32(data, 20);
            //Array.Copy(data, 24, message, 0, message.Length);

            return new Tuple<EnvelopType, byte[]>(new EnvelopType(operation_type, facet_src, facet_dst, src, dst, tag), data);
        }
    }

    class EnvelopKey
    {
        private EnvelopType envelop = null;
        public EnvelopKey(EnvelopType envelop)
        {
            this.envelop = envelop;
        }

        public override string ToString()
        {
            string key = base.ToString();
            switch (envelop.Item1)
            {
                case AliencommunicatorOperation.SEND:
                case AliencommunicatorOperation.SYNC_SEND:
                case AliencommunicatorOperation.SEND_ARRAY:
                    //              key = string.Format ("SR-{0}-{1}-{2}-{3}-{4}",envelop.Item2, envelop.Item3, envelop.Item4, envelop.Item5, envelop.Item6);
                    key = string.Format("{0}-{1}-{2}-{3}", envelop.Item2, envelop.Item3, envelop.Item4, envelop.Item5);
                    break;
                case AliencommunicatorOperation.RECEIVE:
                case AliencommunicatorOperation.RECEIVE_ARRAY:
                    //              key = string.Format ("SR-{1}-{0}-{3}-{2}-{4}",envelop.Item2, envelop.Item3, envelop.Item4, envelop.Item5, envelop.Item6);
                    key = string.Format("{1}-{0}-{3}-{2}", envelop.Item2, envelop.Item3, envelop.Item4, envelop.Item5);
                    break;
                case AliencommunicatorOperation.PROBE:
                    break;
                case AliencommunicatorOperation.ALL_GATHER:
                    break;
                case AliencommunicatorOperation.ALL_GATHER_FLATTENED:
                    break;
                case AliencommunicatorOperation.ALL_REDUCE:
                    break;
                case AliencommunicatorOperation.ALL_REDUCE_ARRAY:
                    break;
                case AliencommunicatorOperation.ALL_TO_ALL:
                    break;
                case AliencommunicatorOperation.ALL_TO_ALL_FLATTENED:
                    break;
                case AliencommunicatorOperation.REDUCE_SCATTER:
                    break;
                case AliencommunicatorOperation.BROADCAST:
                    break;
                case AliencommunicatorOperation.BROADCAST_ARRAY:
                    break;
                case AliencommunicatorOperation.SCATTER:
                    break;
                case AliencommunicatorOperation.SCATTER_FROM_FLATTENED:
                    break;
                case AliencommunicatorOperation.GATHER:
                    break;
                case AliencommunicatorOperation.GATHER_FLATTENED:
                    break;
                case AliencommunicatorOperation.REDUCE:
                    break;
                case AliencommunicatorOperation.REDUCE_ARRAY:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return key;
        }

        public override bool Equals(object obj)
        {
            EnvelopKey fooItem = obj as EnvelopKey;

            return fooItem.ToString().Equals(this.ToString());
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

    }

    public class ProducerConsumerQueue<T> : BlockingCollection<T>
    {
        /// <summary>
        /// Initializes a new instance of the ProducerConsumerQueue, Use Add and TryAdd for Enqueue and TryEnqueue and Take and TryTake for Dequeue and TryDequeue functionality
        /// </summary>
        public ProducerConsumerQueue()
            : base(new ConcurrentQueue<T>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the ProducerConsumerQueue, Use Add and TryAdd for Enqueue and TryEnqueue and Take and TryTake for Dequeue and TryDequeue functionality
        /// </summary>
        /// <param name="maxSize"></param>
        public ProducerConsumerQueue(int maxSize)
            : base(new ConcurrentQueue<T>(), maxSize)
        {
        }



    }

}
