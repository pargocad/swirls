using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;

namespace org.hpcshelf.backend.ec2.family.Storage
{
	public interface IEC2FamilyStorage : BaseIEC2FamilyStorage, IEC2Family
	{
	}
}