/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_11Way
{
	public interface BaseISetAssociative_11Way : BaseICacheMappingSetAssociative<org.hpcshelf.quantifier.IntUp.IntUp>, IQualifierKind 
	{
	}
}