using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X4
{
 public interface IGoogleSizeX4 : BaseIGoogleSizeX4, IGoogleSize
 {
 }
}