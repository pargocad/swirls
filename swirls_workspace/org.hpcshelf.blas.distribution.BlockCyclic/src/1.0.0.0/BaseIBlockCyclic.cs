/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.data.DataDistribution;

namespace org.hpcshelf.blas.distribution.BlockCyclic
{
	public interface BaseIBlockCyclic : BaseIDataDistribution, IQualifierKind 
	{
	}
}