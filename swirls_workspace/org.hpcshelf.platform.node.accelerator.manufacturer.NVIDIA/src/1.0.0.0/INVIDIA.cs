using org.hpcshelf.platform.node.accelerator.Manufacturer;

namespace org.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA
{
	public interface INVIDIA : BaseINVIDIA, IAcceleratorManufacturer
	{
	}
}