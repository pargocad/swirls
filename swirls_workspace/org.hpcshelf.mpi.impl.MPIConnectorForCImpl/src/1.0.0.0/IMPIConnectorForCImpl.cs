using System;
using System.Collections.Generic;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.mpi.MPIConnectorForC;
using org.hpcshelf.binding.channel.Binding;
using org.hpcshelf.mpi.intercommunicator.BasicSendReceive;
using System.Runtime.InteropServices;

namespace org.hpcshelf.mpi.impl.MPIConnectorForCImpl
{
	public class IMPIConnectorForCImpl : BaseIMPIConnectorForCImpl, IMPIConnectorForC
	{	   
	   IBasicSendReceive port = null;
	   
	   public override void after_initialize()
	   {
	      Console.WriteLine("IMPIConnectorForCImpl - after_initialize 2 -- {0} / {1}", this.UnitSizeInFacet.Count, this.FacetInstanceCount);

		  int facet = this.Facet;
		  int[] facet_size = new int[this.FacetInstanceCount];
		  int[] facet_instance = new int[this.FacetInstanceCount];
		  int i=0;
		  foreach (KeyValuePair<int,IDictionary<string,int>> f in this.UnitSizeInFacet)
	      {
	          Console.WriteLine("IMPIConnectorForCImpl - after_initialize 2.1 --- f.Key={0} i={1} this.Facet={2}", f.Key, i, this.Facet);
	         
	          if (f.Key == this.FacetInstance)
	             facet = i;
	         
	          int size = 0;
	          foreach (KeyValuePair<string, int> unit_facet in f.Value)
	             size += unit_facet.Value;
	          facet_size[i] = size;
	          facet_instance[i] = f.Key;
		      i++;
		      
		      Console.WriteLine("IMPIConnectorForCImpl - after_initialize 2.2 --- {0}", f.Key);		      
	      }  
	      
	      Console.WriteLine("IMPIConnectorForCImpl - after_initialize 3");
	      
		  int channel_key = BasicSendReceiveImpl.registerChannel(Channel.ChannelKey, Channel.HPCShelfSend, Channel.HPCShelfRecv); // this connector has a single channel
		  
		  port = new BasicSendReceiveImpl(facet, this.FacetInstanceCount, facet_size, facet_instance);
		  		  
	      Console.WriteLine("IMPIConnectorForCImpl - after_initialize 4");
	   }
	   
	 	public override void main()
		{		   
		   Console.WriteLine("IMPIConnectorForCImpl - MAIN - BEFORE Port.Server = port");
		   Port.Server = port;
		   Console.WriteLine("IMPIConnectorForCImpl - MAIN - AFTER Port.Server = port");
		}
		
		public override void release1()
	    {
            Services.releasePort("channel");
            Services.releasePort("port");

            Port.Server = null;

            base.release1();
	    }		
	}
		
    class BasicSendReceiveImpl : IBasicSendReceive
    {
        // The connector operations will be offered to the host computation component.
        // The registerChannel operation will be used to register the native operations of channel(s).
       
        public BasicSendReceiveImpl(int facet, int facet_count, int[] facet_size, int[] facet_instance)
        {
           this.facet = facet;
           this.facet_count = facet_count;
           this.facet_size = facet_size;
           this.facet_instance = facet_instance;
        }
       
		[DllImport("libconnector.so", EntryPoint = "connector_send")]
		public static extern int send(int key, IntPtr buf, int count, int datatype, int facet, int dest, int tag);
		
		[DllImport("libconnector.so", EntryPoint = "connector_receive")]
		public static extern int receive(int key, IntPtr buf, int count, int datatype, int facet, int source, int tag, IntPtr status);
		
		[DllImport("libconnector.so", EntryPoint = "registerChannel")]
		public static extern int registerChannel(int channel_key, TypeOfChannelSend send, TypeOfChannelRecv recv);

        public TypeOfConnectorSend Send    { get { return send; } }
	    public TypeOfConnectorReceive Receive { get { return receive; } }   
	    
	    int facet = -1;
	    public int Facet { get { return facet; } }
	    
	    int facet_count = -1;
	    public int FacetCount { get { return facet_count; } }
	    
	    int[] facet_size = null;
	    public int[] FacetSize { get { return facet_size; } }	    

	    int[] facet_instance = null;
	    public int[] FacetInstance { get { return facet_instance; } }	    
    }
}
