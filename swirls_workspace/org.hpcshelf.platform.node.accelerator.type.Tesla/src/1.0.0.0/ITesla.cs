using org.hpcshelf.platform.node.accelerator.Type;

namespace org.hpcshelf.platform.node.accelerator.type.Tesla
{
	public interface ITesla : BaseITesla, IAcceleratorType<org.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA>
	{
	}
}