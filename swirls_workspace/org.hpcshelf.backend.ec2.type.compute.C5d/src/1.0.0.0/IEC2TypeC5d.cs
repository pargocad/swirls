using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.compute.C5d
{
	public interface IEC2TypeC5d : BaseIEC2TypeC5d, IEC2Type<family.Computation.IEC2FamilyComputation>
	{
	}
}