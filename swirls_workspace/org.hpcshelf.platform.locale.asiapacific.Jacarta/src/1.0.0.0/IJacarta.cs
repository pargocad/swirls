using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.Jacarta
{
	public interface IJacarta : BaseIJacarta, IAsiaPacific
	{
	}
}