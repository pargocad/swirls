/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.quantifier.IntUp;

namespace org.hpcshelf.platform.Power
{
	public interface BaseIPower<P0, P1, P2, P3> : IQualifierKind 
		where P0:IntDown
		where P1:IntUp
		where P2:IntDown
		where P3:IntUp
	{
	}
}