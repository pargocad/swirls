using org.hpcshelf.platform.node.accelerator.Type;

namespace org.hpcshelf.platform.node.accelerator.type.Virtex
{
	public interface IVirtex : BaseIVirtex, IAcceleratorType<org.hpcshelf.platform.node.accelerator.manufacturer.Xilinx.IXilinx>
	{
	}
}