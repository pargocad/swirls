/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X2g
{
 public interface BaseIGoogleSizeX2g : BaseIGoogleSize, IQualifierKind 
 {
 }
}