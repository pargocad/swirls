using org.hpcshelf.platform.node.processor.Manufacturer;

namespace org.hpcshelf.platform.node.processor.manufacturer.Intel
{
 public interface IIntel : BaseIIntel, IProcessorManufacturer
 {
 }
}