using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X30
{
 public interface IGoogleSizeX30 : BaseIGoogleSizeX30, IGoogleSize
 {
 }
}