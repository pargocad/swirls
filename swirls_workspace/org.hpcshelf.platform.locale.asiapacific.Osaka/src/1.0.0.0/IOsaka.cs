using org.hpcshelf.platform.locale.asiapacific.Japan;

namespace org.hpcshelf.platform.locale.asiapacific.Osaka
{
	public interface IOsaka : BaseIOsaka, IJapan
	{
	}
}