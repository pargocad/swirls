/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.northamerica.US_East;

namespace org.hpcshelf.platform.locale.northamerica.Virginia
{
	public interface BaseIVirginia : BaseIUS_East, IQualifierKind 
	{
	}
}