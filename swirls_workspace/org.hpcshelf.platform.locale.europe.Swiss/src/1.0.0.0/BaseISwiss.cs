/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.europe.WestEurope;

namespace org.hpcshelf.platform.locale.europe.Swiss
{
	public interface BaseISwiss : BaseIWestEurope, IQualifierKind 
	{
	}
}