/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.os.linux.ubuntu.CodeName;

namespace org.hpcshelf.platform.node.os.linux.ubuntu.codename.XenialXerus
{
	public interface BaseIXenialXerius : BaseICodeName, IQualifierKind 
	{
	}
}