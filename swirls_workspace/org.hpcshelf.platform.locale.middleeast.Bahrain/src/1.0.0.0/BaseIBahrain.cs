/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.MiddleEast;

namespace org.hpcshelf.platform.locale.middleeast.Bahrain
{
	public interface BaseIBahrain : BaseIMiddleEast, IQualifierKind 
	{
	}
}