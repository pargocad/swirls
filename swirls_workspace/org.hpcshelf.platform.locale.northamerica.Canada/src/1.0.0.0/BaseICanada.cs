/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.NorthAmerica;

namespace org.hpcshelf.platform.locale.northamerica.Canada
{
	public interface BaseICanada : BaseINorthAmerica, IQualifierKind 
	{
	}
}