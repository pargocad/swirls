/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.AnyWhere;

namespace org.hpcshelf.platform.locale.Asia
{
	public interface BaseIAsia : BaseIAnyWhere, IQualifierKind 
	{
	}
}