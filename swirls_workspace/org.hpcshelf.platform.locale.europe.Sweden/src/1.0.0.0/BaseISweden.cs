/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.europe.NorthEurope;

namespace org.hpcshelf.platform.locale.europe.Sweden
{
	public interface BaseISweden : BaseINorthEurope, IQualifierKind 
	{
	}
}