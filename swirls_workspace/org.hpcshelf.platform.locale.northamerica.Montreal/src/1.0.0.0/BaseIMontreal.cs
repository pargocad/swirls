/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.northamerica.Canada;

namespace org.hpcshelf.platform.locale.northamerica.Montreal
{
	public interface BaseIMontreal : BaseICanada, IQualifierKind 
	{
	}
}