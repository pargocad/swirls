/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Series;

namespace org.hpcshelf.platform.node.processor.series.intel.Xeon_E7_8000
{
	public interface BaseIXeonE78000 : BaseIProcessorSeries<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, org.hpcshelf.platform.node.processor.family.intel.Xeon_E7.IXeonE7>, IQualifierKind 
	{
	}
}