using org.hpcshelf.platform.node.processor.Manufacturer;

namespace org.hpcshelf.platform.node.processor.manufacturer.AMD
{
	public interface IAMD : BaseIAMD, IProcessorManufacturer
	{
	}
}