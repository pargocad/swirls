using org.hpcshelf.blas.configure.UPLO;

namespace org.hpcshelf.blas.configure.uplo.NA
{
	public interface NA : BaseNA, IUPLO
	{
	}
}