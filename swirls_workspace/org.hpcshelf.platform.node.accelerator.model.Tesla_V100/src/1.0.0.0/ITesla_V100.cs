using org.hpcshelf.platform.node.accelerator.Model;

namespace org.hpcshelf.platform.node.accelerator.model.Tesla_V100
{
	public interface ITesla_V100 : BaseITesla_V100, IAcceleratorModel<org.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA, org.hpcshelf.platform.node.accelerator.type.Tesla.ITesla, org.hpcshelf.platform.node.accelerator.architecture.Volta.IVolta>
	{
	}
}