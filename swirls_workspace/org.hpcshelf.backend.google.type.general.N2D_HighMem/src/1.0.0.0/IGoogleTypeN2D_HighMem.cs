using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.N2D_HighMem
{
 public interface IGoogleTypeN2D_HighMem : BaseIGoogleTypeN2D_HighMem, IGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>
 {
 }
}