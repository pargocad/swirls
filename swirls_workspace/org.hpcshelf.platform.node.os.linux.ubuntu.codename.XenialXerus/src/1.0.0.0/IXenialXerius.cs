using org.hpcshelf.platform.node.os.linux.ubuntu.CodeName;

namespace org.hpcshelf.platform.node.os.linux.ubuntu.codename.XenialXerus
{
	public interface IXenialXerius : BaseIXenialXerius, ICodeName
	{
	}
}