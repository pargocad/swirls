using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;
using org.hpcshelf.backend.ec2.family.GeneralPurpose;

namespace org.hpcshelf.backend.ec2.type.general.T3a
{
	public interface IEC2TypeT3a : BaseIEC2TypeT3a, IEC2Type<IEC2FamilyGeneralPurpose>
	{
	}
}