using org.hpcshelf.common.BrowserConnector;

namespace org.hpcshelf.mpi.BrowserConnectorForC
{
	public interface IBrowserRootForC : BaseIBrowserRootForC, IBrowserRoot<org.hpcshelf.common.browser.RecvDataPortType.IRecvDataPortType>
	{
	}
}