/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Family;

namespace org.hpcshelf.platform.node.processor.family.intel.Xeon_Platinum
{
	public interface BaseIXeonPlatinum : BaseIProcessorFamily<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>, IQualifierKind 
	{
	}
}