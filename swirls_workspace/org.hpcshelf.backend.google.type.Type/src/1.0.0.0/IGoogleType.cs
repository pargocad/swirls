using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.family.Family;

namespace org.hpcshelf.backend.google.type.Type
{
 public interface IGoogleType<F> : BaseIGoogleType<F>
  where F:IGoogleFamily
 {
 }
}