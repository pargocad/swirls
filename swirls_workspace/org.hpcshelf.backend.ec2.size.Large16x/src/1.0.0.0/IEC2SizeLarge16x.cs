using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large16x
{
	public interface IEC2SizeLarge16x : BaseIEC2SizeLarge16x, IEC2Size
	{
	}
}