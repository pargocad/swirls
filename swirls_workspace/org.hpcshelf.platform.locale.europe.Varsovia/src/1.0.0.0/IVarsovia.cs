using org.hpcshelf.platform.locale.europe.Poland;

namespace org.hpcshelf.platform.locale.europe.Varsovia
{
	public interface IVarsovia : BaseIVarsovia, IPoland
	{
	}
}