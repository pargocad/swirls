./swirls_run.exe core set ip 127.0.0.1
./swirls_run.exe new system one_connector_testing --clear_log
./swirls_run.exe create contract platform_contract_2 name=org.hpcshelf.platform.Platform,maintainer=org.hpcshelf.platform.maintainer.LocalHost,node-count=2
./swirls_run.exe new platform platform_0 --contract=\!platform_contract_2
./swirls_run.exe new platform platform_1 --contract=\!platform_contract_2
./swirls_run.exe new platform platform_2 --contract=\!platform_contract_2
./swirls_run.exe new platform platform_3 --contract=\!platform_contract_2
./swirls_run.exe create computation org.hpcshelf.testing.MPIProgram713 --source=one_connector --language=C++ --port=intercommunicator_port_type=org.hpcshelf.mpi.intercommunicator.BasicSendReceive
./swirls_run.exe new computation computation_0 --contract=name=org.hpcshelf.testing.MPIProgram713 --platform=platform_0
./swirls_run.exe new computation computation_1 --contract=name=org.hpcshelf.testing.MPIProgram713 --platform=platform_1
./swirls_run.exe new computation computation_2 --contract=name=org.hpcshelf.testing.MPIProgram713 --platform=platform_2
./swirls_run.exe new computation computation_3 --contract=name=org.hpcshelf.testing.MPIProgram713 --platform=platform_3
./swirls_run.exe new connector browser --contract=name=org.hpcshelf.mpi.wrapper.WBrowserConnector --platform=local:0 --platform=platform_0:1 --platform=platform_1:1 --platform=platform_2:1 --platform=platform_3:1
./swirls_run.exe new connector intercommunicator --contract=name=org.hpcshelf.mpi.MPIConnectorForC --platform=platform_0 --platform=platform_1 --platform=platform_2 --platform=platform_3
./swirls_run.exe new service-binding binding_0 --contract=name=org.hpcshelf.mpi.binding.IntercommunicatorBinding,intercommunicator_port_type=org.hpcshelf.mpi.intercommunicator.BasicSendReceive --user-port=computation_0.port --provider-port=intercommunicator.port:0
./swirls_run.exe new service-binding binding_1 --contract=name=org.hpcshelf.mpi.binding.IntercommunicatorBinding,intercommunicator_port_type=org.hpcshelf.mpi.intercommunicator.BasicSendReceive --user-port=computation_1.port --provider-port=intercommunicator.port:1
./swirls_run.exe new service-binding binding_2 --contract=name=org.hpcshelf.mpi.binding.IntercommunicatorBinding,intercommunicator_port_type=org.hpcshelf.mpi.intercommunicator.BasicSendReceive --user-port=computation_2.port --provider-port=intercommunicator.port:2
./swirls_run.exe new service-binding binding_3 --contract=name=org.hpcshelf.mpi.binding.IntercommunicatorBinding,intercommunicator_port_type=org.hpcshelf.mpi.intercommunicator.BasicSendReceive --user-port=computation_3.port --provider-port=intercommunicator.port:3
./swirls_run.exe new service-binding browser_binding_application --contract=name=org.hpcshelf.common.BrowserBinding,browser_port_type=org.hpcshelf.common.browser.RecvDataPortType --user-port=application --provider-port=browser.browse_port
./swirls_run.exe new service-binding browser_binding_0 --contract=name=org.hpcshelf.common.BrowserBinding,browser_port_type=org.hpcshelf.common.browser.wrapper.WSendDataPortType --user-port=computation_0.browser_port --provider-port=browser.send_data_port:0
./swirls_run.exe new service-binding browser_binding_1 --contract=name=org.hpcshelf.common.BrowserBinding,browser_port_type=org.hpcshelf.common.browser.wrapper.WSendDataPortType --user-port=computation_1.browser_port --provider-port=browser.send_data_port:1
./swirls_run.exe new service-binding browser_binding_2 --contract=name=org.hpcshelf.common.BrowserBinding,browser_port_type=org.hpcshelf.common.browser.wrapper.WSendDataPortType --user-port=computation_2.browser_port --provider-port=browser.send_data_port:2
./swirls_run.exe new service-binding browser_binding_3 --contract=name=org.hpcshelf.common.BrowserBinding,browser_port_type=org.hpcshelf.common.browser.wrapper.WSendDataPortType --user-port=computation_3.browser_port --provider-port=browser.send_data_port:3
./swirls_run.exe resolve platform_0 platform_1 platform_2 platform_3 computation_0 computation_1 computation_2 computation_3
./swirls_run.exe deploy platform_0 platform_1 platform_2 platform_3
./swirls_run.exe deploy computation_0 computation_1 computation_2 computation_3
./swirls_run.exe instantiate platform_0 platform_1 platform_2 platform_3
./swirls_run.exe instantiate computation_0 computation_1 computation_2 computation_3
./swirls_run.exe resolve browser 
./swirls_run.exe deploy browser
./swirls_run.exe instantiate browser
./swirls_run.exe resolve intercommunicator
./swirls_run.exe deploy intercommunicator
./swirls_run.exe instantiate intercommunicator
./swirls_run.exe resolve binding_0 binding_1 binding_2 binding_3
./swirls_run.exe deploy binding_0 binding_1 binding_2 binding_3
./swirls_run.exe instantiate binding_0 binding_1 binding_2 binding_3
./swirls_run.exe resolve browser_binding_application browser_binding_0 browser_binding_1 browser_binding_2 browser_binding_3
./swirls_run.exe deploy browser_binding_application browser_binding_0 browser_binding_1 browser_binding_2 browser_binding_3
./swirls_run.exe instantiate browser_binding_application browser_binding_0 browser_binding_1 browser_binding_2 browser_binding_3
./swirls_run.exe run --async=_ computation_0 computation_1 computation_2 computation_3 browser intercommunicator
./swirls_run.exe browse browser_binding_application
