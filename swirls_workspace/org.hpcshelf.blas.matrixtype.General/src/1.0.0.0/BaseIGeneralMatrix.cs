/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.MatrixType;

namespace org.hpcshelf.blas.matrixtype.General
{
	public interface BaseIGeneralMatrix : BaseIMatrixType, IQualifierKind 
	{
	}
}