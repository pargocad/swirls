using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.general.T2
{
	public interface IEC2TypeT2 : BaseIEC2TypeT2, IEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>
	{
	}
}