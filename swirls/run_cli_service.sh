#!/bin/sh

export CORE_ADDRESS=200.19.177.100      # it may be changed by using 'core set ip <ip>'
#export CORE_ADDRESS=127.0.0.1
export CORE_PORT=8080                   # it may be changed by using 'core set port <port>'
#export PlatformSAFe_ADDRESS=127.0.0.1  # it is now calculated automatically.
export PlatformSAFe_PORT=8079           # default port is set here (e.g., 8079). You can change by using 'platform_SAFe set port <port>'.
export TEMPLATE_CODE_ABSTRACT_MPI="$Swirls_HOME/MPILauncher.hpe"
export TEMPLATE_CODE_CONCRETE_MPI_C="$Swirls_HOME/MPILauncherForCImpl.hpe"
export TEMPLATE_CODE_CONCRETE_MPI_PYTHON="$Swirls_HOME/MPILauncherForPythonImpl.hpe"
export TEMPLATE_CODE_CONCRETE_MPI_CPLUSPLUS="$Swirls_HOME/MPILauncherForCPlusPlusImpl.hpe"

chmod +x $Swirls_HOME/swirls_cli_service.exe
rm $Swirls_HOME/swirls_cli_service.lock
screen -dm bash -c 'mono-service $Swirls_HOME/swirls_cli_service.exe -d:$1 -l:$Swirls_HOME/swirls_cli_service.lock --debug --no-daemon'

