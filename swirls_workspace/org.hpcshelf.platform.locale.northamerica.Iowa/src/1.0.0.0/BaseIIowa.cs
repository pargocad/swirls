/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.northamerica.US_East;

namespace org.hpcshelf.platform.locale.northamerica.Iowa
{
	public interface BaseIIowa : BaseIUS_East, IQualifierKind 
	{
	}
}