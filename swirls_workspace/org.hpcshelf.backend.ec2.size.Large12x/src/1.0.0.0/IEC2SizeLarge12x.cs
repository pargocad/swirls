using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large12x
{
	public interface IEC2SizeLarge12x : BaseIEC2SizeLarge12x, IEC2Size
	{
	}
}