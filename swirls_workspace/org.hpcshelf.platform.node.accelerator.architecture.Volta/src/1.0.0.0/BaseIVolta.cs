/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.accelerator.Architecture;

namespace org.hpcshelf.platform.node.accelerator.architecture.Volta
{
	public interface BaseIVolta : BaseIAcceleratorArchitecture, IQualifierKind 
	{
	}
}