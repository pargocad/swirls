/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.accelerated.G4dn
{
	public interface BaseIEC2TypeG4dn : BaseIEC2Type<family.Acceleration.IEC2FamilyAcceleration>, IQualifierKind 
	{
	}
}