/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X16
{
 public interface BaseIGoogleSizeX16 : BaseIGoogleSize, IQualifierKind 
 {
 }
}