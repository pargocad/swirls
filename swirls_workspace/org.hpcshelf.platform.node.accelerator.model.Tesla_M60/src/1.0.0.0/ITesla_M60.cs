using org.hpcshelf.platform.node.accelerator.Model;

namespace org.hpcshelf.platform.node.accelerator.model.Tesla_M60
{
	public interface ITesla_M60 : BaseITesla_M60, IAcceleratorModel<org.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA, org.hpcshelf.platform.node.accelerator.type.Tesla.ITesla, org.hpcshelf.platform.node.accelerator.architecture.Maxwell.IMaxwell>
	{
	}
}