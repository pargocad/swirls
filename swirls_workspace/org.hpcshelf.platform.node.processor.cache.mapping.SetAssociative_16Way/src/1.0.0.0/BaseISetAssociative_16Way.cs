/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_16Way
{
	public interface BaseISetAssociative_16Way : BaseICacheMappingSetAssociative<org.hpcshelf.quantifier.IntUp.IntUp>, IQualifierKind 
	{
	}
}