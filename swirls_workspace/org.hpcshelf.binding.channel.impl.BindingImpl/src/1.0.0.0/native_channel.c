#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "native_channel.h"

// MPI_Comm root_comm;

int next_conversation_tag = 100;

int takeNextConversationTag()
{
	next_conversation_tag++;
	switch (next_conversation_tag)
	{
		TAG_SEND_OPERATION:	next_conversation_tag++; break;
		INT_MAX: next_conversation_tag = 100; break;
	}
	return next_conversation_tag;
}

int key=0;
MPI_Comm channels[255];

int registerChannel(MPI_Comm root_comm_arg)
{
	printf("registerChannel -- native_channel.c -- key=%d, count=%d comm=%d\n", key, key, root_comm_arg);
    channels[key] = root_comm_arg;
	// root_comm = root_comm_arg;
	return key++;
}

void send1(MPI_Comm comm, int operation, int tag1, int tag2)
{
    int buf[2] = {operation, tag1};
	MPI_Send(buf, 2, MPI_INT, 0, tag2, comm);
}

void send3(MPI_Comm comm, int facet, int source, int tag1, int tag2)
{
	int buf[3] = {facet, source, tag1};
	MPI_Send(buf, 3, MPI_INT, 0, tag2, comm);
}

int send2(MPI_Comm comm, int facet, int target, int tag1, const void* buf, int count, MPI_Datatype datatype, int tag2)
{
	int rank; MPI_Comm_rank(comm, &rank);

    int bufsize;
    // MPI_Type_size(datatype, &bufsize);
    MPI_Pack_size(count, datatype, comm, &bufsize);

	printf("%d: HPCShelfSend 30 %d %d %d %d \n", rank, facet, target, tag1, tag2);

    int outbuf_size = 4 * sizeof(int) + bufsize;
	void* outbuf = (void*) malloc(outbuf_size);

	int position = 0;
	printf("%d: HPCShelfSend 31 %d %d %d %d bufsize=%d outbuf_size=%d\n", rank, facet, target, tag1, tag2, bufsize, outbuf_size);
	MPI_Pack(&facet,   1,  MPI_INT,  outbuf, outbuf_size, &position, comm);
	//printf("%d: HPCShelfSend 32 %d %d %d %d \n", rank, facet, target, tag1, tag2);
	MPI_Pack(&target,  1,  MPI_INT,  outbuf, outbuf_size, &position, comm);
	//printf("%d: HPCShelfSend 33 %d %d %d %d \n", rank, facet, target, tag1, tag2);
	MPI_Pack(&tag2,    1,  MPI_INT,  outbuf, outbuf_size, &position, comm);
	//printf("%d: HPCShelfSend 34 %d %d %d %d \n", rank, facet, target, tag1, tag2);
	MPI_Pack(&bufsize, 1,  MPI_INT,  outbuf, outbuf_size, &position, comm);
	//printf("%d: HPCShelfSend 35 %d %d %d %d \n", rank, facet, target, tag1, tag2);
	MPI_Pack((void*)buf, count, datatype, outbuf, outbuf_size, &position, comm);
	//printf("%d: HPCShelfSend 36 %d %d %d %d \n", rank, facet, target, tag1, tag2);

	MPI_Send(&position, 1, MPI_INT, 0, tag1, comm);
	int errorCode = MPI_Send(outbuf, position, MPI_PACKED, 0, tag1, comm);
	//printf("%d: HPCShelfSend 37 %d %d %d %d position=%d\n", rank, facet, target, tag1, tag2, position);

	free(outbuf);
	//printf("%d: HPCShelfSend 38 %d %d %d %d position=%d\n", rank, facet, target, tag1, tag2, position);

	return errorCode;
}

//int buf_recv = 10000000;
//void* buffer_receive = NULL;

void receive(MPI_Comm comm, int source, int tag, void* result, int count, MPI_Datatype datatype, MPI_Status* status)
{
	//if (buffer_receive == NULL)
	//	buffer_receive = (void*) malloc(buf_recv);

	//int error = MPI_Recv(buffer_receive, buf_recv, MPI_PACKED, source, tag, comm, status);

	int bufsize;
	MPI_Recv(&bufsize, 1, MPI_INT, source, tag, comm, status); //ignored
	printf("native_channel.c --- count=%d / bufsize=%d\n", count, bufsize);
	int error = MPI_Recv(result, count, datatype, source, tag, comm, status);

	//int size_;
	//int position = 0;
	//error = MPI_Unpack(buffer_receive, buf_recv, &position, &size_, 1, MPI_INT, comm);
	//int typesize;
    //MPI_Type_size(datatype, &typesize);
	//assert(size*typesize > size_);
    //printf("size=%d size__=%d", size, size_);
	//error = MPI_Unpack(buffer_receive, buf_recv, &position, result, size_, datatype, comm);
}

int HPCShelfSend(
		int channel_key,
		const void *buf,
		int count,
		MPI_Datatype datatype,
		int facet,
		int target,
		int tag)
{
	int conversation_tag = takeNextConversationTag();

	MPI_Comm root_comm = channels[channel_key];
	int rank; MPI_Comm_rank(root_comm, &rank);

	printf("%d: HPCShelfSend 1 %d %d %d count=%d\n", rank, facet, target, tag, count);
    send1(root_comm, SEND, conversation_tag, TAG_SEND_OPERATION);
	printf("%d: HPCShelfSend 3 %d %d %d \n", rank, facet, target, tag);
	send2(root_comm, facet, target, conversation_tag, buf, count, datatype, tag);
	printf("%d: HPCShelfSend 4 %d %d %d \n", rank, facet, target, tag);
	return MPI_SUCCESS;
}

int HPCShelfRecv(
		int channel_key,
		void *buf,
		int count,
		MPI_Datatype datatype,
		int facet,
		int source,
		int tag,
		MPI_Status *status)
{
	MPI_Comm root_comm = channels[channel_key];
	int rank; MPI_Comm_rank(root_comm, &rank);

	int conversation_tag = takeNextConversationTag();

	printf("%d: native_channel - go send1 facet=%d source=%d conversation_tag=%d\n", rank, facet, source, conversation_tag);
	send1(root_comm, RECEIVE, conversation_tag, TAG_SEND_OPERATION);
	printf("%d: native_channel - go send3 facet=%d source=%d conversation_tag=%d\n", rank, facet, source, conversation_tag);
	send3(root_comm, facet, source, tag, conversation_tag);
	printf("%d: native_channel - receive - facet=%d source=%d tag=%d - BEFORE\n", rank, facet, source, tag < 0 ? tag : conversation_tag);
	receive(root_comm, 0, tag < 0 ? tag : conversation_tag, buf, count, datatype, status);
	printf("%d: native_channel - receive - facet=%d source=%d tag=%d - AFTER\n", rank, facet, source, tag < 0 ? tag : conversation_tag);
	return MPI_SUCCESS;
}


