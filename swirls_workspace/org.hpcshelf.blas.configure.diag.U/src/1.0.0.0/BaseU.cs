/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.configure.DIAG;

namespace org.hpcshelf.blas.configure.diag.U
{
	public interface BaseU : BaseIDIAG, IQualifierKind 
	{
	}
}