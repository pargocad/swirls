/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.backend.google.storage.Type;
using org.hpcshelf.platform.node.Storage;

namespace org.hpcshelf.backend.google.GoogleInstanceStorage
{
	public interface BaseIGoogleInstanceStorage<SIZ, LAT, BAND, NETBAND, TYPE> : BaseIStorage<SIZ, LAT, BAND, NETBAND>, IQualifierKind 
		where SIZ:IntUp
		where LAT:IntDown
		where BAND:IntUp
		where NETBAND:IntUp
		where TYPE:IGoogleStorageType
	{
	}
}