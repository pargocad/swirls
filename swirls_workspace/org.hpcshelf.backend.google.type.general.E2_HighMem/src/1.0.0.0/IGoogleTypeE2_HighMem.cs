using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.E2_HighMem
{
 public interface IGoogleTypeE2_HighMem : BaseIGoogleTypeE2_HighMem, IGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>
 {
 }
}