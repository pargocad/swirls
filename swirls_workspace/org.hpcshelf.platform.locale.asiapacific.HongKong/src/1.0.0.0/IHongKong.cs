using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.HongKong
{
	public interface IHongKong : BaseIHongKong, IAsiaPacific
	{
	}
}