/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.accelerator.Architecture;

namespace org.hpcshelf.platform.node.accelerator.architecture.Ultrascale_plus
{
	public interface BaseIUltrascale_plus : BaseIAcceleratorArchitecture, IQualifierKind 
	{
	}
}