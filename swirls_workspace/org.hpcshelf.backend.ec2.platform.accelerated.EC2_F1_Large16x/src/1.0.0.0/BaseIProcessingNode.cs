/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;
using org.hpcshelf.backend.ec2.size.Size;
using org.hpcshelf.backend.ec2.type.Type;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.Platform;
using org.hpcshelf.platform.maintainer.EC2;

namespace org.hpcshelf.backend.ec2.EC2Platform
{
 public interface BaseIProcessingNode<F, L, T, N> : BaseIProcessingNode<IEC2, N>, IEnvironmentKind 
  where F:IEC2Family
  where L:IEC2Size
  where T:IEC2Type<F>
  where N:IntUp
 {
 }
}