using org.hpcshelf.blas.matrixtype.General;

namespace org.hpcshelf.blas.matrixtype.Symmetric
{
	public interface ISymmetricMatrix : BaseISymmetricMatrix, IGeneralMatrix
	{
	}
}