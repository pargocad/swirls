/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Manufacturer;

namespace org.hpcshelf.platform.node.processor.Family
{
 public interface BaseIProcessorFamily<MAN> : IQualifierKind 
  where MAN:IProcessorManufacturer
 {
 }
}