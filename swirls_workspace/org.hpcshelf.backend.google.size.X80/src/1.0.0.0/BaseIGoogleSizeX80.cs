/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X80
{
 public interface BaseIGoogleSizeX80 : BaseIGoogleSize, IQualifierKind 
 {
 }
}