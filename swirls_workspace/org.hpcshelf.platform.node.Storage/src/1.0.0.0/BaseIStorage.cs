/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.quantifier.IntDown;

namespace org.hpcshelf.platform.node.Storage
{
	public interface BaseIStorage<SIZ, LAT, BAND, NETBAND> : IQualifierKind 
		where SIZ:IntUp
		where LAT:IntDown
		where BAND:IntUp
		where NETBAND:IntUp
	{
	}
}