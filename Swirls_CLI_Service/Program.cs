﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceProcess;
using System.Threading;
using DocoptNet;
using HPCShelfCLIBase;
using static HPCShelfCLIBase.C;

namespace HPCShelfCLI
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            System.Net.ServicePointManager.DefaultConnectionLimit = 100;

            launchPlatformSAfe();
            ServiceBase.Run(new ServiceBase[] { new CLIService() });
        }
    }


    public class Session : ISession
    {
        private IDictionary<int, BlockingCollection<Tuple<int, int, string>>> message_queue = new ConcurrentDictionary<int, BlockingCollection<Tuple<int, int, string>>>();
        public IDictionary<int, BlockingCollection<Tuple<int, int, string>>> MessageQueue { get { return message_queue; } }

        private int count = 1;

        public void send_output(IDictionary<string, object> m_header, string message)
        {
            int handle = (int)m_header["handle"];

            Console.WriteLine("Out[{0}]: {1}", count, message);
            MessageQueue[handle].Add(new Tuple<int, int, string>(handle, count++, message));
            Console.WriteLine("MessageQueue[{0}].Add(new Tuple<int, string>({1}, {2}))", handle, count-1, message);
        }

        public void send_output_finish(int handle)
        {
            MessageQueue[handle].Add(new Tuple<int, int, string>(handle, -1, null));
        }
    }

    [ServiceContract]
    public interface ICLIServices0Wrapper
    {
        [OperationContract]
        string run_command(int handle, string command);

        [OperationContract]
        bool tryGetMessage(int[] handle, out string message);

        [OperationContract]
        bool checkCommand(string[] args, int handle);

        [OperationContract]
        int[] isWaitCommand(string[] args);

        [OperationContract]
        void shutdownSystems();
    }


    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class CLIServices0Wrapper : ICLIServices0Wrapper
    {
        private ICLIServices s;
        public CLIServices0Wrapper(ICLIServices s)
        {
            this.s = s;
        }

        public string run_command(int handle, string command)
        {
            FinishAction remove_handle_action = () => 
            {
                CLIService.SESSION.send_output_finish(handle);
            };

            return s.run_command(command, new Dictionary<string, object>() { { "handle", handle} }, remove_handle_action);
        }

        public bool tryGetMessage(int[] handle_array, out string message)
        {
            int count = -1;
            BlockingCollection<Tuple<int, int, string>>[] qs;
            try
            {
                message = null;

                IList<BlockingCollection<Tuple<int, int, string>>> qs_list = new List<BlockingCollection<Tuple<int, int, string>>>();

                foreach (int h in handle_array)
                    if (CLIService.SESSION.MessageQueue.ContainsKey(h))
                        qs_list.Add(CLIService.SESSION.MessageQueue[h]);
                    
                qs = new BlockingCollection<Tuple<int, int, string>>[qs_list.Count];
                qs_list.CopyTo(qs, 0);

                Tuple<int, int, string> item;
                BlockingCollection<Tuple<int, int, string>>.TakeFromAny(qs, out item);

                int handle = item.Item1;
                count = item.Item2;
                if (count > 0)
                    message = string.Format("Out[{0}]: {1}", item.Item2, item.Item3);

                if (count <= 0)
                {
                    CLIService.SESSION.MessageQueue.Remove(handle);
                    if (async_command_handles_inv.ContainsKey(handle))
                    {
                        string async_flag = async_command_handles_inv[handle];
                        async_command_handles.Remove(async_flag);
                        async_command_handles_inv.Remove(handle);
                    }
                }

            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.Error.WriteLine(e.InnerException.Message);
                    Console.Error.WriteLine(e.InnerException.StackTrace);
                }
                throw e;
            }
            return count > 0 || qs.Length > 1;
        }

        public void shutdownSystems()
        {
            s.shutdownSystems();
        }

        private IDictionary<string, int> async_command_handles = new Dictionary<string, int>();
        private IDictionary<int, string> async_command_handles_inv = new Dictionary<int, string>();

        public bool checkCommand(string[] args, int handle)
        {
            CLIService.SESSION.MessageQueue[handle] = new BlockingCollection<Tuple<int, int, string>>();

            IDictionary<string, ValueObject> arguments = s.parse(args);
            string async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
            if (async_flag != null)
            {
                async_command_handles[async_flag] = handle;
                async_command_handles_inv[handle] = async_flag;
            }
            return async_flag != null;
        }

        public int[] isWaitCommand(string[] args)
        {
            int[] handles = null;
            IDictionary<string, ValueObject> arguments = s.parse(args);

            if (arguments["async_wait"].IsTrue)
            {
                IList<int> handle_list = new List<int>();
                ArrayList name_list = (ArrayList)arguments["<name_list>"].Value;
                for (int i = 0; i < name_list.Count; i++)
                {
                    string async_flag = (string)((ValueObject)name_list[i]).Value;
                    if (async_command_handles.ContainsKey(async_flag))
                        handle_list.Add(async_command_handles[async_flag]);
                }
                handles = new int[handle_list.Count];
                handle_list.CopyTo(handles, 0);
            }

            return handles;
        }
    }

    public class CLIService : ServiceBase
    {
        private static ServiceHost m_svcHost;

        public static Session SESSION = null;
        //public static Session Session { get { return session; } }

        private static void StartHTTPService(int nPort)
        {
            string strAdr = "http://localhost:" + nPort.ToString() + "/CLIService";
            try
            {
                SESSION = new Session();

                Uri adrbase = new Uri(strAdr);
                CLIServices0 s = new CLIServices0(SESSION);
                m_svcHost = new ServiceHost(new CLIServices0Wrapper(s), adrbase);

                m_svcHost.OpenTimeout = new TimeSpan(0, 10, 0);
                m_svcHost.CloseTimeout = new TimeSpan(0, 10, 0);

                ServiceMetadataBehavior mBehave = new ServiceMetadataBehavior();
                m_svcHost.Description.Behaviors.Add(mBehave);
                m_svcHost.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexHttpBinding(), "mex");

                BasicHttpBinding httpb = new BasicHttpBinding();
                m_svcHost.AddServiceEndpoint(typeof(ICLIServices0Wrapper), httpb, strAdr);
                m_svcHost.Open();
                Console.WriteLine("\n\nService is Running as >> " + strAdr);
            }
            catch (Exception eX)
            {
                m_svcHost = null;
                Console.WriteLine("Service can not be started as >> [" + strAdr + "] \n\nError Message [" + eX.Message + "]");
            }
        }


        public CLIService()
        {
            Console.WriteLine("SERVICE");
        }

        protected override void OnStart(string[] args)
        {
            Console.WriteLine("START");
            StartHTTPService(8074);
        }


        protected override void OnStop()
        {
            Console.WriteLine("STOP");
        }




        /*      private const string usage = @"Naval Fate.

            Usage:
              naval_fate.exe ship new <name>...
              naval_fate.exe ship <name> move <x> <y> [--speed=<kn>]
              naval_fate.exe ship shoot <x> <y>
              naval_fate.exe mine (set|remove) <x> <y> [--moored | --drifting]
              naval_fate.exe (-h | --help)
              naval_fate.exe --version

            Options:
              -h --help     Show this screen.
              --version     Show version.
              --speed=<kn>  Speed in knots [default: 10].
              --moored      Moored (anchored) mine.
              --drifting    Drifting mine.

            ";
        */
    }
}