/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.maintainer.Google;

namespace org.hpcshelf.platform.maintainer.google.GoogleLocalhost
{
	public interface BaseIGoogleLocalHost : BaseIGoogle, IEnvironmentKind 
	{
	}
}