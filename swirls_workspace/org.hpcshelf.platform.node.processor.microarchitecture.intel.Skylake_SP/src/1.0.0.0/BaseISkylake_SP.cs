/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake;

namespace org.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake_SP
{
	public interface BaseISkylake_SP : BaseISkylake, IQualifierKind 
	{
	}
}