using org.hpcshelf.platform.locale.AnyWhere;

namespace org.hpcshelf.platform.locale.Europe
{
	public interface IEurope : BaseIEurope, IAnyWhere
	{
	}
}