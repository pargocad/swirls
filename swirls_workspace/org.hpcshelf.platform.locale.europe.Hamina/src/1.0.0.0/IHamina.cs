using org.hpcshelf.platform.locale.europe.Finland;

namespace org.hpcshelf.platform.locale.europe.Hamina
{
	public interface IHamina : BaseIHamina, IFinland
	{
	}
}