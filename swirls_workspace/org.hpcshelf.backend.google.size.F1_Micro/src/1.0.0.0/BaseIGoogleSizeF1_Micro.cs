/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.F1_Micro
{
 public interface BaseIGoogleSizeF1_Micro : BaseIGoogleSize, IQualifierKind 
 {
 }
}