using org.hpcshelf.platform.node.processor.Microarchitecture;

namespace org.hpcshelf.platform.node.processor.microarchitecture.intel.CascadeLake
{
	public interface ICascadeLake : BaseICascadeLake, IProcessorMicroarchitecture<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>
	{
	}
}