using org.hpcshelf.kinds;
using org.hpcshelf.types.Data;
using System;

namespace org.hpcshelf.types.Integer { 

	public interface IInteger : BaseIInteger, IData
	{
		IIntegerInstance newInstance(int i);
	} // end main interface 

	public interface IIntegerInstance : IDataInstance, ICloneable
	{
		int Value { set; get; }
	}

} // end namespace 
