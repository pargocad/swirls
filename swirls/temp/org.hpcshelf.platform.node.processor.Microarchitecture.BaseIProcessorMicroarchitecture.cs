/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Manufacturer;

namespace org.hpcshelf.platform.node.processor.Microarchitecture
{
	public interface BaseIProcessorMicroarchitecture<MAN> : IQualifierKind 
		where MAN:IProcessorManufacturer
	{
	}
}