using org.hpcshelf.platform.node.accelerator.Architecture;

namespace org.hpcshelf.platform.node.accelerator.architecture.Volta
{
	public interface IVolta : BaseIVolta, IAcceleratorArchitecture
	{
	}
}