using org.hpcshelf.platform.node.accelerator.Manufacturer;

namespace org.hpcshelf.platform.node.accelerator.Type
{
	public interface IAcceleratorType<MAN> : BaseIAcceleratorType<MAN>
		where MAN:IAcceleratorManufacturer
	{
	}
}