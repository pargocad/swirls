using org.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake;

namespace org.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake_SP
{
	public interface ISkylake_SP : BaseISkylake_SP, ISkylake
	{
	}
}