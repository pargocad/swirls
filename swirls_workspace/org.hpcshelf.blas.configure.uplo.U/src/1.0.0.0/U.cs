using org.hpcshelf.blas.configure.UPLO;

namespace org.hpcshelf.blas.configure.uplo.U
{
	public interface U : BaseU, IUPLO
	{
	}
}