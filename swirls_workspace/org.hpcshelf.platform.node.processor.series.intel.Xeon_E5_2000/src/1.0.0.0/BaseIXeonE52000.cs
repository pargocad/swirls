/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Series;

namespace org.hpcshelf.platform.node.processor.series.intel.Xeon_E5_2000
{
	public interface BaseIXeonE52000 : BaseIProcessorSeries<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, org.hpcshelf.platform.node.processor.family.intel.Xeon_E5.IXeonE5>, IQualifierKind 
	{
	}
}