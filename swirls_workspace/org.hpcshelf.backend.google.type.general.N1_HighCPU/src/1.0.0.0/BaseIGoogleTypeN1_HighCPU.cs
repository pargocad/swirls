/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.N1_HighCPU
{
 public interface BaseIGoogleTypeN1_HighCPU : BaseIGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>, IQualifierKind 
 {
 }
}