/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X64
{
 public interface BaseIGoogleSizeX64 : BaseIGoogleSize, IQualifierKind 
 {
 }
}