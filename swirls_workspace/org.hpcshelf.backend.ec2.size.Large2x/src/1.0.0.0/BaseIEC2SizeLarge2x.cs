/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large2x
{
	public interface BaseIEC2SizeLarge2x : BaseIEC2Size, IQualifierKind 
	{
	}
}