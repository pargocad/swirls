/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.configure.SIDE;

namespace org.hpcshelf.blas.configure.side.NA
{
	public interface BaseNA : BaseISIDE, IQualifierKind 
	{
	}
}