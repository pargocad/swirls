/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.europe.CentralEurope;

namespace org.hpcshelf.platform.locale.europe.Germany
{
	public interface BaseIGermany : BaseICentralEurope, IQualifierKind 
	{
	}
}