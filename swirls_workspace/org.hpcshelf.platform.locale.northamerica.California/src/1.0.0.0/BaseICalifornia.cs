/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.northamerica.US_West;

namespace org.hpcshelf.platform.locale.northamerica.California
{
	public interface BaseICalifornia : BaseIUS_West, IQualifierKind 
	{
	}
}