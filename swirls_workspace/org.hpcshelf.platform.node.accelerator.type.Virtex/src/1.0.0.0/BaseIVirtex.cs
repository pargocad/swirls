/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.accelerator.Type;

namespace org.hpcshelf.platform.node.accelerator.type.Virtex
{
	public interface BaseIVirtex : BaseIAcceleratorType<org.hpcshelf.platform.node.accelerator.manufacturer.Xilinx.IXilinx>, IQualifierKind 
	{
	}
}