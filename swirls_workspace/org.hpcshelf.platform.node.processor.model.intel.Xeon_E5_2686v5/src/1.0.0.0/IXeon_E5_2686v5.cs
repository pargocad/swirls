using org.hpcshelf.platform.node.processor.Model;

namespace org.hpcshelf.platform.node.processor.model.intel.Xeon_E5_2686v5
{
	public interface IXeon_E5_2686v5 : BaseIXeon_E5_2686v5, IProcessorModel<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, org.hpcshelf.platform.node.processor.family.intel.Xeon_E5.IXeonE5, org.hpcshelf.platform.node.processor.series.intel.Xeon_E5_2000.IXeonE52000, org.hpcshelf.platform.node.processor.microarchitecture.intel.Broadwell.IBroadwell>
	{
	}
}