/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.quantifier.IntDown;

namespace org.hpcshelf.platform.node.Memory
{
	public interface BaseIMemory<SIZ, LAT, BAND> : IQualifierKind 
		where SIZ:IntUp
		where LAT:IntDown
		where BAND:IntUp
	{
	}
}