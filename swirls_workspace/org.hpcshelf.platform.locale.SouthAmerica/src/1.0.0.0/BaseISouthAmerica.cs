/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.AnyWhere;

namespace org.hpcshelf.platform.locale.SouthAmerica
{
	public interface BaseISouthAmerica : BaseIAnyWhere, IQualifierKind 
	{
	}
}