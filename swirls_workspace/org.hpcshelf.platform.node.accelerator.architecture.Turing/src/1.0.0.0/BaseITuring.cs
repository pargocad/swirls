/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.accelerator.Architecture;

namespace org.hpcshelf.platform.node.accelerator.architecture.Turing
{
	public interface BaseITuring : BaseIAcceleratorArchitecture, IQualifierKind 
	{
	}
}