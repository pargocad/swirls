using org.hpcshelf.kinds;
using org.hpcshelf.platform.Maintainer;

namespace org.hpcshelf.platform.maintainer.LocalHost
{
	public interface ILocalHost : BaseILocalHost, IMaintainer
	{
	}
}