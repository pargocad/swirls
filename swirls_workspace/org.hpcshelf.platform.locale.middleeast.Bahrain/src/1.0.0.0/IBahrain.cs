using org.hpcshelf.platform.locale.MiddleEast;

namespace org.hpcshelf.platform.locale.middleeast.Bahrain
{
	public interface IBahrain : BaseIBahrain, IMiddleEast
	{
	}
}