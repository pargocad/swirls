/* Automatically Generated Code */

using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.mpi.Language;
using org.hpcshelf.binding.channel.Binding;
using org.hpcshelf.common.BrowserBinding;
using org.hpcshelf.common.BrowserPortType;
using org.hpcshelf.mpi.BrowserConnectorForC;
using org.hpcshelf.common.browser.SendDataPortType;

namespace org.hpcshelf.mpi.impl.BrowserConnectorForCImpl 
{
	public abstract class BaseIBrowserPeerForCImpl: Synchronizer, BaseIBrowserPeerForC
	{
		private ILanguage language = null;

		protected ILanguage Language
		{
			get
			{
				if (this.language == null)
					this.language = (ILanguage) Services.getPort("language");
				return this.language;
			}
		}
		private IChannel channel = null;

		protected IChannel Channel
		{
			get
			{
				if (this.channel == null)
					this.channel = (IChannel) Services.getPort("channel");
				return this.channel;
			}
		}
		private IBrowserBinding<ISendDataPortType> send_data_port = null;

		public IBrowserBinding<ISendDataPortType> Send_data_port
		{
			get
			{
				if (this.send_data_port == null)
					this.send_data_port = (IBrowserBinding<ISendDataPortType>) Services.getPort("send_data_port");
				return this.send_data_port;
			}
		}
		private ISendDataPortType send_data_port_type = null;

		protected ISendDataPortType Send_data_port_type
		{
			get
			{
				if (this.send_data_port_type == null)
					this.send_data_port_type = (ISendDataPortType) Services.getPort("send_data_port_type");
				return this.send_data_port_type;
			}
		}
	}
}