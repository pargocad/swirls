using org.hpcshelf.platform.node.processor.Manufacturer;

namespace org.hpcshelf.platform.node.processor.Family
{
 public interface IProcessorFamily<MAN> : BaseIProcessorFamily<MAN>
  where MAN:IProcessorManufacturer
 {
 }
}