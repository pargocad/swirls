using org.hpcshelf.kinds;
using org.hpcshelf.types.Data;
using System;

namespace org.hpcshelf.types.Complex { 

	public interface IComplex : BaseIComplex, IData
	{
		IComplexInstance newInstance(System.Numerics.Complex i);
	} // end main interface 

	public interface IComplexInstance : IDataInstance, ICloneable
	{
		System.Numerics.Complex Value { set; get; }
	}

} // end namespace 
