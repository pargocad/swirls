/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;
using org.hpcshelf.backend.ec2.family.Storage;

namespace org.hpcshelf.backend.ec2.type.storage.H1
{
	public interface BaseIEC2TypeH1 : BaseIEC2Type<IEC2FamilyStorage>, IQualifierKind 
	{
	}
}