/* Automatically Generated Code */

using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.mpi.Language;
using org.hpcshelf.binding.channel.Binding;
using org.hpcshelf.common.BrowserBinding;
using org.hpcshelf.common.BrowserPortType;
using org.hpcshelf.mpi.wrapper.WBrowserConnector;
using org.hpcshelf.common.browser.wrapper.WSendDataPortType;

namespace org.hpcshelf.mpi.wrapper.impl.WBrowserConnectorImpl 
{
	public abstract class BaseIWBrowserPeerImpl: Synchronizer, BaseIWBrowserPeer
	{
		private ILanguage language = null;

		protected ILanguage Language
		{
			get
			{
				if (this.language == null)
					this.language = (ILanguage) Services.getPort("language");
				return this.language;
			}
		}
		private IChannel channel = null;

		protected IChannel Channel
		{
			get
			{
				if (this.channel == null)
					this.channel = (IChannel) Services.getPort("channel");
				return this.channel;
			}
		}
		private IBrowserBinding<IWSendDataPortType> send_data_port = null;

		public IBrowserBinding<IWSendDataPortType> Send_data_port
		{
			get
			{
				if (this.send_data_port == null)
					this.send_data_port = (IBrowserBinding<IWSendDataPortType>) Services.getPort("send_data_port");
				return this.send_data_port;
			}
		}
		private IWSendDataPortType send_data_port_type = null;

		protected IWSendDataPortType Send_data_port_type
		{
			get
			{
				if (this.send_data_port_type == null)
					this.send_data_port_type = (IWSendDataPortType) Services.getPort("send_data_port_type");
				return this.send_data_port_type;
			}
		}
	}
}