/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.configure.SIDE;

namespace org.hpcshelf.blas.configure.side.R
{
	public interface BaseR : BaseISIDE, IQualifierKind 
	{
	}
}