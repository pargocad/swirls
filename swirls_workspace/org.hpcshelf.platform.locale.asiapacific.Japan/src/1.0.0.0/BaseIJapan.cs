/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.Japan
{
	public interface BaseIJapan : BaseIAsiaPacific, IQualifierKind 
	{
	}
}