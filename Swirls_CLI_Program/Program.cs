﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.Threading;
using org.hpcshelf.command;

namespace HPCShelfCLIProgram
{
    class MainClass
    {
        [ServiceContract]
        public interface ICLIServices0Wrapper
        {
            [OperationContract]
            string run_command(int handle, string[] args);

            [OperationContract]
            bool tryGetMessage(int[] handle, out string message);

            [OperationContract]
            bool checkCommand(string[] args, int handle);

            [OperationContract]
            int[] isWaitCommand(string[] args);

            [OperationContract]
            void shutdownSystems();
        }

        public static void Main(string[] args)
        {
            System.Net.ServicePointManager.DefaultConnectionLimit = 100;

            if (args[0].Equals("init"))
            {
                launchCLIService();
            }
            else
            {
                ICLIServices0Wrapper s = Get("localhost", 8074);

                IList<int> handle_list = new List<int>();
                int handle = (new object()).GetHashCode();
                handle_list.Add(handle);

                bool wait_messages =  true;

                bool isAsync = s.checkCommand(args, handle);
                if (isAsync)
                {
                    wait_messages = false;
                }
                else
                {
                    int[] wait_handles = s.isWaitCommand(args);
                    if (wait_handles != null)
                    {
                        foreach (int h in wait_handles)
                        {
                            handle_list.Add(h);
                        }
                    }
                }

                Thread t = new Thread(() => {
                    s.run_command(handle, args); 
                });

                t.Start();

                if (wait_messages)
                {
                    int[] handle_array = new int[handle_list.Count];
                    handle_list.CopyTo(handle_array, 0);

                    string message;
                    while (s.tryGetMessage(handle_array, out message))
                        Console.WriteLine(message);
                }
            }
        }

        public static void launchCLIService()
        {
            try
            {
                string path = System.Environment.GetEnvironmentVariable("Swirls_HOME");
                if (path == null)
                {
                    Console.WriteLine("Swirls_HOME not set. Using the current directory.");
                    path = Utils.CURRENT_FOLDER;
                }

                string script_command = Path.Combine(path, "run_cli_service.sh");

                Console.WriteLine("STARTING CLI service: {0}", script_command);
                Utils.commandExecBash(script_command);
                Console.WriteLine("ok !");
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("\nError while launching PlatformSAFe services: {0}", e.Message);
                Console.Error.WriteLine("-- STACK TRACE -- \n{0}", e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.Error.WriteLine("INNER EXCEPTION: {0}", e.InnerException.Message);
                    Console.Error.WriteLine("-- STACK TRACE -- \n{0}", e.InnerException.StackTrace);
                }
            }
        }

        private static ICLIServices0Wrapper Get(string strServer, int nPort)
		{
            ICLIServices0Wrapper mathSvcObj = null;

			ChannelFactory<ICLIServices0Wrapper> channelFactory = null;
			EndpointAddress ep = null;

			BasicHttpBinding httpb = new BasicHttpBinding();
			httpb.OpenTimeout = new TimeSpan(0, 10, 0);
			httpb.CloseTimeout = new TimeSpan(0, 10, 0);
			httpb.SendTimeout = new TimeSpan(0, 10, 0);
			httpb.ReceiveTimeout = new TimeSpan(0, 10, 0);
			channelFactory = new ChannelFactory<ICLIServices0Wrapper>(httpb);

			// End Point Address
			string strEPAdr = "http://" + strServer + ":" + nPort.ToString() + "/CLIService";
			try
			{
				// Create End Point
				ep = new EndpointAddress(strEPAdr);

				// Create Channel
				mathSvcObj = channelFactory.CreateChannel(ep);

				//channelFactory.Close();
			}
			catch (Exception eX)
			{
				// Something unexpected happended .. 
				Console.WriteLine("Error while performing operation [" +
				  eX.Message + "] \n\n Inner Exception [" + eX.InnerException + "]");
			}

			return mathSvcObj;
		}
	}
}
