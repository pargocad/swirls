using org.hpcshelf.platform.locale.northamerica.UnitedStates;

namespace org.hpcshelf.platform.locale.northamerica.US_West
{
	public interface IUS_West : BaseIUS_West, IUnitedStates
	{
	}
}