using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.N2D_HighCPU
{
 public interface IGoogleTypeN2D_HighCPU : BaseIGoogleTypeN2D_HighCPU, IGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>
 {
 }
}