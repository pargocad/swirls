	using System;
	using System.IO;
	using System.Text;
    using System.Collections.Generic;
	using org.hpcshelf.DGAC;
	using org.hpcshelf.unit;
	using org.hpcshelf.kinds;
	using org.hpcshelf.mpi.Language;
	using org.hpcshelf.platform.Maintainer;
	using org.hpcshelf.types.YesOrNo;
	using org.hpcshelf.quantifier.IntUp;
	using org.hpcshelf.platform.locale.AnyWhere;
	using org.hpcshelf.quantifier.DecimalDown;
	using org.hpcshelf.platform.node.processor.Manufacturer;
	using org.hpcshelf.platform.node.processor.Family;
	using org.hpcshelf.platform.node.processor.Series;
	using org.hpcshelf.platform.node.processor.Microarchitecture;
	using org.hpcshelf.platform.node.processor.Model;
	using org.hpcshelf.platform.node.processor.cache.Mapping;
	using org.hpcshelf.quantifier.IntDown;
	using org.hpcshelf.platform.node.processor.Cache;
	using org.hpcshelf.platform.node.processor.Core;
	using org.hpcshelf.platform.node.Processor;
	using org.hpcshelf.platform.node.accelerator.Manufacturer;
	using org.hpcshelf.platform.node.accelerator.Type;
	using org.hpcshelf.platform.node.accelerator.Architecture;
	using org.hpcshelf.platform.node.accelerator.Model;
	using org.hpcshelf.platform.node.Accelerator;
	using org.hpcshelf.platform.node.Memory;
	using org.hpcshelf.platform.node.Storage;
	using org.hpcshelf.platform.node.OS;
	using org.hpcshelf.platform.Node;
	using org.hpcshelf.platform.interconnection.Topology;
	using org.hpcshelf.platform.Interconnection;
	using org.hpcshelf.platform.Performance;
	using org.hpcshelf.platform.Power;
	using org.hpcshelf.platform.Platform;
	using org.hpcshelf.mpi.MPILauncher;
	using org.hpcshelf.mpi.intercommunicator.BasicSendReceive;
	using org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect;
	using org.hpcshelf.mpi.binding.IntercommunicatorBinding;
	using System.Runtime.InteropServices;
	using org.hpcshelf.common.browser.wrapper.WSendDataPortType;
	using org.hpcshelf.common.BrowserBinding;
	using MPI;
	using org.hpcshelf.DGAC.utils;
	using ENV = System.Environment;
	using org.hpcshelf.common.browser.wrapper.WSendDataPortType;
	using System.Threading;
	
	namespace org.hpcshelf.mpi.impl.MPILauncherForPythonImpl
	{	   
	   
	    using MPI_Comm = Int32;
	   
		public class IProcessLauncherForPythonImpl<L, BS, PLATFORM_M, PLATFORM_VIR, PLATFORM_DED, PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS, PLATFORM_NOD, PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP, PLATFORM_NET, PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND, PLATFORM_STO, PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2, PLATFORM_PER, PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3, PLATFORM_POW, PLATFORM> : BaseIProcessLauncherForPythonImpl<L, BS, PLATFORM_M, PLATFORM_VIR, PLATFORM_DED, PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS, PLATFORM_NOD, PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP, PLATFORM_NET, PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND, PLATFORM_STO, PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2, PLATFORM_PER, PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3, PLATFORM_POW, PLATFORM>, IProcessLauncher<L, BS, PLATFORM_M, PLATFORM_VIR, PLATFORM_DED, PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS, PLATFORM_NOD, PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP, PLATFORM_NET, PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND, PLATFORM_STO, PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2, PLATFORM_PER, PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3, PLATFORM_POW, PLATFORM>
			where L:ILanguage
			where BS:IWSendDataPortType
			where PLATFORM_M:IMaintainer
			where PLATFORM_VIR:IYesOrNo
			where PLATFORM_DED:IYesOrNo
			where PLATFORM_N:IntUp
			where PLATFORM_LOC:IAnyWhere
			where PLATFORM_CPH:DecimalDown
			where PLATFORM_POWER:DecimalDown
			where PLATFORM_NOD_PRO_PCT:IntUp
			where PLATFORM_NOD_PRO_MAN:IProcessorManufacturer
			where PLATFORM_NOD_PRO_FAM:IProcessorFamily<PLATFORM_NOD_PRO_MAN>
			where PLATFORM_NOD_PRO_SER:IProcessorSeries<PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM>
			where PLATFORM_NOD_PRO_MIC:IProcessorMicroarchitecture<PLATFORM_NOD_PRO_MAN>
			where PLATFORM_NOD_PRO_MOD:IProcessorModel<PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC>
			where PLATFORM_NOD_PRO_NCT:IntUp
			where PLATFORM_NOD_PRO_CLK:IntUp
			where PLATFORM_NOD_PRO_TPC:IntUp
			where PLATFORM_NOD_PRO_MAP1i:ICacheMapping
			where PLATFORM_NOD_PRO_SIZ1i:IntUp
			where PLATFORM_NOD_PRO_LAT1i:IntDown
			where PLATFORM_NOD_PRO_LINSIZ1i:IntUp
			where PLATFORM_NOD_PRO_CL1i:ICache<PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i>
			where PLATFORM_NOD_PRO_MAP1d:ICacheMapping
			where PLATFORM_NOD_PRO_SIZ1d:IntUp
			where PLATFORM_NOD_PRO_LAT1d:IntDown
			where PLATFORM_NOD_PRO_LINSIZ1d:IntUp
			where PLATFORM_NOD_PRO_CL1d:ICache<PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d>
			where PLATFORM_NOD_PRO_MAP2:ICacheMapping
			where PLATFORM_NOD_PRO_SIZ2:IntUp
			where PLATFORM_NOD_PRO_LAT2:IntDown
			where PLATFORM_NOD_PRO_LINSIZ2:IntUp
			where PLATFORM_NOD_PRO_CL2:ICache<PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2>
			where PLATFORM_NOD_PRO_COR:ICore<PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2>
			where PLATFORM_NOD_PRO_MAP3:ICacheMapping
			where PLATFORM_NOD_PRO_SIZ3:IntUp
			where PLATFORM_NOD_PRO_LAT3:IntDown
			where PLATFORM_NOD_PRO_LINSIZ3:IntUp
			where PLATFORM_NOD_PRO_CL3:ICache<PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3>
			where PLATFORM_TsFMA:DecimalDown
			where PLATFORM_TdFMA:DecimalDown
			where PLATFORM_TiADD:DecimalDown
			where PLATFORM_TiSUB:DecimalDown
			where PLATFORM_TiMUL:DecimalDown
			where PLATFORM_TiDIV:DecimalDown
			where PLATFORM_TsADD:DecimalDown
			where PLATFORM_TsSUB:DecimalDown
			where PLATFORM_TsMUL:DecimalDown
			where PLATFORM_TsDIV:DecimalDown
			where PLATFORM_TdADD:DecimalDown
			where PLATFORM_TdSUB:DecimalDown
			where PLATFORM_TdMUL:DecimalDown
			where PLATFORM_TdDIV:DecimalDown
			where PLATFORM_NOD_PRO:IProcessor<PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV>
			where PLATFORM_NOD_ACC_NCT:IntUp
			where PLATFORM_NOD_ACC_MAN:IAcceleratorManufacturer
			where PLATFORM_NOD_ACC_TYP:IAcceleratorType<PLATFORM_NOD_ACC_MAN>
			where PLATFORM_NOD_ACC_ARC:IAcceleratorArchitecture
			where PLATFORM_NOD_ACC_MOD:IAcceleratorModel<PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC>
			where PLATFORM_NOD_ACC_MEM:IntUp
			where PLATFORM_NOD_ACC:IAccelerator<PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM>
			where PLATFORM_NOD_MEM_SIZ:IntUp
			where PLATFORM_NOD_MEM_LAT:IntDown
			where PLATFORM_NOD_MEM_BAND:IntUp
			where PLATFORM_NOD_MEM:IMemory<PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND>
			where PLATFORM_NOD_STO_LAT:IntDown
			where PLATFORM_NOD_STO_SIZ:IntUp
			where PLATFORM_NOD_STO_BAND:IntUp
			where PLATFORM_NOD_STO_NETBAND:IntUp
			where PLATFORM_NOD_STO:IStorage<PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND>
			where PLATFORM_NOD_OS:IOperatingSystem
			where PLATFORM_NOD:INode<PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS>
			where PLATFORM_NET_STT:IntDown
			where PLATFORM_NET_NLT:IntDown
			where PLATFORM_NET_BAN:IntUp
			where PLATFORM_NET_TOP:ITopology
			where PLATFORM_NET:IInterconnection<PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP>
			where PLATFORM_STO_SIZ:IntUp
			where PLATFORM_STO_LAT:IntDown
			where PLATFORM_STO_BAND:IntUp
			where PLATFORM_STO_NETBAND:IntUp
			where PLATFORM_STO:IStorage<PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND>
			where PLATFORM_PER_P0:IntUp
			where PLATFORM_PER_P1:IntUp
			where PLATFORM_PER_P2:IntUp
			where PLATFORM_PER:IPerformance<PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2>
			where PLATFORM_POW_P0:IntDown
			where PLATFORM_POW_P1:IntUp
			where PLATFORM_POW_P2:IntDown
			where PLATFORM_POW_P3:IntUp
			where PLATFORM_POW:IPower<PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3>
			where PLATFORM:IProcessingNode<PLATFORM_M, PLATFORM_VIR, PLATFORM_DED, PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS, PLATFORM_NOD, PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP, PLATFORM_NET, PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND, PLATFORM_STO, PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2, PLATFORM_PER, PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3, PLATFORM_POW>
		{		   
	   
		  //  private bool is_registered = false;
            static TypeOfConnectorSend[] connector_send;
	        static TypeOfConnectorReceive[] connector_receive;
            static int[] facet = null;
            static int[] facet_count = null;
            static int[][] facet_size = null;
            static int[][] facet_instance = null; 
		   
			public override void main()
	        {  	           
               Intracommunicator comm = this.Communicator;
               IList<string> port_names = new List<string>();
               int rank = comm.Rank;
           	   int this_facet_instance = this.FacetInstance;

	           Console.WriteLine("IProcessLauncher 0");
	           
	           foreach (string s in this.Slice.Keys)
	           {
	              Console.WriteLine("IProcessLauncher 00 -- port={0}", s);
	              if (s.StartsWith("port"))
	                 port_names.Add(s);
	           }
	           
	           int n = port_names.Count;
	           
	           Console.WriteLine("IProcessLauncher 1 -- n={0}", n);
	           
	           IIntercommunicatorBinding<IBasicSendReceive>[] port = new IIntercommunicatorBinding<IBasicSendReceive>[n];
	           connector_send = new TypeOfConnectorSend[n];
	           connector_receive = new TypeOfConnectorReceive[n];
	           facet = new int[n];
               facet_count = new int[n];
	           facet_size = new int[n][];
               facet_instance = new int[n][]; 

	           Console.WriteLine("IProcessLauncher 2 -- n={0}", n);

	           for (int i=0; i<n; i++)
	           {	           
	               port[i] = (IIntercommunicatorBinding<IBasicSendReceive>) Services.getPort(port_names[i]);
		           IBasicSendReceive client_port = (IBasicSendReceive) port[i].Client;
		           connector_send[i] = client_port.Send;
		           connector_receive[i] = client_port.Receive;
		           facet[i] = client_port.Facet;
		           facet_count[i] = client_port.FacetCount;
		           facet_size[i] = client_port.FacetSize;
		           facet_instance[i] = client_port.FacetInstance;
		           Console.WriteLine("REGISTER CHANNEL n={0}, i={1}, facet[{1}]={2}, facet_count[{1}]={3}, facet_instance[{1}]={4}, facet_size[{1}]={5}, send[{1}]={6}, receive[{1}]={7}", n, i, facet[i], facet_count[i], facet_instance[i], facet_size[i], connector_send[i], connector_receive[i]);
		           foreach (int uuu in facet_instance[i])
		              Console.WriteLine("facet_instance[{0}]={1}", i, uuu);
		           foreach (int uuu in facet_size[i])
		              Console.WriteLine("facet_size[{0}]={1}", i, uuu);
	           }
	           
	           foreach (string p in port_names)
	               Services.releasePort(p);
	           	           
               string run_dir = System.Environment.GetEnvironmentVariable("MPIRUN_PATH");
               Console.WriteLine("{1}/{2}: MPIRUN_PATH yyy= {0}", run_dir, this_facet_instance, rank);
	           
               string path;
               path = ENV.GetEnvironmentVariable("PYTHON");
               if (path == null || path.Equals(""))
                  path = "python3";
               else 
                  path = path.Trim();

                //Intracommunicator comm = this.Communicator;
                //Intercommunicator inter_comm;

               Console.WriteLine("{1}/{2}: MPIRUN_PATH x = {0}", run_dir, this_facet_instance, rank);

               string[] argv = new string[1];
               argv[0] = Path.Combine(run_dir, "<file_name>");
               int maxprocs = comm.Size;
               int[] error_codes = new int[maxprocs];

               Console.WriteLine("{1}/{2}: MPIRUN_PATH xx = {0}", run_dir, this_facet_instance, rank);

	           IBrowserBinding<IWSendDataPortType> browse_port = (IBrowserBinding<IWSendDataPortType>)Services.getPort("browser_port");
	           IWSendDataPortType browser = (IWSendDataPortType)browse_port.Client;

               Console.WriteLine("{1}/{2}: MPIRUN_PATH xxx = {0}", run_dir, this_facet_instance, rank);

               Console.WriteLine("{0}: BEFORE SPAWN --- path={1} / argv[0]={2}", comm.Rank, path, argv[0]);
               // inter_comm = comm.Spawn(path, argv, maxprocs, 0);
               MPI_Comm inter_comm = Spawn(comm.comm, path, argv, maxprocs, 0, browser, this_facet_instance);
               Console.WriteLine("{0}: AFTER SPAWN 1", comm.Rank);
               // browser.Browse("BEFORE SPAWN " + comm.Rank);
		    }
	        
	        const int REQUEST_FACET = 0;
	        const int REQUEST_FACET_COUNT = 1;
	        const int REQUEST_FACET_SIZE = 2;
	        const int REQUEST_FACET_INSTANCE = 3;
	        
	        static int Spawn(MPI_Comm comm, 
	                         string command, 
	                         string[] argv, 
	                         int maxprocs, 
	                         int root, 
	                         IWSendDataPortType browser, 
	                         int facet_instance)
	        {
	            MPI_Comm intercomm;	            
	            
	            unsafe
	            {
	                ASCIIEncoding ascii = new ASCIIEncoding();
               	    char[] command_ = command.ToCharArray();
               	    int l = ascii.GetByteCount(command_);
               	    byte* byte_command_p = stackalloc byte[l + 1];
                    fixed (char* command__ = command_)
                    {
                        if (l > 0)
                        {
                            ascii.GetBytes(command__, command_.Length, byte_command_p, l);
                        }	                       
                        byte_command_p[l] = 0;
                    }
	               	
	                // Copy args into C-style argc/argv
	                byte** my_argv = stackalloc byte*[argv.Length];
	                	                
	                for (int argidx = 0; argidx < argv.Length; ++argidx)
	                {
	                    // Copy argument into a byte array (C-style characters)
	                    char[] arg = argv[argidx].ToCharArray();
	                    fixed (char* argp = &arg[0])
	                    {
	                        int length = ascii.GetByteCount(arg);
	                        byte* c_arg = stackalloc byte[length + 1];
	                        if (length > 0)
	                        {
	                            ascii.GetBytes(argp, arg.Length, c_arg, length);
	                        }
	                        my_argv[argidx] = c_arg;
	                        my_argv[argidx][length] = 0;
	                    }
	                }
	                if (argv == null || argv.Length == 0)
	                {
	                    //my_argv = Unsafe.MPI_ARGV_NULL;
	                    my_argv = (byte**)0;
	                }	                
	 	
                    Unsafe.MPI_Comm_spawn(byte_command_p, my_argv, maxprocs, X.get_hosts_info_for_spawn(), root, comm, out intercomm, (int*)0);
	                
	                int rank;
	                Unsafe.MPI_Comm_rank(intercomm, out rank);
	                    	                  	                  
	                Thread t_browse = create_thread_browse(rank, intercomm, browser); 
	                t_browse.Start();
	                  
	            //    Thread t_send_data = create_thread_send_data(rank, intercomm); 
	            //    t_send_data.Start();
	                  
	             //   Thread t_receive_data = create_thread_receive_data(rank, intercomm); 
	             //   t_receive_data.Start();
	                
	              //  Thread t_query = create_thread_query_topology(rank, intercomm);
	              //  t_query.Start();	
	                
	                t_browse.Join(); 	                  
	             //   t_send_data.Abort(); 
	              //  t_receive_data.Abort(); 
	             //   t_query.Abort();            
	            }
	
	            return intercomm;
	        }
	        	 
	        static Thread create_thread_browse(int rank, MPI_Comm intercomm, IWSendDataPortType browser)
	        {
	            Thread r = new Thread(() => 
                {
                    unsafe
                    {
		                string message;
		                do
		                {
		                    byte[] size_ = new byte[4]; 
		                    fixed (byte* s = size_)
		                    {
		                       IntPtr s_buf = (IntPtr) s;
		                       Unsafe.MPI_Status status;	                    
		                       Unsafe.MPI_Recv(s_buf, 4, Unsafe.MPI_BYTE, rank, 111, intercomm, out status);
		                    }
		                   
		                    int size = (int) size_[3];
		                    	                   
		                    byte[] m = new byte[13 + size + 2];
			                fixed (byte* p = m)
			                {   
			                    IntPtr buf = (IntPtr) p;
			                    Unsafe.MPI_Status status;	                    
			                    Unsafe.MPI_Recv(buf, m.Length, Unsafe.MPI_BYTE, rank, 222, intercomm, out status);
			                }
			                
			                char[] mm = new char[m.Length - 13];
			                for (int i=13; i<m.Length-2; i++)	
			                    mm[i-13] = (char)m[i];
		                
		                   //ASCIIEncoding ascii = new System.Text.ASCIIEncoding.ASCIIEncoding();
	                       message = new String(mm);
	                       browser.Browse(message); 
	                       Console.WriteLine(message);	                   
		                } 
		                while (!message.EndsWith("done"));
		                    
		                //Unsafe.MPI_Barrier(comm);
		                
	                    browser.Browse("!EOS");
	                }
		        });
		     
                return r;	               
	        } 

	        static Thread create_thread_send_data(int rank, MPI_Comm intercomm)
	        {
	            Thread r = new Thread(() => 
                {
                    unsafe
                    {
		                int[] p = new int[6]; 
		                do
		                {
						    fixed (int* s = p)
						    {
						       IntPtr s_buf = (IntPtr) s;
						       Unsafe.MPI_Status status;	                    
						       Unsafe.MPI_Recv(s_buf, 6, Unsafe.MPI_INT, rank, 333, intercomm, out status);
						    }
						    
						    int count = p[0];
						    int facet = p[1];
						    int target = p[2];
						    int datatype = p[3];
						    int tag = p[4];
						    int key = p[5];
						       
				                
						    int datatype_size;
						    Unsafe.MPI_Type_size(datatype, out datatype_size);
						    						    
				            Console.WriteLine("-- HPCShelf_Send -- count={0}, facet={1}, target={2}, datatype={3}, tag={4}, key={5}, datatype_size={6}", count, facet, target, datatype, tag, key, datatype_size);
				            
						    byte[] data = new byte[count*datatype_size];
							fixed (byte* buf = data)
							{   								 
							     Unsafe.MPI_Status status;
								 Unsafe.MPI_Recv((IntPtr) buf, count, datatype, rank, 444, intercomm, out status);
   							     connector_send[key](key, (IntPtr) buf, count, datatype, facet, target, tag);
							}				        
		                }
		                while (true);
                    }
                });
	           
                return r;	               
	        }
	        
	        static Thread create_thread_receive_data(int rank, MPI_Comm intercomm)
	        {
	            Thread r = new Thread(() => 
                {                
                	unsafe
                	{
		                int[] p = new int[6]; 
		                do
		                {
				               
				            fixed (int* s = p)
				            {
				               IntPtr s_buf = (IntPtr) s;
				               Unsafe.MPI_Status status;
				               Unsafe.MPI_Recv(s_buf, 6, Unsafe.MPI_INT, rank, 555, intercomm, out status);
				            }

                            int count = p[0];
			                int facet = p[1];
			                int source = p[2];
			                int datatype = p[3];
			                int tag = p[4];
			                int key = p[5];
			                
			               
						    int datatype_size;
						    Unsafe.MPI_Type_size(datatype, out datatype_size);						    
						    						 
				            Console.WriteLine("-- HPCShelf_Recv -- count={0}, facet={1}, source={2}, datatype={3}, tag={4}, key={5}, datatype_size={6}", count, facet, source, datatype, tag, key, datatype_size);
				            
						    byte[] data = new byte[count*datatype_size];
						    fixed (byte* d = data)
						    { 
						    	IntPtr buf = (IntPtr) d;
						    	Unsafe.MPI_Status status; 
							    connector_receive[key](key, buf, count, datatype, facet, source, tag, (IntPtr) (&status));
							    Unsafe.MPI_Send(buf, count, datatype, rank, 666, intercomm);
						    }
		                }
		                while (true);
                    }
                });
	           
                return r;	               
	        }    	        
	            	        
	        static Thread create_thread_query_topology(int rank, MPI_Comm intercomm)
	        {
	            Thread r = new Thread(() => 
                {
                	unsafe
                	{
		                int[] p = new int[2]; 
		                
		                do
		                {
				            fixed (int* s = p)
				            {
				               IntPtr s_buf = (IntPtr) s;
				               Unsafe.MPI_Status status;	                    
				               Unsafe.MPI_Recv(s_buf, 2, Unsafe.MPI_INT, rank, 777, intercomm, out status);
				            }
				            
				            int[] reply = null;
				            int size = default(int); IntPtr ptr_size = (IntPtr) (&size);
				               
				            int query = p[0];
				            int key = p[1];
				            
				            Console.WriteLine("{0}: TOPOLOGY QUERY (query={1}, key={2})", rank, query, key);
				            
				            switch (query)
				            {
				                case REQUEST_FACET:
				                        size = 1;
				                        reply = new int[size];
				                        reply[0] = facet[key];
				                        Console.WriteLine("{0}: TOPOLOGY QUERY (FACET = {1})", rank, facet[key]);
				                        break;
				                case REQUEST_FACET_COUNT:
				                        size = 1;
				                        reply = new int[size];
				                        reply[0] = facet_count[key];
				                        Console.WriteLine("{0}: TOPOLOGY QUERY (FACET_COUNT = {1})", rank, facet_count[key]);
				                        break;
				                case REQUEST_FACET_SIZE:
				                        size = facet_size[key].Length;
		  						        Unsafe.MPI_Send(ptr_size, 1, Unsafe.MPI_INT, rank, 888, intercomm);
				                        reply = new int[size];
				                        facet_size[key].CopyTo(reply, 0);
				                        Console.WriteLine("{0}: TOPOLOGY QUERY (FACET_SIZE[{1}] = {2})", rank, facet[key], facet_size[key][facet[key]]);
				                        break;
				                case REQUEST_FACET_INSTANCE:
				                        size = facet_instance[key].Length;
  								        Unsafe.MPI_Send(ptr_size, 1, Unsafe.MPI_INT, rank, 888, intercomm);
				                        reply = new int[size];
				                        facet_instance[key].CopyTo(reply, 0);
				                        Console.WriteLine("{0}: TOPOLOGY QUERY (FACET_INSTANCE[{1}] = {2})", rank, facet[key], facet_instance[key][facet[key]]);
				                        break;                              
				            }  
				               
						    
						    Console.WriteLine("TOPOLOGY QUERY: begin send ...");
						    fixed (int* reply_pointer = reply)
						    {   
						       IntPtr buf = (IntPtr) reply_pointer;
						       Unsafe.MPI_Send(buf, size, Unsafe.MPI_INT, rank, 999, intercomm);
						    }
						    Console.WriteLine("TOPOLOGY QUERY: ... end send");
						}
						while (true);
	       	        }   
                });
	           
                return r;	               
	        }    	        
		}
		
		
		
	    class X
		{
		    [DllImport("libgethostsinfo.so", EntryPoint = "get_hosts_info_for_spawn")]
		    public static extern int get_hosts_info_for_spawn();
		}
	        		
	}
