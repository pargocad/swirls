using org.hpcshelf.platform.locale.northamerica.US_East;

namespace org.hpcshelf.platform.locale.northamerica.SouthCaroline
{
	public interface ISouthCaroline : BaseISouthCaroline, IUS_East
	{
	}
}