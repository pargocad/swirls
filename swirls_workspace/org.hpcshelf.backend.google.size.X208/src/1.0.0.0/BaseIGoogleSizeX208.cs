/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X208
{
 public interface BaseIGoogleSizeX208 : BaseIGoogleSize, IQualifierKind 
 {
 }
}