/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.Taiwan
{
	public interface BaseITaiwan : BaseIAsiaPacific, IQualifierKind 
	{
	}
}