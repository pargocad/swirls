/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large16x
{
	public interface BaseIEC2SizeLarge16x : BaseIEC2Size, IQualifierKind 
	{
	}
}