using org.hpcshelf.platform.locale.northamerica.US_East;

namespace org.hpcshelf.platform.locale.northamerica.Ohio
{
	public interface IOhio : BaseIOhio, IUS_East
	{
	}
}