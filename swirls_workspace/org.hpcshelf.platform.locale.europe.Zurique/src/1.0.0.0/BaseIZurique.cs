/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.europe.Swiss;

namespace org.hpcshelf.platform.locale.europe.Zurique
{
	public interface BaseIZurique : BaseISwiss, IQualifierKind 
	{
	}
}