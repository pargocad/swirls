/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Family;

namespace org.hpcshelf.platform.node.processor.family.intel.Xeon_E5
{
	public interface BaseIXeonE5 : BaseIProcessorFamily<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>, IQualifierKind 
	{
	}
}