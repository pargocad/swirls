/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.compute.C5
{
	public interface BaseIEC2TypeC5 : BaseIEC2Type<family.Computation.IEC2FamilyComputation>, IQualifierKind 
	{
	}
}