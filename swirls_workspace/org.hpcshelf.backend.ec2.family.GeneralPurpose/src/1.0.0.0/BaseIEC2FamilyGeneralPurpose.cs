/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;

namespace org.hpcshelf.backend.ec2.family.GeneralPurpose
{
	public interface BaseIEC2FamilyGeneralPurpose : BaseIEC2Family, IQualifierKind 
	{
	}
}