using org.hpcshelf.common.BrowserPortType;

namespace org.hpcshelf.common.browser.wrapper.WSendDataPortType
{
	public interface IWSendDataPortType : BaseIWSendDataPortType, IBrowserPortType
	{	   
	   void Browse(string message);
    }
}