/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.Europe;

namespace org.hpcshelf.platform.locale.europe.WestEurope
{
	public interface BaseIWestEurope : BaseIEurope, IQualifierKind 
	{
	}
}