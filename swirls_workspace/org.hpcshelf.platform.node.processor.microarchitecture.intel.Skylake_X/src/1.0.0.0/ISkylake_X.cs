using org.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake;

namespace org.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake_X
{
	public interface ISkylake_X : BaseISkylake_X, ISkylake
	{
	}
}