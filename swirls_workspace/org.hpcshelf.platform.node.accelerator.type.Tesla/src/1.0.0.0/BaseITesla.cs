/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.accelerator.Type;

namespace org.hpcshelf.platform.node.accelerator.type.Tesla
{
	public interface BaseITesla : BaseIAcceleratorType<org.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA>, IQualifierKind 
	{
	}
}