using org.hpcshelf.platform.locale.Asia;

namespace org.hpcshelf.platform.locale.AsiaPacific
{
	public interface IAsiaPacific : BaseIAsiaPacific, IAsia
	{
	}
}