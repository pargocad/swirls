/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.mpi.Language;

namespace org.hpcshelf.mpi.language.C
{
	public interface BaseILanguage_C : BaseILanguage, IQualifierKind 
	{
	}
}