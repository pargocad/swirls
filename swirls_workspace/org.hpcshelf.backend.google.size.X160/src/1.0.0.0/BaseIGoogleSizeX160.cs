/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X160
{
 public interface BaseIGoogleSizeX160 : BaseIGoogleSize, IQualifierKind 
 {
 }
}