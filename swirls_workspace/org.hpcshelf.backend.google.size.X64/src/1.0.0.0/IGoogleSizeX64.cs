using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X64
{
 public interface IGoogleSizeX64 : BaseIGoogleSizeX64, IGoogleSize
 {
 }
}