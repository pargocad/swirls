using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.memory.HighMemory
{
	public interface IEC2TypeHighMemory : BaseIEC2TypeHighMemory, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}