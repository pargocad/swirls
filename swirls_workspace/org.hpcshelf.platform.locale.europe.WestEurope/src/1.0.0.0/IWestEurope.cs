using org.hpcshelf.platform.locale.Europe;

namespace org.hpcshelf.platform.locale.europe.WestEurope
{
	public interface IWestEurope : BaseIWestEurope, IEurope
	{
	}
}