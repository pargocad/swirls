/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.europe.CentralEurope;

namespace org.hpcshelf.platform.locale.europe.France
{
	public interface BaseIFrance : BaseICentralEurope, IQualifierKind 
	{
	}
}