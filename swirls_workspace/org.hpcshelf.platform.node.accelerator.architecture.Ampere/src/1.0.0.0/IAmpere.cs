using org.hpcshelf.platform.node.accelerator.Architecture;

namespace org.hpcshelf.platform.node.accelerator.architecture.Ampere
{
	public interface IAmpere : BaseIAmpere, IAcceleratorArchitecture
	{
	}
}