/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.accelerated.A2_HighCPU
{
 public interface BaseIGoogleTypeA2_HighCPU : BaseIGoogleType<family.Acceleration.IGoogleFamilyAcceleration>, IQualifierKind 
 {
 }
}