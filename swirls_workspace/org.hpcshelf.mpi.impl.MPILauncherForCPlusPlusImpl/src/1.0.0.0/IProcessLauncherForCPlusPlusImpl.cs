	using System;
	using System.IO;
	using System.Threading;
	using System.Text;
    using System.Collections.Generic;
	using org.hpcshelf.DGAC;
	using org.hpcshelf.unit;
	using org.hpcshelf.kinds;
	using org.hpcshelf.mpi.Language;
	using org.hpcshelf.platform.Maintainer;
	using org.hpcshelf.types.YesOrNo;
	using org.hpcshelf.quantifier.IntUp;
	using org.hpcshelf.platform.locale.AnyWhere;
	using org.hpcshelf.quantifier.DecimalDown;
	using org.hpcshelf.platform.node.processor.Manufacturer;
	using org.hpcshelf.platform.node.processor.Family;
	using org.hpcshelf.platform.node.processor.Series;
	using org.hpcshelf.platform.node.processor.Microarchitecture;
	using org.hpcshelf.platform.node.processor.Model;
	using org.hpcshelf.platform.node.processor.cache.Mapping;
	using org.hpcshelf.quantifier.IntDown;
	using org.hpcshelf.platform.node.processor.Cache;
	using org.hpcshelf.platform.node.processor.Core;
	using org.hpcshelf.platform.node.Processor;
	using org.hpcshelf.platform.node.accelerator.Manufacturer;
	using org.hpcshelf.platform.node.accelerator.Type;
	using org.hpcshelf.platform.node.accelerator.Architecture;
	using org.hpcshelf.platform.node.accelerator.Model;
	using org.hpcshelf.platform.node.Accelerator;
	using org.hpcshelf.platform.node.Memory;
	using org.hpcshelf.platform.node.Storage;
	using org.hpcshelf.platform.node.OS;
	using org.hpcshelf.platform.Node;
	using org.hpcshelf.platform.interconnection.Topology;
	using org.hpcshelf.platform.Interconnection;
	using org.hpcshelf.platform.Performance;
	using org.hpcshelf.platform.Power;
	using org.hpcshelf.platform.Platform;
	using org.hpcshelf.mpi.MPILauncher;
	using org.hpcshelf.mpi.intercommunicator.BasicSendReceive;
	using org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect;
	using org.hpcshelf.mpi.binding.IntercommunicatorBinding;
	using System.Runtime.InteropServices;
	using org.hpcshelf.common.browser.wrapper.WSendDataPortType;
	using org.hpcshelf.common.BrowserBinding;
	using MPI;
	using org.hpcshelf.DGAC.utils;
	using ENV = System.Environment;
	using System.Collections.Concurrent;
	using org.hpcshelf.common.browser.wrapper.WSendDataPortType;
	
	namespace org.hpcshelf.mpi.impl.MPILauncherForCPlusPlusImpl
	{	   
	   
	    using MPI_Comm = Int32;
	   
		public class IProcessLauncherForCPlusPlusImpl<L, BS, PLATFORM_M, PLATFORM_VIR, PLATFORM_DED, PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS, PLATFORM_NOD, PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP, PLATFORM_NET, PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND, PLATFORM_STO, PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2, PLATFORM_PER, PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3, PLATFORM_POW, PLATFORM> : BaseIProcessLauncherForCPlusPlusImpl<L, BS, PLATFORM_M, PLATFORM_VIR, PLATFORM_DED, PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS, PLATFORM_NOD, PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP, PLATFORM_NET, PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND, PLATFORM_STO, PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2, PLATFORM_PER, PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3, PLATFORM_POW, PLATFORM>, IProcessLauncher<L, BS, PLATFORM_M, PLATFORM_VIR, PLATFORM_DED, PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS, PLATFORM_NOD, PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP, PLATFORM_NET, PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND, PLATFORM_STO, PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2, PLATFORM_PER, PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3, PLATFORM_POW, PLATFORM>
			where L:ILanguage
			where BS:IWSendDataPortType
			where PLATFORM_M:IMaintainer
			where PLATFORM_VIR:IYesOrNo
			where PLATFORM_DED:IYesOrNo
			where PLATFORM_N:IntUp
			where PLATFORM_LOC:IAnyWhere
			where PLATFORM_CPH:DecimalDown
			where PLATFORM_POWER:DecimalDown
			where PLATFORM_NOD_PRO_PCT:IntUp
			where PLATFORM_NOD_PRO_MAN:IProcessorManufacturer
			where PLATFORM_NOD_PRO_FAM:IProcessorFamily<PLATFORM_NOD_PRO_MAN>
			where PLATFORM_NOD_PRO_SER:IProcessorSeries<PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM>
			where PLATFORM_NOD_PRO_MIC:IProcessorMicroarchitecture<PLATFORM_NOD_PRO_MAN>
			where PLATFORM_NOD_PRO_MOD:IProcessorModel<PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC>
			where PLATFORM_NOD_PRO_NCT:IntUp
			where PLATFORM_NOD_PRO_CLK:IntUp
			where PLATFORM_NOD_PRO_TPC:IntUp
			where PLATFORM_NOD_PRO_MAP1i:ICacheMapping
			where PLATFORM_NOD_PRO_SIZ1i:IntUp
			where PLATFORM_NOD_PRO_LAT1i:IntDown
			where PLATFORM_NOD_PRO_LINSIZ1i:IntUp
			where PLATFORM_NOD_PRO_CL1i:ICache<PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i>
			where PLATFORM_NOD_PRO_MAP1d:ICacheMapping
			where PLATFORM_NOD_PRO_SIZ1d:IntUp
			where PLATFORM_NOD_PRO_LAT1d:IntDown
			where PLATFORM_NOD_PRO_LINSIZ1d:IntUp
			where PLATFORM_NOD_PRO_CL1d:ICache<PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d>
			where PLATFORM_NOD_PRO_MAP2:ICacheMapping
			where PLATFORM_NOD_PRO_SIZ2:IntUp
			where PLATFORM_NOD_PRO_LAT2:IntDown
			where PLATFORM_NOD_PRO_LINSIZ2:IntUp
			where PLATFORM_NOD_PRO_CL2:ICache<PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2>
			where PLATFORM_NOD_PRO_COR:ICore<PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2>
			where PLATFORM_NOD_PRO_MAP3:ICacheMapping
			where PLATFORM_NOD_PRO_SIZ3:IntUp
			where PLATFORM_NOD_PRO_LAT3:IntDown
			where PLATFORM_NOD_PRO_LINSIZ3:IntUp
			where PLATFORM_NOD_PRO_CL3:ICache<PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3>
			where PLATFORM_TsFMA:DecimalDown
			where PLATFORM_TdFMA:DecimalDown
			where PLATFORM_TiADD:DecimalDown
			where PLATFORM_TiSUB:DecimalDown
			where PLATFORM_TiMUL:DecimalDown
			where PLATFORM_TiDIV:DecimalDown
			where PLATFORM_TsADD:DecimalDown
			where PLATFORM_TsSUB:DecimalDown
			where PLATFORM_TsMUL:DecimalDown
			where PLATFORM_TsDIV:DecimalDown
			where PLATFORM_TdADD:DecimalDown
			where PLATFORM_TdSUB:DecimalDown
			where PLATFORM_TdMUL:DecimalDown
			where PLATFORM_TdDIV:DecimalDown
			where PLATFORM_NOD_PRO:IProcessor<PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV>
			where PLATFORM_NOD_ACC_NCT:IntUp
			where PLATFORM_NOD_ACC_MAN:IAcceleratorManufacturer
			where PLATFORM_NOD_ACC_TYP:IAcceleratorType<PLATFORM_NOD_ACC_MAN>
			where PLATFORM_NOD_ACC_ARC:IAcceleratorArchitecture
			where PLATFORM_NOD_ACC_MOD:IAcceleratorModel<PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC>
			where PLATFORM_NOD_ACC_MEM:IntUp
			where PLATFORM_NOD_ACC:IAccelerator<PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM>
			where PLATFORM_NOD_MEM_SIZ:IntUp
			where PLATFORM_NOD_MEM_LAT:IntDown
			where PLATFORM_NOD_MEM_BAND:IntUp
			where PLATFORM_NOD_MEM:IMemory<PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND>
			where PLATFORM_NOD_STO_LAT:IntDown
			where PLATFORM_NOD_STO_SIZ:IntUp
			where PLATFORM_NOD_STO_BAND:IntUp
			where PLATFORM_NOD_STO_NETBAND:IntUp
			where PLATFORM_NOD_STO:IStorage<PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND>
			where PLATFORM_NOD_OS:IOperatingSystem
			where PLATFORM_NOD:INode<PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS>
			where PLATFORM_NET_STT:IntDown
			where PLATFORM_NET_NLT:IntDown
			where PLATFORM_NET_BAN:IntUp
			where PLATFORM_NET_TOP:ITopology
			where PLATFORM_NET:IInterconnection<PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP>
			where PLATFORM_STO_SIZ:IntUp
			where PLATFORM_STO_LAT:IntDown
			where PLATFORM_STO_BAND:IntUp
			where PLATFORM_STO_NETBAND:IntUp
			where PLATFORM_STO:IStorage<PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND>
			where PLATFORM_PER_P0:IntUp
			where PLATFORM_PER_P1:IntUp
			where PLATFORM_PER_P2:IntUp
			where PLATFORM_PER:IPerformance<PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2>
			where PLATFORM_POW_P0:IntDown
			where PLATFORM_POW_P1:IntUp
			where PLATFORM_POW_P2:IntDown
			where PLATFORM_POW_P3:IntUp
			where PLATFORM_POW:IPower<PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3>
			where PLATFORM:IProcessingNode<PLATFORM_M, PLATFORM_VIR, PLATFORM_DED, PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS, PLATFORM_NOD, PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP, PLATFORM_NET, PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND, PLATFORM_STO, PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2, PLATFORM_PER, PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3, PLATFORM_POW>
		{		   
	   
            static TypeOfConnectorSend[] connector_send;
	        static TypeOfConnectorReceive[] connector_receive;
            static int[] facet = null;
            static int[] facet_count = null;
            static int[][] facet_size = null;
            static int[][] facet_instance = null; 
		   
			public override void main()
	        {  	         
	           	Intracommunicator comm = this.Communicator;
	           	int rank = comm.Rank;
	           	int this_facet_instance = this.FacetInstance;
		           	
	            try 
	            {
		           	
		            Console.WriteLine("{2}/{0}: IProcessLauncher SPAWN in {1}", rank, MPI.Environment.ProcessorName, this_facet_instance);
	                IDictionary<string, int> port_key = new ConcurrentDictionary<string, int>();
	                IDictionary<int, MPI_Comm> comm_port = new ConcurrentDictionary<int, MPI_Comm>();	                
		           
		            {	           
		               IList<string> port_names = new List<string>();
		
			           Console.WriteLine("{1}/{0}: IProcessLauncher 0", rank, facet_instance);
			           
			           foreach (string s in this.Slice.Keys)
			           {
			              Console.WriteLine("{2}/{0}: IProcessLauncher 00 -- port={1}", rank, s, this_facet_instance);
			              if (s.StartsWith("port"))
			                 port_names.Add(s);
			           }
			           
			           int n = port_names.Count;
			           
			           Console.WriteLine("{2}/{0}: IProcessLauncher 1 -- n={1}", rank, n, this_facet_instance);
			           
			           IIntercommunicatorBinding<IBasicSendReceive>[] port = new IIntercommunicatorBinding<IBasicSendReceive>[n];
			           connector_send = new TypeOfConnectorSend[n];
			           connector_receive = new TypeOfConnectorReceive[n];
			           facet = new int[n];
		               facet_count = new int[n];
			           facet_size = new int[n][];
		               facet_instance = new int[n][]; 
		
			           Console.WriteLine("{2}/{0}: IProcessLauncher 2 -- n={1}", rank, n, this_facet_instance);
		
			           for (int i=0; i<n; i++)
			           {	   
			               port_key[port_names[i]] = i;        
			               port[i] = (IIntercommunicatorBinding<IBasicSendReceive>) Services.getPort(port_names[i]);
				           IBasicSendReceive client_port = (IBasicSendReceive) port[i].Client;
				           connector_send[i] = client_port.Send;
				           connector_receive[i] = client_port.Receive;
				           facet[i] = client_port.Facet;
				           facet_count[i] = client_port.FacetCount;
				           facet_size[i] = client_port.FacetSize;
				           facet_instance[i] = client_port.FacetInstance;
				           Console.WriteLine("{9}/{8}: REGISTER CHANNEL n={0}, i={1}, facet[{1}]={2}, facet_count[{1}]={3}, facet_instance[{1}]={4}, facet_size[{1}]={5}, send[{1}]={6}, receive[{1}]={7}", n, i, facet[i], facet_count[i], facet_instance[i], facet_size[i], connector_send[i], connector_receive[i], rank, this_facet_instance);
				           foreach (int uuu in facet_instance[i])
				              Console.WriteLine("{3}/{2}: facet_instance[{0}]={1}", i, uuu, rank, this_facet_instance);
				           foreach (int uuu in facet_size[i])
				              Console.WriteLine("{3}/{2}: facet_size[{0}]={1}", i, uuu, rank, this_facet_instance);
			           }
			           
	                   Console.WriteLine("{1}/{0}: IProcessLauncher 3", rank, this_facet_instance);
	                
			           foreach (string p in port_names)
			               Services.releasePort(p);
			               
	                   Console.WriteLine("{1}/{0}: IProcessLauncher 4", rank, this_facet_instance);
		            }
		           
	
	                //string home_dir = System.Environment.GetEnvironmentVariable("HOME");
	                                
	                string run_dir = System.Environment.GetEnvironmentVariable("MPIRUN_PATH");
	                Console.WriteLine("{1}/{2}: MPIRUN_PATH = {0}", run_dir, this_facet_instance, rank);
	
	                string path = Path.Combine(run_dir,  "<file_name>");
	                //string[] argv = new string[2] {"-f", "worker_peers_filename"};
	                
	                
	                string args_file_name = Path.Combine(run_dir, "args");
	                string[] argv = File.Exists(args_file_name) ? File.ReadAllLines(args_file_name) : new string[0];
	                Console.WriteLine("ARGS FILE NAME = {0} -- {1}", args_file_name, File.Exists(args_file_name));
	                foreach (string arg in argv)
	                   Console.WriteLine("{1}/{2}: arg = {0}", arg, this_facet_instance, rank);   
	                   
	                int maxprocs = comm.Size;
	                int[] error_codes = new int[maxprocs];
	                
	                Console.WriteLine("{1}/{2}: IProcessLauncher 5 = {0}", path, this_facet_instance, rank);
	
		            IBrowserBinding<IWSendDataPortType> browse_port = (IBrowserBinding<IWSendDataPortType>)Services.getPort("browser_port");
		            IWSendDataPortType browser = (IWSendDataPortType)browse_port.Client;
	
	                //Directory.SetCurrentDirectory(path_dir);
	                //Console.WriteLine("CURRENT DIRECTORY is {0}", path_dir);
	
	                Console.WriteLine("{3}/{0}: BEFORE SPAWN C ++--- path={1} -- {2}", comm.Rank, path, MPI.Environment.ProcessorName, this_facet_instance);
	                MPI_Comm inter_comm = Spawn(comm.comm, path, argv, maxprocs, 0, browser, port_key, comm_port, this_facet_instance);
	                Console.WriteLine("{0}/{1}: AFTER SPAWN C ++ ", comm.Rank, this_facet_instance);
	           }
               catch (Exception e)
               {
                  Console.WriteLine("{2}/{1}: EXCEPTION (IProcessLauncher): message={0}", e.Message, rank, this_facet_instance);
                  Console.WriteLine("{2}/{1}: EXCEPTION (IProcessLauncher): stack={0}", e.StackTrace, rank, this_facet_instance);
               }
	        }
	        	        
	        const int REQUEST_FACET = 0;
	        const int REQUEST_FACET_COUNT = 1;
	        const int REQUEST_FACET_SIZE = 2;
	        const int REQUEST_FACET_INSTANCE = 3;
	        const int REQUEST_PORT = 4;
	        const int REQUEST_FINALIZE = 5;
	        const int REQUEST_INITIALIZE = 6;
	        const int REQUEST_FACET_WORLD = 7;
	        
	        static int Spawn(MPI_Comm comm, string command, string[] argv, int maxprocs, int root, IWSendDataPortType browser, IDictionary<string, int> port_key, IDictionary<int, MPI_Comm> comm_port, int facet_instance)
	        {
	            MPI_Comm intercomm;	            
	            
	            unsafe
	            {
	                ASCIIEncoding ascii = new ASCIIEncoding();
               	    char[] command_ = command.ToCharArray();
               	    int l = ascii.GetByteCount(command_);
               	    byte* byte_command_p = stackalloc byte[l + 1];;
                    fixed (char* command__ = command_)
                    {
                        if (l > 0)
                        {
                            ascii.GetBytes(command__, command_.Length, byte_command_p, l);
                        }	                       
                        byte_command_p[l] = 0;
                    }
	               	
	                // Copy args into C-style argc/argv
	                byte** my_argv = stackalloc byte*[argv.Length];
	                	                
	                for (int argidx = 0; argidx < argv.Length; ++argidx)
	                {
	                    // Copy argument into a byte array (C-style characters)
	                    char[] arg = argv[argidx].ToCharArray();
	                    fixed (char* argp = &arg[0])
	                    {
	                        int length = ascii.GetByteCount(arg);
	                        byte* c_arg = stackalloc byte[length + 1];
	                        if (length > 0)
	                        {
	                            ascii.GetBytes(argp, arg.Length, c_arg, length);
	                        }
	                        my_argv[argidx] = c_arg;
	                        my_argv[argidx][length] = 0;
	                    }
	                }
	                if (argv == null || argv.Length == 0)
	                {
	                    //my_argv = Unsafe.MPI_ARGV_NULL;
	                    my_argv = (byte**)0;
	                }	                
	 	
                    Unsafe.MPI_Comm_spawn(byte_command_p, my_argv, maxprocs, X.get_hosts_info_for_spawn(), root, comm, out intercomm, (int*)0);
	                
	                int rank;
	                Unsafe.MPI_Comm_rank(intercomm, out rank);
	                  	                  
	                Thread t_browse = create_thread_browse(facet_instance, rank, intercomm, browser); 
	                t_browse.Start();
	                  
	                Thread t_query = create_thread_query_topology(facet_instance, rank, port_key, comm_port, intercomm);
	                t_query.Start();	

	                t_browse.Join();  
	                t_query.Join();            
	            }
	
	            return intercomm;
	        }	    
	        
	        static Thread create_thread_browse(int facet_instance, int rank, MPI_Comm intercomm, IWSendDataPortType browser)
	        {
	            Thread r = new Thread(() => 
                {
                   int errorCode = default(int);
                   try
                   {
	                    unsafe
	                    {
	                    	string message = null;
			                int size; IntPtr ptr_size = (IntPtr) (&size);
			                do
			                {
			                    Unsafe.MPI_Status status;
							    Console.WriteLine("{1}/{0}: HPCShelf_Browse --Unsafe.MPI_Recv - BEFORE", rank, facet_instance);	                    
						        errorCode = Unsafe.MPI_Recv(ptr_size, 1, Unsafe.MPI_INT, rank, 111, intercomm, out status);
							    Console.WriteLine("{1}/{0}: HPCShelf_Browse -- Unsafe.MPI_Recv - AFTER {2}", rank, facet_instance, errorCode);	                    
							    if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, facet_instance, rank);
							       
						        byte[] m = new byte[size];
								fixed (byte* buf = &m[0])
								{   
								     errorCode = Unsafe.MPI_Recv((IntPtr)buf, size, Unsafe.MPI_BYTE, rank, 222, intercomm, out status);
								     if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, facet_instance, rank);
								}
						    		                		                
						   		char[] mm = new char[m.Length];
						  		 for (int i=0; i<mm.Length; i++)	
						       		mm[i] = (char)m[i];		                
						    
		   		            	message = new String(mm);
						    		                
					            Console.WriteLine("{1}/{0}: -- HPCShelf_Browse --", rank, facet_instance);
					            
				            	browser.Browse(message); 
				            	//Console.WriteLine(message);		                       		            
			                }
			                while (!message.EndsWith("done"));
			                
			                browser.Browse("!EOS");
			                Console.WriteLine("{1}/{0}: BROWSE THREAD FINISHED ...", rank, facet_instance);
	                    }
                   }
                   catch (Exception e)
                   {
                      Console.WriteLine("{2}/{1}: EXCEPTION (HPCShelf_Browse): message={0}", e.Message, rank, facet_instance);
                      Console.WriteLine("{2}/{1}: EXCEPTION (HPCShelf_Browse): stack={0}", e.StackTrace, rank, facet_instance);
                   }
		     });
		     
             return r;	               
	    }             	        
            	        
        static Thread create_thread_port_recv(int facet_instance, int rank, int key, MPI_Comm intercomm)
        {
            Thread r = new Thread(() => 
            {     
               int errorCode = default(int);     
               try
               {      
                	unsafe
                	{
		                int[] p = new int[6]; 
		                
			            fixed (int* s = &p[0])
			            {
			               IntPtr s_buf = (IntPtr) s;
			               Unsafe.MPI_Status status;
					       Console.WriteLine("{1}/{0}: HPCShelf_Recv key={2} -- Unsafe.MPI_Recv - BEFORE", rank, facet_instance, key);	                    
			               errorCode = Unsafe.MPI_Recv(s_buf, 5, Unsafe.MPI_INT, rank, 555, intercomm, out status);
   					       Console.WriteLine("{0}/{1}: IProcessLauncher -- MPI_Send 1 - AFTER errorCode={2}", facet_instance, rank, errorCode);
			               if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, facet_instance, rank);
					       Console.WriteLine("{1}/{0}: HPCShelf_Recv key={2}-- Unsafe.MPI_Recv - AFTER {3}", rank, facet_instance, key, errorCode);	                    
			            }
              
                        int count = p[0];
		                int facet = p[1];
		                int source = p[2];
		                int datatype_size = p[3];
		                int tag = p[4];
		               
		                if (facet > 0)
		                {
		                    Thread recv_next = create_thread_port_recv(facet_instance, rank, key, intercomm); 
                            recv_next.Start();			                    
		                   
				            Console.WriteLine("{7}/{6}: -- HPCShelf_Recv key={8} -- count={0}, facet={1}, source={2}, tag={3}, key={4}, datatype_size={5}", count, facet, source, tag, key, datatype_size, rank, facet_instance, key);
				            
						    byte[] data = new byte[count*datatype_size];
						    fixed (byte* d = data)
						    { 
						    	IntPtr buf = /* Marshal.AllocHGlobal(count*datatype_size); */(IntPtr) d;
						    	Unsafe.MPI_Status status;    
							    connector_receive[key](key, buf, count*datatype_size, Unsafe.MPI_BYTE, facet, source, tag, (IntPtr) (&status));
							    Console.WriteLine("{3}/{2}: xxx key={4} Unsafe.MPI_Send({0}, {1}, Unsafe.MPI_BYTE, {2}, 666, intercomm);", buf, count*datatype_size, rank, facet_instance, key);
							    errorCode = Unsafe.MPI_Send(buf, count*datatype_size, Unsafe.MPI_BYTE, rank, 666, intercomm);
							    Console.WriteLine("{0}/{1}: IProcessLauncher -- MPI_Send 2 - AFTER errorCode={2}", facet_instance, rank, errorCode);
							    if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, facet_instance, rank);
							//    Marshal.FreeHGlobal(buf);
						    }
		                }
                    }
               }
               catch (Exception e)
               {
                  Console.WriteLine("{2}/{0}: key={3} EXCEPTION (HPCShelf_Recv): message={1}", rank, e.Message, facet_instance, key);
                  Console.WriteLine("{2}/{0}: key={3} EXCEPTION (HPCShelf_Recv): stack={1}", rank, e.StackTrace, facet_instance, key);
               }	                    
            });
           
            return r;	               
        }    	        

        static Thread create_thread_port_send(int facet_instance, int rank, int key, MPI_Comm intercomm)
        {
            Thread r = new Thread(() => 
            {
               int errorCode = default(int);
               try
               {
                    unsafe
                    {
		                int[] p = new int[5];
		                 
					    fixed (int* s = &p[0])
					    {
					       IntPtr s_buf = (IntPtr) s;
					       Unsafe.MPI_Status status;
					       Console.WriteLine("{1}/{0}: HPCShelf_Send key={2} --Unsafe.MPI_Recv - BEFORE", rank, facet_instance, key);	                    
					       errorCode = Unsafe.MPI_Recv(s_buf, 5, Unsafe.MPI_INT, rank, 333, intercomm, out status);
   					       Console.WriteLine("{0}/{1}: IProcessLauncher -- MPI_Recv 1 - AFTER errorCode={2}", facet_instance, rank, errorCode);
					       if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, facet_instance, rank);
					       Console.WriteLine("{1}/{0}: HPCShelf_Send key={2} -- Unsafe.MPI_Recv - AFTER {3}", rank, facet_instance, key, errorCode);	                    
					    }
						    
					    int count = p[0];
					    int facet = p[1];
					    int target = p[2];
					    int datatype_size = p[3];
					    int tag = p[4];
				            
				        if (facet > 0)
				        {    
		                    Thread send_next = create_thread_port_send(facet_instance, rank, key, intercomm); 
                            send_next.Start();
                            			                    
				            Console.WriteLine("{7}/{6}: -- HPCShelf_Send key={8} -- count={0}, facet={1}, target={2}, tag={3}, key={4}, datatype_size={5}", count, facet, target, tag, key, datatype_size, rank, facet_instance, key);
					            
						    byte[] data = new byte[count*datatype_size];
							fixed (byte* buf = data)
							{ 
							     //IntPtr buf = Marshal.AllocHGlobal(count*datatype_size);  								 
							     Unsafe.MPI_Status status;
								 errorCode = Unsafe.MPI_Recv((IntPtr) buf, count*datatype_size, Unsafe.MPI_BYTE, rank, 444, intercomm, out status);
							     Console.WriteLine("{0}/{1}: IProcessLauncher -- MPI_Recv 2 - AFTER errorCode={2}", facet_instance, rank, errorCode);
								 if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, facet_instance, rank);
	         				     connector_send[key](key, (IntPtr) buf, count*datatype_size, Unsafe.MPI_BYTE, facet, target, tag);
	         				     //Marshal.FreeHGlobal(buf);
							}
				        }				        
                    }
               }
               catch (Exception e)
               {
                  Console.WriteLine("{2}/{0}: key={3} EXCEPTION (HPCShelf_Send): message={1}", rank, e.Message, facet_instance, key);
                  Console.WriteLine("{2}/{0}: key={3} EXCEPTION (HPCShelf_Send): stack={1}", rank, e.StackTrace, facet_instance, key);
               }
            });
           
            return r;	               
        }

	        static Thread create_thread_query_topology
	                               (int this_facet_instance, 
	                                int rank, 
	                                IDictionary<string, int> port_key, 
	                                IDictionary<int, MPI_Comm> comm_port, 
	                                MPI_Comm intercomm)
	        {
	            Thread r = new Thread(() => 
                {
                   int errorCode = default(int);
                   try
                   {
	                	unsafe
	                	{
			                int[] p = new int[2]; 
			                
				            fixed (int* s = &p[0])
				            {
				               IntPtr s_buf = (IntPtr) s;
				               Unsafe.MPI_Status status;	                    
						       Console.WriteLine("{1}/{0}: TOPOLOGY QUERY -- Unsafe.MPI_Recv - BEFORE", rank, this_facet_instance);	                    
				               errorCode = Unsafe.MPI_Recv(s_buf, 2, Unsafe.MPI_INT, rank, 777, intercomm, out status);
						       Console.WriteLine("{1}/{0}: TOPOLOGY QUERY -- Unsafe.MPI_Recv - AFTER {2}", rank, this_facet_instance, errorCode);	                    
				               if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this_facet_instance, rank);					               
				            }
				            
				            int[] reply = null;
				            int size = default(int); IntPtr ptr_size = (IntPtr) (&size);
				               
				            int query = p[0];
				            int key = p[1];
				            
				            Console.WriteLine("{3}/{0}: TOPOLOGY QUERY (query={1}, key={2})", rank, query, key, this_facet_instance);
				            
				            if (query == REQUEST_FINALIZE)
				            {
		                        size = 1;
		                        reply = new int[1] { 0 };
				            }
				            else
				            {
				               Thread t_query_next = create_thread_query_topology(this_facet_instance, rank, port_key, comm_port, intercomm);
	                           t_query_next.Start();
				                    
					           switch (query)
					            {
					                case REQUEST_PORT:
					                         int length = key;					                        
					                         byte[] m = new byte[length];
											 fixed (byte* buf = &m[0])
											 {   
								                  Unsafe.MPI_Status status;
								                  Console.WriteLine("{1}/{0}: REQUEST_PORT - before recv 1", rank, this_facet_instance);
								 			      errorCode = Unsafe.MPI_Recv((IntPtr)buf, length, Unsafe.MPI_CHAR, rank, 888, intercomm, out status);
								 			      if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this_facet_instance, rank);
								                  Console.WriteLine("{1}/{0}: REQUEST_PORT - after recv 1", rank, this_facet_instance);
											 }
									    		                		                
									   		 char[] mm = new char[m.Length];
									  		 for (int i=0; i<mm.Length; i++)	
									       		mm[i] = (char)m[i];		                
						    
		   		            	             string port_name = new String(mm);
		   		            	            
						                     Console.WriteLine("{3}/{0}: REQUEST_PORT - port_key[{1}] ? {2}", rank, port_name, port_key.ContainsKey(port_name), this_facet_instance);
					                        
					                         size = 1;
					                         reply = new int[1] { port_key[port_name] };
					                        
					                         MPI_Comm intercomm_port; 
					                         errorCode = Unsafe.MPI_Comm_dup(intercomm, out intercomm_port);
					                         if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this_facet_instance, rank);
					                        
					                         int port = port_key[port_name]; 
					                        
					                         comm_port[port] = intercomm_port;
					                         
					                         Thread send_init = create_thread_port_send(this_facet_instance, rank, port, intercomm_port); 
                                             send_init.Start();
                            
					                         Thread recv_init = create_thread_port_recv(this_facet_instance, rank, port, intercomm_port); 
                                             recv_init.Start();					                         
					                         
					                         Console.WriteLine("{0}/{1}: COMM_PORT[{2}]={3} port_name={4}", this_facet_instance, rank, port, intercomm_port, port_name);
					                        
					                         Console.WriteLine("{2}/{0}: TOPOLOGY QUERY (PORT_NAME = {1} PORT_KEY = {2})", rank, port_name, key, this_facet_instance);
					                         break;
					                case REQUEST_FACET_WORLD:
					                         size = 1;
					                         reply = new int[size];
					                         reply[0] = this_facet_instance;
					                         Console.WriteLine("{2}/{0}: TOPOLOGY QUERY (FACET_WORLD = {1})", rank, reply[0], this_facet_instance);
					                         break;
					                case REQUEST_FACET:
					                        size = 1;
					                        reply = new int[size];
					                        reply[0] = facet[key];
					                        Console.WriteLine("{2}/{0}: TOPOLOGY QUERY (FACET = {1})", rank, facet[key], this_facet_instance);
					                        break;
					                case REQUEST_FACET_COUNT:
					                        size = 1;
					                        reply = new int[size];
					                        reply[0] = facet_count[key];
					                        Console.WriteLine("{2}/{0}: TOPOLOGY QUERY (FACET_COUNT = {1})", rank, facet_count[key], this_facet_instance);
					                        break;
					                case REQUEST_FACET_SIZE:
					                        size = facet_size[key].Length;
			  						        errorCode = Unsafe.MPI_Send(ptr_size, 1, Unsafe.MPI_INT, rank, 888, intercomm);
			  						        if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this_facet_instance, rank);
					                        reply = new int[size];
					                        facet_size[key].CopyTo(reply, 0);
					                        Console.WriteLine("{3}/{0}: TOPOLOGY QUERY (FACET_SIZE[{1}] = {2})", rank, facet[key], facet_size[key][facet[key]], this_facet_instance);
					                        break;
					                case REQUEST_FACET_INSTANCE:
					                        size = facet_instance[key].Length;
	  								        errorCode = Unsafe.MPI_Send(ptr_size, 1, Unsafe.MPI_INT, rank, 888, intercomm);
	  								        if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this_facet_instance, rank);
					                        reply = new int[size];
					                        facet_instance[key].CopyTo(reply, 0);
					                        Console.WriteLine("{3}/{0}: TOPOLOGY QUERY (FACET_INSTANCE[{1}] = {2})", rank, facet[key], facet_instance[key][facet[key]], this_facet_instance);
					                        break;          
					                case REQUEST_INITIALIZE:
					                        size = 2;
					                        reply = new int[2] { port_key.Count, 0 };
					                        break;                    
					            }  
				            }					               
						    
						    Console.WriteLine("{1}/{0}: TOPOLOGY QUERY: begin send ...", rank, this_facet_instance);
						    fixed (int* reply_pointer = &reply[0])
						    {   
						       IntPtr buf = (IntPtr) reply_pointer;
						       errorCode = Unsafe.MPI_Send(buf, size, Unsafe.MPI_INT, rank, 999, intercomm);
						       if (errorCode != Unsafe.MPI_SUCCESS) throw MPIException(errorCode, this_facet_instance, rank);
						    }
						    Console.WriteLine("{1}/{0}: TOPOLOGY QUERY: ... end send", rank, this_facet_instance);
		       	        }
                   }
                   catch (Exception e)
                   {
                      Console.WriteLine("{2}/{0}: EXCEPTION (HPCShelf_Topology): message={1}", rank, e.Message, this_facet_instance);
                      Console.WriteLine("{2}/{0}: EXCEPTION (HPCShelf_Topology): stack={1}", rank, e.StackTrace, this_facet_instance);
                   }		       	           
                });
	           
                return r;	               
	        }    	        
	        
		    private static Exception MPIException(int errorCode, int facet_instance, int rank)
	        {
	           Exception e = MPI.Environment.TranslateErrorIntoException(errorCode);
	           Console.WriteLine("{0}/{1}: MPI Exception - MESSAGE = {2}", facet_instance, rank, e.Message);
	           Console.WriteLine("{0}/{1}: MPI Exception - STACK = {2}", facet_instance, rank, e.StackTrace);
	           if (e.InnerException != null)
	           {
		           Console.WriteLine("{0}/{1}: MPI Inner Exception - MESSAGE = {2}", facet_instance, rank, e.InnerException.Message);
		           Console.WriteLine("{0}/{1}: MPI Inner Exception - STACK = {2}", facet_instance, rank, e.InnerException.StackTrace);
	           }
	           return e;
	        }
	        
		}
		
		class X
		{
		    [DllImport("libgethostsinfo.so", EntryPoint = "get_hosts_info_for_spawn")]
		    public static extern int get_hosts_info_for_spawn();
		}
			        		
	}