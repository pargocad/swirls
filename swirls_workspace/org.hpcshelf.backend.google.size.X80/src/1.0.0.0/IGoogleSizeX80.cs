using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X80
{
 public interface IGoogleSizeX80 : BaseIGoogleSizeX80, IGoogleSize
 {
 }
}