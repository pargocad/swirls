using org.hpcshelf.platform.locale.AnyWhere;

namespace org.hpcshelf.platform.locale.Asia
{
	public interface IAsia : BaseIAsia, IAnyWhere
	{
	}
}