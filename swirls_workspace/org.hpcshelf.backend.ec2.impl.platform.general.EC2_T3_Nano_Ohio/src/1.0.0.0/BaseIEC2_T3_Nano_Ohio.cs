/* Automatically Generated Code */

using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.platform.Node;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.locale.northamerica.Ohio;
using org.hpcshelf.quantifier.DecimalDown;
using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.platform.node.processor.Cache;
using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_8Way;
using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_16Way;
using org.hpcshelf.platform.node.processor.Core;
using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_11Way;
using org.hpcshelf.platform.node.Processor;
using org.hpcshelf.platform.node.processor.manufacturer.Intel;
using org.hpcshelf.platform.node.processor.family.intel.Xeon_E5;
using org.hpcshelf.platform.node.processor.series.intel.Xeon_E5_2000;
using org.hpcshelf.platform.node.processor.model.intel.Xeon_E5_2686v5;
using org.hpcshelf.platform.node.processor.microarchitecture.intel.Broadwell;
using org.hpcshelf.platform.node.accelerator.Manufacturer;
using org.hpcshelf.platform.node.accelerator.Model;
using org.hpcshelf.platform.node.accelerator.Type;
using org.hpcshelf.platform.node.accelerator.Architecture;
using org.hpcshelf.platform.node.Accelerator;
using org.hpcshelf.platform.node.Memory;
using org.hpcshelf.backend.ec2.EC2InstanceStorage;
using org.hpcshelf.backend.ec2.storage.type.EBS;
using org.hpcshelf.platform.node.os.linux.Ubuntu_16_04_LTS;
using org.hpcshelf.platform.locale.AnyWhere;
using org.hpcshelf.platform.Interconnection;
using org.hpcshelf.platform.interconnection.Topology;
using org.hpcshelf.types.YesOrNo;
using org.hpcshelf.backend.ec2.family.Family;
using org.hpcshelf.platform.node.Storage;
using org.hpcshelf.backend.ec2.type.Type;
using org.hpcshelf.backend.ec2.family.GeneralPurpose;
using org.hpcshelf.platform.Power;
using org.hpcshelf.backend.ec2.size.Size;
using org.hpcshelf.platform.Performance;
using org.hpcshelf.platform.Maintainer;
using org.hpcshelf.backend.ec2.platform.general.EC2_T3_Nano;

namespace org.hpcshelf.backend.ec2.impl.platform.general.EC2_T3_Nano_Ohio 
{
	public abstract class BaseIEC2_T3_Nano_Ohio<NCT, NOD_STO_SIZ, CPH, LOC>: Environment, BaseIProcessingNode<NCT, NOD_STO_SIZ, CPH, LOC>
		where NCT:IntUp
		where NOD_STO_SIZ:IntUp
		where CPH:DecimalDown
		where LOC:IOhio
	{
		private INode<NCT, LOC, CPH, DecimalDown, IntUp, IntUp, IntUp, IntUp, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_16Way, IntUp, IntDown, IntUp>, ICore<IntUp, IntUp, IntUp, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_16Way, IntUp, IntDown, IntUp>>, IntUp, IntDown, IntUp, ICache<ISetAssociative_11Way, IntUp, IntDown, IntUp>, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, IProcessor<IntUp, IIntel, IXeonE5, IXeonE52000, IXeon_E5_2686v5, IBroadwell, IntUp, IntUp, IntUp, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_16Way, IntUp, IntDown, IntUp>, ICore<IntUp, IntUp, IntUp, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_16Way, IntUp, IntDown, IntUp>>, IntUp, IntDown, IntUp, ICache<ISetAssociative_11Way, IntUp, IntDown, IntUp>, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown>, IntUp, IAcceleratorManufacturer, IAcceleratorModel<IAcceleratorManufacturer, IAcceleratorType<IAcceleratorManufacturer>, IAcceleratorArchitecture>, IAcceleratorType<IAcceleratorManufacturer>, IAcceleratorArchitecture, IntUp, IAccelerator<IntUp, IAcceleratorManufacturer, IAcceleratorType<IAcceleratorManufacturer>, IAcceleratorArchitecture, IAcceleratorModel<IAcceleratorManufacturer, IAcceleratorType<IAcceleratorManufacturer>, IAcceleratorArchitecture>, IntUp>, IntUp, IntDown, IntUp, IMemory<IntUp, IntDown, IntUp>, IEC2InstanceStorage<NOD_STO_SIZ, IntDown, IntUp, IntUp, IEC2StorageType_EBS>, IUbuntu_16_04_LTS> node = null;

		protected INode<NCT, LOC, CPH, DecimalDown, IntUp, IntUp, IntUp, IntUp, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_16Way, IntUp, IntDown, IntUp>, ICore<IntUp, IntUp, IntUp, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_16Way, IntUp, IntDown, IntUp>>, IntUp, IntDown, IntUp, ICache<ISetAssociative_11Way, IntUp, IntDown, IntUp>, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, IProcessor<IntUp, IIntel, IXeonE5, IXeonE52000, IXeon_E5_2686v5, IBroadwell, IntUp, IntUp, IntUp, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_16Way, IntUp, IntDown, IntUp>, ICore<IntUp, IntUp, IntUp, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_16Way, IntUp, IntDown, IntUp>>, IntUp, IntDown, IntUp, ICache<ISetAssociative_11Way, IntUp, IntDown, IntUp>, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown>, IntUp, IAcceleratorManufacturer, IAcceleratorModel<IAcceleratorManufacturer, IAcceleratorType<IAcceleratorManufacturer>, IAcceleratorArchitecture>, IAcceleratorType<IAcceleratorManufacturer>, IAcceleratorArchitecture, IntUp, IAccelerator<IntUp, IAcceleratorManufacturer, IAcceleratorType<IAcceleratorManufacturer>, IAcceleratorArchitecture, IAcceleratorModel<IAcceleratorManufacturer, IAcceleratorType<IAcceleratorManufacturer>, IAcceleratorArchitecture>, IntUp>, IntUp, IntDown, IntUp, IMemory<IntUp, IntDown, IntUp>, IEC2InstanceStorage<NOD_STO_SIZ, IntDown, IntUp, IntUp, IEC2StorageType_EBS>, IUbuntu_16_04_LTS> Node
		{
			get
			{
				if (this.node == null)
					this.node = (INode<NCT, LOC, CPH, DecimalDown, IntUp, IntUp, IntUp, IntUp, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_16Way, IntUp, IntDown, IntUp>, ICore<IntUp, IntUp, IntUp, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_16Way, IntUp, IntDown, IntUp>>, IntUp, IntDown, IntUp, ICache<ISetAssociative_11Way, IntUp, IntDown, IntUp>, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, IProcessor<IntUp, IIntel, IXeonE5, IXeonE52000, IXeon_E5_2686v5, IBroadwell, IntUp, IntUp, IntUp, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_16Way, IntUp, IntDown, IntUp>, ICore<IntUp, IntUp, IntUp, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_8Way, IntUp, IntDown, IntUp>, IntUp, IntDown, IntUp, ICache<ISetAssociative_16Way, IntUp, IntDown, IntUp>>, IntUp, IntDown, IntUp, ICache<ISetAssociative_11Way, IntUp, IntDown, IntUp>, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown, DecimalDown>, IntUp, IAcceleratorManufacturer, IAcceleratorModel<IAcceleratorManufacturer, IAcceleratorType<IAcceleratorManufacturer>, IAcceleratorArchitecture>, IAcceleratorType<IAcceleratorManufacturer>, IAcceleratorArchitecture, IntUp, IAccelerator<IntUp, IAcceleratorManufacturer, IAcceleratorType<IAcceleratorManufacturer>, IAcceleratorArchitecture, IAcceleratorModel<IAcceleratorManufacturer, IAcceleratorType<IAcceleratorManufacturer>, IAcceleratorArchitecture>, IntUp>, IntUp, IntDown, IntUp, IMemory<IntUp, IntDown, IntUp>, IEC2InstanceStorage<NOD_STO_SIZ, IntDown, IntUp, IntUp, IEC2StorageType_EBS>, IUbuntu_16_04_LTS>) Services.getPort("node");
				return this.node;
			}
		}
		private LOC node-locale = default(LOC);

		protected LOC Node-locale
		{
			get
			{
				if (this.node-locale == null)
					this.node-locale = (LOC) Services.getPort("node-locale");
				return this.node-locale;
			}
		}
		private IInterconnection<IntDown, IntDown, IntUp, ITopology> interconnection = null;

		protected IInterconnection<IntDown, IntDown, IntUp, ITopology> Interconnection
		{
			get
			{
				if (this.interconnection == null)
					this.interconnection = (IInterconnection<IntDown, IntDown, IntUp, ITopology>) Services.getPort("interconnection");
				return this.interconnection;
			}
		}
		private IYesOrNo virtual = null;

		protected IYesOrNo Virtual
		{
			get
			{
				if (this.virtual == null)
					this.virtual = (IYesOrNo) Services.getPort("virtual");
				return this.virtual;
			}
		}
		private IYesOrNo dedicated = null;

		protected IYesOrNo Dedicated
		{
			get
			{
				if (this.dedicated == null)
					this.dedicated = (IYesOrNo) Services.getPort("dedicated");
				return this.dedicated;
			}
		}
		private IYesOrNo placement_group = null;

		protected IYesOrNo Placement_group
		{
			get
			{
				if (this.placement_group == null)
					this.placement_group = (IYesOrNo) Services.getPort("placement_group");
				return this.placement_group;
			}
		}
		private IEC2Family family = null;

		protected IEC2Family Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2Family) Services.getPort("family");
				return this.family;
			}
		}
		private IStorage<IntUp, IntDown, IntUp, IntUp> storage = null;

		protected IStorage<IntUp, IntDown, IntUp, IntUp> Storage
		{
			get
			{
				if (this.storage == null)
					this.storage = (IStorage<IntUp, IntDown, IntUp, IntUp>) Services.getPort("storage");
				return this.storage;
			}
		}
		private IEC2Type<IEC2FamilyGeneralPurpose> type = null;

		protected IEC2Type<IEC2FamilyGeneralPurpose> Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2Type<IEC2FamilyGeneralPurpose>) Services.getPort("type");
				return this.type;
			}
		}
		private IPower<IntDown, IntUp, IntDown, IntUp> power = null;

		protected IPower<IntDown, IntUp, IntDown, IntUp> Power
		{
			get
			{
				if (this.power == null)
					this.power = (IPower<IntDown, IntUp, IntDown, IntUp>) Services.getPort("power");
				return this.power;
			}
		}
		private IEC2Size size = null;

		protected IEC2Size Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2Size) Services.getPort("size");
				return this.size;
			}
		}
		private IPerformance<IntUp, IntUp, IntUp> performance = null;

		protected IPerformance<IntUp, IntUp, IntUp> Performance
		{
			get
			{
				if (this.performance == null)
					this.performance = (IPerformance<IntUp, IntUp, IntUp>) Services.getPort("performance");
				return this.performance;
			}
		}
		private IMaintainer maintainer = null;

		protected IMaintainer Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IMaintainer) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}