using org.hpcshelf.blas.MatrixType;

namespace org.hpcshelf.blas.matrixtype.General
{
	public interface IGeneralMatrix : BaseIGeneralMatrix, IMatrixType
	{
	}
}