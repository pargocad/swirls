/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.CONFIG;

namespace org.hpcshelf.blas.configure.TRANS
{
	public interface BaseITRANS : BaseICONFIG, IQualifierKind 
	{
	}
}