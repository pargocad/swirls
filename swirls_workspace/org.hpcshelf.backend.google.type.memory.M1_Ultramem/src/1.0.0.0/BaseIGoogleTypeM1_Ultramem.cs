/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.memory.M1_Ultramem
{
 public interface BaseIGoogleTypeM1_Ultramem : BaseIGoogleType<family.Memory.IGoogleFamilyMemory>, IQualifierKind 
 {
 }
}