using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X208
{
 public interface IGoogleSizeX208 : BaseIGoogleSizeX208, IGoogleSize
 {
 }
}