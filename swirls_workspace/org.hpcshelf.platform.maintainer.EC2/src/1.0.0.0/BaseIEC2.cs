/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.Maintainer;

namespace org.hpcshelf.platform.maintainer.EC2
{
	public interface BaseIEC2 : BaseIMaintainer, IQualifierKind 
	{
	}
}