/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.family.Family;

namespace org.hpcshelf.backend.google.family.GeneralPurpose
{
 public interface BaseIGoogleFamilyGeneralPurpose : BaseIGoogleFamily, IQualifierKind 
 {
 }
}