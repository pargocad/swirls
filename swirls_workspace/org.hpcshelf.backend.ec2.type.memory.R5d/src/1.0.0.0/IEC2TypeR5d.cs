using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.memory.R5d
{
	public interface IEC2TypeR5d : BaseIEC2TypeR5d, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}