/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.storage.I3en
{
	public interface BaseIEC2TypeI3en : BaseIEC2Type<family.Storage.IEC2FamilyStorage>, IQualifierKind 
	{
	}
}