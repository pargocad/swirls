using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;

namespace org.hpcshelf.backend.ec2.family.GeneralPurpose
{
	public interface IEC2FamilyGeneralPurpose : BaseIEC2FamilyGeneralPurpose, IEC2Family
	{
	}
}