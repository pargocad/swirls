using org.hpcshelf.platform.Maintainer;

namespace org.hpcshelf.platform.maintainer.SAFeHost
{
	public interface ISAFeHost : BaseISAFeHost, IMaintainer
	{
	}
}