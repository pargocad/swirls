using org.hpcshelf.platform.locale.europe.France;

namespace org.hpcshelf.platform.locale.europe.Paris
{
	public interface IParis : BaseIParis, IFrance
	{
	}
}