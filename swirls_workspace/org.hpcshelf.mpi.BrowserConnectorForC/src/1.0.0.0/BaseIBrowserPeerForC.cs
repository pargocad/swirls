/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.common.BrowserBinding;
using org.hpcshelf.common.BrowserConnector;
using org.hpcshelf.common.browser.SendDataPortType;

namespace org.hpcshelf.mpi.BrowserConnectorForC
{
    public interface BaseIBrowserPeerForC : BaseIBrowserPeer<org.hpcshelf.mpi.language.C.ILanguage_C, org.hpcshelf.common.browser.SendDataPortType.ISendDataPortType>, ISynchronizerKind 
	{
		new IBrowserBinding<ISendDataPortType> Send_data_port {get;}
	}
}