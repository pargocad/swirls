/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X48
{
 public interface BaseIGoogleSizeX48 : BaseIGoogleSize, IQualifierKind 
 {
 }
}