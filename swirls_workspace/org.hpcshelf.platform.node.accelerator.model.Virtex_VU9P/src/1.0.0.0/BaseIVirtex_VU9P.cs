/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.accelerator.Model;

namespace org.hpcshelf.platform.node.accelerator.model.Virtex_VU9P
{
	public interface BaseIVirtex_VU9P : BaseIAcceleratorModel<org.hpcshelf.platform.node.accelerator.manufacturer.Xilinx.IXilinx, org.hpcshelf.platform.node.accelerator.type.Virtex.IVirtex, org.hpcshelf.platform.node.accelerator.architecture.Ultrascale_plus.IUltrascale_plus>, IQualifierKind 
	{
	}
}