using org.hpcshelf.platform.maintainer.EC2;

namespace org.hpcshelf.platform.maintainer.ec2.EC2Localhost
{
	public interface IEC2LocalHost : BaseIEC2LocalHost, IEC2
	{
	}
}