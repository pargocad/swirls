using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.compute.C5
{
	public interface IEC2TypeC5 : BaseIEC2TypeC5, IEC2Type<family.Computation.IEC2FamilyComputation>
	{
	}
}