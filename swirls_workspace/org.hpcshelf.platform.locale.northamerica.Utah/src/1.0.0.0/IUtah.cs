using org.hpcshelf.platform.locale.northamerica.US_West;

namespace org.hpcshelf.platform.locale.northamerica.Utah
{
	public interface IUtah : BaseIUtah, IUS_West
	{
	}
}