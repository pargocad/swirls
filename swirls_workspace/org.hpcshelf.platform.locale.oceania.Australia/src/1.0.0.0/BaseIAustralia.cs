/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.Oceania;

namespace org.hpcshelf.platform.locale.oceania.Australia
{
	public interface BaseIAustralia : BaseIOceania, IQualifierKind 
	{
	}
}