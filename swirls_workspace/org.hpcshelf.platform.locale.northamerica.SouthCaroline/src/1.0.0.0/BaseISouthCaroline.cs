/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.northamerica.US_East;

namespace org.hpcshelf.platform.locale.northamerica.SouthCaroline
{
	public interface BaseISouthCaroline : BaseIUS_East, IQualifierKind 
	{
	}
}