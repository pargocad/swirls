using org.hpcshelf.mpi.Language;
using org.hpcshelf.mpi.IntercommunicatorPortType;

namespace org.hpcshelf.mpi.MPIConnector
{   
	public interface IMPIConnector<L,S> : BaseIMPIConnector<L,S>
		where L:ILanguage
		where S:IIntercommunicatorPortType
	{
	}
}