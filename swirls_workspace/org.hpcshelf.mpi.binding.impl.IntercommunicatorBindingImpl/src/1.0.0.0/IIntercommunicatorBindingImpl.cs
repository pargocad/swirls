using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.mpi.IntercommunicatorPortType;
using org.hpcshelf.mpi.binding.IntercommunicatorBinding;
using System.Threading;

namespace org.hpcshelf.mpi.binding.impl.IntercommunicatorBindingImpl
{
	public class IIntercommunicatorBindingImpl<S> : BaseIIntercommunicatorBindingImpl<S>, IIntercommunicatorBinding<S>
		where S:IIntercommunicatorPortType
	{
		public override void main()
		{
		}
		
		private ManualResetEvent sync = new ManualResetEvent(false);
				

        public IIntercommunicatorPortType Client
        {
              get
              {
                Console.WriteLine("IIntercommunicatorBindingImpl - CLIENT WAIT - BEFORE");                 
     			sync.WaitOne ();
     			Console.WriteLine("IIntercommunicatorBindingImpl - CLIENT WAIT - AFTER");
     			
	      		return server;
              }
        }

        private S server = default(S);

        public S Server
        {
              set
              {
	   			this.server = value; 
                if (this.server == null)
                {
                    Console.WriteLine("IIntercommunicatorBindingImpl - SERVER RESET");
                    sync.Reset();
                }
                else
                {
                    Console.WriteLine("IIntercommunicatorBindingImpl - SERVER SET");
                    sync.Set ();
                }
              }
        }
	}
}
