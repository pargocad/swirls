#include <cstdlib>
#include <mpi.h>
#include <iostream>
#include <omp.h>
#include <cstring>
#include <hpcshelf.h>

#define ARR_SIZE 3000

using std::cout;

int rank, size;

/*
MPI_Comm parent_comm;
int HPCShelf_Browse(const char *message)
{
    printf("%s", message); 

    if (rank < 0)
    {
      MPI_Comm_get_parent(&parent_comm);
      MPI_Comm_rank(parent_comm, &rank);
      printf("taking rank (%d) and parent_communicator (%d)", rank, parent_comm);
    }
    
    char ranked_message[6 + strlen(message) + 1];
    sprintf(ranked_message, "%d: ", rank);
    strcat(ranked_message, message);

    int message_size = strlen(ranked_message);

    MPI_Send(&message_size, 1, MPI_INT, rank, 111, parent_comm);
    MPI_Send(ranked_message, message_size + 1, MPI_CHAR, rank, 222, parent_comm);
    
    return 0;
}
*/

void troca(int *a, int *b)
{
    int swap;

    swap = *a;
    *a = *b;
    *b = swap;
}

int *particao_ptr(int *pivo, int *inicio, int *fim)
{
    int *i = inicio;
    int *j = inicio;

    for (j; j != fim; ++j)
    {
        if (*j < *pivo)
        {
            troca(i, j);
            i++;
        }
    }
    troca(i, pivo);

    return i;
}

void qsort_ptr(int *inicio, int *fim)
{
    if (inicio != fim)
    {
        int *p = particao_ptr(fim - 1, inicio, fim);

#pragma omp task default(none) firstprivate(inicio, p)
        {
            qsort_ptr(inicio, p);
        }
#pragma omp task default(none) firstprivate(fim, p)
        {
            qsort_ptr(p + 1, fim);
        }
    }
}

void rand_array(int size, int *arr)
{
    srand(time(nullptr));
    for (int i = 0; i < size; i++)
    {
        arr[i] = rand() % 9;
    }
}

void parallel_qsort(int *local_v, int local_v_size, int *sorted_list)
{
    int size2 = size * size;
    int *all_samples = new int[size2];
    int *local_sample = new int[size];
    int *samples_pivots = new int[size - 1];
    int sendcounts[size] = {};
    int recvcounts[size] = {};
    int *sdispls = new int[size];
    int *rdispls = new int[size];
    int *sublist;
    int sublist_size[1] = {0};
    int *all_sublist_sizes = new int[size];
    int *all_sublist_displs = new int[size];

#pragma omp parallel default(none) shared(local_v, local_v_size)
    {
#pragma omp single nowait
        {
            qsort_ptr(local_v, local_v + local_v_size);
        }
    }

    for (int i = 0; i < size; i++)
    {
        local_sample[i] = local_v[i * (ARR_SIZE / (size2))];
    }

    MPI_Gather(local_sample, size, MPI_INT, all_samples, size, MPI_INT, 0, MPI_COMM_WORLD);

    if (rank == 0)
    {
#pragma omp parallel default(none) shared(all_samples, size2, local_v, local_v_size)
        {
#pragma omp single nowait
            {
                qsort_ptr(all_samples, all_samples + size2);
            }
        }
        for (int i = 1; i < size; i++)
        {
            samples_pivots[i - 1] = all_samples[i * size];
        }
    }
    MPI_Bcast(samples_pivots, size - 1, MPI_INT, 0, MPI_COMM_WORLD);

    int j = 0;
    for (int i = 0; i < local_v_size; i++)
    {
        while (samples_pivots[j] < local_v[i] && j < size - 1)
        {
            j++;
        }

        sendcounts[j]++;
    }

    MPI_Alltoall(sendcounts, 1, MPI_INT, recvcounts, 1, MPI_INT, MPI_COMM_WORLD);

    sdispls[0] = 0;
    rdispls[0] = 0;
    for (int i = 1; i < size; i++)
    {
        sdispls[i] = sdispls[i - 1] + sendcounts[i - 1];
        rdispls[i] = rdispls[i - 1] + recvcounts[i - 1];
    }

    for (int i = 0; i < size; i++)
    {
        sublist_size[0] += recvcounts[i];
    }

    all_sublist_displs[0] = 0;

    sublist = new int[sublist_size[0]];

    MPI_Alltoallv(local_v, sendcounts, sdispls, MPI_INT, sublist, recvcounts, rdispls, MPI_INT, MPI_COMM_WORLD);

    MPI_Gather(sublist_size, 1, MPI_INT, all_sublist_sizes, 1, MPI_INT, 0, MPI_COMM_WORLD);

#pragma omp parallel default(none) shared(local_v, local_v_size, sublist, sublist_size)
    {
#pragma omp single nowait
        {
            qsort_ptr(sublist, sublist + sublist_size[0]);
        }
    }

    for (int i = 1; i < size; i++)
    {
        all_sublist_displs[i] = all_sublist_sizes[i - 1] + all_sublist_displs[i - 1];
    }

    MPI_Gatherv(sublist, sublist_size[0], MPI_INT, sorted_list, all_sublist_sizes, all_sublist_displs, MPI_INT, 0, MPI_COMM_WORLD);
}

char message[ARR_SIZE*2+1];

int main(int argc, char **argv)
{
    int *v = new int[ARR_SIZE];
    int *local_v, *local_v_sizes, *displs;
    int *sorted_list = new int[ARR_SIZE];

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    rand_array(ARR_SIZE, v);
    
    message[0] = '\0';    
    char s[3];
    for (int *i = v; i != v + 30; ++i)
    {
	 	sprintf(s,"%d,", *i);
	 	strcat(message, s);
	 	cout << *i << ",";
    }

    cout << '\n';
    
    HPCShelf_Browse(message);

    MPI_Status status;

    local_v_sizes = new int[size];
    for (int i = 0; i < size - 1; i++)
    {
        local_v_sizes[i] = ARR_SIZE / size;
    }

    displs = new int[size];
    displs[0] = 0;
    for (int i = 1; i < size; i++)
    {
        displs[i] = displs[i - 1] + local_v_sizes[i - 1];
    }
    cout << "Processo " << rank << '\n';

    local_v_sizes[size - 1] = (ARR_SIZE / size) + (ARR_SIZE % size);

    local_v = new int[local_v_sizes[rank]];
    MPI_Scatterv(v, local_v_sizes, displs, MPI_INT, local_v, local_v_sizes[rank], MPI_INT, 0, MPI_COMM_WORLD);

    parallel_qsort(local_v, local_v_sizes[rank], sorted_list);

    if (rank == 0)
    {
		message[0] = '\0';    
		char s[3];
		for (int i = 0; i < ARR_SIZE; i++)
		{
			sprintf(s, "%d,", sorted_list[i]);
			strcat(message, s);
			cout << sorted_list[i] << ",";
		}
    }

    cout << '\n';    
    
    HPCShelf_Browse(message);
	
    HPCShelf_Browse("done");
    
    MPI_Finalize();

    return 0;
}
