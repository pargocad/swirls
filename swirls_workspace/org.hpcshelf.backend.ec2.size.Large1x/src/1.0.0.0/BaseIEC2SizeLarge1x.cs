/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large1x
{
	public interface BaseIEC2SizeLarge1x : BaseIEC2Size, IQualifierKind 
	{
	}
}