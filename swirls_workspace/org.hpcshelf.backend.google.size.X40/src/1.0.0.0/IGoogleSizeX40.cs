using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X40
{
 public interface IGoogleSizeX40 : BaseIGoogleSizeX40, IGoogleSize
 {
 }
}