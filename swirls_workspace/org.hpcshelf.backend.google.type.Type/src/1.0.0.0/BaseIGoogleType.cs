/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.family.Family;

namespace org.hpcshelf.backend.google.type.Type
{
 public interface BaseIGoogleType<F> : IQualifierKind 
  where F:IGoogleFamily
 {
 }
}