/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;

namespace org.hpcshelf.backend.ec2.type.Type
{
	public interface BaseIEC2Type<F> : IQualifierKind 
		where F:IEC2Family
	{
	}
}