/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X16g
{
 public interface BaseIGoogleSizeX16g : BaseIGoogleSize, IQualifierKind 
 {
 }
}