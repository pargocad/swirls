using org.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace org.hpcshelf.platform.node.processor.cache.mapping.FullyAssociative
{
	public interface ICacheMappingFullyAssociative : BaseICacheMappingFullyAssociative, ICacheMappingSetAssociative<org.hpcshelf.quantifier.IntUp.IntUp>
	{
	}
}