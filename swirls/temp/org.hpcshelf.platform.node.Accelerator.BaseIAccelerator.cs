/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.node.accelerator.Manufacturer;
using org.hpcshelf.platform.node.accelerator.Type;
using org.hpcshelf.platform.node.accelerator.Architecture;
using org.hpcshelf.platform.node.accelerator.Model;

namespace org.hpcshelf.platform.node.Accelerator
{
	public interface BaseIAccelerator<ACT, MAN, TYP, ARC, MOD, MEM> : IQualifierKind 
		where ACT:IntUp
		where MAN:IAcceleratorManufacturer
		where TYP:IAcceleratorType<MAN>
		where ARC:IAcceleratorArchitecture
		where MOD:IAcceleratorModel<MAN, TYP, ARC>
		where MEM:IntUp
	{
	}
}