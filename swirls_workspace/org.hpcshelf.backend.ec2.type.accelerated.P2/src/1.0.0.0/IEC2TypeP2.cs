using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.accelerated.P2
{
	public interface IEC2TypeP2 : BaseIEC2TypeP2, IEC2Type<family.Acceleration.IEC2FamilyAcceleration>
	{
	}
}