using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X128
{
 public interface IGoogleSizeX128 : BaseIGoogleSizeX128, IGoogleSize
 {
 }
}