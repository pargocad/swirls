/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentPortType;
using org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner;

namespace org.hpcshelf.binding.environment.EnvironmentPortTypeMultiplePartner
{
	public interface BaseIEnvironmentPortTypeMultiplePartner : BaseIEnvironmentPortTypeSinglePartner, IQualifierKind 
	{
	}
}