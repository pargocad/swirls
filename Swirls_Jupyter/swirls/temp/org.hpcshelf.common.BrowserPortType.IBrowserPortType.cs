using org.hpcshelf.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace org.hpcshelf.common.BrowserPortType
{
	public interface IBrowserPortType : BaseIBrowserPortType, IEnvironmentPortTypeMultiplePartner
	{
	}
}