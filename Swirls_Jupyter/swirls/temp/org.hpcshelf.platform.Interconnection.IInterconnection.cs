using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.interconnection.Topology;

namespace org.hpcshelf.platform.Interconnection
{
	public interface IInterconnection<STT, NLT, BAN, TOP> : BaseIInterconnection<STT, NLT, BAN, TOP>
		where STT:IntDown
		where NLT:IntDown
		where BAN:IntUp
		where TOP:ITopology
	{
	}
}