/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;
using org.hpcshelf.backend.ec2.family.Computation;

namespace org.hpcshelf.backend.ec2.type.compute.C4
{
	public interface BaseIEC2TypeC4 : BaseIEC2Type<IEC2FamilyComputation>, IQualifierKind 
	{
	}
}