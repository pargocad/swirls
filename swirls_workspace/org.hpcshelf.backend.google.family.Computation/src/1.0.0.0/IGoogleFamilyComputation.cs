using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.family.Family;

namespace org.hpcshelf.backend.google.family.Computation
{
 public interface IGoogleFamilyComputation : BaseIGoogleFamilyComputation, IGoogleFamily
 {
 }
}