/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Series;

namespace org.hpcshelf.platform.node.processor.series.amd.EPYC_7000
{
	public interface BaseIEPYC7000 : BaseIProcessorSeries<org.hpcshelf.platform.node.processor.manufacturer.AMD.IAMD, org.hpcshelf.platform.node.processor.family.amd.EPYC.IEPYC>, IQualifierKind 
	{
	}
}