/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.northamerica.US_West;

namespace org.hpcshelf.platform.locale.northamerica.Oregon
{
	public interface BaseIOregon : BaseIUS_West, IQualifierKind 
	{
	}
}