using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X416
{
 public interface IGoogleSizeX416 : BaseIGoogleSizeX416, IGoogleSize
 {
 }
}