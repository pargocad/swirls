using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large2x
{
	public interface IEC2SizeLarge2x : BaseIEC2SizeLarge2x, IEC2Size
	{
	}
}