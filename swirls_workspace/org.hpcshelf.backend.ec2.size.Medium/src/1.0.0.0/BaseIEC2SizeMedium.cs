/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Medium
{
	public interface BaseIEC2SizeMedium : BaseIEC2Size, IQualifierKind 
	{
	}
}