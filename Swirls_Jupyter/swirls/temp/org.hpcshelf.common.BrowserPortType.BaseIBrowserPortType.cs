/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace org.hpcshelf.common.BrowserPortType
{
	public interface BaseIBrowserPortType : BaseIEnvironmentPortTypeMultiplePartner, IQualifierKind 
	{
	}
}