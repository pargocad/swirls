using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large10x
{
	public interface IEC2SizeLarge10x : BaseIEC2SizeLarge10x, IEC2Size
	{
	}
}