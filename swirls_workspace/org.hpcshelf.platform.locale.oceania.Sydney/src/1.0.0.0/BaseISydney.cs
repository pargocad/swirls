/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.oceania.Australia;

namespace org.hpcshelf.platform.locale.oceania.Sydney
{
	public interface BaseISydney : BaseIAustralia, IQualifierKind 
	{
	}
}