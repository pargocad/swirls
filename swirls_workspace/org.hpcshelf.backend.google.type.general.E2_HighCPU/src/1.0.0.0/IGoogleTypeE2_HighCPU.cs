using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.E2_HighCPU
{
 public interface IGoogleTypeE2_HighCPU : BaseIGoogleTypeE2_HighCPU, IGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>
 {
 }
}