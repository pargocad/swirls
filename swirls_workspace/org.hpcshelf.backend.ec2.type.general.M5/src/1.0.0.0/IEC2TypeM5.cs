using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.general.M5
{
	public interface IEC2TypeM5 : BaseIEC2TypeM5, IEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>
	{
	}
}