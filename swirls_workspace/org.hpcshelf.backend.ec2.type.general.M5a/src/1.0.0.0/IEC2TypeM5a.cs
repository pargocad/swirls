using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.general.M5a
{
	public interface IEC2TypeM5a : BaseIEC2TypeM5a, IEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>
	{
	}
}