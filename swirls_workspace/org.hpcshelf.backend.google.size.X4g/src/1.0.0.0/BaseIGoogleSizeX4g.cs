/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X4g
{
 public interface BaseIGoogleSizeX4g : BaseIGoogleSize, IQualifierKind 
 {
 }
}