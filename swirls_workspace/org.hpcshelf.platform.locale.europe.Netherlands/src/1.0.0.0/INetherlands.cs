using org.hpcshelf.platform.locale.europe.WestEurope;

namespace org.hpcshelf.platform.locale.europe.Netherlands
{
	public interface INetherlands : BaseINetherlands, IWestEurope
	{
	}
}