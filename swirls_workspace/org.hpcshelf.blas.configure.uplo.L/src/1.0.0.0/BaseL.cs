/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.blas.configure.UPLO;

namespace org.hpcshelf.blas.configure.uplo.L
{
	public interface BaseL : BaseIUPLO, IQualifierKind 
	{
	}
}