/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake;

namespace org.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake_X
{
	public interface BaseISkylake_X : BaseISkylake, IQualifierKind 
	{
	}
}