using org.hpcshelf.platform.node.processor.Model;

namespace org.hpcshelf.platform.node.processor.model.intel.Xeon_Platinum_8176M
{
	public interface IXeon_Platinum_8176M : BaseIXeon_Platinum_8176M, IProcessorModel<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, org.hpcshelf.platform.node.processor.family.intel.Xeon_Platinum.IXeonPlatinum, org.hpcshelf.platform.node.processor.series.intel.Xeon_Platinum_8000.IXeonPlatinum8000, org.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake_SP.ISkylake_SP>
	{
	}
}