using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.N1_Shared
{
 public interface IGoogleTypeN1_Shared : BaseIGoogleTypeN1_Shared, IGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>
 {
 }
}