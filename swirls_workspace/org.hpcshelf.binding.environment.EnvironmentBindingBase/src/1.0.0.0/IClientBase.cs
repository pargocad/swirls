using org.hpcshelf.kinds;
using org.hpcshelf.binding.environment.EnvironmentPortType;

namespace org.hpcshelf.binding.environment.EnvironmentBindingBase
{
	public interface IClientBase<C> : BaseIClientBase<C>
		where C:IEnvironmentPortType
	{
		C Client { get; }
	}
}