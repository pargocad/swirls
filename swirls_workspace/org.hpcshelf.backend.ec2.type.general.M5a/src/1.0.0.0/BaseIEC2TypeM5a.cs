/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.general.M5a
{
	public interface BaseIEC2TypeM5a : BaseIEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>, IQualifierKind 
	{
	}
}