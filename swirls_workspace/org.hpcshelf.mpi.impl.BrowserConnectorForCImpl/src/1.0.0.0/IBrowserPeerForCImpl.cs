using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.mpi.BrowserConnectorForC;
using org.hpcshelf.common.browser.SendDataPortType;
using org.hpcshelf.binding.channel.Binding;
using System.Runtime.InteropServices;

namespace org.hpcshelf.mpi.impl.BrowserConnectorForCImpl
{
    public class IBrowserPeerForCImpl : BaseIBrowserPeerForCImpl, IBrowserPeerForC
    {
        ISendDataPortType send_data_port = null;

        private int connector_key = -1;

        public int ConnectorKey { get { return connector_key; } }

        public override void after_initialize()
        {
            Console.WriteLine("REGISTER CHANNEL 1 -- browser channel");
            TypeOfChannelSend s = Channel.HPCShelfSend;
            Console.WriteLine("REGISTER CHANNEL 2 -- browser channel");
            TypeOfChannelRecv r = Channel.HPCShelfRecv;
            Console.WriteLine("REGISTER CHANNEL 3 -- browser channel");

            connector_key = SendDataPortTypeImpl.registerChannel(Channel.ChannelKey, s, r); // this connector has a single channel
            Console.WriteLine("REGISTER CHANNEL 4 -- browser channel");

            send_data_port = new SendDataPortTypeImpl(connector_key);
        }

        public override void main()
        {
            Send_data_port.Server = send_data_port;
        }

        public override void release1()
        {
            Services.releasePort("channel");
            Services.releasePort("send_data_port");

            Send_data_port.Server = null;

            base.release1();
        }
    }

    class SendDataPortTypeImpl : ISendDataPortType
    {
        [DllImport("browser_connector.so", EntryPoint = "output_message")]
        public static extern void output_message(int key, string message);

        [DllImport("browser_connector.so", EntryPoint = "registerChannel")]
        public static extern int registerChannel(int channel_key, TypeOfChannelSend send, TypeOfChannelRecv recv);

        public TypeOfBrowserOutputMessage OutputMessage { get { return output_message; } }

        private int connector_key;
        public int ConnectorKey { get { return connector_key; } }

        public SendDataPortTypeImpl(int connector_key)
        {
            this.connector_key = connector_key;
        }
    }
}
