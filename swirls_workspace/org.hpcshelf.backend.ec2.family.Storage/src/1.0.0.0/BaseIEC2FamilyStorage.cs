/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;

namespace org.hpcshelf.backend.ec2.family.Storage
{
	public interface BaseIEC2FamilyStorage : BaseIEC2Family, IQualifierKind 
	{
	}
}