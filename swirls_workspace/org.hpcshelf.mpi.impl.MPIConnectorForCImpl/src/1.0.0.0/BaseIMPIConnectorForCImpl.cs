/* Automatically Generated Code */

using System;
using org.hpcshelf.DGAC;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using org.hpcshelf.mpi.Language;
using org.hpcshelf.binding.environment.EnvironmentBindingBaseDirect;
using org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner;
using org.hpcshelf.binding.channel.Binding;
using org.hpcshelf.mpi.IntercommunicatorPortType;
using org.hpcshelf.mpi.MPIConnectorForC;
using org.hpcshelf.mpi.intercommunicator.BasicSendReceive;
using org.hpcshelf.mpi.binding.IntercommunicatorBinding;

namespace org.hpcshelf.mpi.impl.MPIConnectorForCImpl 
{
	public abstract class BaseIMPIConnectorForCImpl: Synchronizer, BaseIMPIConnectorForC
	{
		private ILanguage language = null;

		protected ILanguage Language
		{
			get
			{
				if (this.language == null)
					this.language = (ILanguage) Services.getPort("language");
				return this.language;
			}
		}
		private IIntercommunicatorBinding<IBasicSendReceive> port = null;

		public IIntercommunicatorBinding<IBasicSendReceive> Port
		{
			get
			{
				if (this.port == null)
					this.port = (IIntercommunicatorBinding<IBasicSendReceive>) Services.getPort("port");
				return this.port;
			}
		}
		private IChannel channel = null;

		protected IChannel Channel
		{
			get
			{
				if (this.channel == null)
					this.channel = (IChannel) Services.getPort("channel");
				return this.channel;
			}
		}
		private IIntercommunicatorPortType intercommunicator_port_type = null;

		protected IIntercommunicatorPortType Intercommunicator_port_type
		{
			get
			{
				if (this.intercommunicator_port_type == null)
					this.intercommunicator_port_type = (IIntercommunicatorPortType) Services.getPort("intercommunicator_port_type");
				return this.intercommunicator_port_type;
			}
		}
	}
}