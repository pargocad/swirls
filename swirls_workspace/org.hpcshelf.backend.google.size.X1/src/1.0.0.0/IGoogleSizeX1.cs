using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X1
{
 public interface IGoogleSizeX1 : BaseIGoogleSizeX1, IGoogleSize
 {
 }
}