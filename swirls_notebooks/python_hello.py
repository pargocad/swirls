from mpi4py import MPI
import numpy as np

def browse(*messages):
   comm = MPI.Comm.Get_parent()
   rank = comm.Get_rank()
   
   message=str(rank) + ': '
   for m in messages:
      message = message + ' ' + str(m)
   
   print('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&')
   print(len(message))
   comm.send(np.intc(len(message)), dest=rank, tag=111)
   
   comm.send(message.encode(), dest=rank, tag=222)



comm = MPI.COMM_WORLD
rank = comm.Get_rank()

browse ('1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789')
browse ('hello world from process ', rank)
browse ('done')
