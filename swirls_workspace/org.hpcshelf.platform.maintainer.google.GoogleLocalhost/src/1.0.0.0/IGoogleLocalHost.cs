using org.hpcshelf.platform.maintainer.Google;

namespace org.hpcshelf.platform.maintainer.google.GoogleLocalhost
{
	public interface IGoogleLocalHost : BaseIGoogleLocalHost, IGoogle
	{
	}
}