using org.hpcshelf.backend.ec2.storage.Type;

namespace org.hpcshelf.backend.ec2.storage.type.NVMe_SSD
{
	public interface IEC2StorageType_NVMe_SSD : BaseIEC2StorageType_NVMe_SSD, IEC2StorageType
	{
	}
}