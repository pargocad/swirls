using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X32
{
 public interface IGoogleSizeX32 : BaseIGoogleSizeX32, IGoogleSize
 {
 }
}