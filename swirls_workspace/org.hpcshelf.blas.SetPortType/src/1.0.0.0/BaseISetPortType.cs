/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.types.Data;
using org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner;

namespace org.hpcshelf.blas.SetPortType
{
	public interface BaseISetPortType<T> : BaseIEnvironmentPortTypeSinglePartner, IQualifierKind 
		where T:IData
	{
	}
}