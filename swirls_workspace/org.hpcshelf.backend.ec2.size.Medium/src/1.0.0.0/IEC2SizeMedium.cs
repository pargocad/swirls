using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Medium
{
	public interface IEC2SizeMedium : BaseIEC2SizeMedium, IEC2Size
	{
	}
}