using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;

namespace org.hpcshelf.backend.ec2.family.Acceleration
{
	public interface IEC2FamilyAcceleration : BaseIEC2FamilyAcceleration, IEC2Family
	{
	}
}