/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.E2_HighMem
{
 public interface BaseIGoogleTypeE2_HighMem : BaseIGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>, IQualifierKind 
 {
 }
}