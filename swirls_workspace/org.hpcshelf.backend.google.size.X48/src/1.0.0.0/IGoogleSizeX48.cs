using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X48
{
 public interface IGoogleSizeX48 : BaseIGoogleSizeX48, IGoogleSize
 {
 }
}