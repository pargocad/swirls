/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.quantifier.IntDown;
using org.hpcshelf.backend.ec2.storage.Type;
using org.hpcshelf.platform.node.Storage;

namespace org.hpcshelf.backend.ec2.EC2InstanceStorage
{
	public interface BaseIEC2InstanceStorage<SIZ, LAT, BAND, NETBAND, TYPE> : BaseIStorage<SIZ, LAT, BAND, NETBAND>, IQualifierKind 
		where SIZ:IntUp
		where LAT:IntDown
		where BAND:IntUp
		where NETBAND:IntUp
		where TYPE:IEC2StorageType
	{
	}
}