using org.hpcshelf.types.Data;
using org.hpcshelf.binding.environment.EnvironmentPortTypeSinglePartner;

namespace org.hpcshelf.blas.SetPortType
{
	public interface ISetPortType<T> : BaseISetPortType<T>, IEnvironmentPortTypeSinglePartner
		where T:IData
	{
	}
}