/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.os.linux.ubuntu.CodeName;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.node.os.Linux;

namespace org.hpcshelf.platform.node.os.linux.Ubuntu
{
	public interface BaseIUbuntu<N, RELEASE_MONTH, RELEASE_YEAR> : BaseILinux, IQualifierKind 
		where N:ICodeName
		where RELEASE_MONTH:IntUp
		where RELEASE_YEAR:IntUp
	{
	}
}