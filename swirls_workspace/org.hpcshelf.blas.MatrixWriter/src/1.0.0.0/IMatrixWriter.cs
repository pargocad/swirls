using org.hpcshelf.blas.data.DataDistribution;
using org.hpcshelf.types.Data;

namespace org.hpcshelf.blas.MatrixWriter
{
	public interface IMatrixWriter<DIST, T> : BaseIMatrixWriter<DIST, T>
		where DIST:IDataDistribution
		where T:IData
	{
	}
}