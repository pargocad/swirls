/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X8
{
 public interface BaseIGoogleSizeX8 : BaseIGoogleSize, IQualifierKind 
 {
 }
}