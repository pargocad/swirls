using org.hpcshelf.platform.node.os.linux.ubuntu.CodeName;
using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.platform.node.os.Linux;

namespace org.hpcshelf.platform.node.os.linux.Ubuntu
{
	public interface IUbuntu<N, RELEASE_MONTH, RELEASE_YEAR> : BaseIUbuntu<N, RELEASE_MONTH, RELEASE_YEAR>, ILinux
		where N:ICodeName
		where RELEASE_MONTH:IntUp
		where RELEASE_YEAR:IntUp
	{
	}
}