using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Small
{
	public interface IEC2SizeSmall : BaseIEC2SizeSmall, IEC2Size
	{
	}
}