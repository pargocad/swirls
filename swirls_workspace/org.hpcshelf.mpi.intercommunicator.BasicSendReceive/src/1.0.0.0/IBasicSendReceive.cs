using System;
using org.hpcshelf.mpi.IntercommunicatorPortType;

namespace org.hpcshelf.mpi.intercommunicator.BasicSendReceive
{
    public delegate int TypeOfConnectorSend    (int key, IntPtr buf, int count, int datatype, int facet, int target, int tag);
	public delegate int TypeOfConnectorReceive (int key, IntPtr buf, int count, int datatype, int facet, int source, int tag, IntPtr status);
   
	public interface IBasicSendReceive : BaseIBasicSendReceive, IIntercommunicatorPortType
	{
	    int   Facet          { get; }
	    int   FacetCount     { get; }
	    int[] FacetSize      { get; }
	    int[] FacetInstance  { get; }
	    
	    TypeOfConnectorSend    Send { get; }
	    TypeOfConnectorReceive Receive { get; }   
	}
}