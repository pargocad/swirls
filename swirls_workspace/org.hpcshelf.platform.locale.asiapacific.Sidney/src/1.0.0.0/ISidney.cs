using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.Sidney
{
	public interface ISidney : BaseISidney, IAsiaPacific
	{
	}
}