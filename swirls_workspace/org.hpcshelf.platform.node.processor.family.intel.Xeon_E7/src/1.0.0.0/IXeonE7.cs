using org.hpcshelf.platform.node.processor.Family;

namespace org.hpcshelf.platform.node.processor.family.intel.Xeon_E7
{
	public interface IXeonE7 : BaseIXeonE7, IProcessorFamily<org.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>
	{
	}
}