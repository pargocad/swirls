/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.common.BrowserPortType;

namespace org.hpcshelf.common.browser.wrapper.WSendDataPortType
{
	public interface BaseIWSendDataPortType : BaseIBrowserPortType, IQualifierKind 
	{
	}
}