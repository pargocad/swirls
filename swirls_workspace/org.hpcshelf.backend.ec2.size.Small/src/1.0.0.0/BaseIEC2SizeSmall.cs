/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Small
{
	public interface BaseIEC2SizeSmall : BaseIEC2Size, IQualifierKind 
	{
	}
}