/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.memory.M1_Megamem
{
 public interface BaseIGoogleTypeM1_Megamem : BaseIGoogleType<family.Memory.IGoogleFamilyMemory>, IQualifierKind 
 {
 }
}