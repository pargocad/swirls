/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.family.Family;

namespace org.hpcshelf.backend.ec2.family.Computation
{
	public interface BaseIEC2FamilyComputation : BaseIEC2Family, IQualifierKind 
	{
	}
}