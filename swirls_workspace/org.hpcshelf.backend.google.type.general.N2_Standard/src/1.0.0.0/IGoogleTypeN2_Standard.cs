using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.N2_Standard
{
 public interface IGoogleTypeN2_Standard : BaseIGoogleTypeN2_Standard, IGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>
 {
 }
}