using org.hpcshelf.common.BrowserPortType;

namespace org.hpcshelf.common.browser.SendDataPortType
{
    public delegate void TypeOfBrowserOutputMessage (int key, string data);
   
	public interface ISendDataPortType : BaseISendDataPortType, IBrowserPortType
	{
	   TypeOfBrowserOutputMessage OutputMessage { get; }
       int ConnectorKey { get; }
    }
}