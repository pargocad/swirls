using org.hpcshelf.platform.locale.AnyWhere;

namespace org.hpcshelf.platform.locale.NorthAmerica
{
	public interface INorthAmerica : BaseINorthAmerica, IAnyWhere
	{
	}
}