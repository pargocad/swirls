using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.G1_Small
{
 public interface IGoogleSizeG1_Small : BaseIGoogleSizeG1_Small, IGoogleSize
 {
 }
}