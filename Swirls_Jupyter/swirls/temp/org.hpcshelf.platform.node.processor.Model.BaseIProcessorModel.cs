/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Manufacturer;
using org.hpcshelf.platform.node.processor.Family;
using org.hpcshelf.platform.node.processor.Series;
using org.hpcshelf.platform.node.processor.Microarchitecture;

namespace org.hpcshelf.platform.node.processor.Model
{
	public interface BaseIProcessorModel<MAN, FAM, SER, MIC> : IQualifierKind 
		where MAN:IProcessorManufacturer
		where FAM:IProcessorFamily<MAN>
		where SER:IProcessorSeries<MAN, FAM>
		where MIC:IProcessorMicroarchitecture<MAN>
	{
	}
}