#!/bin/bash
echo "STARTING - Core Services at $2"
nohup ssh -i $1 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@$2 "/home/ubuntu/swirls/run_core_services_local.sh" &
echo "STARTED - CoreServices at $2"
