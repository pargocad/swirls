/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.mpi.Language;

namespace org.hpcshelf.language.CPlusPlus
{
	public interface BaseILanguage_CPlusPlus : BaseILanguage, IQualifierKind 
	{
	}
}