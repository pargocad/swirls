/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.N1_Shared
{
 public interface BaseIGoogleTypeN1_Shared : BaseIGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>, IQualifierKind 
 {
 }
}