/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Metal
{
	public interface BaseIEC2SizeMetal : BaseIEC2Size, IQualifierKind 
	{
	}
}