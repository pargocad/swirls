using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.compute.C5n
{
	public interface IEC2TypeC5n : BaseIEC2TypeC5n, IEC2Type<family.Computation.IEC2FamilyComputation>
	{
	}
}