using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.accelerated.A2_HighCPU
{
 public interface IGoogleTypeA2_HighCPU : BaseIGoogleTypeA2_HighCPU, IGoogleType<family.Acceleration.IGoogleFamilyAcceleration>
 {
 }
}