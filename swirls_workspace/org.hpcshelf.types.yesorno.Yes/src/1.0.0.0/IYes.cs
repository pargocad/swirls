using org.hpcshelf.types.YesOrNo;

namespace org.hpcshelf.types.yesorno.Yes
{
	public interface IYes : BaseIYes, IYesOrNo
	{
	}
}