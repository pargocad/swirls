using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.memory.R5ad
{
	public interface IEC2TypeR5ad : BaseIEC2TypeR5ad, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}