/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.Europe;

namespace org.hpcshelf.platform.locale.europe.CentralEurope
{
	public interface BaseICentralEurope : BaseIEurope, IQualifierKind 
	{
	}
}