using org.hpcshelf.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace org.hpcshelf.mpi.IntercommunicatorPortType
{
   
	public interface IIntercommunicatorPortType : BaseIIntercommunicatorPortType, IEnvironmentPortTypeMultiplePartner
	{
	}
}