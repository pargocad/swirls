using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.Taiwan
{
	public interface ITaiwan : BaseITaiwan, IAsiaPacific
	{
	}
}