using org.hpcshelf.quantifier.IntUp;
using org.hpcshelf.quantifier.IntDown;

namespace org.hpcshelf.platform.node.Memory
{
	public interface IMemory<SIZ, LAT, BAND> : BaseIMemory<SIZ, LAT, BAND>
		where SIZ:IntUp
		where LAT:IntDown
		where BAND:IntUp
	{
	}
}