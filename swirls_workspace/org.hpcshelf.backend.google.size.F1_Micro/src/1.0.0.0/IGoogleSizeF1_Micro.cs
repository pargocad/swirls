using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.F1_Micro
{
 public interface IGoogleSizeF1_Micro : BaseIGoogleSizeF1_Micro, IGoogleSize
 {
 }
}