/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.common.BrowserBinding;
using org.hpcshelf.common.BrowserConnector;
using org.hpcshelf.common.browser.wrapper.WSendDataPortType;

namespace org.hpcshelf.mpi.wrapper.WBrowserConnector
{
    public interface BaseIWBrowserPeer : BaseIBrowserPeer<org.hpcshelf.mpi.Language.ILanguage, org.hpcshelf.common.browser.wrapper.WSendDataPortType.IWSendDataPortType>, ISynchronizerKind 
	{
		new IBrowserBinding<IWSendDataPortType> Send_data_port {get;}
	}
}