using org.hpcshelf.kinds;
using org.hpcshelf.mpi.Language;

namespace org.hpcshelf.language.CPlusPlus
{
	public interface ILanguage_CPlusPlus : BaseILanguage_CPlusPlus, ILanguage
	{
	}
}