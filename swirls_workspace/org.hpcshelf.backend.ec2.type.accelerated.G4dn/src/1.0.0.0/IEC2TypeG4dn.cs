using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.accelerated.G4dn
{
	public interface IEC2TypeG4dn : BaseIEC2TypeG4dn, IEC2Type<family.Acceleration.IEC2FamilyAcceleration>
	{
	}
}