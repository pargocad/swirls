using org.hpcshelf.types.Data;
using org.hpcshelf.blas.data.DataDistribution;
using org.hpcshelf.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace org.hpcshelf.blas.MatrixPortType
{
	public interface IMatrixPortType<T, DIST> : BaseIMatrixPortType<T, DIST>, IEnvironmentPortTypeMultiplePartner
		where T:IData
		where DIST:IDataDistribution
	{
	}
}