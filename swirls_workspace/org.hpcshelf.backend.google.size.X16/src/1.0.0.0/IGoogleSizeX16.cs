using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.X16
{
 public interface IGoogleSizeX16 : BaseIGoogleSizeX16, IGoogleSize
 {
 }
}