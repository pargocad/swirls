using org.hpcshelf.blas.configure.TRANS;

namespace org.hpcshelf.blas.configure.trans.NA
{
	public interface NA : BaseNA, ITRANS
	{
	}
}