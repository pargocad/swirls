using org.hpcshelf.types.Data;

namespace org.hpcshelf.blas.SetParameterConnector
{
	public interface IGet<T_GET> : BaseIGet<T_GET>
		where T_GET:IData
	{
	}
}