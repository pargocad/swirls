/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.processor.Model;

namespace org.hpcshelf.platform.node.processor.model.amd.EPYC_7571
{
	public interface BaseIEPYC_7571 : BaseIProcessorModel<org.hpcshelf.platform.node.processor.manufacturer.AMD.IAMD, org.hpcshelf.platform.node.processor.family.amd.EPYC.IEPYC, org.hpcshelf.platform.node.processor.series.amd.EPYC_7000.IEPYC7000, org.hpcshelf.platform.node.processor.microarchitecture.amd.Zen.IZen>, IQualifierKind 
	{
	}
}