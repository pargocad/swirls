using org.hpcshelf.kinds;
using org.hpcshelf.kind.Binding;
using System;
//using MPI;

namespace org.hpcshelf.binding.channel.Binding
{
   	public delegate int TypeOfChannelSend (int channel_key, IntPtr buf, int count, int datatype, int facet, int target, int tag);
	public delegate int TypeOfChannelRecv (int channel_key, IntPtr buf, int count, int datatype, int facet, int source, int tag, IntPtr status);   

	public interface IChannel : BaseIChannel, IPeer, Aliencommunicator
	{
        TypeOfChannelSend HPCShelfSend { get; }
	    TypeOfChannelRecv HPCShelfRecv { get; } 
	    int ChannelKey { get; }
	}
}