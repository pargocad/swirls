/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.size.Size;

namespace org.hpcshelf.backend.google.size.G1_Small
{
 public interface BaseIGoogleSizeG1_Small : BaseIGoogleSize, IQualifierKind 
 {
 }
}