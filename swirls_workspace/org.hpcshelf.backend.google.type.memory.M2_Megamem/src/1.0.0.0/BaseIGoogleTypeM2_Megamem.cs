/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.memory.M2_Megamem
{
 public interface BaseIGoogleTypeM2_Megamem : BaseIGoogleType<family.Memory.IGoogleFamilyMemory>, IQualifierKind 
 {
 }
}