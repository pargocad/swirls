using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.compute.C2_Standard
{
 public interface IGoogleTypeC2_Standard : BaseIGoogleTypeC2_Standard, IGoogleType<family.Computation.IGoogleFamilyComputation>
 {
 }
}