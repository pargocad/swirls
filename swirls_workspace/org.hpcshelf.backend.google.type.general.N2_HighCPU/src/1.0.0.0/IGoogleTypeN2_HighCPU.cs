using org.hpcshelf.kinds;
using org.hpcshelf.backend.google.type.Type;

namespace org.hpcshelf.backend.google.type.general.N2_HighCPU
{
 public interface IGoogleTypeN2_HighCPU : BaseIGoogleTypeN2_HighCPU, IGoogleType<family.GeneralPurpose.IGoogleFamilyGeneralPurpose>
 {
 }
}