using org.hpcshelf.kinds;
using org.hpcshelf.types.Data;
using System;

namespace org.hpcshelf.types.SFloat { 

	public interface ISFloat : BaseISFloat, IData
	{
		ISFloatInstance newInstance(double d);
	} // end main interface 

	public interface ISFloatInstance : IDataInstance, ICloneable
	{
		double Value { set; get; }
	}

} // end namespace 
