using org.hpcshelf.blas.configure.DIAG;

namespace org.hpcshelf.blas.configure.diag.N
{
	public interface N : BaseN, IDIAG
	{
	}
}