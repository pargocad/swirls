/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.node.accelerator.Manufacturer;

namespace org.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA
{
	public interface BaseINVIDIA : BaseIAcceleratorManufacturer, IQualifierKind 
	{
	}
}