/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.mpi.IntercommunicatorPortType;

namespace org.hpcshelf.mpi.intercommunicator.BasicSendReceive
{
	public interface BaseIBasicSendReceive : BaseIIntercommunicatorPortType, IQualifierKind 
	{
	}
}