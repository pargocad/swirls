using System;
using System.Collections.Generic;
using org.hpcshelf.common.BrowserPortType;

namespace org.hpcshelf.common.browser.RecvDataPortType
{
	public interface IRecvDataPortType : BaseIRecvDataPortType, IBrowserPortType
	{
	   IList<string> receive_output_message();
	}
}