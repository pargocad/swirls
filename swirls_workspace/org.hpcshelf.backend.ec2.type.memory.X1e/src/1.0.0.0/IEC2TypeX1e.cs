using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.type.Type;

namespace org.hpcshelf.backend.ec2.type.memory.X1e
{
	public interface IEC2TypeX1e : BaseIEC2TypeX1e, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}