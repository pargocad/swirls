using org.hpcshelf.platform.locale.AsiaPacific;

namespace org.hpcshelf.platform.locale.asiapacific.India
{
	public interface IIndia : BaseIIndia, IAsiaPacific
	{
	}
}