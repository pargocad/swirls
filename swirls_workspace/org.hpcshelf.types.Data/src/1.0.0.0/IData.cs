using org.hpcshelf.kinds;

namespace org.hpcshelf.types.Data { 

public interface IData : BaseIData
{
		// void loadFrom (IData o);
		// IData newInstance ();
		// IData clone();

		object Instance { get; set;}
		object newInstance ();

} // end main interface 

	public interface IDataInstance
	{
		object ObjValue { set; get; }
	}

} // end namespace 
