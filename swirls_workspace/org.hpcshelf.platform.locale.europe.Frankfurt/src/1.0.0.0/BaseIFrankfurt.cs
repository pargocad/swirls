/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.locale.europe.Germany;

namespace org.hpcshelf.platform.locale.europe.Frankfurt
{
	public interface BaseIFrankfurt : BaseIGermany, IQualifierKind 
	{
	}
}