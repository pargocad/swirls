using org.hpcshelf.kinds;
using org.hpcshelf.backend.ec2.size.Size;

namespace org.hpcshelf.backend.ec2.size.Large3x
{
	public interface IEC2SizeLarge3x : BaseIEC2SizeLarge3x, IEC2Size
	{
	}
}