/* AUTOMATICALLY GENERATE CODE */

using org.hpcshelf.kinds;
using org.hpcshelf.platform.Maintainer;

namespace org.hpcshelf.platform.maintainer.LocalHost
{
	public interface BaseILocalHost : BaseIMaintainer, IQualifierKind 
	{
	}
}